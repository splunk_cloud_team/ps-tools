TRACE = True
import gzip
import io
import json
import logging
import os
import posixpath
import re
import sys
import uuid
from collections import OrderedDict
from hashlib import sha1, sha256

from .exceptions import *
from .consts import *
from ..utils import *
from ..utils import _valid_guid, _guid_pattern

# _guid_pattern = r'[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}'
# _bid_pattern = r'^[a-z_0-9]*~\d+~{}$'.format(_guid_pattern)
# _path_pattern = r'^[dr]b_\d{{10}}_\d{{10}}_\d+(?:_(?P<guid>{}))?$'.format(_guid_pattern)

# _valid_guid = re.compile(_guid_pattern)
# _valid_bid = re.compile(_bid_pattern)
# _valid_path = re.compile(_path_pattern)

# def _is_valid_bid(value):
    
#     valid =  _valid_bid.match(value)
#     if not valid: raise InvalidFormat('Bucket ID (bid) must be in the format "{}"'.format(_valid_bid.pattern))
#     return True
    
# def _is_valid_path(value):
    
#     valid = _valid_path.match(value)
#     if not valid: raise InvalidPath('{} is not a valid Splunk bucket directory path.'.format(value))
    
#     complete = valid.group("guid")
    
#     return valid.group("guid")

# def _get_journal_header(file):
#     """reads the header from the uncompressed journal and returns the earliest timestamp

#     Args:
#         file (str): path to the journal.gz file

#     Raises:
#         InvalidFormat: Not a valid journal

#     Returns:
#         dict: {'VERSION': <int>, 'EARLIEST': <int>, 'SITE': <str>}
#     """    
#     _result = {}
    
#     with gzip.open(file, "rb") as f:
#         data = f.read(48)
    
#     if int.from_bytes(data[0:2], byteorder='little') != 266: raise InvalidFormat("This is not a Splunk journal Version 1")
    
#     _result['VERSION'] = int(data[1])
#     _result['EARLIEST'] = int.from_bytes(data[3:7], byteorder='little')
#     _result['SITE'] = (lambda x, y: x if y == 2 else "")(data[21:20+int(data[19])].decode('utf-8'), int(data[20]))
    
#     return _result

# def _get_index_earliest(file):
#     """opens the journal file and retrieves the active (earliest) time

#     Args:
#         file (str): path to the journal.gz file

#     Returns:
#         int: epoch seconds
#     """ 
#     with open(file, "rb") as f:
#         data = f.read(10)
        
#     return int.from_bytes(data[4:8], byteorder='little')

class ReceiptJson():  
    """Python class representing a receipt.json file
    
    Args:
    guid (str|uuid.UUID): The Splunk GUID for this instance uses as "uploaded_guid"

    Raises:
        InvalidFormat: Improperly formatted UUID string or object
    """         
    @unrecoverable_wrapper
    @debug_wrapper(TRACE)    
    def __init__(self, guid):
        
        """Python class representing a receipt.json file
        
        Args:
        guid (str|uuid.UUID): The Splunk GUID for this instance uses as "uploaded_guid"

        Raises:
            InvalidFormat: Improperly formatted UUID string or object
        """
        self.logger = logging.getLogger(__name__)
        self.logger.debug('hello')
        self._validated = False
        
        if isinstance(guid, uuid.UUID):
            self._guid = str(guid).upper()
            
        elif isinstance(guid, str) and _valid_guid.match(guid.upper()):
            self._guid = str(uuid.UUID(guid)).upper()
            self.logger.debug("MY GUID IS: {}".format(self._guid))
        else:
            raise InvalidFormat("GUID '{}' is improperly formatted.".format(guid))
        
        self._object_prefix = posixpath.join(".", "guidSplunk-{}".format(self._guid))
        self._receipt = OrderedDict({"objects": [], "manifest": OrderedDict({}), "user_data": OrderedDict({'uploader_guid': guid})})
        self._meta = {'object_prefix': posixpath.join(".", "guidSplunk-{}".format(self._guid))}
        
        # self._meta = {
        #     'earliest_event': 0, 
        #     'index_event': 0, 
        #     'first_idx_time': 0, 
        #     'last_idx_time': 0, 
        #     'source_path': "", 
        #     'object_prefix': posixpath.join(".", "guidSplunk-{}".format(self._guid))
        #     }
        self._index_latest = -1

    @unrecoverable_wrapper
    @debug_wrapper(TRACE)
    def read(self, io_object):
        
        if isinstance(io_object, io.IOBase):
            try:
                io_object.mode.index("r")
            except ValueError as e:
                self.logger.error(e)
                self.logger.debug(e, exc_info=True, stack_info=True)
                raise InvalidAssignment("{} must be an io object opened for read.".format(io_object))
            
            _tmp = json.load(io_object)
            
            for section in ["manifest", "user_data"]:
                for key, value in _tmp[section].items():
                    self._receipt[section][key] = value
            
            self._receipt["objects"] = self.sort_objects(_tmp["objects"])

    @unrecoverable_wrapper
    @debug_wrapper(TRACE)        
    def readfile(self, file_path):
        
        if os.path.isfile(file_path):
            with open(file_path, "r") as f:
                self.read(f)
        else:
            raise InvalidArgument("File {} does not exist".format(file_path))
    @unrecoverable_wrapper
    @debug_wrapper(TRACE)
    def validate(self):
        pass

    @unrecoverable_wrapper
    @debug_wrapper(TRACE)
    def fix_path(self, path):
        _tmp_path = path.split("_")
        if len(_tmp_path) == 4:
            _tmp_path.append(self._guid)       
        return "_".join(str(x) for x in _tmp_path)
    
    @unrecoverable_wrapper
    @debug_wrapper(TRACE)
    def create_from_manifest(self, row):
        
        self.manifest = row
        self.path = row['path']

    @unrecoverable_wrapper
    @debug_wrapper(TRACE)
    def create_from_dir(self, dir_path, update_manifest=False):
        """Creates a ReceiptJson from the contents of a local
        Splunk Bucket Directory

        Args:
            dir_path (str): path to the Splunk Bucket
        """
        
        try:
            # this is a (worst case) guess because it doesn't really matter
            # and this gets us close to the value expected
            BLOCKSIZE = 4096
                
            # always make sure the path is set properly
            self.path = os.path.basename(dir_path)
            
            # always get the journal header
            _header = get_journal_header(os.path.join(dir_path, 'rawdata', 'journal.gz'))
            
            _tmp_path = self.path.split("_")
            # convert all parts of the path that are integers
            # so they can be compared later
            for i, v in enumerate(_tmp_path):
                try: _tmp_path[i] = int(v)
                except ValueError: continue
            
            # set the starting values for earliest/latest
            self._meta['earliest_event'] = self._meta.get('earliest_event', _tmp_path[2])
            self._meta['latest_event'] = self._meta.get('latest_event', _tmp_path[1])
            self._meta['first_index_time'] = self._meta.get('first_index_time', _header.get('EARLIEST', ""))

            # get a copy of the current metadata
            _tmp = dict(self.manifest)
            
            # convert all parts of the manifest that are integers
            # so they can be compared later
            for k, v in _tmp.items():
                try: _tmp[k] = int(v)
                except ValueError: continue
            
            _stats = os.stat(dir_path)
            _tmp['size_on_disk'] = 0
            
            # walk the source path and construct the metadata
            self.objects = []
            has_journal = False
            for _root, _, _files in os.walk(dir_path, topdown=True):
                for _file in _files:
                    if _file not in EXPECTED_FILES and not _file.endswith('.tsidx') and not _file.endswith('.csv.gz'):
                        continue
                    src_path = os.path.normpath(os.path.join(_root, _file))
                    _object_size = os.stat(src_path).st_size
                    _tmp['size_on_disk'] += ((_object_size // BLOCKSIZE) * BLOCKSIZE) + BLOCKSIZE
                    _rel_file = os.path.relpath(src_path, dir_path)
                    _object_name = posixpath.join(self._meta['object_prefix'], _rel_file.replace(os.sep, posixpath.sep))
                    self.objects.append({"name": _object_name, "size": _object_size})
                    
                    # this part checks the name of the file and will update the metadata
                    if _file == ".rawSize":
                        with open(src_path, "r") as f:
                            _tmp['raw_size'] = int(f.readline().rstrip("\n"))
                            
                    elif _file.startswith("journal"):
                        has_journal = True
                        _tmp['journal_size'] = int(_object_size)
                        
                    elif _file.endswith(".mini.tsidx"):
                        _tmp['tsidx_minified'] = 1
                
                    elif _file in ["Hosts.data", "Sources.data", "SourceTypes.data"]:
                        if _file == "Hosts.data":
                            with open(src_path, "r") as f:
                                _, _host_count, _count, _earliest, _latest, _indexed = [ int(x) for x in f.readline().rstrip("\n").split() ]
                        elif _file == "Sources.data":
                            with open(src_path, "r") as f:
                                _, _source_count, _count, _earliest, _latest, _indexed = f.readline().rstrip("\n").split()
                        elif _file == "SourceTypes.data":
                            with open(src_path, "r") as f:
                                _, _sourcetype_count, _count, _earliest, _latest, _indexed = f.readline().rstrip("\n").split()
                                
                        # fix the directory names 
                        _tmp_path[3] = (lambda x, y: x if x < y else y)(int(_earliest), _tmp_path[3])
                        _tmp_path[2] = (lambda x, y: x if x > y else y)(int(_latest), _tmp_path[2])
                        
                        # update with the correct event count
                        _tmp['event_count'] = (lambda x, y: x if x > y else y)(int(_count), _tmp['event_count'])
                        
                        # always update the latest _index_time
                        self._meta['last_index_time'] = (lambda x, y: x if x > y else y)(int(_indexed), self._meta.get('last_index_time', 0))
            try:
                assert has_journal == True
            except AssertionError:
                raise AssertionError(f"{dir_path} has no rawdata")
            
            # always overwrite the bucket_info.csv
            with open(os.path.join(dir_path, "bucket_info.csv"), "w") as f:
                f.writelines('"indextime_et","indextime_lt","frozen_in_cluster"\n')
                f.writelines("{},{},{}".format(self._meta.get('first_index_time', ""), self._meta.get('last_index_time', ""), '""'))
            
            if update_manifest: self._manifest = _tmp
            
        except Exception as e:
            self.logger.debug(e, exc_info=True, stack_info=True)
            self.logger.error(e)
            raise e
    
    def sort_objects(self, object_list):
        
        return sorted(object_list, key=lambda x: x["name"])
    
    # Properties
    @property
    def origin_guid(self):
        return self.manifest.get("id").split("~")[2]
    
    @property
    def s2_path(self):
        try:
            _index, _suffix = self.manifest.get('id', "").split("~",1)
        except ValueError:
            return None
        _guid = _suffix.split("~")[1]
        _s2_bucket_prefix = posixpath.sep.join([ sha1(_suffix.encode('utf-8')).hexdigest()[x:x+2] for x in range(0,4,2) ])
        return posixpath.join(_index, "db", _s2_bucket_prefix, _suffix)
    
    @property
    def index(self):
        return self.manifest.get("id").split("~")[0]
    
    @property
    def id_num(self):
        return int(self.manifest.get("id", "").split("~")[1])
    
    @property
    def user_data(self):
        return self._receipt["user_data"]
    
    @user_data.setter
    def user_data(self, value):
        
        if isinstance(value, dict):
            self._receipt["user_data"] = value
        else:
            raise InvalidAssignment("Argument {} received when {} expected.".format(type(value), type(self._receipt["user_data"])))
        
        # return self._receipt

    @property
    def objects(self):
        return self._receipt["objects"]

    @objects.setter
    def objects(self, value):
        """Sets the entire list of objects by assignment

        Args:
            value (dict): {"name": "str", "size": int}

        Raises:
            InvalidAssignment: Thrown if value is not a list

        Returns:
            dict: entire receipt.json as a python dict
        """        
        
        if isinstance(value, list):
            self._receipt["objects"] = value
        else:
            raise InvalidAssignment("Argument {} received when {} expected.".format(type(value), type(self._receipt["objects"])))
        
        # return self._receipt
    
    @property
    def manifest(self):
        return self._receipt.get("manifest", None)
    
    @manifest.setter
    def manifest(self, value):
        
        if isinstance(value, dict):
            self._receipt["manifest"] = value
        else:
            raise InvalidAssignment("Argument {} received when {} expected.".format(type(value), type(self._receipt["manifest"])))
        
    
    @property
    def object_prefix(self):
        return self._object_prefix
    
    @property
    def event_count(self):
        return self.manifest.get("event_count", None)
        
    @event_count.setter
    def event_count(self, value):
        try: int(value)
        except ValueError: raise InvalidAssignment("Argument '{}' received when {} expected".format(value, type(1)))
        else: self.manifest["event_count"] = str(value)
    
    @property
    def frozen_in_cluster(self):
        return self.manifest.get("frozen_in_cluster", None)

    @frozen_in_cluster.setter
    def frozen_in_cluster(self, value):
        try: int(value)
        except ValueError: raise InvalidAssignment("Argument '{}' received when {} expected".format(value, type(1)))
        else: self.manifest["frozen_in_cluster"] = str(value)
    
    @property
    def host_count(self):
        return self.manifest.get("host_count", None)
    
    @host_count.setter
    def host_count(self, value):
        try: int(value)
        except ValueError: raise InvalidAssignment("Argument '{}' received when {} expected".format(value, type(1)))
        else: self.manifest["host_count"] = str(value)
    
    @property
    def id(self):
        return self.manifest.get("id", None)
    
    @id.setter
    def id(self, value):
        # TODO validate
        self.manifest["id"] = str(value)
    
    @property
    def journal_size(self):
        return self.manifest.get("journal_size", None)
    
    @journal_size.setter
    def journal_size(self, value):
        try: int(value)
        except ValueError: raise InvalidAssignment("Argument '{}' received when {} expected".format(value, type(1)))
        else: self.manifest["journal_size"] = str(value)
    
    @property
    def modtime(self):
        return self.manifest.get("modtime", None)
    
    @modtime.setter
    def modtime(self, value):
        try: str(value)
        except ValueError: raise InvalidAssignment("Argument '{}' received when {} expected".format(value, type("")))
        else: self.manifest["modtime"] = str(value)
    
    @property
    def origin_site(self):
        return self.manifest.get("origin_site", None)
    
    @origin_site.setter
    def origin_site(self, value):
        # TODO validate
        self.manifest["origin_site"] = str(value)
    
    @property
    def path(self):
        return self.manifest.get("path", None)
    
    @path.setter
    def path(self, value):
        self.manifest["path"] = str(self.fix_path(value))
    
    @property
    def raw_size(self):
        return self.manifest.get("raw_size", None)
    
    @raw_size.setter
    def raw_size(self, value):
        try: int(value)
        except ValueError: raise InvalidAssignment("Argument '{}' received when {} expected".format(value, type(1)))
        else: self.manifest["raw_size"] = str(value)
    
    @property
    def size_on_disk(self):
        return self.manifest.get("size_on_disk", None)
    
    @size_on_disk.setter
    def size_on_disk(self, value):
        try: int(value)
        except ValueError: raise InvalidAssignment("Argument '{}' received when {} expected".format(value, type(1)))
        else: self.manifest["size_on_disk"] = str(value)
    
    @property
    def source_count(self):
        return self.manifest.get("source_count", None)
    
    @source_count.setter
    def source_count(self, value):
        try:
            int(value)
        except ValueError:
            raise InvalidAssignment("Argument '{}' received when {} expected".format(value, type(1)))
        else:
            self.manifest["source_count"] = str(value)
    
    @property
    def sourcetype_count(self):
        return self.manifest.get("sourcetype_count", None)
    
    @sourcetype_count.setter
    def sourcetype_count(self, value):
        try:
            int(value)
        except ValueError:
            raise InvalidAssignment("Argument '{}' received when {} expected".format(value, type(1)))
        else:
            self.manifest["sourcetype_count"] = str(value)
            
    @property
    def tsidx_minified(self):
        return self.manifest.get("tsidx_minified", None)
    
    @tsidx_minified.setter
    def tsidx_minified(self, value):
        try: int(value)
        except ValueError: raise InvalidAssignment("Argument '{}' received when {} expected".format(value, type(1)))
        else: self.manifest["tsidx_minified"] = str(value)
    
    @property
    def content_hash(self):
        try:
            self.user_data.pop("content_hash")
            
        except KeyError:
            pass
        
        return sha256(json.dumps(self._receipt, separators=(',', ':'), sort_keys=False).encode("utf-8")).hexdigest().upper()
    
    @property
    def uploader_guid(self):
        return self.user_data.get("uploader_guid", None)
    
    @uploader_guid.setter
    def uploader_guid(self, value):
        if isinstance(value, (str, bytes)):
            self.user_data["uploader_guid"] = str(value)
        else:
            raise InvalidAssignment("Argument '{}' received when {} expected".format(value, type("a")))
    
    @property
    def cipher_blob(self):
        return self.user_data.get("cipher_blob", None)
    
    @cipher_blob.setter
    def cipher_blob(self, value):
        if isinstance(value, (str, bytes)):
            self.user_data["cipher_blob"] = str(value)
        else:
            raise InvalidAssignment("Argument '{}' received when {} expected".format(value, type("a")))
    
    @property
    def encryption(self):
        return self.manifest.get("encryption", None)
    
    def __str__(self):
        return "{}".format(self._receipt)
    
    @property
    def metadata(self):
        return self._meta
    
    @property
    def json(self):
        self.user_data["content_hash"] = self.content_hash
        return json.dumps(self._receipt, separators=(',', ':'), sort_keys=False)

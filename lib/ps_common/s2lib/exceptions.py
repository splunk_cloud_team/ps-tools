class s2libException(Exception):
    pass
class ReceiptJsonError(s2libException):
    pass
class InvalidAssignment(ReceiptJsonError):
    pass
class InvalidArgument(ReceiptJsonError):
    pass
class InvalidFormat(ReceiptJsonError):
    pass
class InvalidPath(ReceiptJsonError):
    pass
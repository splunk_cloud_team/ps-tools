EXPECTED_FILES = [
    '.rawSize',
    '.sizeManifest4.1',
    'bloomfilter',
    'bucket_info.csv',
    'Hosts.data',
    'merged_lexicon.lex',
    'optimize.result',
    'Sources.data',
    'SourceTypes.data',
    'splunk-autogen-params.dat',
    'Strings.data',
    'journal.gz',
    'journal.zst',
    'journal.lz4',
    'slicemin.dat',
    'slicesv2.dat'
]
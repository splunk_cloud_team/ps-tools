MULTISITE_PRIMARY = 'primaries_by_site.site0={}'
CLUSTER_PRIMARY = '{}.bucket_flags=0xffffffffffffffff'

CLUSTER_KEYS = [
    'cluster_label',
    'disabled',
    'manager_uri',
    'master_uri',
    'mode',
    'multisite',
    'site'
]

PEER_KEYS = [
    'register_search_address'
]

PRIMARY_KEYS = [
    'bucket_size',
    'frozen',
    'index',
    'origin_site',
    'standalone'
]

INDEX_KEYS = [
    'homePath_expanded',
    'coldPath_expanded',
    'thawedPath_expanded',
    'isInternal',
    'datatype',
    'disabled',
    'isReady',
    'isVirtual',
    'lastInitSequenceNumber',
    'lastInitTime',
    'frozenTimePeriodInSecs'
]

INFO_KEYS = [
    'guid',
    'isForwarding',
    'server_roles',
    'startup_time'
]

SETTINGS_KEYS = [
    'SPLUNK_HOME'
]

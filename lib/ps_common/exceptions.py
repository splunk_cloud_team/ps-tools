class DataSnapshotError(Exception):
    def __init__(self, *args, **kwargs):
        super().__init__(*args)
        self.__dict__.update(kwargs)

class UnrecoverableError(DataSnapshotError):
    pass
class RecoverableError(DataSnapshotError):
    pass
class BadCredentials(UnrecoverableError):
    pass
class ConnectionFailure(UnrecoverableError):
    pass
class ValidationError(RecoverableError):
    pass
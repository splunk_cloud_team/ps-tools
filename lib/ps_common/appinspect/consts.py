
SPLUNKBASE_AUTH = "https://api.splunk.com/2.0/rest/login/splunk"
AI_URL = "https://appinspect.splunk.com"
AI_VALIDATE = AI_URL + "/v1/app/validate"
AI_STATUS = AI_URL + "/v1/app/validate/status/{request_id}"
AI_REPORT = AI_URL + "/v1/app/report/{request_id}"
AI_INFO = AI_URL + "/v1/info"
AI_PING = AI_URL + "/v1/ping"

REVIEW_AUTH = "https://review.us.appinspect.splunk.com/api/1.0/login"
REVIEW_UPLOAD = "https://review.us.appinspect.splunk.com/api/1.0/tickets"
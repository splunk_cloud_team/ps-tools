DEBUG=False
TRACE=False
GBL_DEBUG=False
GBL_TRACE=False
import json
import logging
import os
import posixpath
import shutil
import sys
import threading
import time
from getpass import getpass
sys.path.append("../..")
from ps_common.logger import logListenerConfig
from ps_common.decorators import debug_wrapper

import requests
import yaml
from requests_toolbelt.multipart.encoder import MultipartEncoder

from . import consts as CONST

# logListenerConfig(debug=bool(DEBUG or GBL_DEBUG), verbose=False, log_dir=log_dir, add_time=False, audit=True)
# logger = logging.getLogger(__name__)
# logger.debug("hello")

class threadedSession(threading.Thread):
    sem = threading.Semaphore(10)
    
    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def __init__(self, user, passwd):

        logger = logging.getLogger(__class__.__qualname__)
        logger.debug("hello")
        self.user = user
        self.passwd = passwd
        self.session = requests.Session()
        self._request_id = None

        try:
            response = self.session.get(CONST.SPLUNKBASE_AUTH, auth=(user, passwd))

        except:
            return None

        try:
            self.session.headers.update({'Authorization': "Bearer {}".format(response.json().get("data").get("token"))})

        except AttributeError:
            logger.error("Authentication Error -- check credentials")
            sys.exit(1)

    def authenticate(self):
        logger = logging.getLogger(__class__.__qualname__)

        try:
            response = self.session.get(CONST.SPLUNKBASE_AUTH, auth=(self.user, self.passwd))

        except:
            return None

        try:
            self.session.headers.update({'Authorization': "Bearer {}".format(response.json().get("data").get("token"))})

        except AttributeError:
            logger.error("Authentication Error -- check credentials")

    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def submitApp(self, appLocation):

        app_name = os.path.basename(appLocation)
        while True:
            with open(appLocation, "rb") as f:
                fields = (
                ('app_package', (app_name, f)),
                # ('included_tags', 'cloud'),
                # ('included_tags', 'self-service'),
                ('included_tags', 'private_app'),
                )
                payload = MultipartEncoder(fields=fields)
                headers = { "Content-Type": payload.content_type, "max-messages": "all" }

                try:
                    response = self.session.post(CONST.AI_VALIDATE, data=payload, headers=headers)
                except:
                    return False

                else:
                    self._request_id = response.json().get("request_id")
                    if self._request_id:
                        return True

    @property
    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def report_url(self):

        try:
            return CONST.AI_REPORT.format(request_id=self._request_id)

        except KeyError:
            return None


    @property
    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def report(self):
        try:
            return(self._get(self.report_url).json().get("reports")[0])
        except:
            return None


    @property
    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def status_url(self):

        try:
            return CONST.AI_STATUS.format(request_id=self._request_id)

        except KeyError:
            return None

    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def _get(self, url, **args):

        while True:
            try:
                response = self.session.get(url, **args)
            except Exception as e:
                print(repr(e), file=sys.stderr)
                time.sleep(2)
                continue
            else:
                if response.status_code in [401, 403]:
                    self.authenticate()
                if response.status_code in [422]:
                    raise Exception(response)
                else:
                    break

        return response

    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def _post(self, url, **args):

        while True:
            try:
                response = self.session.post(url, **args)
            except Exception as e:
                print(repr(e), file=sys.stderr)
                time.sleep(1)
                continue
            else:
                break

        return response


    @property
    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def status(self):

        self._status = self._get(self.status_url)

        # return (lambda x: x.json().get("status") if x.status_code == 200 else x.status_code)(self._status)
        return (lambda x: x.json().get("status") if x.status_code == 200 else 'PROCESSING')(self._status)


    @property
    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def info(self):

        try:
            return self._status.json().get("info")
        except AttributeError:
            return None
        except:
            raise

    @property
    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def skipped(self):

        try:
            return self.info.get("skipped")
        except AttributeError:
            return None
        except:
            raise

    @property
    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def success(self):

        try:
            return self.info.get("success")
        except AttributeError:
            return None
        except:
            raise

    @property
    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def manual_check(self):

        try:
            return self.info.get("manual_check")
        except AttributeError:
            return None
        except:
            raise

    @property
    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def failure(self):

        try:
            return self.info.get("failure")
        except AttributeError:
            return None
        except:
            raise

    @property
    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def warning(self):

        try:
            return self.info.get("warning")
        except AttributeError:
            return None
        except:
            raise

    @property
    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def error(self):

        try:
            return self.info.get("error")
        except AttributeError:
            return None
        except:
            raise

    @property
    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def not_applicable(self):

        try:
            return self.info.get("not_applicable")
        except AttributeError:
            return None
        except:
            raise

    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def process(self, appList):
        for app in appList:
            logger = logging.getLogger(os.path.basename(app))

            while not self.submitApp(app):
                logger.warning("Upload Failure -- Retrying")
                time.sleep(1)

            old_status = ""

            while True:

                status = self.status
                if status == "SUCCESS":
                    if old_status != "":
                        print("")

                    print("PROCESSING COMPLETE")
                    break

                elif status != old_status:
                    if old_status != "":
                        print("")

                    old_status = status
                    print(status, sep=' ', end=' ')
                else:
                    print(".", sep=' ', end='')

                sys.stdout.flush()
                time.sleep(2)

            _report = self.report
            _app_name = _report.get("app_package_id")
            _app_version =_report.get("app_version")
            _groups = _report.get("groups")
            _logger = logging.getLogger(":".join([_app_name, _app_version]))

            # print(yaml.safe_dump(s._get(s.report_url).json(), allow_unicode=True, sort_keys=True))
            if self.failure == 0 and self.error == 0 and self.manual_check == 0:
                _logger.info("Application Successfully Vetted for SplunkCloud installation ({})".format(CONST.AI_REPORT.format(request_id=self._request_id)))
                for item in _groups[0].get("checks"):
                    if item["result"] not in ["success", "not_applicable"]:
                        _logger.debug("** {}".format(item["description"]))
                        for message in item["messages"]:
                            _logger.debug("\t==> {}".format(message["message"]))
                        print("")
                return True

            else:
                _logger.error("Application Failed App Vetting: failures={}, errors={}, manual_checks={}".format(self.failure, self.error, self.manual_check))
                for group in _groups:
                    for item in group.get("checks"):
                        if item["result"] in ["error", "failure", "manual_check"]:
                            _logger.error("** {} : {}".format(item["result"], item["description"]))
                            for message in item["messages"]:
                                _logger.error("\t==> {}".format(message["message"]))
                return False



class Session():

    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def __init__(self, user, passwd):

        logger = logging.getLogger(__class__.__qualname__)
        logger.debug("hello")
        self.user = user
        self.passwd = passwd
        self.session = requests.Session()
        self._request_id = None
        # self._base_url = "https://appinspect.splunk.com"

        # uri = "https://api.splunk.com/2.0/rest/login/splunk"

        try:
            response = self.session.get(CONST.SPLUNKBASE_AUTH, auth=(user, passwd))

        except:
            return None

        try:
            self.session.headers.update({'Authorization': "Bearer {}".format(response.json().get("data").get("token"))})

        except AttributeError:
            logger.error("Authentication Error -- check credentials")
            sys.exit(1)

    def authenticate(self):
        logger = logging.getLogger(__class__.__qualname__)

        try:
            response = self.session.get(CONST.SPLUNKBASE_AUTH, auth=(self.user, self.passwd))

        except:
            return None

        try:
            self.session.headers.update({'Authorization': "Bearer {}".format(response.json().get("data").get("token"))})

        except AttributeError:
            logger.error("Authentication Error -- check credentials")

    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def submitApp(self, appLocation):

        app_name = os.path.basename(appLocation)
        while True:
            with open(appLocation, "rb") as f:
                fields = (
                ('app_package', (app_name, f)),
                # ('included_tags', 'cloud'),
                # ('included_tags', 'self-service'),
                ('included_tags', 'private_app'),
                )
                payload = MultipartEncoder(fields=fields)
                headers = { "Content-Type": payload.content_type, "max-messages": "all" }

                try:
                    response = self.session.post(CONST.AI_VALIDATE, data=payload, headers=headers)
                except:
                    return False

                else:
                    self._request_id = response.json().get("request_id")
                    if self._request_id:
                        return True

    @property
    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def report_url(self):

        try:
            return CONST.AI_REPORT.format(request_id=self._request_id)

        except KeyError:
            return None


    @property
    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def report(self):
        try:
            return(self._get(self.report_url).json().get("reports")[0])
        except:
            return None


    @property
    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def status_url(self):

        try:
            return CONST.AI_STATUS.format(request_id=self._request_id)

        except KeyError:
            return None

    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def _get(self, url, **args):

        while True:
            try:
                response = self.session.get(url, **args)
            except Exception as e:
                print(repr(e), file=sys.stderr)
                time.sleep(2)
                continue
            else:
                if response.status_code in [401, 403]:
                    self.authenticate()
                if response.status_code in [422]:
                    raise Exception(response)
                else:
                    break

        return response

    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def _post(self, url, **args):

        while True:
            try:
                response = self.session.post(url, **args)
            except Exception as e:
                print(repr(e), file=sys.stderr)
                time.sleep(1)
                continue
            else:
                break

        return response


    @property
    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def status(self):

        self._status = self._get(self.status_url)

        # return (lambda x: x.json().get("status") if x.status_code == 200 else x.status_code)(self._status)
        return (lambda x: x.json().get("status") if x.status_code == 200 else 'PROCESSING')(self._status)


    @property
    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def info(self):

        try:
            return self._status.json().get("info")
        except AttributeError:
            return None
        except:
            raise

    @property
    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def skipped(self):

        try:
            return self.info.get("skipped")
        except AttributeError:
            return None
        except:
            raise

    @property
    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def success(self):

        try:
            return self.info.get("success")
        except AttributeError:
            return None
        except:
            raise

    @property
    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def manual_check(self):

        try:
            return self.info.get("manual_check")
        except AttributeError:
            return None
        except:
            raise

    @property
    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def failure(self):

        try:
            return self.info.get("failure")
        except AttributeError:
            return None
        except:
            raise

    @property
    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def warning(self):

        try:
            return self.info.get("warning")
        except AttributeError:
            return None
        except:
            raise

    @property
    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def error(self):

        try:
            return self.info.get("error")
        except AttributeError:
            return None
        except:
            raise

    @property
    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def not_applicable(self):

        try:
            return self.info.get("not_applicable")
        except AttributeError:
            return None
        except:
            raise

    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def process(self, appList):
        for app in appList:
            logger = logging.getLogger(os.path.basename(app))

            while not self.submitApp(app):
                logger.warning("Upload Failure -- Retrying")
                time.sleep(1)

            old_status = ""

            while True:

                status = self.status
                if status == "SUCCESS":
                    if old_status != "":
                        print("")

                    print("PROCESSING COMPLETE")
                    break

                elif status != old_status:
                    if old_status != "":
                        print("")

                    old_status = status
                    print(status, sep=' ', end=' ')
                else:
                    print(".", sep=' ', end='')

                sys.stdout.flush()
                time.sleep(2)

            _report = self.report
            _app_name = _report.get("app_package_id")
            _app_version =_report.get("app_version")
            _groups = _report.get("groups")
            _logger = logging.getLogger(":".join([_app_name, _app_version]))

            # print(yaml.safe_dump(s._get(s.report_url).json(), allow_unicode=True, sort_keys=True))
            if self.failure == 0 and self.error == 0 and self.manual_check == 0:
                _logger.info("Application Successfully Vetted for SplunkCloud installation ({})".format(CONST.AI_REPORT.format(request_id=self._request_id)))
                for item in _groups[0].get("checks"):
                    if item["result"] not in ["success", "not_applicable"]:
                        _logger.debug("** {}".format(item["description"]))
                        for message in item["messages"]:
                            _logger.debug("\t==> {}".format(message["message"]))
                        print("")
                return True

            else:
                _logger.error("Application Failed App Vetting: failures={}, errors={}, manual_checks={}".format(self.failure, self.error, self.manual_check))
                for group in _groups:
                    for item in group.get("checks"):
                        if item["result"] in ["error", "failure", "manual_check"]:
                            _logger.error("** {} : {}".format(item["result"], item["description"]))
                            for message in item["messages"]:
                                _logger.error("\t==> {}".format(message["message"]))
                return False


class Liverpool():

    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def __init__(self, username, passwd):
        logger = logging.getLogger(__class__.__qualname__)
        logger.debug("hello")
        self.session = requests.Session()
        headers = {"Accept": "application/json"}
        data = {"splunk_username": username, "splunk_password": passwd}

        try:
            response = self.session.post(CONST.REVIEW_AUTH, headers=headers, data=data, verify=False)

        except Exception as e:
            print(repr(e))
            exit(2)

        try:
            self.session.headers.update({"Authorization": "JWT {}".format(response.json().get("jwt_token"))})
        except AttributeError:
            logger.error("Authentication Error -- check credentials")
            sys.exit(1)

    @debug_wrapper(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
    def uploadApp(self, app_path, customer="Splunk_Internal"):

        data = {"customer_name": customer}

        try:
            with open(app_path, "rb") as f:
                response = self.session.post(CONST.REVIEW_UPLOAD, data=data, files={"app_package": f}, verify=False)
                return response.json()
        except:
            raise


if __name__ =="__main__":
    app = "/home/doymi01/Code/python-projects/cigna/diags/diag-ch3pr609215-2022-09-09_08-45-08_vetted/custom/esi_all_amt_props-227377_ch3pr609215_2022-09-15_11-52-46_custom_1.0.0.tgz"
    user = input("Provide your Splunkbase username: ")
    passwd = getpass()

    s = Session(user, passwd)
    # app = [sys.argv[1]]
    # app = "/Users/mdoyle/Downloads/splunk-add-on-for-unix-and-linux_800.tgz"
    s.process(app)

    l = Liverpool(user, passwd)
    # app = sys.argv[1]
    l.uploadApp(app)
import logging
import os
import re
import xml.etree.ElementTree as ET

global logger
errors = re.compile(r'(?:dbxquery|loadjob\s+savedsearch=[^:]+:[^:]+:[\w]+|https?:\/\/.+?\/\S+?\/app\/\S*|https?:\/\/.+?:8089\/\S*)')

def fixtime(element, child):

    # for key in [("earliestTime", "earliest"), ("latestTime", "latest"), ("earliest", "earliest"), ("latest", "latest")]:
    for key in [("earliestTime", "earliest"), ("latestTime", "latest")]:

        try:
            _tmp = element.find(key[0])

        except AttributeError:
            pass

        else:
            if _tmp is not None:
                _new = ET.SubElement(child, key[1])
                _new.text = str(_tmp.text)
                element.remove(_tmp)

    return element

def _process_new(app, app_name, element, file, flavor):

    def log(element, file, action="unknown"):
        logger.info("Found {!r} in {!r} action={}".format(" ".join(ET.tostring(element, encoding='unicode').split()), file, action))

    # global logger
    # logger = logging.getLogger(os.path.basename(file))
    logger = logging.getLogger(app_name + " ({})".format(flavor))
    audit = logging.getLogger(app_name + ":audit")

    manual_check = []
    try:
        manual_check = list(set(errors.findall(element.text)))
    except TypeError:
        pass
    if len(manual_check) > 0:
        logger.warning("Found {} in {} -- MANUAL CHECK REQUIRED --".format(manual_check, file))

    if element.tag == "form" or element.tag == "dashboard":
        _ver = element.get('version', "1.0")
        try:
            major, minor = _ver.split(".")
        except ValueError:
            major = _ver.split(".")[0]
            minor = None
        except AttributeError:
            major = "1"
            minor = "0"
        if major == "1" and int(minor) < 1:
            element.set('version', '1.1')
        elif int(major) > 1:
            return element

    if element.find("option[@name='afterLabel']") is not None:
        _this = element.find("option[@name='afterLabel']")
        log(_this, file, "corrected")
        logger.info("Found tag={} attrib={}".format(_this.tag, _this.attrib))
        _this.set('name', "unit")

    if element.find("option[@name='beforeLabel']") is not None:
        _this = element.find("option[@name='beforeLabel']")
        log(_this, file)
        _this.set('name', "unit")
        _new = ET.SubElement(element, "option")
        _new.set('name', "unitPosition")
        _new.text = "before"

    if element.find('searchString') is not None:
        _this = element.find('searchString')
        _earliest = element.find('earliestTime')
        _latest = element.find('latestTime')

        search = ET.Element("search")
        search.tail = "\n"
        query = ET.SubElement(search, "query")
        query.text = str(_this.text)
        earliest = ET.SubElement(search, "earliest")
        earliest.text = str(_earliest.text)
        latest = ET.SubElement(search, "latest")
        latest.text = str(_latest.text)

        eT = element.find("earliestTime")
        lT = element.find("latestTime")
        e = element.find("earliest")
        l = element.find("latest")

        # if eT:
        #     earliest.text = str(eT.text).strip()
        #     element.remove(eT)
        # elif e:
        #     earliest.text = str(e.text).strip()
        #     element.remove(e)

        # if lT:
        #     latest.text = str(lT.text).strip()
        #     element.remove(lT)
        # elif l:
        #     latest.text = str(l.text).strip()
        #     element.remove(l)
        
        element.insert(list(element).index(_this), ET.Comment(ET.tostring(_this, encoding='unicode')))
        element.insert(list(element).index(_this), search)
        element.remove(_this)

        log(_this, file, "corrected")
        exit()
        # element = fixtime(element, _this)

    if element.find("searchTemplate") is not None:
        _this = element.find('searchTemplate')
        log(_this, file, "corrected")
        search = ET.Element("search", attrib={'id': 'base_search'})
        search.tail = "\n"
        query = ET.SubElement(search, "query")
        query.text = str(_this.text)
        element.insert(list(element).index(_this), ET.Comment(ET.tostring(_this, encoding='unicode').strip()))
        element.insert(list(element).index(_this), search)
        element.remove(_this)

        element = fixtime(element, _this)

    if element.find("searchPostProcess") is not None:
        _this = element.find('searchPostProcess')
        log(_this, file, "corrected")
        search = ET.Element("search", attrib={'base': 'base_search'})
        query = ET.SubElement(search, "query")
        query.text = str(_this.text)
        element.insert(list(element).index(_this), ET.Comment(ET.tostring(_this, encoding='unicode').strip()))
        element.insert(list(element).index(_this), search)
        
        element = fixtime(element, _this)

    if element.find("populatingSearch") is not None:

        _this = element.find('populatingSearch')
        log(_this, file, "corrected")
        _this.tag = "search"
        searchString = _this.text
        _this.text = None

        _query = ET.SubElement(_this, "query")
        _query.text = str(searchString)

        for k, v in [ (k, _this.attrib[k]) for k in ["earliest", "latest"] if k in _this.attrib]:
            _tmp = ET.SubElement(_this, k)
            _tmp.text = str(v)

        for k, v in [ (k, _this.attrib[k]) for k in _this.attrib.keys() if k not in ["earliest", "latest"]]:
            _tmp = ET.SubElement(element, k)
            _tmp.text = str(v)

        _this.attrib = {}

    if element.find("searchName") is not None:
        _this = element.find('searchName')
        log(_this, file, "corrected")
        _this.tag = "search"
        searchString = _this.text
        _this.text = None
        _this.attrib = {"ref": searchString}

    if element.find("option[@name='linkView']") is not None:
        for _this in element.findall("option[@name='linkView']"):
            log(_this, file, "commented")
            element.insert(list(element).index(_this), ET.Comment(ET.tostring(_this, encoding='unicode').strip()))
            element.remove(_this)

    if element.find("option[@name='linkFields']") is not None:

        _this = element.find("option[@name='linkFields']")
        log(_this, file, "commented")
        element.insert(list(element).index(_this), ET.Comment(ET.tostring(_this, encoding='unicode').strip()))
        element.remove(_this)

    if element.find("option[@name='linkSearch']") is not None:

        _this = element.find("option[@name='linkSearch']")
        log(_this, file, "commented")
        element.insert(list(element).index(_this), ET.Comment(ET.tostring(_this, encoding='unicode').strip()))
        element.remove(_this)

    if element.find("option[@name='classField']") is not None:

        _this = element.find("option[@name='classField']")
        log(_this, file, "commented")
        element.insert(list(element).index(_this), ET.Comment(ET.tostring(_this, encoding='unicode').strip()))
        element.remove(_this)

    if element.find("option[@name='additionalClass']") is not None:

        _this = element.find("option[@name='additionalClass']")
        log(_this, file, "commented")
        element.insert(list(element).index(_this), ET.Comment(ET.tostring(_this, encoding='unicode').strip()))
        element.remove(_this)

    if element.find("option[@name='displayRowNumbers']") is not None:
        _this = element.find("option[@name='displayRowNumbers']")
        log(_this, file, "commented")
        element.insert(list(element).index(_this), ET.Comment(ET.tostring(_this, encoding='unicode').strip()))
        element.remove(_this)

    if element.find('seed') is not None:
        _this = element.find('seed')
        log(_this, file, "corrected")
        _this.tag = "initialValue"

    if element.find("option[@name='height']") is not None:
        _this = element.find("option[@name='height']")
        if str(_this.text).endswith("px"):
            log(_this, file, "corrected")
            _this.text = str(_this.text).rstrip("px")

    element = fixtime(element, element)

    for child in element:
        child = _process_new(app, app_name, child, file, flavor)

    return element



def _process(element, file):

    manual_check = []
    try:
        manual_check = list(set(errors.findall(element.text)))
    except TypeError:
        pass
    if len(manual_check) > 0:
        logger.warning("Found {} in {} -- MANUAL CHECK REQUIRED --".format(manual_check, file))

    if element.tag == "form" or element.tag == "dashboard":
        _ver = element.get('version', "1.0")
        try:
            major, minor = _ver.split(".")
        except ValueError:
            major = _ver.split(".")[0]
            minor = None
        except AttributeError:
            major = "1"
            minor = "0"
        if major == "1" and int(minor) < 1:
            logger.debug("Found tag={} attrib={}".format(element.tag, element.attrib))
            element.set('version', '1.1')
            logger.debug("Corrected tag={} attrib={}".format(element.tag, element.attrib))
        elif int(major) > 1:
            return element
            # logger.info("Found tag={} attrib={} in {}".format(element.tag, element.attrib, file))
            # element.set('version', f"{major}.0")
            # logger.info("Corrected tag={} attrib={} in {}".format(element.tag, element.attrib, file))

    if element.find("option[@name='afterLabel']") is not None:
        _this = element.find("option[@name='afterLabel']")
        logger.info("Found tag={} attrib={}".format(_this.tag, _this.attrib))
        _this.set('name', "unit")

    if element.find("option[@name='beforeLabel']") is not None:

        _this = element.find("option[@name='beforeLabel']")
        logger.info("Found tag={} attrib={}".format(_this.tag, _this.attrib))
        _this.set('name', "unit")
        logger.debug("Corrected to tag={} attrib={}".format(_this.tag, _this.attrib))
        _new = ET.SubElement(element, "option")
        _new.set('name', "unitPosition")
        _new.text = "before"
        logger.debug("Adding new tag={} attrib={}".format(_new.tag, _new.attrib))

    if element.find('searchString') is not None:
        _this = element.find('searchString')
        logger.info(f"Deprecated Syntax '{element.tag}'")
        _this.tag = "search"
        searchString = _this.text
        _this.text = None

        _query = ET.SubElement(_this, "query")
        _query.text = str(searchString)

        element = fixtime(element, _this)
        logger.debug("Corrected to {} in {}".format(ET.tostring(element, encoding='unicode'), file))

    if element.find("searchTemplate") is not None:

        _this = element.find('searchTemplate')
        logger.info(f"Deprecated Syntax '{_this.tag}'")
        _this.tag = "search"
        _this.set("id", "base_search")
        searchString = _this.text
        _this.text = None

        _query = ET.SubElement(_this, "query")
        _query.text = str(searchString)

        element = fixtime(element, _this)
        logger.debug(f"Corrected Syntax '{_this.tag}'")

    if element.find("searchPostProcess") is not None:

        _this = element.find('searchPostProcess')
        logger.info(f"Deprecated Syntax '{_this.tag}'")
        _this.tag = "search"
        _this.set("base", "base_search")
        searchString = _this.text
        _this.text = None

        _query = ET.SubElement(_this, "query")
        _query.text = str(searchString)

        element = fixtime(element, _this)
        logger.debug(f"Corrected Syntax '{_this.tag}'")

    if element.find("populatingSearch") is not None:

        _this = element.find('populatingSearch')
        logger.info(f"Deprecated Syntax '{_this.tag}'")
        _this.tag = "search"
        searchString = _this.text
        _this.text = None

        _query = ET.SubElement(_this, "query")
        _query.text = str(searchString)

        for k, v in [ (k, _this.attrib[k]) for k in ["earliest", "latest"] if k in _this.attrib]:
            _tmp = ET.SubElement(_this, k)
            _tmp.text = str(v)

        for k, v in [ (k, _this.attrib[k]) for k in _this.attrib.keys() if k not in ["earliest", "latest"]]:
            _tmp = ET.SubElement(element, k)
            _tmp.text = str(v)

        _this.attrib = {}
        logger.debug(f"Corrected Syntax '{_this.tag}'")

    if element.find("searchName") is not None:

        _this = element.find('searchName')
        logger.info("Deprecated Syntax '{}'".format(_this.tag))
        _this.tag = "search"
        searchString = _this.text
        _this.text = None
        _this.attrib = {"ref": searchString}
        logger.debug(f"Corrected Syntax '{_this.tag}'")

    if element.find("option[@name='linkView']") is not None:

        for _this in element.findall("option[@name='linkView']"):
            logger.info(f"Deprecated Syntax '{_this.tag}'")
            _tmp = _this.tag
            element.append(ET.Comment(_tmp))
            element.remove(_this)
        logger.debug(f"Corrected Syntax '{element.tag}'")
        # _drilldown = element.find("option[@name='drilldown']")
        # searchLink = str(_this.text)
        # _tmp = ET.SubElement(element, "drilldown")
        # _link = ET.SubElement(_tmp, "link")
        # _link.text = searchLink
        # if _drilldown is not None:
        #     _drilldown.text = "all"
        # # remove this option
        # element.remove(_this)
        # logger.info(f"Corrected Syntax '{ET.tostring(element, encoding='unicode')}'")

    if element.find("option[@name='linkFields']") is not None:

        _this = element.find("option[@name='linkFields']")
        logger.info(f"Deprecated Syntax '{_this.tag}'")
        _tmp = _this.tag
        element.append(ET.Comment(_tmp))
        element.remove(_this)
        logger.debug(f"Corrected Syntax '{ET.tostring(element, encoding='unicode')}'")
        # _drilldown = element.find("option[@name='drilldown']")

        # if _drilldown is not None:
        #     _drilldown.text = "all"
        # # remove this option
        # element.remove(_this)

    if element.find("option[@name='linkSearch']") is not None:

        _this = element.find("option[@name='linkSearch']")
        logger.info(f"Deprecated Syntax '{_this.tag}'")
        _tmp = _this.tag
        element.append(ET.Comment(_tmp))
        element.remove(_this)
        logger.debug(f"Corrected Syntax '{ET.tostring(element, encoding='unicode')}'")        # _drilldown = element.find("option[@name='drilldown']")

        # if _drilldown is not None:
        #     _drilldown.text = "all"

        # searchLink = str(_this.text)
        # _tmp = ET.SubElement(element, "drilldown")
        # _link = ET.SubElement(_tmp, "link")
        # _link.text = searchLink
        # # remove this option
        # element.remove(_this)
        # logger.info(f"Corrected Syntax '{ET.tostring(element, encoding='unicode')}'")

    if element.find("option[@name='classField']") is not None:

        _this = element.find("option[@name='classField']")
        logger.info(f"Deprecated Syntax '{_this.tag}'")
        _tmp = _this.tag
        element.append(ET.Comment(ET.tostring(_tmp, encoding='unicode')))
        element.remove(_this)
        logger.debug(f"Corrected Syntax '{ET.tostring(element, encoding='unicode')}'")        # _drilldown = element.find("option[@name='drilldown']")

        # if _drilldown is not None:
        #     _drilldown.text = "all"
        # # remove this option
        # element.remove(_this)
        # logger.info(f"Corrected Syntax\n{ET.tostring(element, encoding='unicode')}\nin {file!r}")

    if element.find("option[@name='additionalClass']") is not None:

        _this = element.find("option[@name='additionalClass']")
        logger.info(f"Deprecated Syntax '{_this.tag}'")
        _tmp = _this.tag
        element.append(ET.Comment(_tmp))
        element.remove(_this)
        logger.debug(f"Corrected Syntax '{ET.tostring(element, encoding='unicode')}'")        # _drilldown = element.find("option[@name='drilldown']")

        # if _drilldown is not None:
        #     _drilldown.text = "all"
        # # remove this option
        # element.remove(_this)
        # logger.info(f"Corrected Syntax '{ET.tostring(element, encoding='unicode')}'")

    if element.find("option[@name='displayRowNumbers']") is not None:

        _this = element.find("option[@name='displayRowNumbers']")
        logger.info(f"Deprecated Syntax '{_this.tag}'")
        _tmp = _this.tag
        element.append(ET.Comment(_tmp))
        element.remove(_this)
        logger.debug(f"Corrected Syntax '{ET.tostring(element, encoding='unicode')}'")

    if element.find('seed') is not None:
        _this = element.find('seed')
        logger.info(f"Deprecated Syntax '{_this.tag}'")
        _this.tag = "initialValue"
        logger.debug(f"Corrected Syntax '{_this.tag}'")

    # if element.find("option[@name='linkView']") is not None:

    #     _this = element.find("option[@name='linkView']")

    #     if element.find("option[@name='drilldown']") is not None:

    #         _tmp = element.find("option[@name='drilldown']")
    #         _tmp.text = "all"
    #         element.remove(_this)

    # if element.find("option[@name='linkFields']") is not None:

    #     _this = element.find("option[@name='linkFields']")

    #     if element.find("option[@name='drilldown']") is not None:

    #         _tmp = element.find("option[@name='drilldown']")
    #         _tmp.text = "all"
    #         element.remove(_this)

    # if element.find("option[@name='linkSearch']") is not None:

    #     _this = element.find("option[@name='linkSearch']")

    #     if element.find("option[@name='drilldown']") is not None:

    #         _tmp = element.find("option[@name='drilldown']")
    #         _tmp.text = "all"

    #     else:
    #         # add the option
    #         pass

    #     logger.info("Deprecated Syntax '{}' in {}".format(_this.tag, file))
    #     searchString = _this.text
    #     element.remove(_this)

    #     _query = ET.SubElement(_this, "link")
    #     _query.text = str(searchString)

    #     element = fixtime(element, _this)
    #     logger.info("Corrected to {} in {}".format(_this.tag, file))

    if element.find("option[@name='height']") is not None:

        _this = element.find("option[@name='height']")
        _this.text = str(_this.text).rstrip("px")

    element = fixtime(element, element)

    for child in element:
        child = _process(child, file)

    return element

def fix_and_write_xml(srcFile, destFile, app=None):
    global logger
    if app:
        file = os.sep.join(srcFile.split(os.sep)[srcFile.split(os.sep).index(os.path.basename(app))+1:])
        logger = logging.getLogger(app)
    else:
        file = srcFile
        logger = logging.getLogger(__name__)

    # logger.debug("Inspecting XML file '{}'".format(file))

    # _tmp = BytesIO(b'''<?xml version='1.1' encoding='utf-8'?>\n''')
    # # _tmp = BytesIO()
    # _tmp.read()

    _src = ET.parse(srcFile)

    _src_root = _src.getroot()
    _src_root = _process(_src_root, file)
    # _src.write(destFile, encoding="utf-8", xml_declaration=True)
    _src.write(destFile, encoding="utf-8", xml_declaration=False)

    # _src.write(_tmp, encoding='utf-8', xml_declaration=False)
    # _tmp.seek(0)
    # with open(destFile, "wb") as f:
    #     f.write(_tmp.read())


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('SRC_PATH')

    args = parser.parse_args()

    for _top, _dirs, _files in os.walk(args.SRC_PATH):
        for _file in [ x for x in _files if x.endswith(".xml") ]:
            _src_file = os.path.join(_top, _file)
            _dest_file = _src_file.replace(".xml", ".xml.tmp")
            fix_and_write_xml(_src_file, _dest_file)
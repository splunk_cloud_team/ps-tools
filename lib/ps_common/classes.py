import configparser
import io
import json
import multiprocessing as mp
import pickle
import re
from distutils.log import debug
from getpass import getpass
from textwrap import wrap

import requests
# from cryptography.fernet import Fernet
from splunklib import client, data
from splunklib.binding import AuthenticationError, connect, handler

from .consts import *
from .decorators import *
from .exceptions import *
from .utils import _decode, store

DEBUG = False
TRACE = False
class ConfParser(configparser.ConfigParser):
    replacements = [
        ('\\\n', '##--ESCAPED_NEWLINE--##'),
        ('[]', '[##--NO_NAME--##]')
        ]
    # override section header regex to support brackets inside
    # SECTCRE = re.compile(r'\[\s*(?P<header>.+?)\s*\]')
    SECTCRE = re.compile(r'\[\s*(?P<header>.+?)\s*\]\s*$')

    @debug_wrapper(DEBUG, TRACE)
    def __init__(self, *args, delimiters='=', allow_no_value=True, defaults=None, strict=False, interpolation=None, **kwargs):
        super().__init__(*args, delimiters=delimiters, allow_no_value=allow_no_value, defaults=defaults, strict=strict, interpolation=interpolation, **kwargs)
        self.optionxform = str

    @debug_wrapper(True)
    def _unescape(self, data, preserve_newlines=True):
        for repl in ConfParser.replacements:
            if preserve_newlines and repl[1] == "##--ESCAPED_NEWLINE--##":
                data = data.replace(repl[1], " ")
            else:
                data = data.replace(repl[1], repl[0])
            # added to address items with no section at the top of the file
        data = re.sub(r"\[##--NO_SECTION--##\]", "", data)
        return data

    @debug_wrapper(DEBUG, TRACE)
    def __str__(self):
        with io.StringIO() as f:
            super().write(f)
            f.seek(0)
            data = f.read()

        # for repl in ConfParser.replacements:
        #     data = data.replace(repl[1], repl[0])
        #     # added to address items with no section at the top of the file
        # data = re.sub(r"\[##--NO_SECTION--##\]", "", data)

        return self._unescape(data)

    @debug_wrapper(DEBUG, TRACE)
    def sorted(self):
        _tmp = ConfParser(allow_no_value=True, defaults=None, strict=False, interpolation=None)
        t = ["##--NO_SECTION--##", "##--NO_NAME--##", "default", "global", "app"]
        s = [x for x in sorted(self.sections(), key=lambda z: z.lower()) if x not in ["default", "global", "##--NO_SECTION--##", "##--NO_NAME--##", "app"]]
        t.extend(s)
        for section in t:
            try:
                _tmp[section] = self[section]
            except KeyError:
                pass
        return _tmp

    @debug_wrapper(True, True)
    def _read_file(self, filename, encoding='utf-8-sig'):
        try:
            with open(filename, "r", encoding=encoding) as f:
                data = f.read()
        except FileNotFoundError:
            return

        # added to remove BOM
        data = re.sub(r"^\xfe\xff|^\xff\xfe|^\xef\xbb\xbf", "", data)

        # added to remove trailing whitespace
        data = re.sub(r"[\s]*$", "", data, re.MULTILINE)

        for repl in ConfParser.replacements:
            data = data.replace(repl[0], repl[1])

        # added to address items with no section at the top of the file
        data = "[##--NO_SECTION--##]\n" + data
        return data

    @debug_wrapper(DEBUG, TRACE)
    def _write_file(self, fileobject, space_around_delimiters=True):
        data = self.__str__()
        fileobject.write(data)

    @debug_wrapper(DEBUG, TRACE)
    def dump(self, fileobject, space_around_delimiters=True):
        self._write_file(fileobject, space_around_delimiters)

    @debug_wrapper(DEBUG, TRACE)
    def dumps(self):
        return self.__str__()

    @debug_wrapper(DEBUG, TRACE)
    def load(self, conf_file, **kwargs):
        self.read_string(self._read_file(conf_file, **kwargs))
        return self

    @debug_wrapper(DEBUG, TRACE)
    def loads(self, string, source='<string>'):
        self.read_string(string, source)
        return self

    @debug_wrapper(DEBUG, TRACE)
    def diff(self, confObj):
        result = ConfParser(allow_no_value=True, strict=False, interpolation=None)
        for section in confObj.sections():
            for k, v in confObj[section].items():

                try:
                    self[section][k]

                except KeyError:
                    try:
                        result[section][k] = v
                    except KeyError:
                        result[section] = {k: v}

                else:
                    if self[section][k] != v:
                        try:
                            result[section][k] = v

                        except KeyError:
                            result[section] = {k: v}

        return result

    @debug_wrapper(DEBUG, TRACE)
    def diff_with_file(self, conf_file, **kwargs):
        tmp = ConfParser(allow_no_value=False, defaults=None, strict=False, interpolation=None)
        tmp.load(conf_file, **kwargs)
        return self.diff(tmp)

    @debug_wrapper(DEBUG, TRACE)
    def merged_with(self, confObj):
        result = ConfParser(allow_no_value=True, strict=False, interpolation=None)
        result.read_dict(self)
        for section in confObj.sections():
            for k, v in confObj[section].items():
                if v is not None and v != "":
                    try:
                        result[section][k] = v
                    except KeyError:
                        result[section] = {k: v}
        return result

    @debug_wrapper(DEBUG, TRACE)
    def merge_with_file(self, conf_file, **kwargs):
        tmp = ConfParser(allow_no_value=False, defaults=None, strict=False, interpolation=None)
        tmp.load(conf_file, **kwargs)
        return self.merged_with(tmp)

    @debug_wrapper(DEBUG, TRACE)
    def merged_with_file(self, conf_file, **kwargs):
        tmp = ConfParser(allow_no_value=False, defaults=None, strict=False, interpolation=None)
        tmp.load(conf_file, **kwargs)
        return self.merged_with(tmp)

    @debug_wrapper(DEBUG, TRACE)
    def safe_get(self, section, option, preserve_newlines=True):
        return self._unescape(super().get(section, option), preserve_newlines=preserve_newlines)

    @debug_wrapper(DEBUG, TRACE)
    def read_dict(self, *args, **kwargs):
        return super().read_dict(*args, **kwargs)


class Splunk(client.Service):

    @debug_wrapper(DEBUG, TRACE)
    @unrecoverable_wrapper
    def __init__(self, **kwargs):
        h = handler(key_file=None, cert_file=None, timeout=600)
        # save the params as-is for later
        # create a temporary decrypted copy
        clear_text = dict(kwargs)
        # key = open('.secret_key', 'rb').read()
        # fernet = Fernet(key)
        # clear_text['password'] = fernet.decrypt(clear_text['password']).decode()

        super(Splunk, self).__init__(handler=h, **clear_text)
        self._kwargs = kwargs
        self._connection = connect(**clear_text)
        self._store = dict()
        store(self._store, self._get_server_info(), INFO_KEYS)
        self._store['cluster'] = {}
        store(self._store['cluster'], self._get_cluster_config(), CLUSTER_KEYS)

    @unrecoverable_wrapper
    @debug_wrapper(DEBUG, TRACE)
    @list_wrapper
    def _get_cluster_config(self):
        return _decode(self._connection.get("cluster/config", f=CLUSTER_KEYS))['feed']['entry']

    @unrecoverable_wrapper
    @debug_wrapper(DEBUG, TRACE)
    @list_wrapper
    def _get_server_info(self):
        return _decode(self._connection.get("server/info", f=INFO_KEYS))['feed']['entry']

    @unrecoverable_wrapper
    @debug_wrapper(DEBUG, TRACE)
    def __new__(cls, *args, **kwargs):
        return object.__new__(cls)

    @property
    @debug_wrapper(DEBUG, TRACE)
    def server_info(self):
        return self._store.get('server-info', {})

    @property
    @debug_wrapper(DEBUG, TRACE)
    def cluster_config(self):
        return self._store['cluster'].get('config', {})

    @property
    @debug_wrapper(DEBUG, TRACE)
    def cm_uri(self):
        _tmp = self._store['cluster']['config'].get('manager_uri', self._store['cluster']['config'].get('master_uri', ""))
        if _tmp.startswith("https://"):
            return _tmp[8:]
        elif _tmp.startswith("http://"):
            return _tmp[7:]
        else:
            return _tmp

    @property
    @debug_wrapper(DEBUG, TRACE)
    def guid(self):
        return self._store['server-info'].get('guid', None)

    @property
    @debug_wrapper(DEBUG, TRACE)
    def cluster_mode(self):
        return self._store['cluster']['config'].get('mode', None)

    @property
    @debug_wrapper(DEBUG, TRACE)
    def cluster_multisite(self):
        if str(self._store['cluster']['config'].get('multisite', '0')).lower() in ["true", "1"]:
            return True
        else:
            return False

    @property
    @debug_wrapper(DEBUG, TRACE)
    def roles(self):
        return self._store['server-info'].get('server_roles', None)

    @debug_wrapper(True)
    def save(self):
        with open(f'.{self.guid}', "wb") as f:
            pickle.dump(self._store, f)
        return self._store

# class Manager(Self):

#     @debug_wrapper(DEBUG, TRACE)
#     def __init__(self, **kwargs):
#         super(Splunk, self).__init__(**kwargs)
#         try:
#             assert self.cluster_mode in ['master', 'manager']
#         except AssertionError:
#             raise UnrecoverableError(f"Cluster Discovery Failed -- expected 'manager' or 'master' received cluster_mode={self.cluster_mode!r}")
#         store(self._store['cluster'], self._get_cluster_peers(), username=kwargs['username'], password=kwargs['password'])
#         with open('peers.json', 'w') as f:
#             json.dumps(self._store['cluster']['peers'], indent=2)

#     @unrecoverable_wrapper
#     @debug_wrapper(DEBUG, TRACE)
#     @list_wrapper
#     def _get_peer_list(self):
#         return _decode(self._connection.get("cluster/master/peers", count=0, f=PEER_KEYS))['feed']['entry']

#     @unrecoverable_wrapper
#     @debug_wrapper(DEBUG, TRACE)
#     @list_wrapper
#     def _get_cluster_peers(self, username, password):
#         self._store['cluster']['peers'] = {}
#         for peer in self._get_peer_list():
#             _host, _port = peer['register_search_address'].split(":")
#             _tmp = Splunk(host=_host, port=_port, username=username, password=password)
#             self._store['cluster']['peers'][_tmp.guid] = _tmp.server_info

class Self(Splunk):

    @debug_wrapper(DEBUG, TRACE)
    def __init__(self, get_primaries=True, **kwargs):
        super(Self, self).__init__(**kwargs)
        store(self._store, self._get_server_settings(), SETTINGS_KEYS)
        self._store['indexes'] = {}
        store(self._store['indexes'], self._get_indexes(), INDEX_KEYS)

        _cluster_mode = self.cluster_mode
        if _cluster_mode in ['slave', 'peer']:
            # discover the cluster
            _manager_uri = self.cm_uri
            try:
                assert _manager_uri not in ['?', '', None]
            except AssertionError:
                raise UnrecoverableError(f'Cluster is misconfigured. manager_uri={_manager_uri!r}')

            cm_host, cm_port = _manager_uri.split(":")
            self._manager = Splunk(host=cm_host, port=cm_port, username=kwargs['username'], password=kwargs['password'])

            try:
                assert self._manager.cluster_mode in ['master', 'manager']
            except AssertionError:
                raise UnrecoverableError(f"Cluster Discovery Failed -- expected 'manager' or 'master' received cluster_mode={self._cm.cluster_mode!r}")

            self._get_cluster_peers(username=kwargs['username'], password=kwargs['password'])

            if get_primaries:
                for index in self.indexes:
                    self._store['indexes'][index['title']]['primaries'] = {}
                    store(self._store['indexes'][index['title']]['primaries'], self._get_primary_buckets(index['title']), PRIMARY_KEYS)

        elif _cluster_mode in ['master', 'manager', 'searchhead']:
            raise UnrecoverableError(f'This script should not be run on this node: mode={_cluster_mode!r}')
        else: pass

        with open('debug_dump.json', 'w') as f:
            f.write(json.dumps(self._store, indent=2))

    @unrecoverable_wrapper
    @debug_wrapper(DEBUG, TRACE)
    @list_wrapper
    def _get_server_settings(self):
        return _decode(self._connection.get("server/settings", count=0, f=SETTINGS_KEYS))['feed']['entry']

    @unrecoverable_wrapper
    @debug_wrapper(DEBUG, TRACE)
    @list_wrapper
    def _get_indexes(self):
        _result = []
        count = 30
        offset = 0
        while True:
            try:
                _tmp = _decode(self._connection.get("data/indexes", count=count, offset=offset, f=INDEX_KEYS, search=['disabled=0']))['feed']['entry']
            except KeyError:
                break
            if isinstance(_tmp, list):
                _result.extend(_tmp)
            else:
                _result.extend([_tmp])
            offset += count
        if len(_result) == 0:
            return {}
        else:
            return _result

    @unrecoverable_wrapper
    @debug_wrapper(DEBUG, TRACE)
    @list_wrapper
    def _get_peer_list(self):
        return _decode(self._manager._connection.get("cluster/master/peers", count=0, f=PEER_KEYS))['feed']['entry']

    @unrecoverable_wrapper
    @debug_wrapper(DEBUG, TRACE)
    @list_wrapper
    def _get_cluster_peers(self, username, password):
        self._store['cluster']['peers'] = {}
        for peer in self._get_peer_list():
            _host, _port = peer['content']['register_search_address'].split(":")
            _tmp = Splunk(host=_host, port=_port, username=username, password=password)
            self._store['cluster']['peers'][_tmp.guid] = _tmp.server_info

    @unrecoverable_wrapper
    @debug_wrapper(DEBUG, TRACE)
    @list_wrapper
    def _get_primary_buckets(self, index):
        _result = []
        # offset = 0
        # count = 1000
        if self.cluster_multisite:
            search_string = MULTISITE_PRIMARY.format(self.guid)
        else:
            search_string = CLUSTER_PRIMARY.format(self.guid)

        # while True:
        #     try:
        #         _tmp = _decode(self._manager._connection.get("cluster/master/buckets", count=count, offset=offset, f=PRIMARY_KEYS, filter=[f'index={index}'], search=search_string))['feed']['entry']
        #     except KeyError:
        #         break
        #     if isinstance(_tmp, list):
        #         _result.extend(_tmp)
        #     else:
        #         _result.extend([_tmp])

        #     offset += count
        # if len(_result) == 0:
        #     return {}
        # else:
        #     return _result
        try:
            return _decode(self._manager._connection.get("cluster/master/buckets", count=0, f=PRIMARY_KEYS, filter=['status=Complete', 'search_state=Searchable', f'index={index}'], search=search_string))['feed']['entry']
        except KeyError:
            return {}

    @property
    @debug_wrapper(DEBUG, TRACE)
    def SPLUNK_HOME(self):
        return self._store['settings']['SPLUNK_HOME']

    @property
    @debug_wrapper(DEBUG, TRACE)
    def cluster_multisite(self):
        return self._manager.cluster_multisite

    @property
    @debug_wrapper(DEBUG, TRACE)
    def indexes(self):
        _result = []
        for name, content in self._store['indexes'].items():
            _tmp = {}
            _tmp['title'] = name
            for k in [x for x in content.keys() if x in INDEX_KEYS]:
                _tmp[k.replace("_expanded","")] = content[k].replace("$_index_name", name)
            _result.append(_tmp)
        return _result

    @debug_wrapper(DEBUG, TRACE)
    def index(self, name):
        result = {}
        _tmp = self._store['indexes'].get(name, None)
        if _tmp is None:
            return result
        else:
            result['title'] = name
            for k in [x for x in _tmp.keys() if x in INDEX_KEYS]:
                result[k.replace("_expanded", "")] = _tmp[k].replace("$_index_name", name)
            return result

    @property
    @debug_wrapper(DEBUG, TRACE)
    def peers(self):
        _mode = self.cluster_mode
        if _mode == 'disabled':
            return {self.guid: self._store['server-info']}
        elif _mode in ['slave', 'peer']:
            return self._store['cluster']['peers']
        else:
            raise UnrecoverableError(f'This script should not be run on this node: mode={_mode!r}')

    @debug_wrapper(True)
    def is_primary_bucket(self, index, bid):
        _mode = self.cluster_mode
        if _mode == 'disabled':
            return True
        elif _mode in ['slave', 'peer']:
            try:
                self._store['indexes'][index]['primaries'][bid]
            except KeyError:
                return False
            else:
                return True
        else:
            raise UnrecoverableError(f'This script should not be run on this node: mode={_mode!r}')



if __name__ == "__main__":
    logListenerConfig(debug=True, verbose=False)
    try:
        with open('.secret_key', 'rb') as f:
            key = f.read()
    except FileNotFoundError:
        key = Fernet.generate_key()
        with open('.secret_key', 'wb') as f:
            f.write(key)

    fernet = Fernet(key)

    try:
        print("Provide the authentication information for Splunk")
        username = input("Enter username (must have 'admin' Splunk role assigned): ")
        password = fernet.encrypt(getpass().encode())
        this = Self(username=username, password=password)
        this.save()
        print(json.dumps(this._store))

    except UnrecoverableError as e:
        raise SystemExit(e)

class StatusWorker_mp(mp.Process):

    @unrecoverable_wrapper
    @debug_wrapper
    def __init__(self, _args, exit_event, resume_event, status_queue, log_queue):
        pass

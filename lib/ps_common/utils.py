TRACE = True
import gzip
import os
import re
import subprocess

import requests
from splunklib import data

from .decorators import *
from .exceptions import *

_guid_pattern = r'[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}'
_bid_pattern = r'^[a-z_0-9]*~\d+~{}$'.format(_guid_pattern)
_path_pattern = r'^[dr]b_\d{{10}}_\d{{10}}_\d+(?:_(?P<guid>{}))?$'.format(_guid_pattern)

_valid_guid = re.compile(_guid_pattern)
_valid_bid = re.compile(_bid_pattern)
_valid_path = re.compile(_path_pattern)

@unrecoverable_wrapper
@debug_wrapper(TRACE)
def store(_store, input, keys):
    assert isinstance(input, list)
    for item in input:
        try:
            _store[item['title']] = {}
        except KeyError:
            continue
        for k in [x for x in item.get('content', {}).keys() if x in keys]:
            _store[item['title']][k] = item['content'][k]
    return _store

@unrecoverable_wrapper
@debug_wrapper(TRACE)
def _decode(response, match=None):
    return data.load(response.body.read().decode('utf-8', 'xmlcharrefreplace'), match)

@unrecoverable_wrapper
@debug_wrapper(TRACE)
def is_valid_bid(value):
    valid =  _valid_bid.match(value)
    if not valid: raise ValidationError('Bucket ID (bid) must be in the format "{}"'.format(_valid_bid.pattern))
    return True

@unrecoverable_wrapper
@debug_wrapper(TRACE)
def is_valid_path(value):
    valid = _valid_path.match(value)
    if not valid: raise ValidationError('{} is not a valid Splunk bucket directory path.'.format(value))    
    complete = valid.group("guid")    
    return valid.group("guid")

@unrecoverable_wrapper
@debug_wrapper(TRACE)
def get_journal_header(file):
    """reads the header from the uncompressed journal and returns the earliest timestamp
    Args:
        file (str): path to the journal.gz file
    Raises:
        ValidationError: Not a valid journal
    Returns:
        dict: {'VERSION': <int>, 'EARLIEST': <int>, 'SITE': <str>}
    """    
    _result = {}
    try:  
        with gzip.open(file, "rb") as f:
            data = f.read(48)
    except FileNotFoundError as e:
        return {}

    if int.from_bytes(data[0:2], byteorder='little') != 266: raise ValidationError("This is not a Splunk journal Version 1")    
    _result['VERSION'] = int(data[1])
    _result['EARLIEST'] = int.from_bytes(data[3:7], byteorder='little')
    _result['SITE'] = (lambda x, y: x if y == 2 else "")(data[21:20+int(data[19])].decode('utf-8'), int(data[20]))    
    return _result

# @debug_wrapper(TRACE)
# @unrecoverable_wrapper
# def get_index_earliest(file):
#     """opens the journal file and retrieves the active (earliest) time
#     Args:
#         file (str): path to the journal.gz file
#     Returns:
#         int: epoch seconds
#     """ 
#     with open(file, "rb") as f:
#         data = f.read(10)        
#     return int.from_bytes(data[4:8], byteorder='little')

REDACT_KEYS = ["password"]

@unrecoverable_wrapper
@debug_wrapper(TRACE)
def isSplunkbaseApp(srcApp, details=False):
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    # url = "https://splunkbase.splunk.com/api/v1/app/?appid={}&include=release,releases.splunk_compatibility,release.splunk_compatibility&instance_type=cloud".format(srcApp)   
    url = "https://splunkbase.splunk.com/api/v1/app/?appid={}&include=release,releases.splunk_compatibility,release.splunk_compatibility".format(srcApp)    
    logger.debug("Checking Splunkbase for {}".format(srcApp))   
    retries = 0
    
    success = False
    while True:
        if retries > 5:
            logger.fatal("Cannot connect to Splunkbase -- Cannot continue")
            raise UnrecoverableError("Cannot Connect to Splunkbase")
        time.sleep(retries)
        try:
            resp = requests.get(url)            
        except:
            logger.error("Problem connecting to Splunkbase")
            retries += 1
            continue
        if resp.status_code != 200:
            logger.warning("Splunkbase: Response {}".format(repr(resp)))
            retries +=1
            continue     
        else:
            break

    if resp.json()["total"] > 0:
        logger.info("--SPLUNKBASE_APP_DETECTED ({})--".format(srcApp))
        # if resp.json()["results"][]
        for item in resp.json()["results"]:
            for k, v in item.items():
                if k == "release":
                    logger.info("\t{}:".format(k))
                    for k1, v1 in item[k].items():
                        if k1 in ["title", "splunk_compatibility", "product_compatibility"]:
                            logger.info("\t\t{}: {}".format(k1.replace("title", "version"), v1))
                        else:
                            logger.debug("\t\t{}: {}".format(k1, v1))
                elif k in ["title", "path"]:
                    logger.info("\t{}: {}".format(k,v))
                else:
                    logger.debug("\t{}: {}".format(k,v))
        if not details:
            return True
        else:
            return True, resp.json()["results"]
    logger.debug("{} is not a Splunkbase app".format(srcApp))

    if not details:
        return False
    else:
        return False, {}

@unrecoverable_wrapper
@debug_wrapper(TRACE)
def redact_values(in_dict, redact=None):
    
    if isinstance(redact, list):
        REDACT_KEYS.extend(redact)
    elif isinstance(redact, str):
        REDACT_KEYS.append(redact)
    elif redact is None:
        pass
    else:
        raise ValueError
    
    return {k: (lambda x: in_dict[x] if x not in REDACT_KEYS else "--REDACTED--")(k) for k in in_dict.keys()}

@unrecoverable_wrapper
@debug_wrapper(TRACE)
def get_origin_site(journal):
    """Returns the origin site of the provided journal file

    Args:
        journal (str): path to the journal.gz

    Returns:
        str: name of origin site (e.g. site2)
    """
    with gzip.open(journal, "rb") as f:
        data = f.read(32)
        
    if (lambda x: True if x == 2 else False)(int(data[20])):
        site = data[21:20+int(data[19])].decode('utf-8')
        return site
    else:
        return ""

@unrecoverable_wrapper
@debug_wrapper(TRACE)    
def get_index_earliest(journal):
    """Returns the epoch time of the earliest _index_time for the provided journal file

    Args:
        journal (str): path to the journal.gz
        
    Returns:
        int: epoch time in seconds
    """
    with open(journal, "rb") as f:
        data = f.read(10)
    
    return int.from_bytes(data[4:8], byteorder='little')

@unrecoverable_wrapper
@debug_wrapper(TRACE)
def get_index_latest(path):
    pass

@unrecoverable_wrapper
@debug_wrapper(TRACE)
def get_index_earliest_latest(path):
    
    _metafiles = ["Hosts.data", "Sources.data", "SourceTypes.data"]
    _journal = os.path.join(path, 'rawdata', 'journal.gz')
    
    _index_earliest = get_index_earliest(_journal)
    print(_index_earliest)
    for _file in [ os.path.join(path, x) for x in _metafiles]:
        with open(_file, "r") as f:
            _latest = f.readline().split()[5]
            print(_latest)

@unrecoverable_wrapper
@debug_wrapper(TRACE)    
def online_fsck(splunk_home, cmd='repair', path="", timeout=600):
    
    # repair the bucket
    cmd = [os.path.join(splunk_home, "bin", "splunk"), "cmd", "splunkd", "fsck", cmd, "--one-bucket", "--bucket-path={}".format(path)]
    _proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding='utf-8')
    try:
        outs, errs = _proc.communicate(timeout=timeout)
    except subprocess.TimeoutExpired as e:
        _proc.kill()
        outs, errs = _proc.communicate()
        raise e
    return (outs, errs)

@unrecoverable_wrapper
@debug_wrapper(TRACE)    
def fix_metadata(splunk_home, path=""):
    
    cmd = [os.path.join(splunk_home, "bin", "splunk"), "cmd", "splunkd", "recover-metadata", str(path), "--fixup-bucket-metadata-after-delete"]
    _proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    try:
        outs, errs = _proc.communicate(timeout=2)
        
    except subprocess.TimeoutExpired as e:
        _proc.kill()
        outs, errs = _proc.communicate()
        raise e

@unrecoverable_wrapper
@debug_wrapper(TRACE)
def create_bucket_info(splunk_home, path=""):
    
    try: fix_metadata(splunk_home, path=path)
    except subprocess.TimeoutExpired:
        try: online_fsck(splunk_home, cmd='repair', path=path)
        except subprocess.TimeoutExpired: return False
    
    return get_index_earliest(os.path.join(path, "rawdata", "journal.gz"))
        

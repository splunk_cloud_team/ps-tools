@placement search-head
[<$EVENTTYPE>]
disabled = [1|0]
search = <string>
priority = <integer, 1 through 10>
description = <string>
tags = <string>
color = <string>

@placement search-head
[<collection-name>]
enforceTypes = true|false
field.<name> = number|bool|string|time
accelerated_fields.<name> = <json>
profilingEnabled = true|false
profilingThresholdMs = <zero or positive integer>
replicate = true|false
replication_dump_strategy = one_file|auto
replication_dump_maximum_file_size = <unsigned integer>
type = internal_cache|undefined
local = <boolean>

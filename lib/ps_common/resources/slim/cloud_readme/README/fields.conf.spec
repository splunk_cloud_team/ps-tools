@placement search-head
[<field name>]
TOKENIZER = <regular expression>
INDEXED = [true|false]
INDEXED_VALUE = [true|false|<sed-cmd>|<simple-substitution-string>]

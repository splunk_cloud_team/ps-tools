import functools
import inspect
import json
import logging
import os
import signal
import sys
import time

from .exceptions import *

def debug_wrapper(ENABLED=True, TRACE=True):
    def debug_decorator(func):

        @functools.wraps(func)
        def trace_debug(*args, **kwargs):
            logger = logging.getLogger(func.__name__)
            frame = inspect.stack(1)
            args_repr = [repr(a) for a in args]
            kwargs_repr = [f"{k}={v!r}" for k, v in kwargs.items()]
            signature = ", ".join(args_repr + kwargs_repr)

            try:
                logger.debug("Calling {}({}) from file='{}', lineno={}, code_context='{}'".format(
                    func.__name__, signature, frame[1].filename, frame[1].lineno, frame[1].code_context[0].strip()))
            finally:
                del frame

            start_time = time.perf_counter()

            try:
                _result = func(*args, **kwargs)
            except Exception as e:
                logger.exception("{}: Exception: {!r}".format(
                    func.__name__, repr(e)))
                raise e

            end_time = time.perf_counter()
            run_time = end_time - start_time
            logger.debug("Finished {!r} in {:.4f} secs".format(
                func.__name__, run_time))

            logger.debug("{!r} returned {!r}".format(
                func.__name__, repr(_result)))
            return _result

        @functools.wraps(func)
        def debug(*args, **kwargs):
            logger = logging.getLogger(func.__name__)

            _result = func(*args, **kwargs)

            logger.debug("RETURNED: {!r}".format(repr(_result)))

            return _result

        if not ENABLED:
            return func
        elif TRACE:
            return trace_debug
        else:
            return debug

    return debug_decorator

def unrecoverable_wrapper(func):
    @functools.wraps(func)
    def unrecoverable_wrapper(*args, **kwargs):
        logger = logging.getLogger(func.__name__)
        try:
            _result = func(*args, **kwargs)
        except RecoverableError as e:
            return _result
        except Exception as e:
            if sys.version_info[0] > 3 or (sys.version_info[0] == 3 and sys.version_info[1] > 7):
                logger.debug(repr(e), stack_info=True, stacklevel=3)
            else:
                logger.debug(repr(e), stack_info=True)
            raise UnrecoverableError(e)
        else:
            return _result
    return unrecoverable_wrapper

def list_wrapper(func):
    @functools.wraps(func)
    def list_wrapper(*args, **kwargs):
        _result = func(*args, **kwargs)
        if isinstance(_result, list):
            return _result
        else:
            return [_result]
    return list_wrapper

def fatal_wrapper(func):
    @functools.wraps(func)
    def kill_all(*args, **kwargs):
        logger = logging.getLogger(func.__name__)
        try:
            _result = func(*args, **kwargs)
        except Exception as e:
            sys.stderr("Unhandled Exception: {}".format(repr(e)))
            logger.critical(repr(e))
            if sys.version_info[0] > 3 or (sys.version_info[0] == 3 and sys.version_info[1] > 7):
                logger.debug(repr(e), stack_info=True, stacklevel=10)
            else:
                logger.debug(repr(e), stack_info=True)
            os.kill(os.getppid(), signal.SIGINT)
        else:
            return _result
    return kill_all

if __name__ == "__main__":
    from logger import logListenerConfig
    logListenerConfig(debug=True, verbose=True, log_dir="test_log")
    x = "dummy"
    test(string=x)

# You'll need these imports in your own code
import logging
import logging.handlers
import multiprocessing as mp
import os


def logListenerConfig(debug=False, verbose=False, log_dir="log", add_time=True, audit=False):
    LOG_DIR = log_dir
    root = logging.getLogger()
    root.setLevel(logging.NOTSET)
    os.makedirs(LOG_DIR, exist_ok=True)
    
    def audit_rec(record):
        return record.name.endswith(":audit")

    def savedsearch_rec(record):
        if record.name.endswith(":savedsearch"):
            record.name = record.name.replace(":savedsearch", "")
            return True
        
        return False

    def exclude_named(record):
        return not (
            record.name.endswith(":audit") or
            record.name.endswith(":savedsearch")
        )

    # Formatters
    if add_time:
        f_verbose = logging.Formatter('%(asctime)s %(levelname)8s [ %(name)s:%(funcName)s() ] file="%(filename)s", line_no="%(lineno)s", %(message)s', datefmt="%Y-%m-%dT%H:%M:%S%z")
        f_console = logging.Formatter('%(levelname)8s [ %(name)s ] - %(message)s', datefmt="%Y-%m-%dT%H:%M:%S%z")
        f_file = logging.Formatter('%(asctime)15s %(levelname)8s [ %(name)s ] - %(message)s', datefmt="%Y-%m-%dT%H:%M:%S%z")
    
    else:
        f_verbose = logging.Formatter('%(levelname)8s [ %(name)s:%(funcName)s() ] file="%(filename)s", line_no="%(lineno)s", %(message)s', datefmt="%Y-%m-%dT%H:%M:%S%z")
        f_console = logging.Formatter('%(levelname)8s [ %(name)s ] - %(message)s', datefmt="%Y-%m-%dT%H:%M:%S%z")
        f_file = logging.Formatter('%(levelname)8s [ %(name)s ] - %(message)s', datefmt="%Y-%m-%dT%H:%M:%S%z")

    # Handlers
    if debug:
        h_debug = logging.handlers.RotatingFileHandler(os.path.normpath(os.path.join(LOG_DIR, "debug.log")), mode="a", maxBytes=50000000, backupCount=0, delay=True)
    # else:
    #     h_debug = logging.FileHandler(os.path.normpath(os.path.join(LOG_DIR, "debug.log")), mode="w")
        
        h_debug.setFormatter(f_file)
        h_debug.setLevel(logging.DEBUG)
        root.addHandler(h_debug)

    
    f_audit = logging.Formatter('%(message)s')
    h_audit = logging.FileHandler(os.path.normpath(os.path.join(LOG_DIR, "audit.log")), mode="w", delay=True)
    h_audit.setFormatter(f_audit)
    h_audit.setLevel(logging.NOTSET)
    h_audit.addFilter(audit_rec)


    f_savedsearch = logging.Formatter('%(levelname)8s [ %(name)s ] - %(message)s')
    h_savedsearch = logging.FileHandler(os.path.normpath(os.path.join(LOG_DIR, "savedsearches.log")), mode="w", delay=True)
    h_savedsearch.setFormatter(f_savedsearch)
    h_savedsearch.setLevel(logging.NOTSET)
    h_savedsearch.addFilter(savedsearch_rec)
        


    h_console = logging.StreamHandler()
    h_console.setLevel(logging.INFO)
    h_console.addFilter(exclude_named)

    h_error = logging.FileHandler(os.path.normpath(os.path.join(LOG_DIR, "errors.log")), mode="a", delay=True)
    h_error.addFilter(exclude_named)
    h_info = logging.FileHandler(os.path.normpath(os.path.join(LOG_DIR,"output.log")), mode="a", delay=True)
    h_info.addFilter(exclude_named)

    if verbose:
        h_console.setFormatter(f_file)
        h_error.setFormatter(f_verbose)
        h_info.setFormatter(f_file)

    else:
        h_console.setFormatter(f_console)
        h_error.setFormatter(f_file)
        h_info.setFormatter(f_file)
    
    h_error.setLevel(logging.WARNING)
    h_info.setLevel(logging.INFO)
    
    root.addHandler(h_console)
    root.addHandler(h_error)
    root.addHandler(h_info)
    root.addHandler(h_audit)
    root.addHandler(h_savedsearch)

def logProducerConfig(queue=None):

    log_level = logging.NOTSET
    
    if queue is not None:    
        h_queue = logging.handlers.QueueHandler(queue)  # Just the one handler needed
        root = logging.getLogger()
        root.addHandler(h_queue)
        # send all messages
        root.setLevel(log_level)
    # else:
    #     logListenerConfig()

def logListenerProcess(queue, debug=False, verbose=False, log_dir="log"):
    logListenerConfig(debug, verbose, log_dir)
    while True:
        try:
            record = queue.get()
            if record is None:
                break
            logger = logging.getLogger(record.name)
            logger.handle(record)
        except Exception:
            import sys, traceback
            print("Error in Log Handler:", file=sys.stderr)
            traceback.print_exc(file=sys.stderr)
            
        queue.task_done()

if __name__ == '__main__':
    
    mp.set_start_method = 'spawn'
    
    def logProducer(in_queue, log_queue):
        
        logProducerConfig(log_queue)
        logger = logging.getLogger(mp.current_process().name)
        logger.info("Hello")
        
        while True:
            item = in_queue.get()
            logger.info("Processing item {}".format(item))
            in_queue.task_done()
        
    
    log_queue = mp.Queue(-1)
    in_queue = mp.JoinableQueue()
    logger = logging.getLogger()
    logger.warning("HELLO!")
    
    listener = mp.Process(target=logListenerProcess, name="LoggingProcess", args=(log_queue,))
    listener.daemon = False
    listener.start()
    
    for _ in range(4):
        worker = mp.Process(target=logProducer, name='logProducer-{}'.format(_), args=(in_queue, log_queue))
        worker.daemon = True
        worker.start()
        
    for _ in range(100):
        in_queue.put(_)
        
    in_queue.join()
    
    log_queue.put(None)

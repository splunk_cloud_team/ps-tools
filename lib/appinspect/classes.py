

import json
import logging
import os
import posixpath
import shutil
import sys
import time
from getpass import getpass
sys.path.append("../..")
from ps_common import logger

import requests
import yaml
from requests_toolbelt.multipart.encoder import MultipartEncoder

from . import consts as CONST

logger = logging.getLogger(__name__)
logger.debug("hello")
class Session():
    
    def __init__(self, user, passwd):
        
        self.session = requests.Session()
        self._request_id = None
        # self._base_url = "https://appinspect.splunk.com"
        
        # uri = "https://api.splunk.com/2.0/rest/login/splunk"
        
        try:
            response = self.session.get(CONST.SPLUNKBASE_AUTH, auth=(user, passwd))
            
        except:
            return None
        
        try:
            self.session.headers.update({'Authorization': "Bearer {}".format(response.json().get("data").get("token"))})
            
        except AttributeError:
            logger.error("Authentication Error -- check credentials")
            sys.exit(1)
        
        
    def submitApp(self, appLocation):
        
        app_name = os.path.basename(appLocation)
        with open(appLocation, "rb") as f:
            fields = (
            ('app_package', (app_name, f)),
            # ('included_tags', 'cloud'),
            # ('included_tags', 'self-service'),
            ('included_tags', 'private_app'),
            )
            payload = MultipartEncoder(fields=fields)
            headers = { "Content-Type": payload.content_type, "max-messages": "all" }
            
            try:
                response = self.session.post(CONST.AI_VALIDATE, data=payload, headers=headers)
            except:
                return False
            
            else:
                self._request_id = response.json().get("request_id")
                # print(response.json())
                # for rel in ['status', 'report']:
                #     self._links[rel] = [ x['href'] for x in response.json().get("links") if x['rel'] == rel][0]
        return True
    
    @property
    def report_url(self):
        
        try:
            return CONST.AI_REPORT.format(request_id=self._request_id)
        
        except KeyError:
            return None

    @property
    def report(self):
        try:
            return(self._get(self.report_url).json().get("reports")[0])
        except:
            return None
    
    @property
    def status_url(self):
        
        try:
            return CONST.AI_STATUS.format(request_id=self._request_id)
        
        except KeyError:
            return None
        
    def _get(self, url, **args):
        
        while True:
            try:
                response = self.session.get(url, **args)
            except Exception as e:
                print(repr(e), file=sys.stderr)
                time.sleep(1)
                continue
            else:
                break
            
        return response
    
    def _post(self, url, **args):
        
        while True:
            try:
                response = self.session.post(url, **args)
            except Exception as e:
                print(repr(e), file=sys.stderr)
                time.sleep(1)
                continue
            else:
                break
            
        return response
    
    @property
    def status(self):
        
        self._status = self._get(self.status_url)
        
        return (lambda x: x.json().get("status") if x.status_code == 200 else x.status_code)(self._status)
    
    @property
    def info(self):
        
        try:
            return self._status.json().get("info")
        except AttributeError:
            return None
        except:
            raise
        
    @property
    def skipped(self):
        
        try:
            return self.info.get("skipped")
        except AttributeError:
            return None
        except:
            raise
        
    @property
    def success(self):
        
        try:
            return self.info.get("success")
        except AttributeError:
            return None
        except:
            raise
        
    @property
    def manual_check(self):
        
        try:
            return self.info.get("manual_check")
        except AttributeError:
            return None
        except:
            raise
        
    @property
    def failure(self):
        
        try:
            return self.info.get("failure")
        except AttributeError:
            return None
        except:
            raise
        
    @property
    def warning(self):
        
        try:
            return self.info.get("warning")
        except AttributeError:
            return None
        except:
            raise
        
    @property
    def error(self):
        
        try:
            return self.info.get("error")
        except AttributeError:
            return None
        except:
            raise
        
    @property
    def not_applicable(self):
        
        try:
            return self.info.get("not_applicable")
        except AttributeError:
            return None
        except:
            raise

    def process(self, appList):
        for app in appList:
            
            while not self.submitApp(app):
                _logger.warning("Upload Failure -- Retrying")
                time.sleep(1)
            
            old_status = ""
            
            while True:
                
                status = self.status
                if status == "SUCCESS":
                    if old_status != "":
                        print("")
                        
                    print("PROCESSING COMPLETE")
                    break
                
                elif status != old_status:
                    if old_status != "":
                        print("")
                        
                    old_status = status
                    print(status, sep=' ', end=' ')
                else:
                    print(".", sep=' ', end='')
                    
                sys.stdout.flush()
                time.sleep(1)
            
            _report = self.report
            _app_name = _report.get("app_package_id")
            _app_version =_report.get("app_version")
            _groups = _report.get("groups")
            _logger = logging.getLogger(":".join([_app_name, _app_version]))

            # print(yaml.safe_dump(s._get(s.report_url).json(), allow_unicode=True, sort_keys=True))
            if self.failure == 0 and self.error == 0 and self.manual_check == 0:
                _logger.info("Application Successfully Vetted for SplunkCloud installation")
                for item in _groups[0].get("checks"):
                    if item["result"] not in ["success", "not_applicable"]:
                        _logger.debug("** {}".format(item["description"]))
                        for message in item["messages"]:
                            _logger.debug("\t==> {}".format(message["message"]))
                        print("")
            else:
                _logger.error("Application Failed App Vetting: failures={}, errors={}, manual_checks={}".format(self.failure, self.error, self.manual_check))
                for group in _groups:
                    for item in group.get("checks"):
                        if item["result"] in ["error", "failure", "manual_check"]:
                            _logger.error("** {} : {}".format(item["result"], item["description"]))
                            for message in item["messages"]:
                                _logger.error("\t==> {}".format(message["message"]))

if __name__ =="__main__":

    user = input("Provide your Splunkbase username: ")
    passwd = getpass()
    
    s = Session(user, passwd)
    app = [sys.argv[1]]
    # app = "/Users/mdoyle/Downloads/splunk-add-on-for-unix-and-linux_800.tgz"
    s.process(app)

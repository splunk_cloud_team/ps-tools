from __future__ import print_function
import threading
from threading import Thread
import logging
import logging.handlers
import os
import random
import sys

import lorem
from lorem.text import TextLorem


def logListenerConfig(debug=False, verbose=False, log_dir="log", debug_file="debug.log", error_file="error.log", output_file="output.log"):
    LOG_DIR = log_dir
    DEBUG_FILE = debug_file
    ERROR_FILE = error_file
    OUTPUT_FILE = output_file
    MAX_BYTES = 50000000
    BACKUP_COUNT = 4
    root = logging.getLogger()
    root.setLevel(logging.NOTSET)
    try:
        os.makedirs(LOG_DIR)
    except OSError as e:
        if e.errno != 17:
            raise
        else:
            pass
    
    # Formatters
    f_verbose = logging.Formatter('%(asctime)s %(levelname)8s [ %(name)s:%(funcName)s() ] file="%(filename)s", line_no="%(lineno)s", %(message)s', datefmt="%Y-%m-%dT%H:%M:%S%z")
    f_console = logging.Formatter('%(levelname)8s - %(message)s', datefmt="%Y-%m-%dT%H:%M:%S%z")
    f_file = logging.Formatter('%(asctime)15s %(levelname)8s [ %(name)s ] - %(message)s', datefmt="%Y-%m-%dT%H:%M:%S%z")
    
    # Handlers
    if debug:
        h_debug = logging.handlers.RotatingFileHandler(os.path.normpath(os.path.join(LOG_DIR, DEBUG_FILE)), mode="a", maxBytes=MAX_BYTES, backupCount=BACKUP_COUNT, delay=True)
    # else:
    #     h_debug = logging.FileHandler(os.path.normpath(os.path.join(LOG_DIR, "debug.log")), mode="w")
        
        h_debug.setFormatter(f_file)
        h_debug.setLevel(logging.DEBUG)
        root.addHandler(h_debug)
    
    h_console = logging.StreamHandler()
    h_console.setLevel(logging.INFO)
    
    if verbose:
        h_console.setFormatter(f_file)

    else:
        h_console.setFormatter(f_console)
    
    h_error = logging.handlers.RotatingFileHandler(os.path.normpath(os.path.join(LOG_DIR, ERROR_FILE)), mode="a", maxBytes=MAX_BYTES, backupCount=BACKUP_COUNT, delay=True)
    h_error.setFormatter(f_file)
    h_error.setLevel(logging.WARNING)
    
    h_info = logging.handlers.RotatingFileHandler(os.path.normpath(os.path.join(LOG_DIR, OUTPUT_FILE)), mode="a", maxBytes=MAX_BYTES, backupCount=BACKUP_COUNT, delay=True)
    h_info.setFormatter(f_file)
    h_info.setLevel(logging.INFO)
    
    # root.addHandler(h_console)
    root.addHandler(h_error)
    root.addHandler(h_info)

def main():
    levels = [logger.info, logger.error, logger.warning, logger.debug, logger.critical]
    message = TextLorem(psep=' ')
    while True:
        random.choice(levels)("[{}] - {}".format(threading.current_thread().name, message.paragraph()))
            
if __name__ == '__main__':
    logListenerConfig(debug=True, log_dir="fake_logs", verbose=True)
    logger = logging.getLogger(os.path.basename(sys.argv[0]))
    threads = []
    for num in range(1, 15):
        t = Thread(target=main, daemon=True)
        t.start()
        threads.append(t)

    for t in threads:
        t.join()
    # main()

for i in *.csv; do
    echo [${i%.csv}_kv]
    for f in `head -1 ${i} | sed "s/\"//g" | sed "s/,/ /g"`; do
        echo field.$f = string
        done
    echo ""
    echo \| inputlookup $i \| outputlookup ${i%.csv}
    echo ""
    done
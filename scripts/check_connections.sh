#!/bin/bash
STACK=stackname

# DOMAIN=<<splunkcloudgc.com OR splunkcloud.com>>

# DOMAIN=splunkcloudgc.com
DOMAIN=splunkcloud.com

# REGION=<<AWS REGION>>
REGION=us-east-1

# for govcloud and GCP the prefixes are "dotted"
# ES=es.
# ITSI=itsi.
# IDM=idm.

# for AWS commercial the prefixes are "dashed"
ES=es-
ITSI=itsi-
IDM=idm-

echo "Validating Connectivity from $(hostname)"
echo ""

IDX_IP_LIST=$(for i in inputs{1..15}.${STACK}.${DOMAIN}; do nslookup ${i} | grep Address: | grep -v "#53"; done | sort -u | awk '{print $2}')
SH_IP_LIST=$(nslookup ${STACK}.${DOMAIN} | grep Address: | grep -v "#53" | sort -u | awk '{print $2}')
ES_IP_LIST=$(nslookup ${ES}${STACK}.${DOMAIN} | grep Address: | grep -v "#53" | sort -u | awk '{print $2}')
ITSI_IP_LIST=$(nslookup ${ITSI}${STACK}.${DOMAIN} | grep Address: | grep -v "#53" | sort -u | awk '{print $2}')
IDM_IP_LIST=$(nslookup ${IDM}${STACK}.${DOMAIN} | grep Address: | grep -v "#53" | sort -u | awk '{print $2}')

echo "Checking Indexer Connectivity on port 9997..."
for ip in ${IDX_IP_LIST}; do
    echo "Q" | timeout 1 openssl s_client -connect ${ip}:9997 >/dev/null 2>&1 </dev/null && echo ${ip}:9997 SUCCESS || RESULT=$?; if [ "$RESULT" == "124" ]; then echo ${ip}:9997 FAILURE; else echo ${ip}:9997 SUCCESS; fi 
    done

echo ""
echo "Checking Public IP..."
    echo "My Public IP: $(curl https://checkip.amazonaws.com 2>/dev/null)"

echo ""
echo "Checking REST Connectivity to ${STACK}.${DOMAIN} on port 8089..."
for ip in ${SH_IP_LIST}; do
    echo "Q" | timeout 1 openssl s_client -connect ${ip}:8089 >/dev/null 2>&1 </dev/null && echo ${ip}:8089 SUCCESS || echo ${ip}:8089 FAILURE
    done

echo ""
echo "Checking REST Connectivity to ${ES}${STACK}.${DOMAIN} on port 8089..."
for ip in ${ES_IP_LIST}; do
    echo "Q" | timeout 1 openssl s_client -connect ${ip}:8089 >/dev/null 2>&1 </dev/null && echo ${ip}:8089 SUCCESS || echo ${ip}:8089 FAILURE
    done

echo ""
echo "Checking REST Connectivity to ${ITSI}${STACK}.${DOMAIN} on port 8089..."
for ip in ${ITSI_IP_LIST}; do
    echo "Q" | timeout 1 openssl s_client -connect ${ip}:8089 >/dev/null 2>&1 </dev/null && echo ${ip}:8089 SUCCESS || echo ${ip}:8089 FAILURE
    done

echo ""
echo "Checking REST Connectivity to ${IDM}${STACK}.${DOMAIN} on port 8089..."
for ip in ${IDM_IP_LIST}; do
    echo "Q" | timeout 1 openssl s_client -connect ${ip}:8089 >/dev/null 2>&1 </dev/null && echo ${ip}:8089 SUCCESS || echo ${ip}:8089 FAILURE
    done

echo ""
echo "Checking UI Connectivity on port 443..."
for ip in ${SH_IP_LIST} ${ES_IP_LIST} ${ITSI_IP_LIST} ${IDM_IP_LIST}; do
    echo "Q" | timeout 1 openssl s_client -connect ${ip}:443 >/dev/null 2>&1 </dev/null && echo ${ip}:443 SUCCESS || echo ${ip}:443 FAILURE
    done

echo ""
echo "Checking AWS Migration Connectivity on port 443..."
for dest in s3.${REGION}.amazonaws.com kms.${REGION}.amazonaws.com; do
    echo "Q" | timeout 1 openssl s_client -connect ${dest}:443 >/dev/null 2>&1 </dev/null && echo ${dest}:443 SUCCESS || echo ${dest}:443 FAILURE
    done

echo ""
echo "Checking GCP Migration Connectivity on port 443..."
for dest in storage.googleapis.com accounts.google.com oauth2.googleapis.com www.googleapis.com; do
    echo "Q" | timeout 1 openssl s_client -connect ${dest}:443 >/dev/null 2>&1 </dev/null && echo ${dest}:443 SUCCESS || echo ${dest}:443 FAILURE
    done

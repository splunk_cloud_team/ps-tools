for i in *.csv; do
    FIELDLIST = `head -1 $i | sed "s/\"//g"`
    echo [$i]; 
    echo filename = $i; 
    echo fields_list = $FIELDLIST; 
    echo case_sensitive_match = false; 
    echo ""; 
    echo [${i%.csv}]; 
    echo external_type = kvstore; 
    echo collection = ${i%.csv}_kv; 
    echo fields_list = $FIELDLIST; 
    echo case_sensitive_match = false; 
    echo ""; 
    done
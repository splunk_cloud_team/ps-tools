#!/bin/bash
VENV=pstools-venv
TOOLS=ps-tools
PYTHONPATH=$PWD/lib
alias pip=pip3
PATH=$PWD/bin:$PATH

if [ "${HOSTTYPE}" != "x86_64" ]; then
    echo "This script has not been tested on your architecture"
    if [ "${HOSTTYPE}x" == "arm64x" ]; then
        echo "There are known issues with ARM platforms related to python virtual environments"
        echo "Your system python must be updated directly"
        echo "You must run these commands yourself"
        echo "python3 -m ensurepip --upgrade"
        echo "python3 -m pip install --upgrade wheel"
        echo "python3 -m pip install -r requirements.txt"
        echo "You should be able to run export PYTHONPATH=\$PWD/lib"
        echo "python3 clean_apps/clean_v2.py --help"
        echo "If that works you are good-to-go"
        exit
    else
        echo "This might work, might not"
        read -p 'Do you want to try anyway? ' -n 1 choice
        echo ""
        if [ $choice != "y" ] && [ $choice != "Y" ]; then
            exit
        fi
    fi
fi

if [ "${VIRTUAL_ENV}x" != "x" ]; then
    echo This setup cannot be run from within a virtual env.
else

    if [ "${SPLUNK_HOME}x" != "x" ]; then
        PATH=$PATH:$SPLUNK_HOME/bin
        # PATH=$SPLUNK_HOME/bin:$PATH
    fi

    PYTHON=`which python3`

    if [ "${PYTHON}x" = "x" ]; then
        echo Cannot find python3 in the PATH
        echo If Splunk >= 8.0 is installed you can 'export SPLUNK_HOME=<<path to splunk>>' and run this setup again.
    else

        echo found python3: ${PYTHON}
        # if [ "${PYTHON}" == "${SPLUNK_HOME}/bin/python3" ]; then
        #     export LD_LIBRARY_PATH=${SPLUNK_HOME}/lib
        # fi
        (if [ ! -f ${VENV}/bin/activate ]; then
            echo creating virtual environment
            ${PYTHON} -m venv ${VENV} --copies
        fi) &&
        echo activating vitrual environment &&
        . ${VENV}/bin/activate &&
        python -m ensurepip --upgrade &&
        python -m pip install --upgrade pip &&
        pip3 install --upgrade wheel &&
        pip3 install -r requirements.txt &&
        (echo ""
        echo Your ps-tools virtual environment has been set up. Ensure you activate it each time you wish to run any of these utilities.
        echo To deactivate the virtual environment just type \'deactivate\' when your prompt shows \(${VENV}\)
        echo To activate again, type \'source $VENV/bin/activate\') || (echo ""
        echo Your ps-tools virtual environment setup has FAILED
        deactivate >/dev/null 2>&1)
    fi
fi
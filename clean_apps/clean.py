TRACE = True
import json
import logging
import os
import pickle
import posixpath
import re
import shutil
import statistics
import sys
import tarfile
from configparser import NoOptionError, NoSectionError
from datetime import datetime
from hashlib import md5, sha1
from xml.etree.ElementTree import ParseError

import lib_path

lib_path.make_lib_path()

from ps_common import *
from ps_common.appinspect import *
from ps_common.classes import ConfParser
from ps_common.fix_xml import fix_and_write_xml
from ps_common.logger import logListenerConfig
from ps_common.utils import isSplunkbaseApp

if __name__ == "__main__":
    try:
        log_dir=sys.argv[2]
    except IndexError:
        log_dir="log"
    logListenerConfig(debug=True, verbose=True, log_dir=log_dir)
logger = logging.getLogger(__name__)

from built_in_apps import *
from constraints import *
from slim import *
from templates import *

modified_apps = []
modified_local = []
inputs_conf = []
realtime_searches = []

def nullif(object, string):
    if object == string:
        return None
    else:
        return object

# TODO
def copy_default(src_path, tmp_path):
    pass

# TODO
@debug_wrapper(TRACE)
def update_version(old_version, type='minor'):
    version, suffix = re.search('([0-9.]+)(.*)$', old_version).groups()
    major, minor, patch = version.split(".")
    if type == 'major':
        major = str(int(major) + 1)
        minor = '0'
        patch = '0'
    elif type == 'minor':
        minor = str(int(minor) + 1)
        patch = '0'
    elif type == 'patch':
        patch = str(int(patch) + 1)
    else:
        return None
    return '.'.join([major, minor, patch]) + suffix

def check_for_loadjob(conf):
    pass

def check_for_savedsearch(conf):
    pass

@debug_wrapper(TRACE)
def process_custom(app, src_path, tmp_path):

    logger.debug(" --> ".join([src_path, tmp_path]))
    app_conf = t_app_conf.merged_with_file(os.path.join(src_path, "default", "app.conf"))
    # app_conf = t_app_conf.merged_with_file(os.path.join(tmp_path, "default", "app.conf"))
    app_conf['package']['id'] = app
    app_version = app_conf.get('launcher', 'version')
    if app_version.endswith("."):
        logger.warning("Incorrect Version format: {}".format(app_version))
        app_version = app_version + "0"
    try:
        major, minor, patch = app_version.split(".")
    except ValueError as e:
        logger.info("{}: {}: {}".format(src_path, app_version, repr(e)))
        _tmp = app_version.split(".")
        if len(_tmp) == 1:
            app_version = ".".join([_tmp[0], "0", "0"])
        elif len(_tmp) == 2:
            app_version = ".".join([_tmp[0], _tmp[1], "0"])
        else:
            app_version = ".".join(_tmp[0:3])
        logger.info("Adjusting Version Number to {}".format(app_version))
        
    app_conf['launcher']['version'] = app_version

    try: del app_conf['install']['install_source_checksum']
    except KeyError: pass

    app_conf['ui']['label'] = next(x for x in [nullif(app_conf['ui'].get('label'), ''), app_conf['package'].get('id')] if x is not None)
    logger.debug("app.conf:\n{}".format(app_conf))

    dest_file = os.path.join(tmp_path, "default", "app.conf")
    os.makedirs(os.path.dirname(dest_file), mode=DIR_MODE, exist_ok=True)
    with open(dest_file, "w") as f:
        app_conf.dump(f)
    os.chmod(dest_file, FILE_MODE)

    meta_conf = t_metadata_conf.merged_with_file(os.path.join(src_path, "metadata", "default.meta"))
    # remove all ownership from default.meta
    # add the owners in local.meta
    meta_conf = fix_default_owner(meta_conf)
    logger.debug("default.meta:\n{}".format(meta_conf))
    dest_file = os.path.join(tmp_path, "metadata", "default.meta")
    os.makedirs(os.path.dirname(dest_file), mode=DIR_MODE, exist_ok=True)
    with open(dest_file, "w") as f:
        meta_conf.dump(f)
    os.chmod(dest_file, FILE_MODE)

    return app_conf, meta_conf

@debug_wrapper(TRACE)
def process_builtin(src_path, tmp_path):
    logger.debug(" --> ".join([src_path, tmp_path]))
    app_conf = ConfParser(allow_no_value=True, defaults=None, strict=False, interpolation=None)
    app_conf.load(os.path.join(src_path, "default", "app.conf"))
    logger.debug("app.conf:\n{}".format(app_conf))
    meta_conf = ConfParser(allow_no_value=True, defaults=None, strict=False, interpolation=None)
    meta_conf.load(os.path.join(src_path, "metadata", "default.meta"))
    logger.debug("default.meta:\n{}".format(meta_conf))
    return app_conf, meta_conf

@debug_wrapper(TRACE)
def process_splunkbase(src_path, tmp_path):
    logger.debug(" --> ".join([src_path, tmp_path]))
    app_conf = ConfParser(allow_no_value=True, defaults=None, strict=False, interpolation=None)
    app_conf.load(os.path.join(src_path, "default", "app.conf"))
    logger.debug("app.conf:\n{}".format(app_conf))
    meta_conf = ConfParser(allow_no_value=True, defaults=None, strict=False, interpolation=None)
    meta_conf.load(os.path.join(src_path, "metadata", "default.meta"))
    logger.debug("default.meta:\n{}".format(meta_conf))
    return app_conf, meta_conf

@debug_wrapper(TRACE)
def create_index_transforms(filename):
    _tmp = ConfParser(allow_no_value=True, defaults=None, strict=False, interpolation=None)
    _tmp.load(filename)
    logger.error(_tmp.dumps())
    _transforms = []
    props = ConfParser(allow_no_value=True, defaults=None, strict=False, interpolation=None)
    transforms = ConfParser(allow_no_value=True, defaults=None, strict=False, interpolation=None)
    for section in _tmp.sections():
        # remove disallowed stazas
        if section.startswith("volume:"):
            logger.warning("Skipping staza [{}]".format(section))
            continue
        elif section in weird_indexes:
            logger.warning("Skipping staza [{}]".format(section))
            continue
        else:
            logger.error(section)
            if not section.startswith("spgi"):
                continue
            _section = section.replace(".", "_")
            if _section != section:
                _transform = 'idx_rewrite_{}_to_{}'.format(section, _section)
                transforms.add_section(_transform)
                _transforms.append(_transform)
                transforms[_transform]['SOURCE_KEY'] = "_MetaData:Index"
                transforms[_transform]['REGEX'] = "^(?i)({})$".format(section.replace(".", "\."))
                transforms[_transform]['DEST_KEY'] = "_MetaData:Index"
                transforms[_transform]['FORMAT'] = _section
    if len(_transforms) > 0:
        props.add_section('host::*')
        props['host::*']['TRANSFORMS-00-IDX_REWRITE'] = ", ".join(_transforms)
    else:
        props = None
        transforms = None

    return props, transforms

@debug_wrapper(TRACE)
def fix_indexes(filename):
    _tmp = ConfParser(allow_no_value=True, defaults=None, strict=False, interpolation=None)
    _tmp.load(filename)
    result = ConfParser(allow_no_value=True, defaults=None, strict=False, interpolation=None)
    for section in _tmp.sections():
        # remove disallowed stazas
        if section.startswith("volume:"):
            continue
        elif section in weird_indexes:
            continue
        else:
            result.add_section(section)
            for k, v in _tmp[section].items():
                if k == "homePath":
                    result[section][k] = posixpath.join("$SPLUNK_DB", section, "db")
                elif k == "coldPath":
                    result[section][k] = posixpath.join("$SPLUNK_DB", section, "colddb")
                elif k == "thawedPath":
                    result[section][k] = posixpath.join("$SPLUNK_DB", section, "thaweddb")
                elif k == "maxGlobalDataSizeMB":
                    result[section][k] = "0"
                elif k in allowed_in_indexes:
                    result[section][k] = v
                else:
                    continue
    return result

@debug_wrapper(TRACE)
def audit_savedsearches(filename):
    _tmp = ConfParser(allow_no_value=True, defaults=None, strict=False, interpolation=None)
    _tmp.load(filename)
    for section in _tmp.sections():
        realtime = False
        loadjob = False
        savedsearch = False
        dbxquery = False

        try:
            realtime = _tmp[section]["dispatch.earliest_time"].startswith("rt")
        except KeyError:
            try:
                realtime = _tmp[section]["dispatch.latest_time"].startswith("rt")
            except KeyError:
                pass

        try:
            loadjob = "loadjob" in re.split('\W+', _tmp[section]["search"])
        except KeyError:
            pass

        try:
            savedsearch = "savedsearch" in re.split('\W+', _tmp[section]["search"])
        except KeyError:
            pass

        try:
            dbxquery = "dbxquery" in re.split('\W+', _tmp[section]["search"])
        except KeyError:
            pass

        if realtime:
            logger.error("Realtime Saved Search '{}' found in '{}'".format(section, filename))
        if loadjob:
            logger.warning("Loadjob Command Present '{}' found in '{}'".format(section, filename))
        if savedsearch:
            logger.warning("Savedsearch Command Present '{}' found in '{}'".format(section, filename))
        if dbxquery:
            logger.error("dbxquery Command Present '{}' found in '{}'".format(section, filename))

@debug_wrapper(TRACE)
def fix_default_owner(confObj):
    owners = []
    for section in [ x for x in confObj.sections() if not x in t_metadata_conf.sections()]:
        for k, v in [(x, confObj[section][x]) for x in confObj[section].keys() if x == 'owner']:
            owners.append((section, k, v))
    for item in owners:
        logger.warning("Removing ownership from default.meta {}".format(item[0]))
        del confObj[item[0]][item[1]]
    return confObj

@debug_wrapper(TRACE)
def hash_changed(filename, filehash, hash_table):
    # find the filename in the table or return True
    try:
        old_hash = hash_table[filename]
    except KeyError:
        logger.debug("First Time seeing {}".format(filename))
        return True
    # compare the hashes return old==new
    if old_hash == filehash:
        return False
    else:
        return True

@debug_wrapper(TRACE)
def get_changed_files(path):
    hashes = []
    # load the saved hashes
    try:
        with open("hashes.pickle", "rb") as f:
            old_hashes = pickle.load(f)
    except FileNotFoundError:
        old_hashes = []
    # get the current hashes
    for root, dirs, files in os.walk(path):
        for file in files:
            relpath = os.path.relpath(os.path.join(root,file), os.path.curdir)
            with open(relpath, "rb") as f:
                data = f.read()
            hashes.append((relpath, md5(data).hexdigest()))

    return list(set(old_hashes) - set(hashes)) + list(set(hashes) - set(old_hashes))

@debug_wrapper(TRACE)
def process_file(file, src_file, dest_file, context):
    if file.startswith(".") or file.startwith("_"):
        logger.debug("Skipping disallowed file {}".format(file))
        return
    elif file == "indexes.conf":
        # insure the stazas meet cloud standards
        indexes_conf = fix_indexes(src_file)
        # props_conf, transforms_conf = create_index_transforms(src_file)
        # if props_conf is not None:
        #     logger.error(props_conf.dumps())
        #     logger.error(transforms_conf.dumps())
        logger.debug(indexes_conf.dumps())
        if indexes_conf.sections() != []:
            os.makedirs(os.path.dirname(dest_file), mode=DIR_MODE, exist_ok=True)
            with open(dest_file, "w") as f:
                    indexes_conf.dump(f)
            os.chmod(dest_file, FILE_MODE)
    elif file == "datamodels.conf":
        this_conf = t_any_conf.diff_with_file(src_file)
        for section in this_conf.sections():
            try:
                accel = this_conf.get(section, 'acceleration')
            except NoOptionError:
                pass
            else:
                if accel:
                    logger.error("Accelerated Datamodel discovered in {} - move the setting to local".format(src_file))
        if this_conf.sections() != []:
            os.makedirs(os.path.dirname(dest_file), mode=DIR_MODE, exist_ok=True)
            # copy the file as-is to the temp directory
            shutil.copyfile(src_file, dest_file)
            os.chmod(dest_file, FILE_MODE)

    elif file.endswith(".conf"):
        if file == "inputs.conf":
            logger.error("MANUAL CHECK: {}".format(src_file))
        elif file == "savedsearches.conf":
            # check for realtime searches
            logger.debug("Validating savedsearches.conf")
            audit_savedsearches(src_file)
        elif file in disallowed_files:
            logger.debug("Skipping disallowed file {}".format(file))
            return

        os.makedirs(os.path.dirname(dest_file), mode=DIR_MODE, exist_ok=True)
        # copy the file as-is to the temp directory
        shutil.copyfile(src_file, dest_file)
        os.chmod(dest_file, FILE_MODE)
    elif file.endswith(".xml"):
        os.makedirs(os.path.dirname(dest_file), mode=DIR_MODE, exist_ok=True)
        # fix xml syntax errors
        fix_and_write_xml(src_file, dest_file)
        os.chmod(dest_file, FILE_MODE)
    elif file.endswith(".json"):
        with open(src_file, "r", encoding='utf-8-sig') as f:
            try: json.load(f)
            except json.decoder.JSONDecodeError as e: logger.error("{}: Invalid JSON: {}".format(src_file, repr(e)))
            else:
                os.makedirs(os.path.dirname(dest_file), mode=DIR_MODE, exist_ok=True)
                # copy the file as-is to the temp directory
                shutil.copyfile(src_file, dest_file)
                os.chmod(dest_file, FILE_MODE)
    elif file.endswith(".html"):
        os.makedirs(os.path.dirname(dest_file), mode=DIR_MODE, exist_ok=True)
        # copy the file as-is to the temp directory
        shutil.copyfile(src_file, dest_file)
        os.chmod(dest_file, FILE_MODE)
    else:
        logger.debug("Skipping disallowed file {}".format(file_key))

@debug_wrapper(TRACE)
def get_file_list(root_path, src_path, dest_path):

    _tmp_src = []
    _tmp_dest = []

    for root, _dirs, files in os.walk(src_path):
        _tmp_src.extend([os.path.relpath(os.path.join(root, x), root_path) for x in files])

    for root, _dirs, files in os.walk(dest_path):
        _tmp_dest.extend([os.path.relpath(os.path.join(root, x), root_path) for x in files])

    _new = list(set(_tmp_src) - set(_tmp_dest))
    _del = list(set(_tmp_dest) - set(_tmp_src))

    return _tmp_src, _new, _del

@debug_wrapper(TRACE)
def main(*args, **kwargs):
    path = os.path.realpath(sys.argv[1])
    # load the saved hashes
    try:
        with open(os.path.join(path, "hashes.pickle"), "rb") as f:
            hash_table = pickle.load(f)
    except FileNotFoundError:
        hash_table = {}

    # get the list of app ids by listing the directory
    for app in [x for x in os.listdir(path) if os.path.isdir(os.path.join(os.path.realpath(path), x)) and not (x.endswith(".tmp") or x.startswith("100-whisper") or x.startswith("100-cloudworks") or x.startswith("075-cloudworks"))]:
        if app in warning_apps:
            logger.error("{} discovered".format(app))
        src_path = os.path.join(os.path.realpath(path), app)

        # check to see if it is a splunkbase app and add it to the splunkbase app list to process later
        if not app in builtin_apps and not isSplunkbaseApp(app):
            flavor = "custom"
            logger.debug("Processing {} as a custom app".format(app))
            tmp_path = src_path + ".custom.tmp"
            sources, adds, deletes = get_file_list(path, src_path, tmp_path)
            app_conf, meta_conf = process_custom(app, src_path, tmp_path)

            for file in deletes:
                logger.warning(f"{app}: File {file} removed from source. Deleting corresponding dest file.")
                os.unlink(os.path.join(path, file))

            for root, dirs, files in os.walk(os.path.join(src_path, "default")):
                for file in [x for x in files if x != "app.conf"]:
                    if file.startswith(".") or file.startswith("_"):
                        continue
                    src_file = os.path.join(root, file)
                    dest_file = src_file.replace(src_path, tmp_path)
                    file_key = os.path.relpath(src_file, path)
                    # save the hash
                    with open(src_file, "rb") as f:
                        data = f.read()
                    file_hash = sha1(data).hexdigest()

                    if not hash_changed(file_key, file_hash, hash_table):
                        logger.debug("File has not changed: {} -- skipping".format(file_key))
                        continue
                    else:
                        # mark the app as modified so we can update the version later
                        modified_apps.append((app, tmp_path))
                        hash_table[file_key] = file_hash
                        logger.debug("Processing file {}".format(file))
                    if file == "indexes.conf":
                        # insure the stazas meet cloud standards
                        indexes_conf = fix_indexes(src_file)
                        # props_conf, transforms_conf = create_index_transforms(src_file)
                        # if props_conf is not None:
                        #     logger.error(props_conf.dumps())
                        #     logger.error(transforms_conf.dumps())
                        logger.debug(indexes_conf.dumps())
                        os.makedirs(os.path.dirname(dest_file), mode=DIR_MODE, exist_ok=True)
                        with open(dest_file, "w") as f:
                                indexes_conf.dump(f)
                        os.chmod(dest_file, FILE_MODE)
                    elif file == "datamodels.conf":
                        this_conf = t_any_conf.diff_with_file(src_file)
                        for section in this_conf.sections():
                            try:
                                accel = this_conf.get(section, 'acceleration')
                            except NoOptionError:
                                pass
                            else:
                                if accel:
                                    logger.error("Accelerated Datamodel discovered in {} - move the setting to local".format(src_file))
                        os.makedirs(os.path.dirname(dest_file), mode=DIR_MODE, exist_ok=True)
                        # copy the file as-is to the temp directory
                        shutil.copyfile(src_file, dest_file)
                        os.chmod(dest_file, FILE_MODE)
                    elif file.endswith(".conf"):
                        if file == "inputs.conf":
                            logger.error("MANUAL CHECK: {}".format(src_file))
                        elif file == "savedsearches.conf":
                            # check for realtime searches
                            logger.debug("Validating savedsearches.conf")
                            audit_savedsearches(src_file)
                        elif file in disallowed_files:
                            logger.debug("Skipping disallowed file {}".format(file_key))
                            continue

                        os.makedirs(os.path.dirname(dest_file), mode=DIR_MODE, exist_ok=True)
                        # copy the file as-is to the temp directory
                        shutil.copyfile(src_file, dest_file)
                        os.chmod(dest_file, FILE_MODE)
                    elif file.endswith(".xml"):
                        os.makedirs(os.path.dirname(dest_file), mode=DIR_MODE, exist_ok=True)
                        # fix xml syntax errors
                        try:
                            fix_and_write_xml(src_file, dest_file)
                        except ParseError as e:
                            logger.error(f"{src_file}: {repr(e)}")
                            continue
                        os.chmod(dest_file, FILE_MODE)
                    elif file.endswith(".json"):
                        with open(src_file, "r", encoding='utf-8-sig') as f:
                            try: json.load(f)
                            except json.decoder.JSONDecodeError as e: logger.error("{}: Invalid JSON: {}".format(src_file, repr(e)))
                            else:
                                os.makedirs(os.path.dirname(dest_file), mode=DIR_MODE, exist_ok=True)
                                # copy the file as-is to the temp directory
                                shutil.copyfile(src_file, dest_file)
                                os.chmod(dest_file, FILE_MODE)
                    elif file.endswith(".html"):
                        os.makedirs(os.path.dirname(dest_file), mode=DIR_MODE, exist_ok=True)
                        # copy the file as-is to the temp directory
                        shutil.copyfile(src_file, dest_file)
                        os.chmod(dest_file, FILE_MODE)
                    else:
                        logger.debug("Skipping disallowed file {}".format(file_key))

            # copy the rest of the "allowed" directories
            for this_dir in [x for x in os.listdir(src_path) if os.path.isdir(os.path.join(src_path, x)) and x in allowed_app_dirs]:
                logger.debug("Processing {}".format(this_dir))
                # do not recurse the lookups directory
                for root, dirs, files in os.walk(os.path.join(src_path, this_dir)):
                    if this_dir == "lookups" and not root.endswith("lookups"):
                        logger.warning("Not recusing the lookups directory")
                        continue
                    for file in files:
                        if file.endswith('.pyc') or file.endswith('.pyo') or file.startswith(".") or file.startswith("_"):
                            continue
                        src_file = os.path.join(root, file)
                        dest_file = src_file.replace(src_path, tmp_path)
                        file_key = os.path.relpath(src_file, path)
                        # save the hash
                        try:
                            with open(src_file, "rb") as f:
                                data = f.read()
                        except FileNotFoundError as e:
                            logger.error("{}: {}".format(src_file, repr(e)))
                            continue
                        file_hash = sha1(data).hexdigest()
                        if not hash_changed(file_key, file_hash, hash_table):
                            logger.debug("File has not changed: {} -- skipping".format(file_key))
                            continue
                        else:
                            # mark the app as modified so we can update the version later
                            modified_apps.append((app, tmp_path))
                            hash_table[file_key] = file_hash
                        logger.debug("Processing file {}".format(file))
                        os.makedirs(os.path.dirname(dest_file), mode=DIR_MODE, exist_ok=True)
                        shutil.copyfile(src_file, dest_file)
                        # if this_dir != "bin":
                        #     os.chmod(dest_file, FILE_MODE)
                        os.chmod(dest_file, FILE_MODE)

        elif app in builtin_apps:
            logger.debug("Processing {} as a built-in app".format(app))
            tmp_path = src_path + ".builtin.tmp"
            flavor = "builtin"
            app_conf, meta_conf = process_builtin(src_path, tmp_path)
            # copy selected files from the lookups directory
        else:
            logger.debug("Processing {} as a Splunkbase app".format(app))
            tmp_path = src_path + ".splunkbase.tmp"
            flavor = "splunkbase"
            app_conf, meta_conf = process_splunkbase(src_path, tmp_path)
            # Look in local.meta or local transforms.conf
            # for lookups defined there?
            # copy selected files from the lookups directory

        # put the stuff we removed from default.meta back into local.meta where it belongs
        local_meta = meta_conf.diff_with_file(os.path.join(src_path, "metadata", "default.meta"))
        if os.path.isfile(os.path.join(src_path, "metadata", "local.meta")):
            local_meta = local_meta.merge_with_file(os.path.join(src_path, "metadata", "local.meta"))
        try: del local_meta['app/install/install_source_checksum']
        except KeyError: pass
        try: del local_meta['app/install/install_source_local_checksum']
        except KeyError: pass

        file_path = os.path.join(tmp_path, "metadata", "local.meta")
        if len(local_meta.sections()) > 0:
            os.makedirs(os.path.dirname(file_path), mode=DIR_MODE, exist_ok=True)
            with open(file_path, "w") as f:
                local_meta.dump(f)
            os.chmod(file_path, FILE_MODE)

        default_conf = ConfParser(allow_no_value=True, defaults=None, strict=False, interpolation=None)
        local_file_count = 0
        # indexes.conf is a special case and should be merged to default
        # don't forget to update modified apps for indexes.conf changes
        for root, dirs, files in os.walk(os.path.join(src_path, "local")):
            for file in files:
                src_file = os.path.join(root, file)
                dest_file = src_file.replace(src_path, tmp_path)
                default_file = src_file.replace(os.sep + "local" + os.sep, os.sep + "default" + os.sep)
                file_key = os.path.relpath(src_file, path)
                # save the hash
                with open(src_file, "rb") as f:
                    data = f.read()
                file_hash = sha1(data).hexdigest()
                if not hash_changed(file_key, file_hash, hash_table):
                    logger.debug("File has not changed: {} -- skipping".format(file_key))
                    continue
                else:
                    modified_local.append((app, tmp_path))
                    hash_table[file_key] = file_hash
                    logger.debug("Processing file {}".format(file_key))

                if file.startswith(".") or file.startswith("_"):
                    continue
                elif file == "indexes.conf":
                    # insure the stazas meet cloud standards
                    indexes_conf = fix_indexes(src_file)
                    # props_conf, transforms_conf = create_index_transforms(src_file)
                    # if props_conf is not None:
                    #     logger.error(props_conf.dumps())
                    #     logger.error(transforms_conf.dumps())
                    logger.debug(indexes_conf.dumps())
                    os.makedirs(os.path.dirname(dest_file), mode=DIR_MODE, exist_ok=True)
                    with open(dest_file, "w") as f:
                            indexes_conf.dump(f)
                    os.chmod(dest_file, FILE_MODE)
                elif file.endswith(".conf"):
                    try: default_conf.load(default_file)
                    except Exception as e:
                        logger.warning("{}: {}".format(default_file, repr(e)))
                        continue
                    else:
                        try:
                            this_conf = default_conf.diff_with_file(src_file)
                        except Exception as e:
                            logger.error("{}: {}".format(src_file, repr(e)))
                            continue
                        if this_conf != {}:
                            os.makedirs(os.path.dirname(dest_file), mode=DIR_MODE, exist_ok=True)
                            with open(dest_file, "w") as f:
                                this_conf.dump(f)
                            os.chmod(dest_file, FILE_MODE)
                elif file.endswith(".xml"):
                    os.makedirs(os.path.dirname(dest_file), mode=DIR_MODE, exist_ok=True)
                    # fix xml syntax errors
                    try:
                        fix_and_write_xml(src_file, dest_file)
                    except ParseError as e:
                        logger.error(f"{src_file}: {repr(e)}")
                        continue
                    os.chmod(dest_file, FILE_MODE)
                elif file.endswith(".json"):
                    os.makedirs(os.path.dirname(dest_file), mode=DIR_MODE, exist_ok=True)
                    shutil.copyfile(src_file, dest_file)
                    os.chmod(dest_file, FILE_MODE)
                elif file.endswith(".html"):
                    os.makedirs(os.path.dirname(dest_file), mode=DIR_MODE, exist_ok=True)
                    shutil.copyfile(src_file, dest_file)
                    os.chmod(dest_file, FILE_MODE)
                else:
                    continue
                local_file_count += 1

        if local_file_count > 0:
            src_file = os.path.join(src_path, "local", "app.conf")
            dest_file = src_file.replace(src_path, tmp_path)
            if flavor == "builtin":
                this_conf = t_builtin_app_conf.merged_with_file(src_file)
            else:
                this_conf = t_shc_app_conf.merged_with_file(src_file)
            os.makedirs(os.path.dirname(dest_file), mode=DIR_MODE, exist_ok=True)
            with open(dest_file, "w") as f:
                this_conf.dump(f)
            os.chmod(dest_file, FILE_MODE)

    for app, tmp_path in set(modified_apps):
        manifest = dict(t_slim_manifest)
        app_conf_path = os.path.join(tmp_path, "default", "app.conf")
        # load the new default app.conf
        app_conf = ConfParser(allow_no_value=True, defaults=None, strict=False, interpolation=None)
        app_conf.load(app_conf_path)
        app_version = app_conf.get('launcher', 'version')
        if not app_version.endswith("cloud"):
            app_version = app_version + "cloud"
            logger.info("New cloud version: {} v{}".format(app, app_version))
        else:
            app_version = update_version(app_version, type='patch')
            logger.info("{} modified. Incrementing version v{}".format(app, app_version))
        app_conf['launcher']['version'] = app_version
        manifest['info']['title'] = app_conf.get('ui', 'label')
        manifest['info']['id']['name'] = app
        manifest['info']['id']['version'] = app_version
        manifest['info']['author'][0]['name'] = app_conf.get('launcher', 'author')
        manifest['info']['description'] = app_conf.get('launcher', 'description')
        if app.startswith("A00-") or True:
            manifest['targetWorkloads'] = list(set(manifest['targetWorkloads'] + ["_indexers"]))
        with open(app_conf_path, "w") as f:
            app_conf.dump(f)
        with open(os.path.join(tmp_path, "app.manifest"), "w") as f:
            json.dump(manifest, f, indent=2)

        # create app tarballs to vet
        try:
            tar = "-".join([tmp_path, app_conf.get('launcher', 'version')]) + ".tgz"
        except NoSectionError:
            pass
            # tar = "-".join([tmp_path, "local_only" + ".tgz"])
        else:
            with tarfile.open(tar, "w:gz") as f:
                for dir in allowed_app_dirs + ["default", os.path.join("metadata", "default.meta")]:
                    try:
                        f.add(os.path.join(tmp_path, dir), arcname=os.path.join(app, dir), recursive=True)
                    except FileNotFoundError:
                        continue

    for app, tmp_path in set(modified_local):
        # create local tarball
        tar = "-".join([tmp_path, "local_only" + ".tgz"])
        with tarfile.open(tar, "w:gz") as f:
            for dir in ["local", os.path.join("metadata", "local.meta")]:
                try:
                    f.add(os.path.join(tmp_path, dir), arcname=os.path.join(app, dir), recursive=True)
                except FileNotFoundError:
                    continue

    with open(os.path.join(path, "hashes.pickle"), "wb") as f:
        pickle.dump(hash_table, f)

    logger.info("All automated fixes completed. Please search dashboards for references to renamed apps, especially loadjob or savedsearch commands.")

    print("{} done.".format(sys.argv[0]))

if __name__ == "__main__":
    main()

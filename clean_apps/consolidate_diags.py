import datetime
import json
import logging
import os
import pickle
import posixpath
import re
import shutil
import statistics
import sys
import tarfile
from configparser import NoOptionError, NoSectionError, ParsingError
from hashlib import md5, sha1
from urllib.parse import quote, quote_plus
from xml.etree.ElementTree import ParseError
from lib_path import make_lib_path
make_lib_path()

import numpy as np
from ps_common import *
from ps_common.appinspect import *
from ps_common.classes import ConfParser
# from ps_common.decorators import debug_wrapper
from ps_common.fix_xml import fix_and_write_xml
from ps_common.logger import logListenerConfig
from ps_common.utils import isSplunkbaseApp
from ps_common.appinspect.classes import Session as AI, Liverpool

from constraints import *
from consts import GBL_DEBUG, GBL_TRACE
from utils import *

def get_diag_list(path):
    return [ {"name": x, "path": os.path.join(path, x), "data": re_diag_name.match(x).groupdict()} for x in os.listdir(path) if os.path.isdir(os.path.join(path, x)) and re_diag_name.match(x) ]

def run(root_path, **kwargs):

    for item in get_diag_list(args.root_path):
        name = item["name"]
        path = item["path"]
        host = item["data"]["host"]
        apps_src = os.path.join(item["path"], "etc", "apps")
        try:
            if os.path.isdir(apps_src):
                item["apps"] = app_list(apps_src)
        except FileNotFoundError:
            pass

        slave_apps_src = os.path.join(item["path"], "etc", "slave-apps")
        try:
            if os.path.isdir(apps_src):
                item["slave-apps"] = app_list(slave_apps_src)
        except FileNotFoundError:
            pass

        for app in item["apps"]:
            full_path = os.path.join(apps_src, app)
            dest_path = full_path.replace(name, "diags-by-app-host")
            for entry in os.listdir(full_path):
                src = os.path.join(full_path, host, entry)
                # add host under the app dfolder
                dest = src.replace(name, "diags-by-app-host")
                # dest = "{}_{}".format(src, host).replace(name, "diag-consolidated")
                os.makedirs(os.path.dirname(dest), exist_ok=True)
                try:
                    os.symlink(src, dest, target_is_directory=os.path.isdir(src))
                except FileExistsError:
                    pass
            # try:
            #     count = len(os.listdir(dest_path))
            #     os.rename(dest_path, "{}_{}".format(dest_path, count))
            # except FileNotFoundError:
            #     pass


                # for file in files:
                #     _tmp = os.path.join(root, file).split(os.sep)
                #     _index = _tmp.index(app)
                #     _tmp[_index + 1] = _tmp[_index + 1] + "_{}".format(host)
                #     dest_file = os.sep.join(_tmp).replace(item["name"], "diag-consolidated")
                #     shutil.copytree()
            

if __name__ == "__main__":
    import argparse
    cli = argparse.ArgumentParser(prog="consolidate_diags", description="Used to consolidate multiple diag directories into a single tree")
    cli.add_argument("root_path", metavar="PATH_TO_DIAGS_TO_CONSOLIDATE", help="Extract multiple diags to a single directory and supply that directory path")
    cli.add_argument("--debug", action="store_true")

    args = cli.parse_args()
    args.root_path = os.path.realpath(args.root_path)

    run(**args.__dict__)
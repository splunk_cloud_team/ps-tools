import json
import logging
import os
import pickle
import posixpath
import re
import shutil
import statistics
import sys
import tarfile
from configparser import NoOptionError, NoSectionError
from getpass import getpass
from hashlib import md5, sha1
from threading import Thread

import lib_path

lib_path.make_lib_path()

from ps_common.appinspect.classes import Session as AI
from ps_common.classes import ConfParser
from ps_common.fix_xml import fix_and_write_xml
from ps_common.logger import logListenerConfig
from ps_common.utils import isSplunkbaseApp

try:
    log_dir = sys.argv[2]
except IndexError:
    log_dir = "log"

logListenerConfig(debug=True, verbose=True, log_dir=log_dir)
logger = logging.getLogger(__name__)

custom_app = re.compile('\.custom\.tmp-')

if __name__ == "__main__":
    path = os.path.realpath(sys.argv[1])
    
    user = input("Provide your Splunkbase username: ")
    passwd = getpass()
    
    s = AI(user, passwd)

    # get the list of app ids by listing the directory
    for app in [x for x in os.listdir(path) if os.path.isfile(os.path.join(os.path.realpath(path), x)) and x.endswith(".tgz") and not x.endswith("local_only.tgz")]:
        if not custom_app.search(app):
            continue
        else:
            logger.info("Processing {}".format(app))
            src_path = os.path.join(os.path.realpath(path), app)
            s.process([src_path])

    logger.info("All automated fixes completed. Please search dashboards for references to renamed apps, especially loadjob or savedsearch commands.")
    
    print("{} done.".format(sys.argv[0]))

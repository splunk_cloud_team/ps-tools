import re
from consts import GBL_DEBUG, GBL_TRACE

re_excluded_patterns = re.compile(r"^splunk_instrumentation$|^splunk_monitoring_console$|^splunkclouduf$|^learned$|^cloud_administration$|^tos&|^075-cloudworks|^100-cloudworks|^100-whisper|^100-s2|\.tmp$|^\.")
re_excluded_bin_patterns = re.compile(r"\.pyc$|\.pyo$|__pycache__")
re_bin_file_patterns = re.compile(r"\.py$|\.sh$|\.ps1")
re_junk_file_patterns = re.compile(r"^\.|^_|^~|\.tmp$|\.swp$")
re_lookup_index_file = re.compile(r"\.index\.alive$")

# validate diag name and extract host
re_diag_name = re.compile(r"(?P<diag>diag-(?P<host>\S+)-(?P<date>\d{4}-\d{2}-\d{2})_(?P<time>\d{2}-\d{2}-\d{2}))$")

# for checking SPL in searches
re_spl_sep = re.compile(r"\s*(?<!\\)[\|]\s*")

re_allowed_in_authentication = re.compile(r"^userToRoleMap_|^roleMap_.*")

re_spl_loadjob = re.compile(r"loadjob\s+savedsearch\s*=[\s\"]*(?P<owner>[^:]+):(?P<app_title>[^:]+):(?P<search_title>[^\"\|\]\s]+)")
re_spl_index = re.compile(r"index\s*=\s*(\"[^\"]\"|\w+)[\s\]]")
re_spl_lookup = re.compile(r"^lookup[^\]]*")
re_spl_outputlookup = re.compile(r"^outputlookup[^\]]*")
re_spl_inputlookup = re.compile(r"^inputlookup[^\]]*")
re_spl_collect = re.compile(r"^collect[^\]]*")
re_spl_dbx = re.compile(r"^(?:filter)?dbx[^\]]{,80}")
re_spl_sendmail = re.compile(r"^sendmail[^\]]*")
re_spl_script = re.compile(r"^script[^\]]*")
re_spl_sendalert = re.compile(r"^sendalert[^\]]*")
re_spl_xs = re.compile(r"^xs[^\]]*")
re_spl_ldap = re.compile(r"^ldap[^\]]*")
re_spl_rest = re.compile(r"^rest[^\]]*")
re_spl_nslookup = re.compile(r"^nslookup[^\]]*")

re_props_class = re.compile(r"^SEDCMD|^TRANSFORMS|^RULESET|^MORE_THAN|^LESS_THAN.*")
re_props_lookup = re.compile(r"^LOOKUP.*")

gdi_props = [
	'_actions',
	'ADD_EXTRA_TIME_FIELDS',
	'ANNOTATE_PUNCT',
	'BREAK_ONLY_BEFORE',
	'BREAK_ONLY_BEFORE_DATE',
	'CHARSET',
	'CHECK_FOR_HEADER',
	'CHECK_METHOD',
	'DATETIME_CONFIG',
	'DETERMINE_TIMESTAMP_DATE_WITH_SYSTEM_TIME',
	'EVENT_BREAKER',
	'EVENT_BREAKER_ENABLE',
	'FIELD_DELIMITER',
	'FIELD_HEADER_REGEX',
	'FIELD_NAMES',
	'FIELD_QUOTE',
	'HEADER_FIELD_ACCEPTABLE_SPECIAL_CHARACTERS',
	'HEADER_FIELD_DELIMITER',
	'HEADER_FIELD_LINE_NUMBER',
	'HEADER_FIELD_QUOTE',
	'HEADER_MODE',
	'INDEXED_EXTRACTIONS',
	'JSON_TRIM_BRACES_IN_ARRAY_NAMES',
	'LB_CHUNK_BREAKER',
	'LB_CHUNK_BREAKER_TRUNCATE',
	'LEARN_MODEL',
	'LEARN_SOURCETYPE',
	'LINE_BREAKER',
	'LINE_BREAKER_LOOKBEHIND',
	'MAX_DAYS_AGO',
	'MAX_DAYS_HENCE',
	'MAX_DIFF_SECS_AGO',
	'MAX_DIFF_SECS_HENCE',
	'MAX_EVENTS',
	'MAX_TIMESTAMP_LOOKAHEAD',
	'METRICS_PROTOCOL',
	'METRIC-SCHEMA-TRANSFORMS',
	'MISSING_VALUE_REGEX',
	'MUST_BREAK_AFTER',
	'MUST_NOT_BREAK_AFTER',
	'MUST_NOT_BREAK_BEFORE',
	'NO_BINARY_CHECK',
	'PREAMBLE_REGEX',
	'PREFIX_SOURCETYPE',
	'SEGMENTATION',
	'SHOULD_LINEMERGE',
	'STATSD-DIM-TRANSFORMS',
	'STATSD_EMIT_SINGLE_MEASUREMENT_FORMAT',
	'TIME_FORMAT',
	'TIME_PREFIX',
	'TIMESTAMP_FIELDS',
	'TRUNCATE',
	'TZ',
	'TZ_ALIAS',
	'detect_trailing_nulls',
	'force_local_processing',
	'given_type',
	'initCrcLength',
	'invalid_cause',
	'is_valid',
	'maxDist',
	'priority',
	'pulldown_type',
	'sourcetype',
	'termFrequencyWeightedDist',
	'unarchive_cmd',
	'unarchive_sourcetype'
	]

# gdi_props = [
# 	"priority",
# 	"CHARSET",
# 	"TRUNCATE",
# 	"LINE_BREAKER",
# 	"LINE_BREAKER_LOOKBEHIND",
# 	"SHOULD_LINEMERGE",
# 	"BREAK_ONLY_BEFORE_DATE",
# 	"BREAK_ONLY_BEFORE",
# 	"MUST_BREAK_AFTER",
# 	"MUST_NOT_BREAK_AFTER",
# 	"MUST_NOT_BREAK_BEFORE",
# 	"MAX_EVENTS",
# 	"DATETIME_CONFIG",
# 	"TIME_PREFIX",
# 	"MAX_TIMESTAMP_LOOKAHEAD",
# 	"TIME_FORMAT",
# 	"DETERMINE_TIMESTAMP_DATE_WITH_SYSTEM_TIME",
# 	"TZ",
# 	"TZ_ALIAS",
# 	"MAX_DAYS_AGO",
# 	"MAX_DAYS_HENCE",
# 	"MAX_DIFF_SECS_AGO",
# 	"MAX_DIFF_SECS_HENCE",
# 	"ADD_EXTRA_TIME_FIELDS",
# 	"CHECK_FOR_HEADER",
# 	"SEGMENTATION",
# 	"ANNOTATE_PUNCT"
# 	]

warning_apps = [
    "Splunk_SA_ExtremeSearch",
	"splunk_app_db_connect",
	"SA-ldapsearch",
	"lookup_editor"
	]

ldap_commands = [
	"ldapsearch",
	"ldapfilter",
	"ldapgroup",
	"ldapfetch",
	]

dbx_commands = [
	"dbxquery",
	"dbxoutput",
	"dbxlookup",
	"filterdbxsourcetype"
	]

warning_commands = [
	"xsaggregateautoregression",
	"xsaggregatecorrelation",
	"xsaggregatelinearregression",
	"xsaggregatenonlinearregression",
	"xsaggregatespearmancorrelation",
	"xsapplyautoregression",
	"xsapplylinearregression",
	"xsapplynonlinearregression",
	"xscloneconcept",
	"xsclonecontext",
	"xsconverttov4",
	"xscreateddcontext",
	"xscreateudcontext",
	"xscreateconcept",
	"xsdeletecontext",
	"xsdeleteconcept",
	"xsdiscovertrend",
	"xsdisplayconcept",
	"xsdisplayconceptattributes",
	"xsdisplaycontext",
	"xsdisplaycontextattributes",
	"xsdisplaywhere",
	"xsfindbestconcept",
	"xsfindmembership",
	"xsgetcompatibility",
	"xsgetcontextfields",
	"xsgetdistance",
	"xsgetwherecix",
	"xslistapps",
	"xslistcontexts",
	"xslistconcepts",
	"xslistuom",
	"xsmergecontexts",
	"xsnormalize",
	"xsperformautoregression",
	"xsperformcorrelation",
	"xsperformlinearregression",
	"xsperformnonlinearregression",
	"xsperformspearmancorrelation",
	"xspreautoregress",
	"xsprecorrelate",
	"xspredict",
	"xsprequadregress",
	"xspreregress",
	"xsprespearmancorrelate",
	"xsrenameconcept",
	"xsrenamecontext",
	"xssearchcontexts",
	"xsupdateconcept",
	"xsupdateddcontext",
	"xsupdateudcontext",
	"xssetcontextfields",
	"xswhere",
	"sendmail",
	"sendalert",
	"script",
	"dnsLookup"
	]

allowed_app_dirs = [
    "appserver",
    "bin",
    "lib",
    "readme",
    "README",
    "static",
    "lookups"
    ]

splunk_conf_files = [
	"alert_actions.conf",
	"app.conf",
	"audit.conf",
	"authentication.conf",
	"authorize.conf",
	"bookmarks.conf",
	"checklist.conf",
	"collections.conf",
	"commands.conf",
	"datamodels.conf",
	"datatypesbnf.conf",
	"default.meta.conf",
	"default-mode.conf",
	"deployment.conf",
	"deploymentclient.conf",
	"distsearch.conf",
	"eventdiscoverer.conf",
	"event_renderers.conf",
	"eventtypes.conf",
	"global-banner.conf",
	"fields.conf",
	"health.conf",
	"indexes.conf",
	"inputs.conf",
	"instance.cfg.conf",
	"limits.conf",
	"literals.conf",
	"macros.conf",
	"messages.conf",
	"metric_alerts.conf",
	"metric_rollups.conf",
	"multikv.conf",
	"outputs.conf",
	"passwords.conf",
	"procmon-filters.conf",
	"props.conf",
	"pubsub.conf",
	"restmap.conf",
	"savedsearches.conf",
	"searchbnf.conf",
	"segmenters.conf",
	"server.conf",
	"serverclass.conf",
	"serverclass.seed.xml.conf",
	"setup.xml.conf",
	"source-classifier.conf",
	"sourcetypes.conf",
	"splunk-launch.conf",
	"tags.conf",
	"telemetry.conf",
	"times.conf",
	"transactiontypes.conf",
	"transforms.conf",
	"ui-prefs.conf",
	"ui-tour.conf",
	"user-prefs.conf",
	"user-seed.conf",
	"viewstates.conf",
	"visualizations.conf",
	"web.conf",
	"wmi.conf",
	"workflow_actions.conf",
	"workload_policy.conf",
	"workload_pools.conf",
	"workload_rules.conf"
    ]

deprecated_savedsearch_options = [
	"vsid"
	]

disallowed_alert_action_items = [
	"mailserver"
	]

disallowed_files = [
    "server.conf",
    "web.conf",
    "limits.conf",
    "passwords.conf",
    "health.conf",
    "viewstates.conf",
    "eventgen.conf",
    "perfmon.conf",
    "deploymentclient.conf",
    "outputs.conf",
    "deployment.conf",
    "default-mode.conf",
	"ui-tour.conf",
	"ui-prefs.conf",
	"telemetry.conf",
	"kerberos_client.conf",
	"akka_application.conf"
    ]

allowed_in_distsearch = [
    "replicationWhitelist",
    "replicationAllowlist",
    "replicationBlacklist",
    "replicationDenylist"
	]

allowed_in_indexes = [
    "homePath",
    "coldPath",
    "thawedPath",
    "frozenTimePeriodInSecs",
    "datatype",
	"disabled"
    ]

# allowed_in_indexes = [
#     "frozenTimePeriodInSecs",
#     "datatype"
#     ]

weird_indexes = [
	"_audit",
	"_configaudit",
	"_configtracker",
	"_internal",
	"_introspection",
	"_metrics",
	"_metrics_rollup",
	"_telemetry",
	"_thefishbucket",
	"history",
	"main",
	"provider-family:hadoop",
	"splunklogger",
	"summary",
	"volume:_splunk_summaries",
]

default_sections = [
    "default",
    "global",
    "##--NO_SECTION--##"
]

conflict_index_names = [
	"audit",
	"defaultdb",
	"fishbucket",
	"historydb",
	"kvstore",
	"persistentstorage",
	"summarydb"
]

disallowed_roles = [
	"role_admin",
	"role_sc_admin",
	"role_power",
	"role_user",
	"role_ess_admin",
	"role_ess_analyst",
	"role_ess_user",
	"role_itoa_user",
	"role_itoa_analyst",
	"role_itoa_team_admin",
	"role_itoa_admin",
	"role_app-installer",
	"role_index-manager",
	"role_internal_ops_admin",
	"role_internal_monitoring",
	"tokens_auth"
	]

disallowed_role_imports = [
	"admin",
	"sc_admin",
	"splunk-system-role",
	"app-installer",
	"index-manager",
	"internal_ops_admin",
	"internal_monitoring"
	]

disallowed_capabilities = [
	"admin_all_objects",
	"change_authentication"
	]

disallowed_authentication_conf_stazas = [

	]
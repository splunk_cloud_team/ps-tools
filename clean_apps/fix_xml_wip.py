import logging
import os
import re
import xml.etree.ElementTree as ET
from io import StringIO, BytesIO
from xml.dom import minidom

class VersionError(Exception):
    pass

def fix_version(element):
  if element.tag in ['dashboard', 'form']:
    _ver = element.get('version', "1.0")
    try:
        major, minor = _ver.split(".")
    except ValueError:
        major = _ver.split(".")[0]
        try:
            minor = _ver.split(".")[1]
        except IndexError:
            minor = 0
    except AttributeError:
        major = "1"
        minor = "0"
    if major == "1" and int(minor) < 1:
        element.set('version', '1.1')
        return 1
  return 0

def process(root, app_name):
  repairs = 0
  repairs = repairs + fix_version(root)
  repairs = repairs + process_afterLabel(root)
  repairs = repairs + process_beforeLabel(root)
  repairs = repairs + process_height(root)
  repairs = repairs + process_populatingSearch(root)
  repairs = repairs + process_searchName(root)
  repairs = repairs + process_searchPostProcess(root)
  repairs = repairs + process_searchString(root)
  repairs = repairs + process_searchTemplate(root)
  repairs = repairs + process_seed(root)
  repairs = repairs + process_refresh_auto_interval(root)
  repairs = repairs + comment_options(root)
  repairs = repairs + fix_earliest(root)
  repairs = repairs + fix_latest(root)
  repairs = repairs + comment_modules(root)
  
  return repairs

def process_afterLabel(root):
  count = 0
  for element in root.findall(".//option[@name='afterLabel']/.."):
    count += 1
    _option = element.find("option[@name='afterLabel']")
    option = ET.Element('option', attrib={'name': 'unit'})

    element.insert(list(element).index(_option), ET.Comment(ET.tostring(_option, encoding='unicode').strip()))
    element.insert(list(element).index(_option), option)
    element.remove(_option)

  return count

def process_beforeLabel(root):
  count = 0
  for element in root.findall(".//option[@name='beforeLabel']/.."):
    count += 1
    _option = element.find("option[@name='beforeLabel']")
    option = ET.Element('option', attrib={'name': 'unit'})
    position = ET.Element('option', attrib={'name': 'unitPosition'})
    position.text = 'before'

    element.insert(list(element).index(_option), ET.Comment(ET.tostring(_option, encoding='unicode').strip()))
    element.insert(list(element).index(_option), position)
    element.insert(list(element).index(_option), option)
    element.remove(_option)
  return count

def process_refresh_auto_interval(root):
  count = 0
  for element in root.findall(".//option[@name='refresh.auto.interval']/.."):
    count += 1
    _option = element.find("option[@name='refresh.auto.interval']")
    _search = element.find('search')

    refresh = ET.SubElement(_search, 'refresh')
    refresh.text = str(_option.text)

    element.insert(list(element).index(_option), ET.Comment(ET.tostring(_option, encoding='unicode').strip()))
    element.remove(_option)
  return count 

def process_height(root):
  count = 0
  for element in root.findall(".//option[@name='height']/.."):
    _option = element.find("option[@name='height']")
    if str(_option.text).endswith("px"):
      count += 1
      option = ET.Element('option', attrib={'name': 'height'})
      option.text = str(_option.text).rstrip("px")

      element.insert(list(element).index(_option), ET.Comment(ET.tostring(_option, encoding='unicode').strip()))
      element.insert(list(element).index(_option), option)
      element.remove(_option)
  return count

def process_populatingSearch(root):
  count = 0
  for element in root.findall('.//populatingSearch/..'):
    count += 1
    _search = element.find('populatingSearch')
    try:
      _earliest = _search.attrib['earliest']
    except KeyError:
      _earliest = None
    try:
      _latest = _search.attrib['latest']
    except KeyError:
      _latest = None
    _value = _search.attrib['fieldForValue']
    _label = _search.attrib['fieldForLabel']
    search = ET.Element('search')
    query = ET.SubElement(search, 'query')
    query.text = _search.text
    if _earliest is not None:
      earliest = ET.SubElement(search, 'earliest')
      earliest.text = str(_earliest)
    if _latest is not None:
      latest = ET.SubElement(search, 'latest')
      latest.text = str(_latest)
    if not element.find('fieldForValue'):
      value = ET.SubElement(element, 'fieldForValue')
      value.text = str(_value)
    if not element.find('fieldForLabel'):
      label = ET.SubElement(element, 'fieldForLabel')
      label.text = str(_label)

    element.insert(list(element).index(_search), ET.Comment(ET.tostring(_search, encoding='unicode').strip()))
    element.insert(list(element).index(_search), search)
    element.remove(_search)
  return count

def process_searchName(root):
  count = 0
  for element in root.findall('.//searchName/..'):
    count += 1
    _search = element.find('searchName')
    search = ET.Element('search', attrib={'ref': _search.text})
    element.insert(list(element).index(_search), ET.Comment(ET.tostring(_search, encoding='unicode').strip()))
    element.insert(list(element).index(_search), search)
    element.remove(_search)
  return count

def process_searchPostProcess(root):
  count = 0
  for element in root.findall(".//searchPostProcess/.."):
    count += 1
    _search = element.find('searchPostProcess')
    search = ET.Element('search', attrib={'base': 'base_search'})
    query = ET.SubElement(search, 'query')
    query.text = _search.text

    element.insert(list(element).index(_search), ET.Comment(ET.tostring(_search, encoding='unicode').strip()))
    element.insert(list(element).index(_search), search)
    element.remove(_search)
  return count

def process_searchString(root):
  count = 0
  for element in root.findall(".//searchString/.."):
    count += 1
    _search = element.find('searchString')
    _earliest = element.find('earliestTime')
    _latest = element.find('latestTime')
    search = ET.Element('search')
    query = ET.SubElement(search, 'query')
    query.text = _search.text
    if _earliest is not None:
      earliest = ET.SubElement(search, 'earliest')
      earliest.text = _earliest.text
      element.insert(list(element).index(_search), ET.Comment(ET.tostring(_earliest, encoding='unicode').strip()))
      element.remove(_earliest)

    if _latest is not None:
      latest = ET.SubElement(search, 'latest')
      latest.text = _latest.text
      element.insert(list(element).index(_search), ET.Comment(ET.tostring(_latest, encoding='unicode').strip()))
      element.remove(_latest)

    element.insert(list(element).index(_search), ET.Comment(ET.tostring(_search, encoding='unicode').strip()))
    element.insert(list(element).index(_search), search)
    element.remove(_search)
  return count

def process_searchTemplate(root):
  count = 0
  for element in root.findall(".//searchTemplate/.."):
    count += 1
    _search = element.find('searchTemplate')
    _earliest = element.find('earliestTime')
    _latest = element.find('latestTime')
    search = ET.Element('search', attrib={'id': 'base_search'})
    query = ET.SubElement(search, 'query')
    query.text = _search.text
    if _earliest is not None:
      earliest = ET.SubElement(search, 'earliest')
      earliest.text = _earliest.text
      element.insert(list(element).index(_search), ET.Comment(ET.tostring(_earliest, encoding='unicode').strip()))
      element.remove(_earliest)

    if _latest is not None:
      latest = ET.SubElement(search, 'latest')
      latest.text = _latest.text
      element.insert(list(element).index(_search), ET.Comment(ET.tostring(_latest, encoding='unicode').strip()))
      element.remove(_latest)

    element.insert(list(element).index(_search), ET.Comment(ET.tostring(_search, encoding='unicode').strip()))
    element.insert(list(element).index(_search), search)
    element.remove(_search)
  return count

def process_seed(root):
  count = 0
  for element in root.findall('.//seed/..'):
    count += 1
    _seed = element.find('seed')
    initial = ET.Element('initialValue')
    initial.text = str(_seed.text)
    element.insert(list(element).index(_seed), ET.Comment(ET.tostring(_seed, encoding='unicode').strip()))
    element.insert(list(element).index(_seed), initial)
    element.remove(_seed)
  return count

def comment_options(root):
  count = 0
  for element in root.findall('.//option/..'):
    for name in [
      'linkView',
      'linkFields',
      'linkSearch',
      'classField',
      'additionalClass',
      'displayRowNumbers']:
      for _option in element.findall("option[@name='{}']".format(name)):
        count += 1
        element.insert(list(element).index(_option), ET.Comment(ET.tostring(_option, encoding='unicode').strip()))
        element.remove(_option)
  return count

def comment_modules(root):
  count = 0
  for element in root.findall('.//module/..'):
      for _module in element.findall("module"):
        count += 1
        element.insert(list(element).index(_module), ET.Comment(ET.tostring(_module, encoding='unicode').strip()))
        element.remove(_module)
  return count

def fix_earliest(root):
  count = 0
  for element in root.findall('.//earliestTime/..'):
    count += 1
    _earliest = element.find('earliestTime')
    if _earliest is not None:
      earliest = ET.SubElement(element, 'earliest')
      earliest.text = _earliest.text
      element.insert(list(element).index(_earliest), ET.Comment(ET.tostring(_earliest, encoding='unicode').strip()))

      element.remove(_earliest)
  return count

def fix_latest(root):
  count = 0
  for element in root.findall('.//latestTime/..'):
    count += 1
    _latest = element.find('latestTime')
    if _latest is not None:
      latest = ET.SubElement(element, 'latest')
      latest.text = _latest.text
      element.insert(list(element).index(_latest), ET.Comment(ET.tostring(_latest, encoding='unicode').strip()))
      element.remove(_latest)
  return count

if __name__ == "__main__":

    _in = BytesIO(b'''<?xml version='1.0' encoding='utf-8'?>
<dashboard>
  <label>test</label>
  <row>
    <table>
      <searchName>RSA_Failed_Login_Attempt</searchName>
      <title>RSA</title>
    </table>
    <table>
      <searchName>User_lockout</searchName>
      <title>Lockouts</title>
      <option name="drilldown">row</option>
    </table>
  </row>
  <row>
    <event>
      <searchName>Domain_Admin_logon_Non_DC</searchName>
      <title>Domain_Admin</title>
    </event>
    <table>
      <searchName>McAfee_Detections</searchName>
      <title>mcafee</title>
    </table>
  </row>
  <row>
    <table>
      <searchName>Websense-Blocks</searchName>
      <title>trend</title>
      <option name="drilldown">all</option>
    </table>
    <chart>
      <searchName>nac</searchName>
      <title>NAC</title>
    </chart>
  </row>
  <row>
    <table>
      <searchName>Summary_test</searchName>
      <title>email</title>
    </table>
  </row>
  <row>
    <single>
      <searchString>index=vpn | transaction fields=Username maxspan=2m maxpause=1m | search "Primary authentication" AND "Session started" | stats count | rangemap field=count low=0-25 elevated=200-1000 default=severe</searchString>
      <title>testing</title>
      <earliestTime>-3d@d</earliestTime>
      <latestTime>now</latestTime>
      <option name="count">10</option>
      <option name="displayRowNumbers">true</option>
      <option name="afterLabel">%</option>
    </single>
  </row>
</dashboard>''')
    # _in.seek(0)
#    _src = ET.parse("/home/nfs/customer_diags/cigna/diags/diag-cilsplkp0201-2022-10-05_15-11-22/etc/apps/app_harmony/default/data/ui/views/gisg_automation_dashboard_clone.xml")
    _src = ET.parse("test.xml")
    _root = _src.getroot()
    process(_root, "test_app")
    print(ET.tostring(_root, encoding='unicode', xml_declaration=False))
    exit()


    f = BytesIO(minidom.parseString(re.sub(r'\n', '', ET.tostring(_root, encoding='unicode', xml_declaration=False))).toprettyxml(indent=2*" ", encoding='utf-8'))

    # f = BytesIO()
    # _tmp.write(f, encoding='utf-8', xml_declaration=False)
    f.seek(0)
    for line in f.readlines():
        print(str(line, encoding='utf-8'))

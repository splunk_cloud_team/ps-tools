class base_app():

    def __init__(self, *args, **kwargs):
        self._store = {
            "confs": {
                "default": [],
                "local": []
            },
            "lookups": [],
            "metadata": {
                "default": None,
                "local": None
            }
        }

    def load(self, path):
        pass

    def dump(self):
        pass

    def vet(self):
        pass

    @property
    def version(self):
        pass

    @property
    def is_valid(self):
        pass

class builtin_app(base_app):

    def __init__(self, *args, **kwargs):
        super(builtin_app).__init__(args, kwargs)

    def process(self):
        pass

class splunkbase_app(base_app):

    def __init__(self, *args, **kwargs):
        super(builtin_app).__init__(args, kwargs)

class custom_app(base_app):
    def __init__(self, *args, **kwargs):
        super(builtin_app).__init__(args, kwargs)

class user_private():
    def __init__(self, *args, **kwargs):
        super(builtin_app).__init__(args, kwargs)
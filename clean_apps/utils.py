DEBUG = False
TRACE = False
import functools
import inspect
import json
import logging
import os
import posixpath
import re
import shutil
import tarfile
import time
import traceback
import xml.etree.ElementTree as ET
from configparser import NoOptionError, NoSectionError, ParsingError
from datetime import datetime
from hashlib import md5, sha1
from xml.etree.ElementTree import ParseError
# from lib.fixups.indexes import filter_indexes

from ps_common.classes import ConfParser
from ps_common.fix_xml import _process_new
from fix_xml_wip import process

from constraints import *
from consts import *
from templates import t_app_conf, t_savedsearches_conf, t_metadata_conf

from consts import GBL_DEBUG, GBL_TRACE
##################################################
# EXCEPTIONS
##################################################
class AppDisabledError(Exception):
    pass
class BadConfError(Exception):
    pass
class EmptyAppError(Exception):
    pass
class InvalidCronError(Exception):
    pass
class InvalidIndexError(Exception):
    pass
class NoChanges(Exception):
    pass
class SetupXMLError(Exception):
    pass
class SymbolicLinkError(Exception):
    pass
class ConfParsingError(Exception):
    pass

##################################################
# DEBUG WRAPPER FUNCTION W/TRACE
##################################################
def debug_wrap(ENABLED=False, TRACE=False):
    def debug_decorator(func):

        @functools.wraps(func)
        def trace_debug(*args, **kwargs):
            logger = logging.getLogger(func.__name__)
            frame = inspect.stack(1)
            args_repr = [repr(a) for a in args]
            kwargs_repr = [f"{k}={v!r}" for k, v in kwargs.items()]
            signature = ", ".join(args_repr + kwargs_repr)

            try:
                logger.debug("Calling {}({}) from file='{}', lineno={}, code_context='{}'".format(
                    func.__name__, signature, frame[1].filename, frame[1].lineno, frame[1].code_context[0].strip()))
            finally:
                del frame

            start_time = time.perf_counter()

            try:
                _result = func(*args, **kwargs)
            except Exception as e:
                logger.exception("{}: Exception: {}".format(
                    func.__name__, repr(e)))
                raise e

            end_time = time.perf_counter()
            run_time = end_time - start_time
            logger.debug("Finished {!r} in {:.4f} secs".format(
                func.__name__, run_time))

            logger.debug("{!r} returned {!r}".format(
                func.__name__, repr(_result)))
            return _result

        @functools.wraps(func)
        def debug(*args, **kwargs):
            logger = logging.getLogger(func.__name__)
            _result = func(*args, **kwargs)
            logger.debug("RETURNED: {!r}".format(repr(_result)))
            return _result

        if not ENABLED:
            return func
        elif TRACE:
            return trace_debug
        else:
            return debug

    return debug_decorator

##################################################
# APP LIST
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def app_list(path):
    return [ x for x in sorted(list_dirs(path), key=lambda z: z.lower()) if not re_excluded_patterns.search(x) ]

##################################################
# AUDIT CONF CHANGES
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def audit_conf_changes(orig, new, app, file):
    _result = []
    _merged = orig.merged_with(new)

    for section in _merged.sections():
        try:
            orig[section]
        except KeyError:
            _result.append("ADD_STANZA - {}:{}:{}".format(app, file, section))
        else:
            try:
                new[section]
            except KeyError:
                _result.append("REMOVE_STANZA - {}:{}:{}".format(app, file, section))

        # for option in [ k for k, v in _merged.items(section) if not (file.endswith(".meta") and k in ["modtime", "version"])]:
        for option, _ in _merged.items(section):
            try:
                _orig_value = orig.safe_get(section, option)
            except (NoSectionError, NoOptionError):
                _orig_value = "NOT_PRESENT"
            except AttributeError:
                raise BadConfError((app, section, option, file)) from None

            try:
                _new_value = new.safe_get(section, option)
            except (NoSectionError, NoOptionError):
                _new_value = "REMOVED"
            except AttributeError:
                raise BadConfError((app, section, option, file)) from None

            if _new_value != _orig_value:
                if _new_value == "REMOVED":
                    change = "REMOVE_SETTING"
                elif _orig_value == "NOT_PRESENT":
                    change = "ADD_SETTING"
                else:
                    change = "CHANGE_SETTING"
                _result.append("{} - {}:{}:{}:{}:{} ==> {}".format(change, app, file, section, option, _orig_value, _new_value))

    return _result

##################################################
# CLEAN APP.CONF
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def clean_app_conf(app_path, default, local, flavor=None):

    # fix up the default app.conf so it meets the requirements
    d_app_conf = t_app_conf.merged_with(default)

    # force the id to match the app directory name
    d_app_conf["package"]["id"] = os.path.basename(app_path)
    # d_app_conf["id"]["name"] = os.path.basename(app_path)

    # fix the label
    d_app_conf['ui']['label'] = next(x for x in [nullif(d_app_conf['ui'].get('label'), ''), d_app_conf['package'].get('id')] if x is not None)

    # check the version format and correct it if required
    app_version = d_app_conf.get("launcher", "version").strip(".")
    _len = len(app_version.split("."))
    if _len == 1:
        app_version = app_version + ".0.0"
    elif _len == 2:
        app_version = app_version + ".0"
    else:
        app_version = ".".join(app_version.split(".")[0:3])

    d_app_conf["launcher"]["version"] = app_version
    # d_app_conf["id"]["version"] = app_version

    # remove garbage
    if flavor in ['custom']:
        try:
            del d_app_conf["install"]["install_source_checksum"]
        except KeyError:
            pass

    remove_empty_stanzas(d_app_conf)

    m_app_conf = d_app_conf.merged_with(local)

    # add the required shclustering stanza for deployer
    if flavor in ['custom', 'splunkbase']:
        m_app_conf["shclustering"] = {"deployer_push_mode": "full"}
    else:
        m_app_conf["shclustering"] = {"deployer_push_mode": "local_only"}

    # remove the is_configured from local because the app will need to be reconfigured in Splunk Cloud
    # if m_app_conf.has_option("install", "is_configured"):
    #     del m_app_conf["install"]["is_configured"]

    remove_empty_stanzas(m_app_conf)

    m_app_conf = d_app_conf.merged_with(m_app_conf)

    return d_app_conf, m_app_conf, app_version

##################################################
# CLEAN_META_CONF
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def clean_meta_conf(d_meta_conf, m_meta_conf, flavor):
    garbage_stanzas = re.compile(r"^viewstates|^app/|^server/|^outputs/|^passwords/|^indexes/|^inputs/")
    garbage_attributes = ["modtime", "version"]

    if flavor == 'custom':
        metas = [d_meta_conf, m_meta_conf]
    else:
        metas = [m_meta_conf]

    for meta in metas:
        for section in meta.sections():
            if garbage_stanzas.search(section):
                del meta[section]
                continue

            for attribute, value in meta.items(section):
                if attribute in garbage_attributes:
                    del meta[section][attribute]
                # elif attribute == "owner" and section != "app":
                elif attribute == "owner":
                    if value in ["none", "nobody", "admin", "sc_admin", "splunk-system-user"]:
                        del meta[section][attribute]
                elif attribute == "import" and value == "__globals__":
                    del meta[section][attribute]

        # remove any empty stanzas
        remove_empty_stanzas(meta)

    return d_meta_conf, m_meta_conf

##################################################
# CLEAN PROPS.CONF
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def clean_props_conf(confObj):

    # garbage_stanzas = re.compile(r"^(?<=.)")
    garbage_stanzas = re.compile(r"^default$")
    garbage_attributes = ["disabled"]

    for section in confObj.sections():
        if garbage_stanzas.search(section):
            del confObj[section]
            continue
        for attribute, value in confObj.items(section):
            if attribute in garbage_attributes:
                del confObj[section][attribute]
                continue

    # remove any empty stanzas
    remove_empty_stanzas(confObj)

##################################################
# COMPUTE CHECKSUMS
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def compute_checksums(app, orig_path, new_path):
    local = r"{}|{}".format(os.path.join(app, "local"), os.path.join(app, "metadata", "local.meta"))
    # ORIGINAL APP HASH
    o_hash = md5()
    # CLEANED APP DEFAULT HASH
    d_hash = md5()
    # CLEANED APP LOCAL HASH
    l_hash = md5()
    # CLEANED APP FULL HASH
    f_hash = md5()
    # COMPUTE ORIGINAL (SOURCE) APP CHECKSUM
    for root, _, files in os.walk(orig_path, topdown=True):
        for file in [ os.path.join(root, x) for x in sorted(files, key=lambda x: os.path.join(root.upper(), x.upper())) if x not in [".md5"]]:
            with open(file, "rb") as f:
                for byte_block in iter(lambda: f.read(4096),b""):
                        o_hash.update(byte_block)

    # COMPUTE THE DEFAULT AND LOCAL AND FULL (CLEANED) APP CHECKSUMS
    for root, _, files in os.walk(new_path, topdown=True):
        for file in [ os.path.join(root, x) for x in sorted(files, key=lambda x: os.path.join(root.upper(), x.upper())) if x not in [".md5"]]:
            with open(file, "rb") as f:
                for byte_block in iter(lambda: f.read(4096),b""):
                    f_hash.update(byte_block)
                    if re.search(local, file):
                        l_hash.update(byte_block)
                    else:
                        d_hash.update(byte_block)
    return o_hash.hexdigest(), d_hash.hexdigest(), l_hash.hexdigest(), f_hash.hexdigest()

##################################################
# COPY FILE
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def copy_file(src, dest, MODE, copy_empty=False):
    if os.path.islink(src):
        raise SymbolicLinkError("{!r} is a symbolic link".format(src))
    if not copy_empty and os.path.getsize(src) == 0:
        # skip copying empty files
        return
    os.makedirs(os.path.dirname(dest), mode=DIR_MODE, exist_ok=True)
    shutil.copyfile(src, dest)
    os.chmod(dest, MODE)

# @debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
# def create_app_checksum(path):
#     sha1_hash = sha1()
#     for root, _, files in os.walk(path, topdown=True):
#         for file in [ os.path.join(root, x) for x in sorted(files, key=lambda x: os.path.join(root.upper(), x.upper())) if x not in [".sha1"]]:
#             with open(file, "rb") as f:
#                 for byte_block in iter(lambda: f.read(4096),b""):
#                         sha1_hash.update(byte_block)

#     return sha1_hash.hexdigest()

# def create_tar(path, flavor):
#     pass

##################################################
# FILTER DISTSEARCH
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def filter_distsearch(confObj):
    '''takes a ConfigParser object filters the stanzas, and returns the modified ConfParser object.'''
    for section in [x for x in confObj.sections() if x not in allowed_in_distsearch]:
        del confObj[section]

    for old, new in [("replicationWhitelist", "replicationAllowlist"), ("replicationBlacklist", "replicationDenylist")]:
        try:
            confObj[old]
        except KeyError:
            pass
        else:
            try:
                confObj[new]
            except KeyError:
                confObj[new] = confObj[old]
                del confObj[old]
    return confObj

##################################################
# FILTER INDEXES
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def filter_indexes(conf):

    for section in conf.sections():
        if section.startswith("volume:"):
            del conf[section]
        else:
            if section in weird_indexes:
                try:
                    del conf[section]
                except KeyError:
                    pass
            elif section in default_sections:
                # _tmp['global_defaults'].update(dict(confObj[section]))
                # fix_attributes(_tmp, section)
                del conf[section]
            else:
                if not re.match(r"[a-z0-9][a-z0-9_-]*$", section) or section in conflict_index_names:
                    raise InvalidIndexError("Invalid index name {} -- correct on-prem before migration".format(section)) from None
                conf[section]["homePath"] = posixpath.join("$SPLUNK_DB", section, "db")
                conf[section]["coldPath"] = posixpath.join("$SPLUNK_DB", section, "colddb")
                conf[section]["thawedPath"] = posixpath.join("$SPLUNK_DB", section, "thaweddb")
                for attribute in [ x for x, y in conf.items(section) if x not in allowed_in_indexes]:
                    del conf[section][attribute]
                # if default_frozentime is not None:
                #     conf[section]['frozenTimePeriodInSecs'] = conf[section].get('frozenTimePeriodInSecs', default_frozentime)

    return conf

##################################################
# GET ALL CONFS
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def get_all_confs(src_path):

    try:
        default = [x for x in os.listdir(os.path.join(src_path, "default")) if x.endswith(".conf") and x != "app.conf"]
    except FileNotFoundError:
        default = []
    try:
        local = [x for x in os.listdir(os.path.join(src_path, "local")) if x.endswith(".conf") and x != "app.conf"]
    except FileNotFoundError:
        local = []

    return [ x for x in set(default).union(set(local)) if not re_junk_file_patterns.search(x) ]

##################################################
# GET CONF OBJS
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def get_conf_objs(app_path, file):
    """
    Effectively loads the default an local versions of a specific conf file
    and returns ConfParser objects for each, plus a merged conf.

    Args:
        app_path (str): path to the app to load the file
        file (str): the name of the conf file (i.e. "savedsearches.conf" or "props.conf")

    Raises:
        ConfParsingError: Raised if there are parsing errors while loading the files

    Returns:
        tuple: d_conf (ConfParser), l_conf (ConfParser), m_conf (ConfParser)
    """
    if file != "meta.conf":
        d_conf_path = os.path.join(app_path, "default", file)
        l_conf_path = os.path.join(app_path, "local", file)
    else:
        d_conf_path = os.path.join(app_path, "metadata", "default.meta")
        l_conf_path = os.path.join(app_path, "metadata", "local.meta")

    try:
        d_conf = ConfParser().load(d_conf_path)
    except ParsingError as e:
        raise ConfParsingError("{!r}: {}".format(d_conf_path, repr(e))) from None

    try:
        l_conf = ConfParser().load(l_conf_path)
    except ParsingError as e:
        raise ConfParsingError("{!r}: {}".format(l_conf_path, repr(e))) from None

    d_conf = ConfParser().load(d_conf_path)

    m_conf = d_conf.merged_with(l_conf)

    return d_conf, l_conf, m_conf

@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def inspect_cron():
    pass
    # TODO

##################################################
# LIST DIRS
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def list_dirs(path):
    return [ x for x in os.listdir(path) if os.path.isdir(os.path.join(path, x)) ]

@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def merge_to_default(d_conf, l_conf):
    d_conf = d_conf.merged_with(l_conf)
    l_conf = None
    return d_conf, l_conf

@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def merge_to_local(d_conf, l_conf):
    l_conf = d_conf.merged_with(l_conf)
    d_conf = None
    return d_conf, l_conf

##################################################
# NULLIF
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def nullif(object, string):
    if object == string:
        return None
    else:
        return object

##################################################
# PARSE CRON SCHEDULE
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def parse_cron_schedule(cron_string):

    try:
        _m, _h, _d, _mo, _dw = cron_string.split()
    except ValueError as e:
        raise InvalidCronError("{!r} is not a valid Splunk cron schedule string".format(cron_string))

    min = _m.split("/")
    hr  = _h.split("/")
    day = _d.split("/")
    mon = _mo.split("/")
    dw  = _dw.split("/")

    return min, hr, day, mon, dw

##################################################
# PROCESS ALERT_ACTIONS.CONF
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def process_alert_actions_conf(d_conf, m_conf, flavor):

    for section in m_conf.sections():
        for item, value in m_conf.items(section):
            if item in disallowed_alert_action_items:
                try:
                    del m_conf[section][item]
                except KeyError:
                    pass
                try:
                    del d_conf[section][item]
                except KeyError:
                    pass

    return d_conf, m_conf

##################################################
# PROCESS APP
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def process_app(app, app_path, in_path, diag, flavor, merge=None, details=None, gdi_path=None, lookup_path=None, st_path=None, fwd_path=None, to_vet_path=None):
    """Processes (cleans) one app according to the "flavor" and "merge" values.

    Args:
        app (str): The name (id) of the app
        app_path (str): The relative path to the app. Used primarily for logging. (e.g. app/appname or slave-apps/appname)
        in_path (str): Full app source path
        diag (str): The name of the diag. (e.g. diag-host-date_time)
        flavor (str): Determines how the app is processed. (builtin|custom|splunkbase|user)
        merge (any, optional): Merge to local or default. Defaults to None.
        details (dict, optional): The splunkbase results. Defaults to None.
        gdi_path (str, optional): The path to the index-time app. Defaults to None.

    Raises:
        AppDisabledError: Raised when disabled = true
        EmptyAppError: Raised when there are no files remaining after cleaning is performed

    Returns:
        dict: details about the cleaned app results
    """

    logger = logging.getLogger(app_path + " ({})".format(flavor))
    audit = logging.getLogger(app_path + ":audit")
    # out_path = in_path.replace(diag, diag + "_{}".format(flavor))
    out_path = in_path.replace(diag, diag + "_{}_{}".format(flavor, (lambda x: 'not-merged' if not x else 'merged-to-{}'.format(x))(merge)))
    # merged_d_path = out_path + "_merged_to_default_for_vetting"
    # merged_l_path = out_path + "_merged_to_local_for_installing"

    #################################################
    ##########                             ##########
    ##########          APP.CONF           ##########
    ##########                             ##########
    #################################################

    ##################################################
    # GET ORIGINAL APP.CONF
    ##################################################
    orig_d_app_conf, orig_l_app_conf, orig_m_app_conf = get_conf_objs(in_path, "app.conf")

    ##################################################
    # GET WORKING COPY OF APP.CONF
    ##################################################
    d_app_conf, l_app_conf, m_app_conf = get_conf_objs(in_path, "app.conf")

    ##################################################
    # SKIP DISABLED APPS
    ##################################################
    try:
        if orig_m_app_conf.get("install", "state") == "disabled":
            raise AppDisabledError("App is disabled and will not be processed.") from None
    except (NoSectionError, NoOptionError):
        pass

    ##################################################
    # PROCESS APP.CONF
    ##################################################
    if flavor == 'user':
        d_app_conf = ConfParser()
        m_app_conf = ConfParser()
        version = None
    else:
        d_app_conf, m_app_conf, version = clean_app_conf(app_path, orig_d_app_conf, orig_l_app_conf, flavor)

    ##################################################
    # CHECK SPLUNKBASE COMPTIBILITY
    ##################################################
    if flavor == "splunkbase":
        _details = details[0]
        release = _details['release']
        _splunkbase = [
            str(_details['title']),
            str(_details['path']),
            str(release['title']),
            str(release['splunk_compatibility']),
            str(release['product_compatibility'])
        ]

        if "Splunk Cloud" not in release["product_compatibility"]:
            _splunkbase.append("Not Supported in Splunk Cloud")
            logger.error("Not Supported in Splunk Cloud")
        else:
            _splunkbase.append("Supported in Splunk Cloud")

        if len(set(["8.0", "8.1", "8.2", "9.0"]).intersection(set(release["splunk_compatibility"]))) < 1:
            _splunkbase.append("Splunk Version Not Supported")
            logger.error("App does not support current Splunk Cloud versions: splunk_compatibility: {}".format(release["splunk_compatibility"]))
        else:
            _splunkbase.append("Splunk Version Supported")

        try:
            this_version = orig_d_app_conf.get("launcher", "version")
        except (NoSectionError, NoOptionError) as e:
            logger.exception("Something is very wrong here")
            _splunkbase.append("Something is very wrong here")
            logger.critical(", ".join(_splunkbase))
            raise e
        if this_version != release["title"]:
            _splunkbase.append("Version Mismatch: {}".format(this_version))
            logger.warning("Version mismatch: Customer Version: {} Splunkbase Version: {}".format(this_version, release["title"]))
        else:
            _splunkbase.append("Up to date")

        if m_app_conf.has_option("install", "is_configured"):
            del m_app_conf["install"]["is_configured"]

        logger.info(", ".join(_splunkbase))

    ##################################################
    # CREATE NEW LOCAL APP.CONF
    ##################################################
    l_app_conf = d_app_conf.diff(m_app_conf)
    remove_empty_stanzas(l_app_conf)

    ##################################################
    # WRITE DEFAULT APP.CONF
    ##################################################
    if flavor == "custom":
        for message in audit_conf_changes(orig_d_app_conf, d_app_conf, app_path + ":default", "app.conf"):
            audit.info(message)
        d_app_conf_path = os.path.join(out_path, "default", "app.conf")
        write_conf(d_app_conf.sorted(), d_app_conf_path)

    ##################################################
    # WRITE LOCAL APP.CONF
    ##################################################
    for message in audit_conf_changes(orig_l_app_conf, l_app_conf, app_path + ":local", "app.conf"):
        audit.info(message)
    l_app_conf_path = os.path.join(out_path, "local", "app.conf")
    write_conf(l_app_conf.sorted(), l_app_conf_path)

    #################################################
    ##########                             ##########
    ##########          METADATA           ##########
    ##########                             ##########
    #################################################

    ##################################################
    # GET ORIGINAL META
    ##################################################
    orig_d_meta_conf, orig_l_meta_conf, orig_m_meta_conf = get_conf_objs(in_path, "meta.conf")

    ##################################################
    # GET WORKING COPY OF META
    ##################################################
    if flavor == 'custom':
        d_meta_conf = ConfParser()
        m_meta_conf = ConfParser()
        d_meta_conf.read_dict(t_metadata_conf)
        d_meta_conf.read_dict(orig_d_meta_conf)
        m_meta_conf.read_dict(d_meta_conf)
        m_meta_conf.read_dict(orig_l_meta_conf)
    elif flavor in ['builtin', 'splunkbase']:
        d_meta_conf, _, m_meta_conf = get_conf_objs(in_path, "meta.conf")
    else:
        d_meta_conf = ConfParser()
        m_meta_conf = ConfParser()

    ##################################################
    # PROCESS META
    ##################################################
    d_meta_conf, m_meta_conf = clean_meta_conf(d_meta_conf, m_meta_conf, flavor)
    remove_meta_owner(d_meta_conf)

    ##################################################
    # MERGE META
    ##################################################
    if merge:
        if merge == 'default':
            d_meta_conf = d_meta_conf.merged_with(m_meta_conf)
        else:
            d_meta_conf = ConfParser()
            d_meta_conf.read_dict(t_metadata_conf)

    ##################################################
    # CREATE NEW LOCAL META
    ##################################################
    l_meta_conf = d_meta_conf.diff(m_meta_conf)

    ##################################################
    # WRITE DEFAULT META
    ##################################################
    if flavor in ['custom']:
        for message in audit_conf_changes(orig_d_meta_conf, d_meta_conf, app_path + ":metadata", "default.meta"):
            audit.info(message)
        d_meta_conf_path = os.path.join(out_path, "metadata", "default.meta")
        write_conf(d_meta_conf.sorted(), d_meta_conf_path)

    ##################################################
    # WRITE LOCAL META
    ##################################################
    for message in audit_conf_changes(orig_l_meta_conf, l_meta_conf, app_path + ":metadata", "local.meta"):
        audit.info(message)
    l_meta_conf_path = os.path.join(out_path, "metadata", "local.meta")
    write_conf(l_meta_conf.sorted(), l_meta_conf_path)

    #################################################
    ##########                             ##########
    ##########         APP CONTENTS        ##########
    ##########                             ##########
    #################################################

    ##################################################
    # PROCESS APP CONFS
    ##################################################
    generating_searches = process_app_confs(app, app_path, in_path, out_path, flavor, merge=merge, gdi_path=gdi_path, lookup_path=lookup_path, st_path=st_path, fwd_path=fwd_path)

    ##################################################
    # PROCESS APP XML
    ##################################################
    process_xml(app, app_path, in_path, out_path, flavor, merge=merge)

    ##################################################
    # PROCESS OTHER FILES
    ##################################################
    process_other(app, app_path, in_path, out_path, flavor, merge=merge)

    ##################################################
    # REMOVE EMPTY APPS
    ##################################################
    count = 0
    for root, _, files in os.walk(out_path):
        for file in files:
            if file != "app.conf":
                count += 1
    if count == 0 and len([x for x in l_app_conf.sections() if x != "shclustering"]) == 0:
        try:
            shutil.rmtree(out_path)
        except FileNotFoundError:
            pass
        finally:
            raise EmptyAppError() from None

    ##################################################
    # COMPUTE THE CHECKSUMS
    ##################################################
    o_checksum, d_checksum, l_checksum, f_checksum = compute_checksums(app, in_path, out_path)

    # return {app: {'path': app_path, 'tarfile': create_tarfile(in_path, in_path.replace(diag, 'tars'), app, 'hostname', 'xxxxx', 'testing', arctype='full'), 'status': 'completed', 'result': 'success', 'checksum': {'original': o_checksum, 'default': d_checksum, 'local': l_checksum, 'full': f_checksum}}}
    return {'app': app, 'version': version, 'app_path': app_path, 'flavor': flavor, 'src_path': in_path, 'dest_path': out_path, 'status': 'completed', 'result': 'success', 'checksum': {'original': o_checksum, 'default_only': d_checksum, 'local_only': l_checksum, 'full': f_checksum}, 'generating_searches': generating_searches}

##################################################
# PROCESS APP CONF FILES
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def process_app_confs(app, app_path, in_path, out_path, flavor, merge=None, gdi_path=None, lookup_path=None, st_path=None, fwd_path=None):

    logger = logging.getLogger(app_path + " ({})".format(flavor))
    audit = logging.getLogger(app_path + ":audit")
    generating_searches = None

    # get all the confs and isolate the local changes
    # in splunkbase apps we don't modify the defaults

    for conf_file in [ x for x in get_all_confs(in_path) if x not in disallowed_files]:

        # index and ingest time props or transforms
        gdi_conf = None

        # lookup transforms and the props that reference them
        # mostly for reference and merging of apps
        lookup_conf = None

        # all conf files used only at search-time including lookups
        st_conf = None

        # EVENT_BREAKERS
        fwd_conf = None

        # all settings that must always be in local
        always_local_conf = None

        # # the empty shell conf if required
        # # not usually present
        # d_install_conf = None

        # # all the locals minus scheduled searches and DMA
        # # same as d_vet_conf
        # # l_install_conf = None

        # the merged default conf needed for vettting
        # used to intstall if the customer wants version
        # controlled apps all merged to default
        d_vet_conf = ConfParser()

        # # only contains enabled searches and DMA
        # l_vet_conf = None

        orig_d_conf, orig_l_conf, orig_m_conf = get_conf_objs(in_path, conf_file)
        d_conf, l_conf, m_conf = get_conf_objs(in_path, conf_file)

        if merge:
            if merge == 'local':
                d_conf = ConfParser()
            else:
                d_conf.read_dict(m_conf)

        ##################################################
        # ALERT_ACTIONS.CONF
        ##################################################
        if conf_file == "alert_actions.conf":

            d_conf, m_conf = process_alert_actions_conf(d_conf, m_conf, flavor)

        ##################################################
        # AUTHENTICATION.CONF
        ##################################################
        elif conf_file == "authentication.conf":

            d_conf, m_conf = process_authentication_conf(d_conf, m_conf, flavor)

        ##################################################
        # AUTHORIZE.CONF
        ##################################################
        elif conf_file == "authorize.conf":

            d_conf, m_conf = process_authorize_conf(d_conf, m_conf, flavor)

        ##################################################
        # DATAMODELS.CONF
        ##################################################
        elif conf_file == "datamodels.conf":

            d_conf, m_conf = process_datamodels_conf(d_conf, m_conf, flavor)

        ##################################################
        # DISTSEARCH.CONF
        ##################################################
        elif conf_file == "distsearch.conf":

            d_conf, m_conf = process_distsearch_conf(d_conf, m_conf, flavor)

        ##################################################
        # EVENTTYPES.CONF
        ##################################################
        elif conf_file == "eventtypes.conf":

            d_conf, m_conf, st_conf = process_eventtypes_conf(d_conf, m_conf, flavor)

        ##################################################
        # FIELDS.CONF
        ##################################################
        elif conf_file == "fields.conf":

            d_conf, m_conf, gdi_conf = process_fields_conf(d_conf, m_conf, flavor)

        ##################################################
        # INDEXES.CONF
        ##################################################
        elif conf_file == "indexes.conf":

            d_conf, m_conf, gdi_conf = process_indexes_conf(d_conf, m_conf, flavor)

        ##################################################
        # INPUTS.CONF
        ##################################################
        elif conf_file == "inputs.conf":

            d_conf, m_conf, gdi_conf = process_inputs_conf(d_conf, m_conf, flavor)

        ##################################################
        # MACROS.CONF
        ##################################################
        elif conf_file == "macros.conf":

            d_conf, m_conf, st_conf = process_macros_conf(d_conf, m_conf, flavor)

        ##################################################
        # PROPS.CONF
        ##################################################
        elif conf_file == "props.conf":

            d_conf, m_conf, gdi_conf, lookup_conf, st_conf, fwd_conf = process_props_conf(app, d_conf, m_conf, flavor)

        ##################################################
        # SAVEDSEARCHES.CONF
        ##################################################
        elif conf_file == "savedsearches.conf":

            if flavor == "custom":
                m_conf = t_savedsearches_conf.merged_with(orig_m_conf)
                m_conf["default"]["request.ui_dispatch_app"] = app
                m_conf["default"]["action.summary_index.origin_app"] = app

            d_conf, m_conf, always_local_conf, generating_searches = process_savedsearches_conf(app, app_path, d_conf, m_conf, flavor)

        ##################################################
        # TAGS.CONF
        ##################################################
        elif conf_file == "tags.conf":

            d_conf, m_conf, st_conf = process_tags_conf(d_conf, m_conf, flavor)

        ##################################################
        # TRANSFORMS.CONF
        ##################################################
        elif conf_file == "transforms.conf":

            d_conf, m_conf, gdi_conf, lookup_conf, st_conf = process_transforms_conf(app, d_conf, m_conf, flavor)

        ##################################################
        # DISALLOWED CONF FILES
        ##################################################
        elif conf_file in disallowed_files:
            continue

        # remove_empty_stanzas(d_conf)

        # create fixed as-is default conf
        # removes DMA and scheduled searches
        as_is_d_conf = ConfParser()
        as_is_d_conf.read_dict(d_conf)

        # isolate the changes that are local
        # including DMA and scheduled searches
        as_is_l_conf = d_conf.diff(m_conf)

        if always_local_conf:
            merged_conf = always_local_conf.diff(m_conf)
        else:
            merged_conf = ConfParser()
            merged_conf.read_dict(m_conf)

        if flavor == "custom":
            # log the changes to the audit.log
            for message in audit_conf_changes(orig_d_conf, d_conf, app_path + ":default", conf_file):
                audit.info(message)

            # write the new default .conf file
            d_conf_path = os.path.join(out_path, "default", conf_file)
            write_conf(d_conf.sorted(), d_conf_path)


        # create the new local conf and write it
        l_conf = d_conf.diff(m_conf)
        # remove_empty_stanzas(l_conf)

        # log the local changes to the audit.log
        for message in audit_conf_changes(orig_l_conf, l_conf, app_path + ":local", conf_file):
            audit.info(message)

        # write the new local .conf file
        l_conf_path = os.path.join(out_path, "local", conf_file)
        write_conf(l_conf.sorted(), l_conf_path)

        if gdi_path and gdi_conf:
            # write the merged conf to the GDI app
            if app == "system":
                _app = "_system"
            else:
                _app = app
            gdi_conf_path = os.path.join(gdi_path, "tmp", "{}_{}".format(_app, conf_file))
            remove_empty_stanzas(gdi_conf)
            write_conf(gdi_conf.sorted(), gdi_conf_path)

        if lookup_path and lookup_conf:
            if app == "system":
                _app = "_system"
            else:
                _app = app
            lookup_conf_path = os.path.join(lookup_path, "tmp", "{}_{}".format(_app, conf_file))
            write_conf(lookup_conf.sorted(), lookup_conf_path)

        if st_path and st_conf:
            if app == "system":
                _app = "_system"
            else:
                _app = app
            st_conf_path = os.path.join(st_path, "tmp", "{}_{}".format(_app, conf_file))
            write_conf(st_conf.sorted(), st_conf_path)

        if fwd_path and fwd_conf:
            if app == "system":
                _app = "_system"
            else:
                _app = app
            fwd_conf_path = os.path.join(fwd_path, "tmp", "{}_{}".format(_app, conf_file))
            write_conf(fwd_conf.sorted(), fwd_conf_path)

    return generating_searches

##################################################
# PROCESS AUTHENTICATION.CONF
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def process_authentication_conf(d_conf, m_conf, flavor):

    for section in m_conf.sections():
        if not re_allowed_in_authentication.match(section):
            try:
                del d_conf[section]
            except KeyError:
                pass

            try:
                del m_conf[section]
            except KeyError:
                # this should never happen, but
                pass
            continue

        elif section.startswith("roleMap_"):
            # Roles must not be mapped to Splunk Cloud Platform system roles:
            # admin, splunk-system-role, app-installer, index-manager,
            # internal_ops_admin, and internal_monitoring.

            # find all that are not allowed and remove or modify them.
            for key, item in m_conf.items(section):
                if key == "admin":
                    for _conf in [d_conf, m_conf]:
                        # check to insure sc_admin role is not
                        # already present and merge admin and sc_admin
                        # if it does
                        try:
                            _sc_admin = [ x.strip() for x in _conf[section]["sc_admin"].split(";")]
                        except KeyError:
                            _sc_admin = []

                        try:
                            del _conf[section]["admin"]
                        except KeyError:
                            continue

                        _admin = [ x.strip() for x in item.split(";")]
                        _conf[section]["sc_admin"] = ";".join(list(set(_sc_admin).union(set(_admin))))

                elif key in [x for x in disallowed_role_imports if x != "sc_admin" ]:
                    for _conf in [d_conf, m_conf]:
                        try:
                            del _conf[section][key]
                        except KeyError:
                            continue

        elif section.startswith("userToRoleMap_"):
            for _conf in [d_conf, m_conf]:
                try:
                    for username, roles, real_name, email in [[k] + _conf[section][k].split('::') for k in _conf[section].keys()]:
                        _roles = set([ (lambda x: 'sc_admin' if x == 'admin' else x.strip())(x) for x in roles.split(";")])
                        roles = ";".join(list(_roles.difference(set([ x for x in disallowed_role_imports if x != "sc_admin" ]))))
                        _conf[section][username] = "::".join([roles, real_name, email])
                except (NoSectionError, NoOptionError, KeyError):
                    pass

    return d_conf, m_conf

##################################################
# PROCESS AUTHORIZE.CONF
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def process_authorize_conf(d_conf, m_conf, flavor):

    for section in m_conf.sections():
        if section in disallowed_roles:
            try:
                del d_conf[section]
            except KeyError:
                pass
            try:
                del m_conf[section]
            except KeyError:
                # this should never happen, but
                pass
            continue

        # find all the capabilities that are not allowed and remove them.
        for key, item in m_conf.items(section):
            if key == "importRoles":
                for _conf in [d_conf, m_conf]:
                    try:
                        _imports = _conf[section]["importRoles"].split(";")
                    except KeyError:
                        continue

                    roles = []
                    for _this in [ x.strip() for x in _imports ]:
                        if _this not in disallowed_role_imports:
                            roles.append(_this)

                    if len(roles) > 0:
                        _conf[section]["importRoles"] = ";".join(roles)
                    else:
                        del _conf[section]["importRoles"]

            elif key in disallowed_capabilities:
                for _conf in [d_conf, m_conf]:
                    try:
                        del _conf[section][key]
                    except KeyError:
                        continue

    return d_conf, m_conf

##################################################
# PROCESS DATAMODELS.CONF
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def process_datamodels_conf(d_conf, m_conf, flavor):

    # ensure DMA is not enabled in default
    if flavor == "custom":
        for section in d_conf.sections():
            try:
                if truth(d_conf.safe_get(section, "acceleration")):
                    del d_conf[section]["acceleration"]
            except (NoSectionError, NoOptionError):
                pass

    return d_conf, m_conf

##################################################
# PROCESS DISTSEARCH.CONF
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def process_distsearch_conf(d_conf, m_conf, flavor):

    for conf in [d_conf, m_conf]:
        conf = filter_distsearch(conf)

    return d_conf, m_conf

##################################################
# PROCESS EVENTTYPES.CONF
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def process_eventtypes_conf(d_conf, m_conf, flavor):

    if flavor in ['builtin', 'splunkbase']:
        st_conf = d_conf.diff(m_conf)
    else:
        st_conf = ConfParser()
        st_conf.read_dict(m_conf)

    return d_conf, m_conf, st_conf

##################################################
# PROCESS FIELDS.CONF
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def process_fields_conf(d_conf, m_conf, flavor):

    if flavor in ['builtin', 'splunkbase']:
        gdi_conf = d_conf.diff(m_conf)
    else:
        gdi_conf = ConfParser()
        gdi_conf.read_dict(m_conf)

    return d_conf, m_conf, gdi_conf

##################################################
# PROCESS INDEXES.CONF
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def process_indexes_conf(d_conf, m_conf, flavor):

    if flavor == "custom":
        filter_indexes(d_conf)

    filter_indexes(m_conf)

    # put a copy of filtered merged into a temp location for GDI
    gdi_conf = ConfParser()
    gdi_conf.read_dict(m_conf)

    if flavor in ["builtin", "splunkbase"]:
        gdi_conf = d_conf.diff(gdi_conf)

    # remove all the default sections so they can be replaced
    # globally on individual stanzas in the combined GDI app
    for _conf in [d_conf, m_conf]:
        for section in default_sections:
            try:
                del _conf[section]
            except KeyError:
                continue

    return d_conf, m_conf, gdi_conf

##################################################
# PROCESS INPUTS.CONF
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def process_inputs_conf(d_conf, m_conf, flavor):

    if flavor in ['builtin', 'splunkbase']:
        gdi_conf = d_conf.diff(m_conf)
    else:
        gdi_conf = ConfParser()
        gdi_conf.read_dict(m_conf)

    return d_conf, m_conf, gdi_conf

##################################################
# PROCESS MACROS.CONF
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def process_macros_conf(d_conf, m_conf, flavor):

    if flavor in ['builtin', 'splunkbase']:
        st_conf = d_conf.diff(m_conf)
    else:
        st_conf = ConfParser()
        st_conf.read_dict(m_conf)

    return d_conf, m_conf, st_conf

@debug_wrap(ENABLED=bool(False or GBL_DEBUG), TRACE=bool(False or GBL_TRACE))
def process_other(app, app_path, in_path, out_path, flavor, merge=False):
    ##################################################
    # OTHER FILES
    ##################################################
    logger = logging.getLogger(app)
    for dir in allowed_app_dirs:
        src_dir = os.path.join(in_path, dir)
        if not os.path.exists(src_dir) or os.path.isfile(src_dir):
            continue

        if flavor == "custom" and dir in ["appserver", "static"]:
            logger.debug("Processing {}".format(dir))
            for root, _dirs, files in os.walk(src_dir):
                for file in [x for x in files if not re_junk_file_patterns.search(x)]:
                    if re.match(r"(?i)readme", file):
                        continue
                    src_file = os.path.join(root, file)
                    dest_file = src_file.replace(in_path, out_path)
                    copy_file(src_file, dest_file, FILE_MODE)

        elif flavor == "custom" and dir in ["bin", "lib"]:
            logger.debug("Processing {}".format(dir))
            for root, _dirs, files in os.walk(src_dir):
                for file in [x for x in files if (not re_junk_file_patterns.search(x) or x in ['__init__.py', '__main__.py'])]:
                    if re.match(r"(?i)readme", file):
                        continue
                    src_file = os.path.join(root, file)
                    # skip the cache files and such
                    if re_excluded_bin_patterns.search(src_file):
                        continue
                    dest_file = src_file.replace(in_path, out_path)
                    # check the extension for executable files in bin only
                    if dir == "bin" and re_bin_file_patterns.search(src_file):
                        mode = 0o700
                    else:
                        mode = FILE_MODE
                    copy_file(src_file, dest_file, mode)

        elif dir == "lookups":
            logger.debug("Processing {}".format(dir))
            # search for files only at the top level
            # anything else is unnecessary
            for file in [ x for x in os.listdir(src_dir) if os.path.isfile(os.path.join(src_dir, x)) and not re_junk_file_patterns.search(x)]:
                if app == 'search' and file in ['geo_attr_countries.csv', 'geo_attr_us_states.csv', 'geo_countries.kmz', 'geo_us_states.kmz']:
                    continue
                if re_lookup_index_file.search(file) or re.match(r"(?i)readme", file):
                    continue
                src_file = os.path.join(src_dir, file)
                dest_file = src_file.replace(in_path, out_path)
                copy_file(src_file, dest_file, FILE_MODE)

        elif flavor == "custom" and re.match(r"(?i)readme", dir):
            logger.debug("Processing {}".format(dir))
            for root, _dirs, files in os.walk(src_dir):
                for file in [x for x in files if not re_junk_file_patterns.search(x)]:
                    src_file = os.path.join(root, file)
                    dest_file = src_file.replace(in_path, out_path)
                    copy_file(src_file, dest_file, FILE_MODE)

##################################################
# PROCESS PROPS.CONF
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def process_props_conf(app, d_conf, m_conf, flavor=None):

    if flavor == "custom":
        clean_props_conf(d_conf)

    clean_props_conf(m_conf)
    # make a temporary copy of merged for search-time
    _tmp = ConfParser()
    _tmp.read_dict(m_conf)

    # make a place for lookup props
    lookup_conf = ConfParser()

    # put a copy of merged into a temp location for GDI
    gdi_conf = ConfParser()
    fwd_conf = ConfParser()
    gdi_conf.read_dict(m_conf)
    for section in gdi_conf.sections():
        for key, _ in gdi_conf.items(section):
            if key not in gdi_props and not re_props_class.search(key):
                del gdi_conf[section][key]

        LINE_BREAKER = gdi_conf[section].get('LINE_BREAKER', r'([\n\r]+)')
        SHOULD_LINEMERGE = gdi_conf[section].get('SHOULD_LINEMERGE', '1')

        if not truth(SHOULD_LINEMERGE):
            fwd_conf[section] = {'EVENT_BREAKER_ENABLE': '1', 'EVENT_BREAKER': LINE_BREAKER}


    # capture the stuff that was not gdi and add it to
    # the search-time conf
    st_conf = gdi_conf.diff(_tmp)

    for section in st_conf.sections():
        for key, val in st_conf.items(section):
            if re_props_lookup.search(key):
                lookup_conf[section] = {key: val}

    # isolate only the local settings
    if flavor in ["builtin", "splunkbase"]:
        lookup_conf = d_conf.diff(lookup_conf)
        st_conf = d_conf.diff(st_conf)
        gdi_conf = d_conf.diff(gdi_conf)

    # if flavor in ["builtin"] or app == 'system':
    #     gdi_conf = d_conf.diff(gdi_conf)

    return d_conf, m_conf, gdi_conf, lookup_conf, st_conf, fwd_conf

##################################################
# PROCESS SAVEDSEARCHES.CONF
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def process_savedsearches_conf(app, app_path, d_conf, m_conf, flavor):

    generating_searches = None
    always_local_conf = ConfParser()
    logger = logging.getLogger(app_path + ":savedsearch")
    for section in m_conf.sections():

        if section in ["default", "##--NO_SECTION--##", "global"]:
            continue

        # check for scheduled searches in default
        # remove them and add them to always_local_conf
        # if flavor in ["custom"]:
        #     try:
        #         del d_conf[section]["enableSched"]
        #     except KeyError:
        #         pass
        try:
            if truth(m_conf[section]["enableSched"]):
                always_local_conf[section]["enableSched"] = "1"
                try:
                    del d_conf[section]["enableSched"]
                except KeyError:
                    pass
        except KeyError:
            pass

        for item in [x for x, _ in m_conf.items(section) if x in deprecated_savedsearch_options]:
            if flavor in ["custom"]:
                try:
                    del d_conf[section][item]
                except KeyError:
                    pass

            del m_conf[section][item]

        try:
            if truth(m_conf[section]["disabled"]):
                continue
        except KeyError:
            pass

        # for the following checks we need to isolate local
        _tmp = d_conf.diff(m_conf)
        for this_path, this_conf in [("local", _tmp), ("default", d_conf)]:
            try:
                m, h, d, mo, dw = this_conf.safe_get(section, "cron_schedule").split()
            except (NoSectionError, NoOptionError):
                pass
            except ValueError:
                logger.critical(("INVALID_CRON", this_path, section, "cron_schedule", this_conf.safe_get(section, "cron_schedule")))

            try:
                _earliest = this_conf.safe_get(section, "dispatch.earliest_time")
            except (NoSectionError, NoOptionError):
                _earliest = ""

            try:
                _latest = this_conf.safe_get(section, "dispatch.latest_time")
            except (NoSectionError, NoOptionError):
                _latest = ""

            if _earliest.startswith("rt") or _latest.startswith("rt"):
                logger.warning(("REALTIME_SEARCH", this_path, section, _earliest, _latest))

        if flavor in ["splunkbase", "builtin"]:
            _conf = _tmp
        else:
            _conf = m_conf

        e_count = 0

        ##################################################
        # CHECK FOR ALERT ACTIONS
        ##################################################
        actions = []
        try:
            for action, value in [ x for x in _conf.items(section) if x[0].startswith("action.")]:
                has_action = re.match(r"^action\.([^\.\s]+)$", action)
                if has_action and truth(value):
                    actions.append(has_action.group(1))
        except (NoSectionError, NoOptionError):
            continue

        if len(actions) > 0:
            e_count += 1
            logger.warning("{!r} has enabled alert_actions {!r}".format(section, actions))

        try:
            search_string = " ".join(_conf.safe_get(section, "search").split())
        except (NoSectionError, NoOptionError):
            continue

        spl_list = re_spl_sep.split(search_string)
        indexes = []
        keys = []
        for command in spl_list:
            index_ref = re_spl_index.findall(command)
            if index_ref:
                indexes.extend(index_ref)

            checks = []
            checks.append(("loadjob", re_spl_loadjob.match(command)))
            checks.append(("lookup", re_spl_lookup.match(command)))
            checks.append(("outputlookup", re_spl_outputlookup.match(command)))
            checks.append(("inputlookup", re_spl_inputlookup.match(command)))
            checks.append(("db_connect", re_spl_dbx.match(command)))
            checks.append(("collect", re_spl_collect.match(command)))
            checks.append(("sendmail", re_spl_sendmail.match(command)))
            checks.append(("script", re_spl_script.match(command)))
            checks.append(("sendalert", re_spl_sendalert.match(command)))
            checks.append(("xtreme", re_spl_xs.match(command)))
            checks.append(("ldap", re_spl_ldap.match(command)))
            checks.append(("rest", re_spl_rest.match(command)))
            checks.append(("nslookup", re_spl_nslookup.match(command)))

            for key, item in [ x for x in checks if x[1] is not None]:
                keys.append(key)
                if key in ["nslookup", "db_connect", "ldap", "script"]:
                    logger.warning("{!r} contains {!r}".format(section, item.group(0)))
                elif key in ["sendmail", "sendalert", "rest", "xtreme"]:
                    logger.warning("{!r} contains {!r}".format(section, item.group(0)))
                elif key in ["outputlookup", "collect"]:
                    logger.warning("{!r} contains {!r}".format(section, item.group(0)))
                    if generating_searches is None:
                        generating_searches = [(section, item.group(0))]
                    else:
                        generating_searches.append((section, item.group(0)))
                else:
                    logger.info("{!r} contains {!r}".format(section, item.group(0)))
                e_count += 1

        if indexes == []:
            logger.info("{!r} references no indexes directly".format(section))
        else:
            for index in indexes:
                if not re.match(r"[a-z0-9][a-z0-9_-]*", index.lower()) and index not in weird_indexes:
                    logger.error("{!r} references invalid 'index={}'".format(section, index))
                    e_count += 1
            logger.info("{!r} references 'index={}'".format(section, ", index=".join(indexes)))

        if len(list(set(keys).intersection({'db_connect', 'outputlookup'}))) > 1:
            logger.error("{!r} may create a lookup based on a database query".format(section))

        if len(list(set(keys).intersection({'ldap', 'outputlookup'}))) > 1:
            logger.error("{!r} may create a lookup based on an LDAP query".format(section))

        if len(list(set(keys).intersection({'db_connect', 'collect'}))) > 1:
            logger.error("{!r} may create a summary based on a database query".format(section))

        if len(list(set(keys).intersection({'ldap', 'collect'}))) > 1:
            logger.error("{!r} may create a summary based on an LDAP query".format(section))

        if e_count == 0:
            logger.info("{!r} has no identifed issues".format(section))

    return d_conf, m_conf, always_local_conf, generating_searches

##################################################
# PROCESS TAGS.CONF
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def process_tags_conf(d_conf, m_conf, flavor):

    if flavor in ['builtin', 'splunkbase']:
        st_conf = d_conf.diff(m_conf)
    else:
        st_conf = ConfParser()
        st_conf.read_dict(m_conf)

    return d_conf, m_conf, st_conf

##################################################
# PROCESS TRANSFORMS.CONF
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def process_transforms_conf(app, d_conf, m_conf, flavor=None):

    if flavor == "custom":
        pass

    # put a copy of filtered merged into a temp location for GDI
    gdi_conf = ConfParser()
    lookup_conf = ConfParser()
    # gdi_conf.read_dict(m_conf)
    for section in m_conf.sections():
        if section.startswith("statsd-dims:") or section.startswith("metric-schema:"):
            gdi_conf[section] = dict(m_conf[section])
        elif m_conf.has_option(section, "DEST_KEY"):
            gdi_conf[section] = dict(m_conf[section])
        elif m_conf.has_option(section, "WRITE_META"):
            gdi_conf[section] = dict(m_conf[section])
        elif m_conf.has_option(section, "CLONE_SOURCETYPE"):
            gdi_conf[section] = dict(m_conf[section])
        elif m_conf.has_option(section, "INGEST_EVAL"):
            gdi_conf[section] = dict(m_conf[section])
        elif m_conf.has_option(section, "STOP_PROCESSING_IF"):
            gdi_conf[section] = dict(m_conf[section])
        elif m_conf.has_option(section, "filename"):
            lookup_conf[section] = dict(m_conf[section])
        elif m_conf.has_option(section, "collection"):
            lookup_conf[section] = dict(m_conf[section])
        elif m_conf.has_option(section, "external_type"):
            lookup_conf[section] = dict(m_conf[section])

    st_conf = gdi_conf.diff(m_conf)

    # isolate only the local settings
    if flavor in ["builtin", "splunkbase"]:
        lookup_conf = d_conf.diff(lookup_conf)
        st_conf = d_conf.diff(st_conf)
        gdi_conf = d_conf.diff(gdi_conf)

    # if app == 'system':
    #     gdi_conf = d_conf.diff(gdi_conf)
    #     st_conf = d_conf.diff(st_conf)

    return d_conf, m_conf, gdi_conf, lookup_conf, st_conf

##################################################
# PROCESS XML AND JSON FILES
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def process_xml(app, app_name, in_path, out_path, flavor=None, merge=False):

    logger = logging.getLogger(app_name)

    re_alerts = r"{}|{}".format(os.path.join(app, "default", "data", "ui", "alerts"), os.path.join(app, "local", "data", "ui", "alerts"))

    validate = {
        "custom": ["default", "local"],
        "splunkbase": ["local"],
        "builtin": ["local"],
        "user": ["local"]
    }

    for dir in validate[flavor]:
        for root, _dirs, files in os.walk(os.path.join(in_path, dir)):
            for file in [ x for x in files if (x.endswith(".xml") or x.endswith(".html") or x.endswith(".json")) and not re_junk_file_patterns.search(x)]:
                if file == "setup.xml":
                    logger.error("SETUP.XML does not work properly in Splunk Cloud. Use a setup view instead.")
                    raise SetupXMLError("App contains setup.xml") from None

                src_file = os.path.join(root, file)
                if file.endswith(".html") and not re.search(re_alerts, src_file):
                    logger.error("{!r} HTML dashboards are deprecated and will not be migrated. Recreate in SimpleXML or Dashboard Studio".format(file))
                    continue

                dest_file = src_file.replace(in_path, out_path)

                # if merge then make sure all the xml files are placed under local
                # except for the nav/default.xml which must be present in default to pass appinspect
                if merge:
                    if merge == 'local' and not dest_file.endswith(os.path.join("default", "data", "ui", "nav", "default.xml")):
                        dest_file = dest_file.replace(os.sep + "default" + os.sep , os.sep + "local" + os.sep)
                    else:
                        dest_file = dest_file.replace(os.sep + "local" + os.sep , os.sep + "default" + os.sep)

                # make sure the file is not empty
                if os.path.getsize(src_file) == 0:
                    try:
                        os.unlink(dest_file)
                    except FileNotFoundError:
                        pass
                    continue

                if file.endswith(".json"):
                    with open(src_file, "r", encoding='utf-8-sig') as f:
                        try:
                            json.load(f)
                        except json.decoder.JSONDecodeError as e:
                            logger.error("{}: Invalid JSON: {}".format(src_file, repr(e)))
                            continue

                    os.makedirs(os.path.dirname(dest_file), mode=DIR_MODE, exist_ok=True)
                    # copy the file as-is to the dest
                    shutil.copyfile(src_file, dest_file)
                    os.chmod(dest_file, FILE_MODE)

                elif file.endswith(".xml"):
                    try:
                        xml_src = ET.parse(src_file)
                        xml_doc = xml_src.getroot()
                        repairs = process(xml_doc, app_name)
                        # xml_doc = _process_new(app, app_name, xml_doc, file, flavor)
                    except ParseError as e:
                        logger.error("{}: Invalid XML: {}".format(file, repr(e)))
                    else:
                        os.makedirs(os.path.dirname(dest_file), mode=DIR_MODE, exist_ok=True)
                        if repairs > 0:
                            xml_src.write(dest_file, encoding="utf-8", xml_declaration=False)
                            logger.warning("{}: {} CORRECTIONS".format(src_file, repairs))
                        else:
                            copy_file(src_file, dest_file, FILE_MODE)
                else:
                    os.makedirs(os.path.dirname(dest_file), mode=DIR_MODE, exist_ok=True)
                    # copy the file as-is to the dest
                    copy_file(src_file, dest_file, FILE_MODE)

##################################################
# REMOVE EMPTY DIRECTORIES
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def remove_empty_directories(path):
    dirs = list(os.walk(path, topdown=False))[1:]
    for dir in dirs:
        if not dir[2]:
            try:
                os.rmdir(dir[0])
            except OSError:
                pass

##################################################
# REMOVE EMPTY STANZAS
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def remove_empty_stanzas(confObj):
    for section in confObj.sections():
        if len(confObj.items(section)) == 0:
            del confObj[section]

##################################################
# REMOVE METADATA OWNER
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def remove_meta_owner(confObj):
    for section in [ x for x in confObj.sections() if x != "app" ]:
        try:
            del confObj[section]["owner"]
        except KeyError:
            continue

##################################################
# TRUTH
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def truth(obj):
    return bool(re.match(r"(?i)t(?:rue)?|1", str(obj)))

##################################################
# USER LIST
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def user_list(path):
    return [ x for x in list_dirs(path) if x != "admin"]

##################################################
# VALID DIAG
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def valid_diag(path):
    _result = False
    if re_diag_name.match(os.path.basename(path)):
        if os.path.isdir(os.path.join(path, "etc")):
            _result = True

    return _result

##################################################
# WRITE CONF
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def write_conf(conf, dest):
    if len(conf.sections()) == 0:
        try:
            os.unlink(dest)
        except FileNotFoundError:
            pass
    else:
        os.makedirs(os.path.dirname(dest), mode=DIR_MODE, exist_ok=True)
        with open(dest, "w") as f:
            conf.dump(f)
        os.chmod(dest, FILE_MODE)

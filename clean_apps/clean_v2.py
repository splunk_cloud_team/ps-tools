DEBUG=True
import csv
import logging
import os
import shutil
import sys
import tarfile

sys.path.append(os.path.join(os.path.dirname(__file__), '..', 'lib'))

from ps_common import *
from ps_common.appinspect import *
from ps_common.appinspect.classes import Session as AI
from ps_common.classes import ConfParser
from ps_common.logger import logListenerConfig
from ps_common.utils import isSplunkbaseApp

from built_in_apps import *
from constraints import *
from consts import *
from consts import GBL_DEBUG, GBL_TRACE
from slim import *
from templates import *
from utils import *

##################################################
# CREATE TARFILE
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def create_tarfile(in_path, out_path, app, host, checksum, desc, arctype=None, username=""):
    if not arctype:
        return None

    include = {
        "for_vetting": allowed_app_dirs + ["default", os.path.join("metadata", "default.meta")],
        "for_installing_default": [os.path.join("default", "app.conf"), os.path.join("default", "data", "ui", "nav", "default.xml"), os.path.join("metadata", "default.meta")],
        "first_local": [os.path.join("local", "app.conf"), "default",  os.path.join("metadata", "local.meta") ],
        "second_local": [os.path.join("local", "app.conf"), "local" ],
        "local_only": ["local", os.path.join("metadata", "local.meta")],
        "default_only": allowed_app_dirs + ["default", os.path.join("metadata", "default.meta")],
        "full": allowed_app_dirs + ["default", "local", "metadata"]
        }

    out_local = "-".join([app, checksum, host, desc])

    tarfile_name = "-".join([app, checksum, host, desc, arctype]) + ".tgz"
    out_tar = os.path.join(out_path, tarfile_name)
    os.makedirs(out_path, exist_ok=True)

    with tarfile.open(out_tar, "w:gz") as f:
        for dir in include[arctype]:
            try:
                f.add(os.path.join(in_path, dir), arcname=os.path.join(username, app, dir), recursive=True)
            except FileNotFoundError:
                continue
    return out_tar

##################################################
# PROCESS (ALL APPS AND USERS)
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def process(src, merge, include=['apps', 'slave-apps', 'peer-apps', 'system', 'users'], what=[], session=None):
    results = []

    GDI_APP_NAME = "A00-CLOUD_MIGRATION_GDI"
    gdi_path = os.path.join(src['root'] + "_GDI", GDI_APP_NAME)
    LOOKUP_APP_NAME = "zzz-CONSOLIDATED_LOOKUPS"
    lookup_path = os.path.join(src['root'] + "_LOOKUPS", LOOKUP_APP_NAME)
    ST_APP_NAME = "zzz-ALL-SEARCHTIME_PROPS_TRANSFORMS"
    st_path = os.path.join(src['root'] + "_SEARCHTIME", ST_APP_NAME)
    FWD_APP_NAME = "A00-ALL-FORWARDER-EVENT-BREAKERS"
    fwd_path = os.path.join(src['root'] + "_FORWARDERS", FWD_APP_NAME)
    to_vet_path = os.path.join(src['root'] + "_TO_VET")

    for tree in sorted([ x for x in src.keys() if x in list(set(['apps', 'slave-apps', 'peer-apps', 'master-apps', 'manager-apps', 'deployment-apps']).intersection(set(include)))]):
        src_tree = os.path.join(src['etc'], tree)
        for app in sorted(src[tree], key=lambda k: k.upper()):
            _result = None
            try:
                app_src = os.path.join(src_tree, app)
                app_path = os.path.join(tree, app)
                logger = logging.getLogger(app_path)
                if app in builtin_apps:
                    flavor = "builtin"
                    if 'builtin' in what:
                        _result = process_app(app, app_path, app_src, src['diag'], "builtin", gdi_path=gdi_path, lookup_path=lookup_path, st_path=st_path, fwd_path=fwd_path, to_vet_path=to_vet_path)
                else:
                    splunkbase, details = isSplunkbaseApp(app, details=True)
                    if splunkbase:
                        flavor = "splunkbase"
                        if 'splunkbase' in what:
                            _result = process_app(app, app_path, app_src, src['diag'], "splunkbase", details=details, gdi_path=gdi_path, lookup_path=lookup_path, st_path=st_path, fwd_path=fwd_path, to_vet_path=to_vet_path)
                    elif 'custom' in what:
                        flavor = "custom"
                        _result = process_app(app, app_path, app_src, src['diag'], "custom", merge=merge, gdi_path=gdi_path, lookup_path=lookup_path, st_path=st_path, fwd_path=fwd_path, to_vet_path=to_vet_path)
            except EmptyAppError:
                _result = {'app': app, 'app_path': app_path, 'flavor': flavor, 'status': 'empty', 'result': 'nothing to do'}
                logger.info("Nothing to do.")
            except AppDisabledError as e:
                _result = {'app': app, 'app_path': app_path, 'flavor': flavor, 'status': 'disabled', 'result': 'nothing to do'}
                logger.warning(e)
            except SetupXMLError as e:
                _result = {'app': app, 'app_path': app_path, 'flavor': flavor, 'status': 'failure', 'result': repr(e)}
                logger.error(e)
            except InvalidIndexError as e:
                _result = {'app': app, 'app_path': app_path, 'flavor': flavor, 'status': 'failure', 'result': repr(e)}
                logger.critical(e)
            except Exception as e:
                _result = {'app': app, 'app_path': app_path, 'flavor': flavor, 'status': 'exception', 'result': repr(e)}
                logger.exception(e)
            finally:
                if _result is not None:
                    results.append(_result)

    for app in sorted(list(set(['system']).intersection(set(include)))):
        _result = None
        try:
            app_src = os.path.join(src['etc'], app)
            app_path = os.path.join("etc", app)
            logger = logging.getLogger(app_path)
            flavor = "builtin"
            _result = process_app(app, app_path, app_src, src['diag'], "builtin", gdi_path=gdi_path, lookup_path=lookup_path, st_path=st_path, fwd_path=fwd_path)
        except EmptyAppError:
            _result = {'app': app, 'app_path': app_path, 'flavor': flavor, 'status': 'empty', 'result': 'nothing to do'}
            logger.info("Nothing to do.")
        except AppDisabledError as e:
            _result = {'app': app, 'app_path': app_path, 'flavor': flavor, 'status': 'disabled', 'result': 'nothing to do'}
            logger.warning(e)
        except SetupXMLError as e:
            _result = {'app': app, 'app_path': app_path, 'flavor': flavor, 'status': 'failure', 'result': repr(e)}
            logger.error(e)
        except InvalidIndexError as e:
            _result = {'app': app, 'app_path': app_path, 'flavor': flavor, 'status': 'failure', 'result': repr(e)}
            logger.critical(e)
        except Exception as e:
            _result = {'app': app, 'app_path': app_path, 'flavor': flavor, 'status': 'exception', 'result': repr(e)}
            logger.exception(e)
        finally:
            if _result is not None:
                results.append(_result)

    for tree in sorted([ x for x in src.keys() if x in list(set(['users']).intersection(set(include)))]):
        src_tree = os.path.join(src['etc'], tree)
        for user in sorted(src[tree], key=lambda k: [ x.upper() for x in k.keys()]):
            for username, apps in user.items():
                for app in sorted(apps, key=lambda k: k.upper()):
                    _result = None
                    try:
                        app_src = os.path.join(src_tree, username, app)
                        app_path = os.path.join(tree, username, app)
                        logger = logging.getLogger(app_path)
                        flavor = "user"
                        _result = process_app(app, app_path, app_src, src['diag'], "user")
                    except EmptyAppError:
                        _result = {'app': app, 'app_path': app_path, 'flavor': flavor, 'status': 'empty', 'result': 'nothing to do'}
                        logger.info("Nothing to do.")
                    except AppDisabledError as e:
                        _result = {'app': app, 'app_path': app_path, 'flavor': flavor, 'status': 'disabled', 'result': 'nothing to do'}
                        logger.warning(e)
                    except SetupXMLError as e:
                        _result = {'app': app, 'app_path': app_path, 'flavor': flavor, 'status': 'failure', 'result': repr(e)}
                        logger.error(e)
                    except InvalidIndexError as e:
                        _result = {'app': app, 'app_path': app_path, 'flavor': flavor, 'status': 'failure', 'result': repr(e)}
                        logger.critical(e)
                    except Exception as e:
                        _result = {'app': app, 'app_path': app_path, 'flavor': flavor, 'status': 'exception', 'result': repr(e)}
                        logger.exception(e)
                    finally:
                        if _result is not None:
                            results.append(_result)

    # finalize the GDI app
    try:
        make_gdi_app(gdi_path, GDI_APP_NAME)
    except Exception as e:
        logger.exception(e)
        results.append({'app': GDI_APP_NAME, 'version': '7.7.7', 'app_path': os.path.relpath(gdi_path, os.path.dirname(src['root'])), 'flavor': 'custom', 'status': 'failed', 'result': repr(e)})
    else:
        results.append({'app': GDI_APP_NAME, 'version': '7.7.7', 'app_path': os.path.relpath(gdi_path, os.path.dirname(src['root'])), 'flavor': 'custom', 'status': 'new', 'result': 'Vet and install this app first for immediate GDI'})

    # finalize the FORWARDER app
    try:
        make_fwd_app(fwd_path, FWD_APP_NAME)
    except Exception as e:
        logger.exception(e)
        results.append({'app': FWD_APP_NAME, 'version': '7.7.7', 'app_path': os.path.relpath(fwd_path, os.path.dirname(src['root'])), 'flavor': 'custom', 'status': 'failed', 'result': repr(e)})
    else:
        results.append({'app': FWD_APP_NAME, 'version': '7.7.7', 'app_path': os.path.relpath(fwd_path, os.path.dirname(src['root'])), 'flavor': 'custom', 'status': 'new', 'result': 'Install this app **ON-PREM-ONLY** to assist with load balancing'})

    # finalize the LOOKUP app
    try:
        make_lookup_app(lookup_path, LOOKUP_APP_NAME)
    except Exception as e:
        logger.exception(e)
        results.append({'app': LOOKUP_APP_NAME, 'version': '7.7.7', 'app_path': os.path.relpath(lookup_path, os.path.dirname(src['root'])), 'flavor': 'custom', 'status': 'failed', 'result': repr(e)})
    else:
        results.append({'app': LOOKUP_APP_NAME, 'version': '7.7.7', 'app_path': os.path.relpath(lookup_path, os.path.dirname(src['root'])), 'flavor': 'custom', 'status': 'new', 'result': 'Place all GLOBAL lookup files into this app (optional)'})

    # finalize the SEARCHTIME app
    try:
        make_searchtime_app(st_path, ST_APP_NAME)
    except Exception as e:
        logger.exception(e)
        results.append({'app': ST_APP_NAME, 'version': '6.6.6', 'app_path': os.path.relpath(st_path, os.path.dirname(src['root'])), 'flavor': 'custom', 'status': 'failed', 'result': repr(e)})
    else:
        results.append({'app': ST_APP_NAME, 'version': '6.6.6', 'app_path': os.path.relpath(st_path, os.path.dirname(src['root'])), 'flavor': 'custom', 'status': 'new', 'result': 'DO NOT INSTALL THIS APP AS-IS - THE PURPOSE IS TO REVIEW GLOBAL SEARCHTIME SETTINGS FOR POSSIBLE CONSOLIDATION'})

    return results

##################################################
# MAKE APP FOR VETTING (ALSO INSTALL AS DEFAULT)
##################################################


##################################################
# MAKE APP FOR INSTALL AS LOCAL
##################################################




##################################################
# MAKE GDI APP (ONLY FOR WHOLE DIAG)
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def make_gdi_app(gdi_path, gdi_app, include_indexes=True, preserve_tmp=True):

    gdi_tmp = os.path.join(gdi_path, "tmp")
    gdi_default = os.path.join(gdi_path, "default")
    dest_confs = ["fields.conf", "inputs.conf", "props.conf", "transforms.conf"]
    if include_indexes:
        dest_confs = dest_confs + ["indexes.conf"]

    for dest_conf in dest_confs:
        gdi_conf = ConfParser()
        
        try:
            for file in sorted([ x for x in os.listdir(gdi_tmp) if x.endswith(dest_conf)], reverse=True):
                gdi_conf = gdi_conf.merged_with_file(os.path.join(gdi_tmp, file))
        except FileNotFoundError:
            continue
        finally:
            write_conf(gdi_conf.sorted(), os.path.join(gdi_default, dest_conf))

        if dest_conf == "indexes.conf":
            authorize_conf = ConfParser()
            authentication_conf = ConfParser()
            acs_create_json = {"indexes": []}
            acs_update_json = {"indexes": []}
            acs_delete_json = {"indexes": []}
            authorize_conf["role_user"] = {"srchIndexesAllowed": "_not_exist", "srchIndexesDefault": "not_exist"}
            authorize_conf["role_power"] = {"srchIndexesAllowed": "_not_exist", "srchIndexesDefault": "not_exist"}
            for section in gdi_conf.sorted().sections():
                role_name = "role_allowed_index_{}".format(section)
                acs_create_json["indexes"].append({"name": section, "SearchableDays": 3650})
                acs_update_json["indexes"].append({"name": section, "SearchableDays": 365, "SplunkArchivalRetentionDays": 1095})
                acs_delete_json["indexes"].append({"name": section})
                try:
                    authentication_conf["roleMap_SAML"][role_name.lstrip("role_")] = "<<{}>>".format(section)
                except KeyError:
                    authentication_conf["roleMap_SAML"] = {role_name.lstrip("role_"): "<<{}>>".format(section)}
                authorize_conf[role_name] = {"srchIndexesAllowed": section, "run_collect": "disabled", "run_mcollect": "disabled", "schedule_rtsearch": "disabled"}
            with open(os.path.join(gdi_default, "acs_index_create.json"), "w") as f:
                json.dump(acs_create_json, f, indent=2)
            with open(os.path.join(gdi_default, "acs_index_delete.json"), "w") as f:
                json.dump(acs_delete_json, f, indent=2)
            with open(os.path.join(gdi_default, "acs_index_update.json"), "w") as f:
                json.dump(acs_update_json, f, indent=2)
            write_conf(authorize_conf, os.path.join(gdi_default, "authorize.conf"))
            write_conf(authentication_conf, os.path.join(gdi_default, "authentication.conf"))


    gdi_app_conf = ConfParser()
    gdi_app_conf.read_dict(t_app_conf)
    gdi_app_conf["package"]["id"] = gdi_app
    gdi_app_conf["launcher"]["version"] = "7.7.7"
    gdi_app_conf["launcher"]['author'] = 'Splunk Professional Services'
    gdi_app_conf["launcher"]['description'] = 'Migrated index-time props, index-time transforms, and indexes'
    gdi_app_conf['ui']['label'] = gdi_app
    write_conf(gdi_app_conf.sorted(),  os.path.join(gdi_default, "app.conf"))

    gdi_meta_conf = ConfParser()
    gdi_meta_conf.read_dict(t_metadata_conf)
    gdi_meta_conf["props"] = {"export": "system"}
    gdi_meta_conf["transforms"] = {"export": "system"}
    gdi_meta_conf["indexes"] = {"export": "system"}
    gdi_meta_conf["authorize"] = {"export": "system"}
    write_conf(gdi_meta_conf.sorted(), os.path.join(gdi_default.replace("default", "metadata"), "default.meta"))

    if not preserve_tmp:
        shutil.rmtree(gdi_tmp, ignore_errors=True)

##################################################
# MAKE FORWARDER PROPS APP (ONLY FOR WHOLE DIAG)
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def make_fwd_app(fwd_path, fwd_app, preserve_tmp=True):

    fwd_tmp = os.path.join(fwd_path, "tmp")
    fwd_default = os.path.join(fwd_path, "default")
    dest_confs = ["props.conf"]

    for dest_conf in dest_confs:
        fwd_conf = ConfParser()
        try:
            for file in sorted([ x for x in os.listdir(fwd_tmp) if x.endswith(dest_conf)], reverse=True):
                fwd_conf = fwd_conf.merged_with_file(os.path.join(fwd_tmp, file))
        except FileNotFoundError:
            continue
        finally:
            write_conf(fwd_conf.sorted(), os.path.join(fwd_default, dest_conf))

    fwd_app_conf = ConfParser()
    fwd_app_conf.read_dict(t_app_conf)
    fwd_app_conf["package"]["id"] = fwd_app
    fwd_app_conf["launcher"]["version"] = "7.7.7"
    fwd_app_conf["launcher"]['author'] = 'Splunk Professional Services'
    fwd_app_conf["launcher"]['description'] = 'EVENT_BREAKER derived from LINE_BREAKER'
    fwd_app_conf['ui']['label'] = fwd_app
    write_conf(fwd_app_conf.sorted(),  os.path.join(fwd_default, "app.conf"))

    fwd_meta_conf = ConfParser()
    fwd_meta_conf.read_dict(t_metadata_conf)
    fwd_meta_conf["props"] = {"export": "system"}
    fwd_meta_conf["transforms"] = {"export": "system"}
    fwd_meta_conf["indexes"] = {"export": "system"}
    write_conf(fwd_meta_conf.sorted(), os.path.join(fwd_default.replace("default", "metadata"), "default.meta"))

    if not preserve_tmp:
        shutil.rmtree(fwd_tmp, ignore_errors=True)


##################################################
# MAKE LOOKUP APP (ONLY FOR WHOLE DIAG)
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def make_lookup_app(lookup_path, app_name, preserve_tmp=True):

    lookup_tmp = os.path.join(lookup_path, "tmp")
    lookup_default = os.path.join(lookup_path, "default")
    dest_confs = ["props.conf", "transforms.conf"]

    for dest_conf in dest_confs:
        lookup_conf = ConfParser()
        try:
            for file in sorted([ x for x in os.listdir(lookup_tmp) if x.endswith(dest_conf)]):
                lookup_conf = lookup_conf.merged_with_file(os.path.join(lookup_tmp, file))
        except FileNotFoundError:
            continue
        finally:
            write_conf(lookup_conf.sorted(), os.path.join(lookup_default, dest_conf))

    lookup_app_conf = ConfParser()
    lookup_app_conf.read_dict(t_app_conf)
    lookup_app_conf["package"]["id"] = app_name
    lookup_app_conf["launcher"]["version"] = "7.7.7"
    lookup_app_conf["launcher"]['author'] = 'Splunk Professional Services'
    lookup_app_conf["launcher"]['description'] = 'Migrated lookup transforms'
    lookup_app_conf['ui']['label'] = "Consolidated Lookup Definitions"
    write_conf(lookup_app_conf.sorted(),  os.path.join(lookup_default, "app.conf"))

    lookup_meta_conf = ConfParser()
    lookup_meta_conf.read_dict(t_metadata_conf)
    lookup_meta_conf["props"] = {"export": "system"}
    lookup_meta_conf["transforms"] = {"export": "system"}
    write_conf(lookup_meta_conf.sorted(), os.path.join(lookup_default.replace("default", "metadata"), "default.meta"))

    if not preserve_tmp:
        shutil.rmtree(lookup_tmp, ignore_errors=True)

##################################################
# MAKE SEARCH TIME APP (ONLY FOR WHOLE DIAG)
##################################################
@debug_wrap(ENABLED=bool(False or GBL_DEBUG or DEBUG), TRACE=bool(False or GBL_TRACE or TRACE))
def make_searchtime_app(st_path, app_name, preserve_tmp=True):

    st_tmp = os.path.join(st_path, "tmp")
    st_default = os.path.join(st_path, "default")
    dest_confs = ["eventtypes.conf", "macros.conf", "props.conf", "tags.conf", "transforms.conf"]

    for dest_conf in dest_confs:
        st_conf = ConfParser()
        try:
            for file in sorted([ x for x in os.listdir(st_tmp) if x.endswith(dest_conf)]):
                st_conf = st_conf.merged_with_file(os.path.join(st_tmp, file))
        except FileNotFoundError:
            continue
        finally:
            write_conf(st_conf.sorted(), os.path.join(st_default, dest_conf))

    st_app_conf = ConfParser()
    st_app_conf.read_dict(t_app_conf)
    st_app_conf["package"]["id"] = app_name
    st_app_conf["launcher"]["version"] = "6.6.6"
    st_app_conf["launcher"]['author'] = 'Splunk Professional Services'
    st_app_conf["launcher"]['description'] = 'Migrated Search Time KOs'
    st_app_conf['ui']['label'] = "Consolidated Global Search Time Stuff"
    write_conf(st_app_conf.sorted(),  os.path.join(st_default, "app.conf"))

    st_meta_conf = ConfParser()
    st_meta_conf.read_dict(t_metadata_conf)
    st_meta_conf["eventtypes"] = {"export": "system"}
    st_meta_conf["fields"] = {"export": "system"}
    st_meta_conf["macros"] = {"export": "system"}
    st_meta_conf["props"] = {"export": "system"}
    st_meta_conf["tags"] = {"export": "system"}
    st_meta_conf["transforms"] = {"export": "system"}
    write_conf(st_meta_conf.sorted(), os.path.join(st_default.replace("default", "metadata"), "default.meta"))

    if not preserve_tmp:
        shutil.rmtree(st_tmp, ignore_errors=True)

##################################################
# FROM A WHOLE DIAG
##################################################
if __name__ == "__main__":
    start_time = time.perf_counter()
    import argparse

    cli = argparse.ArgumentParser()
    cli.add_argument('diag', help="Path to the customer provided diag", metavar='DIAG_PATH')
    cli.add_argument('--merge', help="Creates a base custom app and merges KO content into local", choices=["local", "default"], default=False)
    # cli.add_argument('--level', help="How aggressivly to clean the apps", choices=["minimal, normal, high"], default="normal")
    cli.add_argument('--with-appinspect', help="Run the defaults through appinspect", action="store_true")
    # cli.add_argument('--value-for-star', help="To help with multiple search head merges, replace * with this group in metadata")
    # cli.add_argument('--rename-apps', action='store_true')
    # cli.add_argument('--rename-users', action='store_true')
    cli.add_argument('--include', action='append')
    cli.add_argument('--type', action='append')

    args = cli.parse_args()
    args.diag=args.diag.rstrip(os.sep)
    if not args.include:
        args.include = ['apps', 'slave-apps', 'peer-apps', 'system', 'users']
    if not args.type:
        args.type = ['builtin', 'custom', 'splunkbase']

    if valid_diag(args.diag):
        src = re_diag_name.match(os.path.basename(args.diag)).groupdict()
    else:
        print("{!r} is not a valid diag path".format(args.diag))
        exit(1)

    log_dir=os.path.join(os.path.dirname(args.diag), "logs", src['diag'])

    logListenerConfig(debug=bool(DEBUG or GBL_DEBUG), verbose=False, log_dir=log_dir, add_time=False, audit=True)
    logger = logging.getLogger(src["diag"])

    if args.with_appinspect:
        user = input("Provide your Splunkbase username: ")
        passwd = getpass()
        session = AI(user, passwd)
        liverpool = None
    else:
        session = None

    src['root'] = args.diag

    dir_list = []

    src["etc"] = os.path.join(src["root"], "etc")

    # if args.rename_apps:
    #     exit()

    apps = os.path.join(src["etc"], "apps")
    if os.path.isdir(apps) and 'apps' in args.include:
        logger.info("apps directory found -- will process")
        src["apps"] = app_list(apps)

    slave_apps = os.path.join(src["etc"], "slave-apps")
    if os.path.isdir(slave_apps) and 'slave-apps' in args.include:
        logger.info("slave-apps directory found -- will process")
        src["slave-apps"] = app_list(slave_apps)

    peer_apps = os.path.join(src["etc"], "peer-apps")
    if os.path.isdir(peer_apps) and 'peer-apps' in args.include:
        logger.info("peer-apps directory found -- will process")
        src["peer-apps"] = app_list(peer_apps)

    system = os.path.join(src["etc"])
    if os.path.isdir(system) and 'system' in args.include:
        logger.info("system directory found -- will process")
        src["system"] = ["system"]

    users = os.path.join(src["etc"], "users")
    src["users"] = []
    if os.path.isdir(users) and 'users' in args.include:
        logger.info("users directory found -- finding all users")
        for user in user_list(users):
            user_etc = os.path.join(users, user)
            src["users"].append({user: app_list(user_etc)})

    results = process(src, merge=args.merge, include=args.include, what=args.type, session=session)

    for app in sorted([ x for x in results if x['status'] == 'success' ], key=lambda x: ":".join([x['app'], x['app_path']])):
        if app['flavor'] == 'custom':
            # create a default for vetting
            # create a default for install
            # create a first local
            # create a second local
            pass
        elif app['flavor'] in ['builtin', 'splunkbase']:
            # create a fake default for vetting
            # create a first local for install
            # creaate a second local
            pass
        elif app['flavor'] == 'user':
            # get the default app.conf from the apps folder
            # create a fake default for vetting
            # create a local for installing
            pass

    ##################################################
    # CREATE RESULTS CSV FILE
    ##################################################
    filename = os.path.join(os.path.dirname(args.diag), "applist-{}-{}-{}.csv".format(src['diag'], "_".join(args.include), "_".join(args.type)))
    fieldnames = ['host', 'diag', 'app', 'version', 'app_path', 'flavor', 'status', 'result', 'has_generating_searches']
    with open(filename, "w", newline='') as f:
        writer = csv.DictWriter(f, extrasaction='ignore', fieldnames=fieldnames, dialect='excel')
        writer.writeheader()
        for result in sorted(results, key=lambda x: ":".join([x['app'], x['app_path']])):
            try:
                if result['generating_searches']:
                    result.update({'has_generating_searches': len(result['generating_searches'])})
                else:
                    result.update({'has_generating_searches': 0})
            except KeyError:
                result.update({'has_generating_searches': 0})
            result.update({'host': src['host'], 'diag': src['diag']})
            writer.writerow(result)

    ##################################################
    # CREATE THE LIST OF GENERATING SEARCHES
    ##################################################
    filename = os.path.join(os.path.dirname(args.diag), "generating_searches-{}-{}-{}.csv".format(src['diag'], "_".join(args.include), "_".join(args.type)))
    fieldnames = ['host', 'diag', 'app', 'version', 'app_path', 'flavor', 'search_name', 'generates']
    with open(filename, "w", newline='') as f:
        writer = csv.DictWriter(f, extrasaction='ignore', fieldnames=fieldnames, dialect='excel')
        writer.writeheader()
        for result in [ r for r in sorted(results, key=lambda x: ":".join([x['app'], x['app_path']])) if r['has_generating_searches'] > 0]:
            for search_name, generates in sorted(result['generating_searches'], key=lambda x: x[0]):
                _record = dict(result)
                _record.update({'search_name': search_name, 'generates': generates})
                writer.writerow(_record)

    ##################################################
    # CREATE TAR FILES
    ##################################################
    for result in [ x for x in results if x['status'] == 'completed']:

        if result['flavor'] in ['splunkbase', 'builtin', 'user']:
            tars = ['local_only']
            merge = ""
        else:
            tars = ['full', 'default_only', 'local_only']
            if args.merge:
                merge = "-merged_to_{}".format(args.merge)
            else:
                merge = '-not_merged'

        for arctype in tars:
            inpath = result['dest_path']
            outpath = os.path.join(os.path.dirname(src['root']), "{}_{}".format(src['diag'], "tarfiles" + merge), arctype)
            desc = os.path.basename(os.path.dirname(result['app_path'])) + "-" + result['flavor'] + merge
            if arctype == 'full':
                checksum = result['checksum']['full'][-6:]
            else:
                checksum = result['checksum']['full'][-6:] + "-" + result['checksum'][arctype][-6:]
            r = create_tarfile(inpath, outpath, result['app'], src['host'], checksum, desc, arctype=arctype)
            logger.info("TARFILE CREATED: {}".format(r))

    end_time = time.perf_counter()
    run_time = end_time - start_time
    minutes, seconds = divmod(run_time, 60)
    logger.info("Finished processing {!r} in {:.0f} minutes {:0>2.4f} seconds".format(
                src['diag'], minutes, seconds))

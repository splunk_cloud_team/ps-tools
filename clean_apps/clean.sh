export PYTHONPATH=../lib
# python minimal.py /home/doymi01/Code/python-projects/spglobal2/diag-va06mhf5501.mhf.mhc-2022-01-05_14-56-16
# python minimal.py /home/doymi01/Code/python-projects/spglobal2/diag-nj12mhf5904.mhf.mhc-2022-01-05_14-56-57
# python minimal.py /home/doymi01/Code/python-projects/pppl/diags/diag-splunk-sh1.pppl.gov-2022-08-15_11-46-28
# python minimal.py /home/doymi01/Code/python-projects/pppl/diags/diag-splunk-sh1.pppl.gov-2022-08-17_11-10-48 --merge
# python minimal.py /home/doymi01/Code/python-projects/pppl/diags/diag-splunksrv3.pppl.gov-2022-08-15_11-48-14 --merge
# python minimal.py /home/doymi01/Code/python-projects/hca/diags/diag-xrdclpspas01003.unix.medcity.net-2022-04-25_10-25-36 --merge
# python minimal.py /home/doymi01/Code/python-projects/hca/diags/diag-xrdclpspes01001.unix.medcity.net-2022-07-29_08-54-50 
# python minimal.py /home/doymi01/Code/python-projects/bls/diags/diag-cfsplnkindx1-2022-07-18_16-14-32 --merge default
# python minimal.py /home/doymi01/Code/python-projects/bls/diags/diag-cfsplnksearch-2022-07-22_09-14-45 --merge local
# python minimal.py /home/doymi01/Code/python-projects/hca/cloud_diags/diag-sh-i-012c9f9323ea149de-2022-08-01_13-34-44
# python minimal.py /home/doymi01/Code/python-projects/mufgbu/2022-03-25/diag-chelmprd206.unionbank.com-2022-02-25_16-33-37
# python minimal.py /home/doymi01/Code/python-projects/doc-esoc/diags/diag-wva-esoc-idx02.csp.noaa.gov-2022-08-29_18-07-09 --merge 
# for i in _tarfiles _custom _builtin _user _splunkbase _vetted; do rm -rf /home/doymi01/Code/python-projects/doc-esoc/diags/diag-fmt-pesp-esv01.csp.noaa.gov-2022-08-29_18-20-24${i}; done
# python minimal.py /home/doymi01/Code/python-projects/doc-esoc/diags/diag-fmt-pesp-esv01.csp.noaa.gov-2022-08-29_18-20-24 --merge local
# python minimal.py /home/doymi01/Code/python-projects/prudential/diags/diag-c0m1-i-0dd0f6e94a334a29f-2022-09-13_21-36-18

### CIGNA
# for i in _tarfiles _custom _builtin _user _splunkbase _GDI; do rm -rf /home/doymi01/Code/python-projects/cigna/diags/diag-ch3pr609215-2022-09-09_08-45-08${i}; done
# python minimal.py /home/doymi01/Code/python-projects/cigna/diags/diag-ch3pr609215-2022-09-09_08-45-08
for i in _tarfiles _custom _builtin _user _splunkbase _GDI; do rm -rf /home/doymi01/Code/python-projects/cigna/diags/diag-ps2ar025561-2022-09-09_08-42-29${i}; done
python minimal.py /home/doymi01/Code/python-projects/cigna/diags/diag-ps2ar025561-2022-09-09_08-42-29 --merge default
for i in _tarfiles _custom _builtin _user _splunkbase _GDI; do rm -rf /home/doymi01/Code/python-projects/cigna/diags/diag-ps2ar025563-2022-09-09_08-23-00${i}; done
python minimal.py /home/doymi01/Code/python-projects/cigna/diags/diag-ps2ar025563-2022-09-09_08-23-00 --merge default
for i in _tarfiles _custom _builtin _user _splunkbase _GDI; do rm -rf /home/doymi01/Code/python-projects/cigna/diags/diag-ps2ar025631-2022-09-09_08-34-07${i}; done
python minimal.py /home/doymi01/Code/python-projects/cigna/diags/diag-ps2ar025631-2022-09-09_08-34-07 --merge default
for i in _tarfiles _custom _builtin _user _splunkbase _GDI; do rm -rf /home/doymi01/Code/python-projects/cigna/diags/diag-ps2pr609510-2022-09-09_08-27-30${i}; done
python minimal.py /home/doymi01/Code/python-projects/cigna/diags/diag-ps2pr609510-2022-09-09_08-27-30 --merge default
for i in _tarfiles _custom _builtin _user _splunkbase _GDI; do rm -rf /home/doymi01/Code/python-projects/cigna/diags/diag-cilsplkp0202-2022-09-09_09-51-58${i}; done
python minimal.py /home/doymi01/Code/python-projects/cigna/diags/diag-cilsplkp0202-2022-09-09_09-51-58 --merge default
for i in _tarfiles _custom _builtin _user _splunkbase _GDI; do rm -rf /home/doymi01/Code/python-projects/cigna/diags/diag-cilsplkp0005-2022-09-09_09-52-35${i}; done
python minimal.py /home/doymi01/Code/python-projects/cigna/diags/diag-cilsplkp0005-2022-09-09_09-52-35 --merge default


# /home/doymi01/Code/python-projects/cigna/diags/diag-cilsplkp0202-2022-09-09_09-51-58

# for i in _tarfiles _custom _builtin _user _splunkbase; do rm -rf /home/doymi01/Code/python-projects/cigna/diags/diag-ps2ar025563-2022-09-09_08-23-00${i}; done
# python minimal.py /home/doymi01/Code/python-projects/cigna/diags/diag-ps2ar025563-2022-09-09_08-23-00

# for i in _tarfiles _custom _builtin _user _splunkbase; do rm -rf /home/doymi01/Code/python-projects/cigna/diags/diag-cilsplkp0202-2022-09-09_09-51-58${i}; done
# python minimal.py /home/doymi01/Code/python-projects/cigna/diags/diag-cilsplkp0202-2022-09-09_09-51-58
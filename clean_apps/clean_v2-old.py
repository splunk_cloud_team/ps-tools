DEBUG=True
import datetime
import json
import logging
import os
import pickle
import posixpath
import re
import shutil
import statistics
import sys
import tarfile
from configparser import NoOptionError, NoSectionError, ParsingError
from hashlib import md5, sha1
from urllib.parse import quote, quote_plus
from xml.etree.ElementTree import ParseError

import numpy as np
from ps_common import *
from ps_common.appinspect import *
from ps_common.classes import ConfParser
# from ps_common.decorators import debug_wrapper
from ps_common.fix_xml import fix_and_write_xml
from ps_common.logger import logListenerConfig
from ps_common.utils import isSplunkbaseApp
from ps_common.appinspect.classes import Session as AI, Liverpool

from consts import GBL_DEBUG, GBL_TRACE
from utils import *

try:
    log_dir=os.path.join("logs", os.path.basename(sys.argv[1]))
except IndexError:
    log_dir="log"

from built_in_apps import *
# from clean import fix_indexes, process_custom, process_splunkbase
from constraints import *
from consts import *
from slim import *
from templates import *
from utils import *
from consts import GBL_DEBUG, GBL_TRACE

@debug_wrap(ENABLED=bool(DEBUG or GBL_DEBUG), TRACE=bool(False or GBL_TRACE))
def process_apps(src, merge=False, session=None, liverpool=None):
    now = datetime.now()
    _time = now.strftime("%Y-%m-%d_%H-%M-%S")

    for k in [ x for x in src.keys() if x in ["apps", "slave-apps"]]:
        for app in src[k]:
            tar_dest = os.path.join(src["root"], src["diag"] + "_tarfiles")
            _host = re.match(r"diag-(\S+)-\d{4}-\d{2}-\d{2}_\d{2}-\d{2}-\d{2}", src["diag"])
            if _host:
                host = _host.group(1)
            else:
                raise SystemExit("This utility expects the diag diretory to be named diag-hostname-YYYY-MM-DD-HH-MM-SS")
            app_path = os.path.join(k, app)
            src_path = os.path.join(src["etc"], app_path)
            logger = logging.getLogger(app)
            if app in warning_apps:
                logger.error("--THIS APP REQUIRES SPECIAL HANDLING--")

            # Splunkbase and Custom apps
            if app not in builtin_apps:
                try:
                    # is this a splunkbase app
                    _splunkbase, result = isSplunkbaseApp(app, details=True)

                    if _splunkbase:
                        logger = logging.getLogger(app_path+" (SPLUNKBASE)")
                    else:
                        logger = logging.getLogger(app_path)

                    if not _splunkbase:
                        flavor="custom"
                        sha1_hash, version, dest_path = process_app_custom(app_path, src_path, diag, merge)


                    else:
                        flavor = "splunkbase"
                        sha1_hash, version, dest_path = process_app_splunkbase(app_path, src_path, diag, result)

                except AppDisabledError as e:
                    logger.warning(repr(e))
                    shutil.rmtree(src_path.replace(diag, "{}_{}".format(diag, flavor)), ignore_errors=True)
                    continue
                except (EmptyAppError, NoChanges) as e:
                    if bool(GBL_DEBUG or DEBUG):
                        logger.exception("Exception: {}".format(repr(e)))
                    continue
                except Exception as e:
                    if bool(GBL_DEBUG or DEBUG):
                        logger.exception("Exception: {}: App not processed successfully.".format(repr(e)))
                    else:
                        logger.error("Exception: {}: App not processed successfully.".format(repr(e)))
                    shutil.rmtree(src_path.replace(diag, "{}_{}".format(diag, flavor)), ignore_errors=True)
                    continue

            # builtin and premium apps
            else:
                try:
                    flavor = "builtin"
                    sha1_hash, version, dest_path = process_app_builtin(app_path, src_path, diag)
                except AppDisabledError as e:
                    logger.error("Skipped Disabled App")
                    shutil.rmtree(src_path.replace(diag, "{}_{}".format(diag, flavor)), ignore_errors=True)
                    continue
                except (EmptyAppError, NoChanges) as e:
                    if bool(GBL_DEBUG or DEBUG):
                        logger.exception("Exception: {}".format(repr(e)))
                    continue
                except Exception as e:
                    if bool(GBL_DEBUG or DEBUG):
                        logger.exception("Exception: {}: App not processed successfully.".format(repr(e)))
                    else:
                        logger.error("Exception: {}: App not processed successfully.".format(repr(e)))
                    shutil.rmtree(src_path.replace(diag, "{}_{}".format(diag, flavor)), ignore_errors=True)
                    continue

            if sha1_hash:
                this_tar_dest = os.path.join(tar_dest, flavor)
                os.makedirs(this_tar_dest, exist_ok=True)

                if version:
                    specs = "_".join(
                        [
                            sha1_hash[-6:],
                            host,
                            _time,
                            flavor,
                            version
                        ]
                    )
                    app_tar_name = "-".join(
                        [
                            app,
                            specs
                        ]
                    ) + ".tgz"

                    app_tarfile = os.path.join(this_tar_dest, app_tar_name)
                    with tarfile.open(app_tarfile, "w:gz") as f:
                        for dir in allowed_app_dirs + ["default", os.path.join("metadata", "default.meta")]:
                            try:
                                f.add(os.path.join(dest_path, dir), arcname=os.path.join(app, dir), recursive=True)
                            except FileNotFoundError:
                                continue
                    if session:
                        if session.process([app_tarfile]):
                            vetted_path = app_tarfile.replace(os.sep + app + os.sep, os.sep).replace("_tarfiles", "_vetted")
                            os.makedirs(os.path.dirname(vetted_path), exist_ok=True)
                            shutil.move(app_tarfile, vetted_path)
                            # create the liverpool appcert just in case
                            # must be on VPN :(
                            if liverpool:
                                liverpool.uploadApp(vetted_path)

                        else:
                            os.unlink(app_tarfile)

                    # create a destination dir by app hash.
                    # for dest, sub in [("by_app", app), ("by_hash", sha1_hash[-6:])]:
                    #     tar_file = os.path.join(src["root"], dest, sub, app_tar_name)
                    #     os.makedirs(os.path.dirname(tar_file), exist_ok=True)
                    #     shutil.copyfile(app_tarfile, tar_file)

                specs = "_".join(
                        [
                            sha1_hash[-6:],
                            host,
                            _time,
                            flavor,
                            "local-only"
                        ]
                    )
                local_tar_name = "-".join(
                    [
                        app,
                        specs
                    ]
                ) + ".tgz"

                local_tarfile = os.path.join(this_tar_dest, local_tar_name)
                with tarfile.open(local_tarfile, "w:gz") as f:
                    for dir in ["local", os.path.join("metadata", "local.meta")]:
                        try:
                            f.add(os.path.join(dest_path, dir), arcname=os.path.join(app, dir), recursive=True)
                        except FileNotFoundError:
                            continue

                # create a destination dir by app hash.
                for dest, sub in [("by_app", app), ("by_hash", sha1_hash[-6:])]:
                    tar_file = os.path.join(src["root"], dest, sub, local_tar_name)
                    os.makedirs(os.path.dirname(tar_file), exist_ok=True)
                    shutil.copyfile(local_tarfile, tar_file)

    # all the apps are now processed
    # combine the GDI props and transforms
    # create the app.conf for it and tar it up
    for path in ["apps", "slave-apps"]:
        try:
            gdi_path = os.path.join(src["root"], src["diag"] + "_custom", "etc", path, "A00-MIGRATED-GDI", "tmp")
            gdi_default = gdi_path.replace("tmp", "default")
            for dest_conf in ["props.conf", "transforms.conf", "indexes.conf"]:
                gdi_props_conf = ConfParser()
                try:
                    for file in sorted([ x for x in os.listdir(gdi_path) if x.endswith(dest_conf)], reverse=True):
                        gdi_props_conf = gdi_props_conf.merged_with_file(os.path.join(gdi_path, file))
                except FileNotFoundError:
                    continue

                write_conf(gdi_props_conf.sorted(), os.path.join(gdi_default, dest_conf))

#            shutil.rmtree(gdi_path, ignore_errors=True)
            gdi_app_conf = ConfParser()
            gdi_app_conf.read_dict(t_app_conf)
            gdi_app_conf["package"]["id"] = "A00-MIGRATED-GDI"
            gdi_app_conf["launcher"]["version"] = "7.7.7"
            gdi_app_conf['ui']['label'] = "A00-MIGRATED-GDI"
            write_conf(gdi_app_conf.sorted(),  os.path.join(gdi_default, "app.conf"))

            gdi_meta_conf = ConfParser()
            gdi_meta_conf.read_dict(t_metadata_conf)
            gdi_meta_conf["props"] = {"export": "system"}
            gdi_meta_conf["transforms"] = {"export": "system"}
            write_conf(gdi_meta_conf.sorted(), os.path.join(gdi_default.replace("default", "metadata"), "default.meta"))

        except Exception as e:
            logger.exception(repr(e))
            shutil.rmtree(os.path.dirname(gdi_path), ignore_errors=True)

    # user KOs
    for z in [ x for x in src["users"] if True]:
        for k in [ x for x in z.keys() if True]:
            for app in z[k]:
                app_path = os.path.join(k, app)
                src_path = os.path.join(src["etc"], "users", app_path)
                try:
                    sha1_hash, version, dest_path = process_app_user(app_path, src_path, diag)
                except (EmptyAppError, NoChanges):
                    continue
                except Exception as e:
                    if bool(GBL_DEBUG or DEBUG):
                        logger.exception("Exception: {}: App not processed successfully.".format(repr(e)))
                    else:
                        logger.error("Exception: {}: App not processed successfully.".format(repr(e)))
                    shutil.rmtree(src_path.replace(diag, "{}_{}".format(diag, flavor)), ignore_errors=True)
                    continue

if __name__ == "__main__":

    import argparse

    cli = argparse.ArgumentParser()
    cli.add_argument('diag', help="Path to the customer provided diag", metavar='DIAG_PATH')
    cli.add_argument('--merge', help="Creates a base custom app and merges KO content into local", action="store_true")
    cli.add_argument('--debug', help="Debug Logging", action="store_true")
    # cli.add_argument('--start-over', help="Reinitializes all the output folders. This causes new tarfiles to build.", action="store_true")
    vet_group = cli.add_mutually_exclusive_group()
    vet_group.add_argument('--with-appinspect', help="Runs the fixed app through appinspect (slow)", action="store_true")
    vet_group.add_argument('--with-liverpool', help="Creates APPCERT for the fixed app -- You must be on VPN for this to work", action="store_true")

    # cli.add_argument('--cron', help="Improve cron schedule and skew (EXPERIMENTAL)", action="store_true")
    # cli.add_argument('--merge', help="EXPERIMENTAL", choices=['default', 'local'], default=None)
    args = cli.parse_args()

    if args.with_liverpool:
        print("You have selected the liverpool (APPCERT) option. This ONLY works on the Splunk VPN.")
        while True:
            ans = input("Are you connected to the VPN? (y/n) ")
            if ans.upper()[0] == "Y":
                break
            elif ans.upper()[0] == "N":
                print("You can still use the appinspect option, or try again when you are on the VPN")
                exit(1)

    logListenerConfig(debug=args.debug, verbose=False, log_dir=log_dir, add_time=False, audit=True)
    logger = logging.getLogger(__name__)

    if not os.path.isdir(os.path.join(args.diag, "etc")):
        raise SystemExit("Invalid diag path: '{}'".format(args.diag))

    root_path, diag = (os.path.dirname(args.diag), os.path.basename(args.diag))

    src = {"root": root_path, "diag": diag}

    dir_list = []

    src["etc"] = os.path.join(src["root"], src["diag"], "etc")

    apps = os.path.join(src["etc"], "apps")
    if os.path.isdir(apps):
        logger.info("apps directory found -- will process")
        src["apps"] = app_list(apps)

    slave_apps = os.path.join(src["etc"], "slave-apps")
    if os.path.isdir(slave_apps):
        logger.info("slave-apps directory found -- will process")
        src["slave-apps"] = app_list(slave_apps)

    # system = os.path.join(src["etc"], "system")
    # if os.path.isdir(system):
    #     logger.info("system directory found -- will process")
    #     src["system"] = "system"

    users = os.path.join(src["etc"], "users")
    src["users"] = []
    if os.path.isdir(users):
        logger.info("users directory found -- finding all users")
        # src["users"] = []
        for user in user_list(users):
            user_etc = os.path.join(users, user)
            src["users"].append({user: app_list(user_etc)})
    
    if args.with_liverpool:
        user = input("Provide your Splunkbase username: ")
        passwd = getpass()
        session = AI(user, passwd)
        liverpool = Liverpool(user, passwd)
    elif args.with_appinspect:
        user = input("Provide your Splunkbase username: ")
        passwd = getpass()
        session = AI(user, passwd)
        liverpool = None
    else:
        session = None
        liverpool = None

    process_apps(src, merge=args.merge, session=session, liverpool=liverpool)


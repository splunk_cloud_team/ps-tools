import json
import logging
import os
import pickle
import posixpath
import re
import shutil
import statistics
import sys
import tarfile
from configparser import NoOptionError, NoSectionError
from hashlib import md5, sha1

import lib_path

lib_path.make_lib_path()

from ps_common.classes import ConfParser
from ps_common.fix_xml import fix_and_write_xml
from ps_common.logger import logListenerConfig
from ps_common.utils import isSplunkbaseApp

logListenerConfig(debug=True, verbose=True)
logger = logging.getLogger(__name__)
logger.debug("hello")

if __name__ == "__main__":
    _tmp = ConfParser(allow_no_value=True, defaults=None, strict=False, interpolation=None)
    _tmp.load(sys.argv[1])
    _result = _tmp.merged_with_file(sys.argv[2])
    print(_result)

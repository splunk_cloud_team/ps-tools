t_slim_manifest = {
    "schemaVersion": "2.0.0",
    "info": {
        "title": None,
        "id": {
            "group": None,
            "name": "",
            "version": ""
            },
        "author": [
            {"name": "",
            "email": None,
            "company": None
            }
            ],
        "releaseDate": None,
        "description": "",
        "classification": {
        "intendedAudience": None,
        "categories": [],
        "developmentStatus": None
        },
        "commonInformationModels": None,
        "license": {
            "name": None,
            "text": None,
            "uri": None
            },
        "privacyPolicy": {
            "name": None,
            "text": None,
            "uri": None
            },
        "releaseNotes": {
            "name": None,
            "text": None,
            "uri": None
            }
        },
    "dependencies": None,
    "tasks": None,
    "inputGroups": None,
    "incompatibleApps": None,
    "platformRequirements": None,
    "supportedDeployments": [
        "_standalone",
        "_distributed",
        "_search_head_clustering"
        ],
    "targetWorkloads": [
        "_search_heads"
        ]
    }

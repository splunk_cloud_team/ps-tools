import json
import logging
import os
import pickle
import posixpath
import re
import shutil
import statistics
import sys
import tarfile
from configparser import NoOptionError, NoSectionError
from getpass import getpass
from hashlib import md5, sha1
from threading import Thread

import lib_path

lib_path.make_lib_path()

from ps_common.appinspect.classes import Session as AI
from ps_common.classes import ConfParser
from ps_common.fix_xml import fix_and_write_xml
from ps_common.logger import logListenerConfig
from ps_common.utils import isSplunkbaseApp

try:
    log_dir = sys.argv[2]
except IndexError:
    log_dir = "log"

logListenerConfig(debug=True, verbose=True, log_dir=log_dir)
logger = logging.getLogger(__name__)

custom_app = re.compile('\.custom\.tmp-')

if __name__ == "__main__":
    from argparse import ArgumentParser
    cli = ArgumentParser()
    cli.add_argument("app", help="path to the app to vet")
    args = cli.parse_args()

    user = input("Provide your Splunkbase username: ")
    passwd = getpass()
    
    s = AI(user, passwd)

    logger.info("Processing {}".format(args.app))
    s.process([args.path])
    
    print("{} done.".format(sys.argv[0]))

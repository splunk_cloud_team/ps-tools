import argparse
import json
import logging
import os
import pickle
import posixpath
import re
import shutil
import statistics
import sys
import tarfile
from configparser import NoOptionError, NoSectionError
from hashlib import md5, sha1

import lib_path

lib_path.make_lib_path()

from ps_common.classes import ConfParser
from ps_common.fix_xml import fix_and_write_xml
from ps_common.logger import logListenerConfig
from ps_common.utils import isSplunkbaseApp

logListenerConfig(debug=True, verbose=True)
logger = logging.getLogger(__name__)

def process(app_path, conf_file):
    _orig_default = ConfParser(allow_no_value=True, defaults=None, strict=False, interpolation=None)
    _orig_default.load(os.path.join(app_path, "default", conf_file))
    _orig_local = ConfParser(allow_no_value=True, defaults=None, strict=False, interpolation=None)
    _orig_local.load(os.path.join(app_path, "local", conf_file))
    _orig_merged = _orig_default.merged_with(_orig_local)

    # _new_default = ConfParser(allow_no_value=True, defaults=None, strict=False, interpolation=None)
    # _new_default.load(os.path.join(new_path, "default", conf_file))
    # _new_local = ConfParser(allow_no_value=True, defaults=None, strict=False, interpolation=None)
    # _new_local.load(os.path.join(new_path, "local", conf_file))
    # _new_merged = _new_default.merged_with(_new_local)

    return _orig_merged

if __name__ == "__main__":
    from argparse import ArgumentParser
    cli = ArgumentParser()
    cli.add_argument("app_path", help="the path to the app")
    cli.add_argument("conf_file", help="the specific conf file (e.g. savedsearches.conf")

    args = cli.parse_args()

    _result = process(args.app_path, args.conf_file)
    print(_result.sorted())

    # # _tmp = ConfParser(allow_no_value=True, defaults=None, strict=False, interpolation=None)
    # _out = ConfParser(allow_no_value=True, defaults=None, strict=False, interpolation=None)
    # # _tmp.load(sys.argv[1])
    # # _result = _tmp.diff_with_file(sys.argv[2])

    # t = ["##--NO_SECTION--##", "##--NO_NAME--##", "default", "global"]
    # s = [x for x in sorted(_result.sections()) if x not in ["default", "global", "##--NO_SECTION--##", "##--NO_NAME--##"]]
    # t.extend(s)
    # for section in t:
    #     try:
    #         _out[section] = _result[section]
    #     except KeyError:
    #         pass
    # print(_out)

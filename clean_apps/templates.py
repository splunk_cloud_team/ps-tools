import re
from ps_common.classes import ConfParser

FILE_MODE = 0o600
DIR_MODE = 0o700

re_app_default = re.compile("/([^/]+)/default/app\.conf")
re_app_local = re.compile("/([^/]+)/local/app\.conf")
re_meta_default = re.compile("/([^/]+)/metadata/default\.meta")
re_meta_local = re.compile("/([^/]+)/metadata/local\.meta")

t_any_conf = ConfParser(allow_no_value=True, defaults=None, strict=False, interpolation=None)

t_app_conf = ConfParser(allow_no_value=True, defaults=None, strict=False, interpolation=None)
t_app_conf.read_dict({
    'install': {'is_configured': '0','state': 'enabled', 'build': '1'},
    'launcher': {'author': 'Splunk Professional Services', 'description': 'App prepared for SplunkCloud migration', 'version':'1.0.0'},
    'package': {'id': '', 'check_for_updates': '0'},
    'ui': {'is_visible': '0', 'label': ''}})

# t_app_conf.read_dict({
#     'id': {'name': '', 'version': ''},
#     'install': {'is_configured': '0','state': 'enabled', 'build': '1'},
#     'launcher': {'author': 'Splunk Professional Services', 'description': 'App prepared for SplunkCloud migration', 'version':'1.0.0'},
#     'package': {'id': '', 'check_for_updates': '0'},
#     'ui': {'is_visible': '0', 'label': ''}})

t_shc_app_conf = ConfParser(allow_no_value=True, defaults=None, strict=False, interpolation=None)
t_shc_app_conf.read_dict({
    'shclustering': {'deployer_push_mode': 'full'}})

t_builtin_app_conf = ConfParser(allow_no_value=True, defaults=None, strict=False, interpolation=None)
t_builtin_app_conf.read_dict({
    'shclustering': {'deployer_push_mode': 'local_only'}})

t_savedsearches_conf = ConfParser(allow_no_value=True, defaults=None, strict=False, interpolation=None)
t_savedsearches_conf.read_dict({
    'default': {
        'dispatchAs': 'user',
        'allow_skew': '99%',
        'schedule_window': 'auto',
        'action.summary_index.origin_app': '',
        'dispatch.indexedRealtime': 'true',
        'request.ui_dispatch_app': ''}})

t_metadata_conf = ConfParser(allow_no_value=True, defaults=None, strict=False, interpolation=None)
t_metadata_conf.read_dict({
    '##--NO_NAME--##': {
        'access': 'read : [ * ], write : [ admin, sc_admin ]',
        'export': 'none'},
    'app': {
        'access': 'read : [ * ], write : [ admin, sc_admin ]',
        'export': 'system',
        'owner': 'admin'}})

t_sa_cim_dma_local_conf = ConfParser(allow_no_value=True, defaults=None, strict=False, interpolation=None)
t_sa_cim_dma_local_conf.read_dict({
    'default': {
        'acceleration.allow_skew': '99%',
		'acceleration.cron_schedule': '*/5 * * * *',
        'acceleration.max_concurrent': '4',
        'acceleration.max_time': '800'
    },
    'Alerts': {
        'acceleration.allow_skew': '99%',
        'acceleration.cron_schedule': '*/5 * * * *',
        'acceleration.max_concurrent': '4',
        'acceleration.max_time': '800'
    },
    'Application_State': {
        'acceleration.allow_skew': '99%',
		'acceleration.cron_schedule': '*/5 * * * *',
        'acceleration.max_concurrent': '4',
        'acceleration.max_time': '800'
    },
    'Authentication': {
        'acceleration.allow_skew': '99%',
		'acceleration.cron_schedule': '*/5 * * * *',
        'acceleration.max_concurrent': '4',
        'acceleration.max_time': '800'
    },
    'Certificates': {
        'acceleration.allow_skew': '99%',
		'acceleration.cron_schedule': '*/5 * * * *',
        'acceleration.max_concurrent': '4',
        'acceleration.max_time': '800'
    },
    'Change': {
        'acceleration.allow_skew': '99%',
		'acceleration.cron_schedule': '*/5 * * * *',
        'acceleration.max_concurrent': '4',
        'acceleration.max_time': '800'
    },
    'Change_Analysis': {
        'acceleration.allow_skew': '99%',
		'acceleration.cron_schedule': '*/5 * * * *',
        'acceleration.max_concurrent': '4',
        'acceleration.max_time': '800'
    },
    'Compute_Inventory': {
        'acceleration.allow_skew': '99%',
		'acceleration.cron_schedule': '*/5 * * * *',
        'acceleration.max_concurrent': '4',
        'acceleration.max_time': '800'
    },
    'Databases': {
        'acceleration.allow_skew': '99%',
		'acceleration.cron_schedule': '*/5 * * * *',
        'acceleration.max_concurrent': '4',
        'acceleration.max_time': '800'
    },
    'DLP': {
        'acceleration.allow_skew': '99%',
		'acceleration.cron_schedule': '*/5 * * * *',
        'acceleration.max_concurrent': '4',
        'acceleration.max_time': '800'
    },
    'Data_Access': {
        'acceleration.allow_skew': '99%',
		'acceleration.cron_schedule': '*/5 * * * *',
        'acceleration.max_concurrent': '4',
        'acceleration.max_time': '800'
    },
    'Endpoint': {
        'acceleration.allow_skew': '99%',
		'acceleration.cron_schedule': '*/5 * * * *',
        'acceleration.max_concurrent': '4',
        'acceleration.max_time': '800'
    },
    'Event_Signatures': {
        'acceleration.allow_skew': '99%',
		'acceleration.cron_schedule': '*/5 * * * *',
        'acceleration.max_concurrent': '4',
        'acceleration.max_time': '800'
    },
    'Email': {
        'acceleration.allow_skew': '99%',
		'acceleration.cron_schedule': '*/5 * * * *',
        'acceleration.max_concurrent': '4',
        'acceleration.max_time': '800'
    },
    'Interprocess_Messaging': {
        'acceleration.allow_skew': '99%',
		'acceleration.cron_schedule': '*/5 * * * *',
        'acceleration.max_concurrent': '4',
        'acceleration.max_time': '800'
    },
    'Intrusion_Detection': {
        'acceleration.allow_skew': '99%',
		'acceleration.cron_schedule': '*/5 * * * *',
        'acceleration.max_concurrent': '4',
        'acceleration.max_time': '800'
    },
    'JVM': {
        'acceleration.allow_skew': '99%',
		'acceleration.cron_schedule': '*/5 * * * *',
        'acceleration.max_concurrent': '4',
        'acceleration.max_time': '800'
    },
    'Malware': {
        'acceleration.allow_skew': '99%',
		'acceleration.cron_schedule': '*/5 * * * *',
        'acceleration.max_concurrent': '4',
        'acceleration.max_time': '800'
    },
    'Network_Resolution': {
        'acceleration.allow_skew': '99%',
		'acceleration.cron_schedule': '*/5 * * * *',
        'acceleration.max_concurrent': '4',
        'acceleration.max_time': '800'
    },
    'Network_Sessions': {
        'acceleration.allow_skew': '99%',
		'acceleration.cron_schedule': '*/5 * * * *',
        'acceleration.max_concurrent': '4',
        'acceleration.max_time': '800'
    },
    'Network_Traffic': {
        'acceleration.allow_skew': '99%',
		'acceleration.cron_schedule': '*/5 * * * *',
        'acceleration.max_concurrent': '4',
        'acceleration.max_time': '800'
    },
    'Performance': {
        'acceleration.allow_skew': '99%',
		'acceleration.cron_schedule': '*/5 * * * *',
        'acceleration.max_concurrent': '4',
        'acceleration.max_time': '800'
    },
    'Splunk_Audit': {
        'acceleration.allow_skew': '99%',
		'acceleration.cron_schedule': '*/5 * * * *',
        'acceleration.max_concurrent': '4',
        'acceleration.max_time': '800'
    },
    'Ticket_Management': {
        'acceleration.allow_skew': '99%',
		'acceleration.cron_schedule': '*/5 * * * *',
        'acceleration.max_concurrent': '4',
        'acceleration.max_time': '800'
    },
    'Updates': {
        'acceleration.allow_skew': '99%',
		'acceleration.cron_schedule': '*/5 * * * *',
        'acceleration.max_concurrent': '4',
        'acceleration.max_time': '800'
    },
    'Vulnerabilities': {
        'acceleration.allow_skew': '99%',
		'acceleration.cron_schedule': '*/5 * * * *',
        'acceleration.max_concurrent': '4',
        'acceleration.max_time': '800'
    },
    'Web': {
        'acceleration.allow_skew': '99%',
		'acceleration.cron_schedule': '*/5 * * * *',
        'acceleration.max_concurrent': '4',
        'acceleration.max_time': '800'
    }})

t_serverclass_conf = ConfParser(allow_no_value=True, defaults=None, strict=False, interpolation=None)
t_serverclass_conf.read_dict({
    'global': {
        'crossServerChecksum': 'true'
    },
    'serverClass:zzz_All_Splunk_Instances': {
        'whitelist.0': '*'
    },
    'serverClass:zzz_Base_Endpoint_Forwarder': {
        'whitelist.0': '*',
        'blacklist.0': '*:CLOUD_IF(?=:|$)',
        'blacklist.1': '*:CLOUD_HF(?=:|$)',
        'blacklist.2': '*:SYSLOG(?=:|$)'
    },
    'serverClass:zzz_Base_Splunk_Cloud_Intermediate_Forwarder': {
        'whitelist.0': '*:CLOUD_IF(?=:|$)'
    },
    'serverClass:zzz_Base_Splunk_Cloud_Utility_Heavy_Forwarder': {
        'whitelist.0': '*:CLOUD_HF(?=:|$)'
    },
    'serverClass:zzz_Base_Syslog_Forwarder': {
        'whitelist.0': '*:SYSLOG(?=:|$)'
    },
    'serverClass:OS_Windows': {
        'whitelist.0': '*',
        'machineTypesFilter': 'windows-*'   
    },
    'serverClass:OS_Linux': {
        'whitelist.0': '*',
        'machineTypesFilter': 'linux-*'   
    },
    'serverClass:OS_MacOs': {
        'whitelist.0': '*',
        'machineTypesFilter': 'darwin-*'   
    },
    'serverClass:ActiveDirectory': {
        'whitelist.0': '^^ADD_ONE_SERVER_EXPLICITLY^^',
        'machineTypesFilter': 'windows-*'   
    },
    'serverClass:zzz_All_Splunk_Instances:app:000-AutoLB': {
        'stateOnClient': 'enabled',
        'restartSplunk': 'true'
    },
    'serverClass:zzz_All_Splunk_Instances:app:A00-ALL-FORWARDER-EVENT-BREAKERS': {
        'stateOnClient': 'enabled',
        'restartSplunk': 'true'
    },
    'serverClass:zzz_Base_Splunk_Cloud_Intermediate_Forwarder:app:000_Base_Cloud_IF': {
        'stateOnClient': 'enabled',
        'restartSplunk': 'true'
    },
    'serverClass:zzz_Base_Splunk_Cloud_Intermediate_Forwarder:app:A00-CLOUD_MIGRATION_GDI': {
        'stateOnClient': 'enabled',
        'restartSplunk': 'true'
    },
    'serverClass:zzz_Base_Splunk_Cloud_Utility_Heavy_Forwarder:app:005_Base_Utility': {
        'stateOnClient': 'enabled',
        'restartSplunk': 'true'
    },
    'serverClass:OS_Windows:app:Basic_Windows_Inputs': {
        'stateOnClient': 'enabled',
        'restartSplunk': 'true'
    },
    'serverClass:ActiveDirectory:app:ActiveDirectory_Inputs': {
        'stateOnClient': 'enabled',
        'restartSplunk': 'true'
    },
    'serverClass:zzz_Base_Syslog_Forwarder:app:TA-syslog-ng': {
        'stateOnClient': 'enabled',
        'restartSplunk': 'true'
    },
    'serverClass:zzz_Base_Syslog_Forwarder:app:syslog-ng_Inputs': {
        'stateOnClient': 'enabled',
        'restartSplunk': 'true'
    },
    })







# [serverClass:All_Splunk_Instances:app:001_SplunkCloud_ALL_Base]
# stateOnClient = enabled
# restartSplunkd = true

# [serverClass:All_Splunk_Instances:app:A00_SplunkCloud_GDI]
# stateOnClient = enabled
# restartSplunkd = true

# [serverClass:Role_SplunkCloud_Intermediate_Forwarder:app:001_SplunkCloud_IF_Base]
# stateOnClient = enabled
# restartSplunkd = true

# [serverClass:Role_SplunkCloud_Syslog_Forwarder:app:001_SplunkCloud_SYSLOG_Forwarder_Base]
# stateOnClient = enabled
# restartSplunkd = true

# [serverClass:Role_SplunkCloud_Syslog_Forwarder:app:TA-syslog-ng]
# stateOnClient = enabled
# restartSplunkd = true

# [serverClass:Role_SplunkCloud_Heavy_Forwarder]
# whitelist.0 = *:CLOUD_HF*

# [serverClass:Role_SplunkCloud_Heavy_Forwarder:app:001_SplunkCloud_HF_Base]
# stateOnClient = enabled
# restartSplunkd = true

# [serverClass:Role_Endpoint_Forwarder]
# whitelist.0 = *
# blacklist.0 = *:CLOUD_HF*
# blacklist.1 = *:CLOUD_IF*
# blacklist.2 = *:SYSLOG*
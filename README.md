# PS-TOOLS #
**Version 3.0.0**

A collection of utilities and classes used to assist with SplunkCloud migrations

## REQUIREMENTS ##

This set of utilities and shared libraries was written using python 3.9 and tested using python 3.8 and requires python>=3.6 
(exception is scan_and_repair utility that is backported to python 2.7)

https://www.python.org/downloads/release/python-3106/

It is recommended to create a virtual environment to install all the required python modules. There is a setup.sh script that you can source to
create the inital virtual environment. If you source the file you will remain in the newly created virtual environment.

## COMMON FEATURES ##

Contained in this repo are common libraries that include standardized logging, parsing, merging, diffing, and writing of 
Splunk conf files, receipt.json handling, and many Splunk SDK wrapper classes. Many of these components are shared with
the various sub-modules to reduce code duplication. As a consequence, the entire repo is generally required for all python
scripts in the package. As a benefit, the individual features can be quickly leveraged to perform tasks for which there is not
a pre-built script. This significantly reduces the developement cycle for new requirements, and allows for individuals to 
create scripts for their exact needs without having to engage the developer directly.

## data_snapshot ##

### NOT COMPATIBLE WITH PREVIOUS VERSIONS OF "bucket archiver" ###

A utility to archive and restore Splunk buckets. Common use cases:

on-prem --> snapshot(archive) --> s3 --> snapshot(restore) --> staging stack(aws) --> smart store --> prod stack(aws)

on-prem --> snapshot(archive) --> snowballEdge --> s3 --> snapshot(restore) --> staging stack(aws) --> smart store --> prod stack(aws)

on-prem --> snapshot(archive) --> gcs --> snapshot(restore) --> staging stack(gcp) --> smart store --> prod stack(gcp)

on-prem --> snapshot(archive) --> GTA --> gcs --> snapshot(restore) --> staging stack(gcp) --> smart store --> prod stack(gcp)

on-prem --> snapshot(archive) --> backup location --> snapshot(restore) --> on-prem (to restore full fidelity after a smart store migration)

existing unencrypted smart store --> classic indexer cluster

classic indexer --> unencrypted smart store

Separate README included with instructions for use


## fixup_apps ##
deprecated

## clean_apps ##

A utility to clean up customer apps to prepare for SSAI upload.
Also good for general app fixup.

Collect the latest diags and prepare a copy for app vetting and installation of custom app defaults

* Replaces or Removes Deprecated XML Syntax
* Updates XML Dashboard  if required
* Removes Ownership from default.meta and Moves it to local.meta
* Creates a proper app.conf
* Identifies realtime searches
* Identifies loadjob references
* Identifies url links
* Identifies syntax errors in conf files
* Removes lookups
* Splits into local and default versions
* Identifies Splunkbase Apps
* Identifies built-in Apps with local settings (search, launcher, Premium Apps)
* Identifies apps of interest (e.g. Extremesearch)
* Removes redundant settings from local/*.conf files (true diff with default)
* Creates app tarfiles that can either be extracted to another directory as the source for the existing app_vet scripts,
or they can be vetted by the included vet_custom app. (defaults only)

To run it use python3 clean.py [app_directory]

Known Issues:
Running it a second time may not work as expected. remove hashes.pickle and all the .tmp directories before running it a second time.

To vet the created apps use python3 vet_custom.py [app_directory]

### Who do I talk to ###

Michael F. Doyle, SplunkCloud PS  
mdoyle@splunk.com
from ntpath import realpath
import os
import sys

class InvalidLibPath(Exception):
    pass

def make_lib_path():
    # Determine how we were called and add the lib directory to the path
    lib_path = os.path.normpath(os.path.realpath((os.path.join(os.path.dirname(sys.argv[0]), "../lib"))))
    
    if os.path.isdir(lib_path):
        sys.path.append(lib_path)
    else:
        raise InvalidLibPath("{} is not a valid directory".format(lib_path))

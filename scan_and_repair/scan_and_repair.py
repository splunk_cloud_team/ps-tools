from __future__ import print_function
import logging
import logging.handlers
import multiprocessing as mp
import os
import subprocess
import sys
import threading
import repair

def logListenerConfig(debug=False, verbose=False, log_dir="log"):
    LOG_DIR = log_dir
    root = logging.getLogger()
    root.setLevel(logging.NOTSET)
    try:
        os.makedirs(LOG_DIR)
    except OSError as e:
        if e.errno != 17:
            raise
        else:
            pass
    
    # Formatters
    f_verbose = logging.Formatter('%(asctime)s %(levelname)8s [ %(name)s:%(funcName)s() ] file="%(filename)s", line_no="%(lineno)s", %(message)s', datefmt="%Y-%m-%dT%H:%M:%S%z")
    f_console = logging.Formatter('%(levelname)8s - %(message)s', datefmt="%Y-%m-%dT%H:%M:%S%z")
    f_file = logging.Formatter('%(asctime)15s %(levelname)8s [ %(name)s ] - %(message)s', datefmt="%Y-%m-%dT%H:%M:%S%z")
    
    # Handlers
    if debug:
        h_debug = logging.handlers.RotatingFileHandler(os.path.normpath(os.path.join(LOG_DIR, "debug.log")), mode="a", maxBytes=50000000, backupCount=0, delay=True)
    # else:
    #     h_debug = logging.FileHandler(os.path.normpath(os.path.join(LOG_DIR, "debug.log")), mode="w")
        
        h_debug.setFormatter(f_file)
        h_debug.setLevel(logging.DEBUG)
        root.addHandler(h_debug)
    
    h_console = logging.StreamHandler()
    h_console.setLevel(logging.INFO)
    
    if verbose:
        h_console.setFormatter(f_file)

    else:
        h_console.setFormatter(f_console)
    
    h_error = logging.FileHandler(os.path.normpath(os.path.join(LOG_DIR, "errors.log")), mode="a", delay=True)
    h_error.setFormatter(f_file)
    h_error.setLevel(logging.WARNING)
    
    h_info = logging.FileHandler(os.path.normpath(os.path.join(LOG_DIR,"output.log")), mode="a", delay=True)
    h_info.setFormatter(f_file)
    h_info.setLevel(logging.INFO)
    
    root.addHandler(h_console)
    root.addHandler(h_error)
    root.addHandler(h_info)

def main(args):
    workers = []
    
    for _root, _dirs, _files in os.walk(os.path.join(args.index_path), topdown=True):
        for bucket in [ x for x in _dirs if (x.startswith('db_') or x.startswith('rb_')) and not x.endswith('-tmp')]:
            path = os.path.normpath(os.path.join(_root, bucket))
            p = repair.repairWorker(kwargs={"path": path, "splunk_home": args.splunk_home, "threads": args.thread_limit, "force_repair": args.force})
            workers.append(p)
            p.daemon = True
            p.start()
    
    for p in [x for x in workers if x.is_alive]:
        success, msg = p.join()
        if not success: 
            logger.error(msg)
        else:
            logger.info(msg)
            
if __name__ == '__main__':
    logListenerConfig(debug=True, log_dir="scan_log", verbose=True)
    logger = logging.getLogger(os.path.basename(sys.argv[0]))
    import argparse
    
    cli = argparse.ArgumentParser()
    cli.add_argument('splunk_home')
    cli.add_argument('index_path')
    cli.add_argument('--force', '-f', help='Force repair of all buckets', action='store_true')
    cli.add_argument('--thread-limit', '-t', type=int, metavar='INT', help=argparse.SUPPRESS, default=30)
    args = cli.parse_args()
    
    main(args)

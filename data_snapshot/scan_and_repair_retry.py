import logging
import multiprocessing as mp
import os
import subprocess
import sys
import threading

from common import *

common.lib_path.make_lib_path()

from ps_common import *
# from ps_common import Splunk, SplunkIndexer, SplunkManager
from ps_common.exceptions import *

logListenerConfig(debug=True, log_dir="scan_log_retry", verbose=True)
# from common import *

@debug_wrapper(True)
def main(args):
    workers = []
    data = []

    with open(f"{args.log_path}/errors.log", "r") as f:
        for line in f:
            try:
                line.split().index('Cannot')
            except ValueError:
                continue
            
            data.append(line.split()[8])
    
    for path in list(set(data)):
        p = repairWorker(kwargs={"path": path, "splunk_home": args.splunk_home, "threads": args.thread_limit, "force_repair": args.force})
        workers.append(p)
        p.daemon = True
        p.start()

    for p in [x for x in workers if x.is_alive]:
        success, msg = p.join()
        if not success: 
            logger.error(msg)
        else:
            logger.info(msg)
            
if __name__ == '__main__':
    logger = logging.getLogger(os.path.basename(sys.argv[0]))
    import argparse
    
    cli = argparse.ArgumentParser()
    cli.add_argument('splunk_home')
    cli.add_argument('log_path')
    cli.add_argument('--force', '-f', help='Force repair of all buckets', action='store_true')
    cli.add_argument('--thread-limit', '-t', type=int, metavar='INT', help=argparse.SUPPRESS, default=30)
    args = cli.parse_args()
    
    main(args)

# data_snapshot #

## NOT COMPATIBLE WITH PREVIOUS VERSIONS OF "bucket archiver" ##

A utility to archive and restore Splunk buckets

## THIS IS NOT A CHEAP/FAST BULK COPY UTILITY ##

This utility is designed to copy buckets safely and accurately as quickly as possible. It is a invalid assumtion that the utility will
achieve actual wire-speed during the transfer. If you are looking for speed above all else, and do not care about the quality of the data,
then you should probaby just use a native "sync" type utility such as rsync, aws s3 sync, or gsutil and take your chances with the hope that everyting was perfect when you copied it.

The development of this utility has taken years, and based on those years of experience the utility has evolved into something that corrects
source data issues before they are propagated to Smart Store where they are extremely difficult to fix. It tracks the progress, logs EVERYTHING, produces json files with the results of the operations and allows for file-by-file auditing.

## Why do I need this ##

Historically, migrating Splunk buckets from an indexer cluster has been problematic and cumbersome. There are various spl searches that
have been published to gather up the required lists, and scripts of varying utility to use the results of those searches to copy buckets
to remote locations. Problems have included lots of copy/paste and manual formatting of search results, having to copy lists of buckets
to each indexer with only the buckets it should upload, buckets moving from warm to cold if Splunk is not completely stopped, source bucket corruption, windows path adjustments, inaccurate determination of copy boundaries, and finally -- they were all very slow because they did things in a single thread.

This utility can be used to identify and copy primary buckets from a "Classic" indexer cluster (not Smart Store) to a remote storage location (aws, gcs, filesystem) in a Smart Store compatible format by querying the cluster master via rest and searching the local index locations for the actual buckets, determining whether the bucket is the primary copy and finally queuing it up for a pool of workers to transfer to the remote location.

## Features ##

* Runs under Splunk python3 (e.g. splunk cmd python3 snapshot.py)
* Supports stand-alone, single site, and multisite indexers as sources and targets
* Properly handles stand-alone buckets and clusted buckets
* Minimizes bucket collisions on restore
* Supports Windows, macOS, and Linux indexers
* Minimizes time that the CM must be in MM by persisting the primary list
* Persistent state for fast recovery (skips buckets already copied successfully)
* Creates audit logs that can be imported into Splunk for detailed reporting
* Creates a buckets.csv file for use with the MDM/s2dc utility
* Creates an indexes.conf file for use during the restore to a staging stack
* Creates a json formatted representation of the Splunk idexer configuration along with the index definitions and (in clustered configurations) the primary buckets for each index.
* Multiple processes for greater throughput (configurable)
* Supports s3, snowball, and other s3-like destinations, as well as nfs mounts (Google Transfer Appliance), and native Google Cloud Storage
* Extendable to azure blob (future release)
* Automated discovery of source and target Splunk architecture
* Extremely Verbose Debug logging
* Periodic status reporting
* Error checking and handling with retry
* Support for receipt.json format allows for extracting non-encrypted smart store buckets back to local indexes
* Supports multiple remote archive locations in a round-robin fashion (useful for finite storage devices such as aws snowball and google transfer appliance)
* Optional online bucket repair prior to copy operation

## How do I get set up? ##

configure settings.conf with the appropriate source or destination

```
{
    "log_dir": "log",
    "work_dir": "var",
    "http_proxy": "http://IP:port" (identical syntax to the OS variables) -or- null (no quotes),
    "https_proxy": "http://IP:port" (identical syntax to the OS variables) -or- null (no quotes),
    "s3_config": [
        {
            "bucket_name": "name of s3 bucket",
            "bucket_path": "key prefix/directory name inside remote bucket (e.g. attempt1)",
            "endpoint_url": "For aws s3: https://s3.<<REGION>>.amazonaws.com, for snowball this is the snowball https://IP:port",
            "aws_access_key_id": "HMAC ACCESS KEY" -or- null (no quotes),
            "aws_secret_access_key": "HMAC SECRET ACCESS KEY" -or- null (no quotes),
            "verify": "For snowball this is the path to the cert file for this particular endpoint" -or- true (no quotes)
        }
    ],
    "gcs_config": [
        {
            "bucket_name": "name of GCS bucket",
            "bucket_path": "key prefix/directory name inside remote bucket (e.g. attempt1)",
            "gcs_auth_file": "this is the path to the gcs_auth.json file for the bucket"
        }
    ],
    "fs_config": [
        {
            "path": "data/test1"
        }
    ]
}
```
For each remote location, there is a section in settings.conf.example that can be used as a template. This is a JSON document, and must be valid. The values for null, true, and false are always lower case and never quoted.

For destinations, there can be more than one configuration in a list [{},{}] where the entries will be used in a round-robin fashion to balance the data across multiple devices. This is helpful for snowballEDGE and other limited capacity devices to reduce the possiblilty of filling one device and and having others be only partially full, then trying to determine how to fit the remaining data onto the partially full devices. 
In order for this to work properly from mulitple source indexers, each settings.conf file must be an exact copy so the order is preserved. 

**--DO NOT TRY TO BE CLEVER--** configure all endpoints for all indexers so the data is balanced. The only exceptions would be if there are devices in multiple data centers
and the traffic must stay local to that data center. In that case, all indexers in that location should archive to all the physical devices shipped to that location -- HOWEVER: this is never recommended as whatever gains are expected in bandwidth will often be nullified by shipping, poor data balance, having to order additional devices for one location halfway through the process, or any number of issues. Generally speaking, the risks outweigh any potential gains.
Customer data is often horribly balanced, and you are just asking for trouble, so don't fight it or you'll end up trying to shoe-horn the last 1,000 buckets by hand onto whatever remaining space you have.

**If you have devices of differing capacity** -- for example a 300TB device and a 100TB device for a total of 400TB, you must create entries here in the proper ratio. Using snowballEDGE for this example of 3:1 you would create:
```
  "s3_config": [
      {device1 config},
      {device1 config},
      {device1 config},
      {device2 config}
  ]
```
which would provide a weighted balance across the 2 devices, where device 1 would receive 3/4 of the data and device 2 would receive the remaining 1/4. 

In cases where there is a large disparity, and the larger device capacity is not an integer multiple of the smaller device (e.g. a 480TB and 100TB combo), 
the settings.conf would require 48 entries for device 1 and 10 entries for device 2 for the ratio 48:10.

It seems ridiculous that the developer did not implement something more robust, but you get what you pay for ;p

For sources, there can be only one entry. It can be just a single entry {}, or a list of one entry [{}]. Multiple items in a list will cause an error condition.

When using SnowballEDGE or Google Transfer Appliance it is important to understand how this utility relates to these technologies. 
Since there can be multiple archive destinations, each entry represents a physical device, and since there can be only one restore source, 
each device must be created as part of a single destination bucket to insure that the full complement of data is available in a single restore source location.
Remember that the restore operation for GTA will be performed with a GCS config -- archive to FS, restore from GCS, whereas a snowballEDGE device provides an s3-like interface, so the archive and restore both use the s3 config parameters.


## s3 and s3-like ##

* **bucket_name:** the actual name of the remote s3 bucket without any prefix (e.g. "my-s3-bucket")
* **bucket_path:** the "subdirectory" inside the bucket that will be the "root" of the data (e.g. "customername" or "2021-04-17") **use the same value for every entry**
* **endpoint_url:** for s3 this will be in the format https://s3.<<region>>.amazonaws.com, for other devices such as on-prem s3-like, minio, and snowballEDGE this will be the IP and port provided in the product documentation.
* **aws_access_key_id:** this is a standard HMAC key to provide access to the bucket. If you have assigned an EC2 policy to the instance, or you have configured environment variables, you can specify the value 'null' without quotes, lower case.
* **aws_secret_access_key:** this is the standard HMAC secret key to provide access to the bucket. If you have assigned an EC2 policy to the instance, or you have configured environment variables, you can specify the value 'null' without quotes, lower case.
* **verify:** this is either true, false, or the path to the cert for verification. When using true or false there are no quotes and the value must be lower case, when providing a path the value must be quoted.

## Google Cloud Storage ##

GCS does provide an s3-like interface, as well as a native interface, however, as a destination the s3-like interface is not supported due to the lack of multipart upload support. Downloads can actually be performed using the s3-like interface with HMAC credentials, but it is recommended to use this native interface.

* **bucket_name:** the actual name of the remote gcs bucket without any prefix (e.g. "my-gcs-bucket")
* **bucket_path:** the "subdirectory" inside the bucket that will be the "root" of the data (e.g. "customername" or "2021-04-17")
* **gcs_auth_file:** the path to the json document with the access credentials -- this is provided when you create the service account and create a key. Only the json format is supported, not the pks12 format, so make sure you request the right one.

## File System ##

Anything that looks like a filesystem can use this configuration. There is one value here -- "path" -- that is the full path to the location including the subdirectory. 
Google Transfer Appliance looks like an NFS mount to the OS, so you would use this as the destination. 
If there are multiple devices required, each indexer would have a mount point for each device, and all mount points would be specifed in this section for the round-robin behavior.

**--DO NOT--** create one mount point for each entry in settings.conf when using the configuration for devices of different sizes -- create only one mount point per physical device, and list that mount point X number of times in settings.conf

## Other Interesting Use Cases ##

Because this utility archives data in a pseudo-smart-store format, including receipt.json, but not using any encryption, the restore functionality can be used to localize existing non-encrypted smart store index data to a classic indexer by configuring the source to point to the smart store index location.

Additionally, the archive format is compatible with MDM/s2dc and also supports Windows source indexers.

## PRE-REQUISITES ##

This utility is designed to be self-configuring wherever possible. For this to function it is required that:

* a common account exist on each Splunk indexer and Cluster Manager with the "admin" role assigned.
* the password for the above account must be the same on all instances
* all indexers can connect to each other on the REST port (default 8089, but can be specified with command-line switch)
* the cluster configuration is available via REST (e.g. the settings are not in an app that is not set for **export=system**)

Once the settings.conf file is prepared, the only other information required is the single username and password above, which is entered from the keyboard on each indexer. Once the password is entered the utility will begin discovering the Splunk indexer configuration and finally start to collect the list of buckets it will upload to the remote location(s) as specified in settings.conf.

This utility is designed to be run from Splunk indexers ONLY. It will not, and should not run on CM.

* Site Affinity for the CM must be disabled -- site must be set for **site=site0**
* The Splunkd on the indexers must be running for this utility to work.
* The indexes to be archived or restored must be defined and not disabled in the running instance of Splunk
* The indexers must be able to connect to the remote REST endpoint on the CM (default 8089)
* The indexers must be able to connect to the remote REST endpoint on **EVERY INDEXER IN THE CLUSTER** (default 8089)
* For Archive: The index cluster (if applicable) must be in a healthy state (All data serchable, SF/RF met)
* For Restore on Staging Stack: The staging index cluster must be **mulitsite=false, SF=1, RF=1, and all site settings on the indexers must be removed**
* The **username/password** must be the same across **ALL INDEXERS AND CM**
* You must install the following python modules: splunk-sdk, boto3 (for s3/snowball/s3-like), google.cloud.storage (for gcs)
* You must install and run this utility on **EVERY INDEXER EVERY TIME -- NO EXCEPTIONS**

**For Archive**

* For actual migration (to SplunkCloud for example), best practice is to configure each source indexer for indexAndForward to the new destination.
* The coordination of the indexAndForward configuration, and the rolling of the hot buckets provides the exact time for bucket cutoff time for each indexer
* It is **imperative** that the outputs.conf be correctly configured to keep all data synchronized between the existing, and the new destinations. If these settings are incorrect, data may end up in one location, or the other, or both -- **both** is the desired result in **all cases** and these settings will insure data must make it to both locations, or the indexer will block (just as it would if there were only a single output destination).


```


        [tcpout]
        indexAndForward = true
        blockOnCloning = true
        dropEventsOnQueueFull = -1
        dropClonedEventsOnQueueFull = -1
        defaultGroup = * (--or comma separated list of explicit groups--)
        

```

* Only warm and cold buckets are copied
* All data to be migrated must exist in the homePath or coldPath -- no data will be copied from thawedPath
* Only searchable copies of buckets will be copied
* Only primary buckets are copied (one copy of each bucket regardless of the number of replicas)
* The script uses the "lastest _indextime" to determine if a bucket was created before, or after the indexer restart time and will not copy buckets after the restart (hopefully when indexAndForward was enabled).
* The first time the utility runs it will create a persistent list of buckets to be copied, and a list of successfully copied buckets. If the utility must be run again (indexAndForward was not enabled as recommended), it is possible to run a "delta" up to the new last restart time.
* Mulitple stand-alone (all-in-one) indexers are supported as sources

**For Restore**

* Restore can be performed from a stand-alone (all-in-one) indexer for small datasets
* For large datasets, a cluster is recommended -- non-multisite SF=1, RF=1
* multiple stand-alone indexers are not supported as destinations

## ARCHIVE ##

```
usage: snapshot.py archive [-h] [--local_port LOCAL_PORT] [--scheme {http,https}] [--debug] [--verbose] [--timeout TIMEOUT] [-w WORKERS] [-t] [--log_dir LOG_DIR] [--conf CONF]
                           [--mp_start_method {fork,spawn,forkserver}] [--threads] [--test-retry] [--guid GUID] [--skip-internal] [-x EARLIEST_EVENT] [--fix_buckets] [--skip-recovery]
                           [--dest-format {bid,classic,s2}] [--freeze]
                           {s3,gcs,filesys,test} ...

positional arguments:
  {s3,gcs,filesys,test}
    s3                  Amazon s3 or s3-like destinations using HMAC and boto3
    gcs                 Google Cloud Storage using the native API and gcs_auth.json
    filesys             Any destination mounted locally -- Google Transfer Appliance, Local File Share, etc.
    test                Dummy transfer module for testing.

optional arguments:
  -h, --help            show this help message and exit

Global Optional Arguments:
  --local_port LOCAL_PORT
                        The local REST port (default 8089)
  --scheme {http,https}
                        Splunk connection scheme
  --debug               Enable DEBUG logging (slower)
  --verbose             Add timestamp and process information to the console log (always present in the logfile)
  --timeout TIMEOUT     Timeout Seconds for REST Calls
  -w WORKERS, --workers WORKERS
                        Number of worker threads (default 4 -- more is not always better)
  -t, --test            Don't actually execute the copy operation
  --log_dir LOG_DIR     Directory for logs.
  --conf CONF           Relative path to the settings.conf file

For Testing ONLY:
  --mp_start_method {fork,spawn,forkserver}
                        DO NOT USE
  --threads             DO NOT USE
  --test-retry          DO NOT USE
  --guid GUID           DO NOT USE

Archive Optional Arguments:
  --skip-internal       Skip internal indexes to save time and space
  -x EARLIEST_EVENT, --earliest_event EARLIEST_EVENT
                        Epoch time of beginning of data window
  --fix_buckets         Repair the buckets before copy

Archive Experimental Arguments -- TESTING ONLY:
  --skip-recovery       Do not attempt to recover prior bucketList
  --dest-format {bid,classic,s2}
                        DO NOT USE
  --freeze              DO NOT USE
```

## RESTORE ##

```
usage: snapshot.py restore [-h] [--local_port LOCAL_PORT] [--scheme {http,https}] [--debug] [--verbose] [--timeout TIMEOUT] [-w WORKERS] [-t] [--log_dir LOG_DIR] [--conf CONF]
                           [--mp_start_method {fork,spawn,forkserver}] [--threads] [--test-retry] [--guid GUID]
                           {s3,gcs,filesys,test} ...

positional arguments:
  {s3,gcs,filesys,test}
    s3                  Amazon s3 or s3-like destinations using HMAC and boto3
    gcs                 Google Cloud Storage using the native API and gcs_auth.json
    filesys             Any destination mounted locally -- Google Transfer Appliance, Local File Share, etc.
    test                Dummy transfer module for testing.

optional arguments:
  -h, --help            show this help message and exit

Global Optional Arguments:
  --local_port LOCAL_PORT
                        The local REST port (default 8089)
  --scheme {http,https}
                        Splunk connection scheme
  --debug               Enable DEBUG logging (slower)
  --verbose             Add timestamp and process information to the console log (always present in the logfile)
  --timeout TIMEOUT     Timeout Seconds for REST Calls
  -w WORKERS, --workers WORKERS
                        Number of worker threads (default 4 -- more is not always better)
  -t, --test            Don't actually execute the copy operation
  --log_dir LOG_DIR     Directory for logs.
  --conf CONF           Relative path to the settings.conf file

For Testing ONLY:
  --mp_start_method {fork,spawn,forkserver}
                        DO NOT USE
  --threads             DO NOT USE
  --test-retry          DO NOT USE
  --guid GUID           DO NOT USE
```

## ADDITIONAL FILES CREATED ##

If you need to delete any of these files you are essentially starting over. DO NOT delete these unless you know what you are doing, or you really want to start from the beginning.

**archiveList.pickle**

* this file contains the complete list of buckets that will be attempted
* the number of entries in this file is the "Total Buckets" value of the periodic status messages
* if you delete this file the indexer will rebuild it based on current information including the last restart time of the peers
* this file is not human readable

**uploadedBuckets.pickle**

* this file contains the titles of all buckets that were successfully uploaded by this indexer
* if you delete this file all buckets will be reattempted, but uploads will still be skipped if the destination already exists
* multiple files can be merged using a utility to get the entire completed list which can be used in a catastropic situation
* this file is not human readable

**primaryList.pickle**

* this file contains the list of primary buckets for this peer as returned by the CM
* if you delete this file a new list of primaries will be created
* this is the most important file and must not be updated on a single indexer, this will require that all the indexers rebuild their lists as well
* do not delete this file unless you want to start all over on all the indexers
* this file is not human readable

**restoreList.pickle**

* this file contains the complete list of buckets that will be attempted
* the number of entries in this file is the "Total Buckets" value of the periodic status messages
* if you delete this file the indexer will rebuild it based on current information including the current index number based on the current peer list
* this file is not human readable

**downloadedBuckets.pickle**

* this file contains the titles of all buckets that were successfully downloaded by this indexer
* if you delete this file all buckets will be reattempted
* multiple files can be merged using a utility to get the entire completed list which can be used in a catastropic situation
* this file is not human readable

## LOGS CREATED ##

Two logs files are always created in the log directory

**output.log**

```
2021-04-18 15:55:06,365     INFO [ common.classes.archiver ] - BucketDiscovery Index=_audit, Action=discover, Status=complete, PrimaryCount=7, ReplicaCount=0, HotCount=1
2021-04-18 15:55:06,367     INFO [ common.classes.archiver ] - BucketDiscovery Index=_internal, Action=discover, Status=complete, PrimaryCount=9, ReplicaCount=0, HotCount=1
2021-04-18 15:55:06,369     INFO [ common.classes.archiver ] - BucketDiscovery Index=_introspection, Action=discover, Status=complete, PrimaryCount=4, ReplicaCount=0, HotCount=1
2021-04-18 15:55:06,370     INFO [ common.classes.archiver ] - BucketDiscovery Index=_telemetry, Action=discover, Status=complete, PrimaryCount=2, ReplicaCount=0, HotCount=1
2021-04-18 15:55:06,372     INFO [ common.classes.archiver ] - BucketDiscovery Index=_thefishbucket, Action=discover, Status=complete, PrimaryCount=0, ReplicaCount=0, HotCount=0
2021-04-18 15:55:06,373     INFO [ common.classes.archiver ] - BucketDiscovery Index=bmo_phantom_alerts, Action=discover, Status=complete, PrimaryCount=0, ReplicaCount=0, HotCount=0
2021-04-18 15:55:06,374     INFO [ common.classes.archiver ] - BucketDiscovery Index=history, Action=discover, Status=complete, PrimaryCount=0, ReplicaCount=0, HotCount=0
2021-04-18 15:55:06,375     INFO [ common.classes.archiver ] - BucketDiscovery Index=main, Action=discover, Status=complete, PrimaryCount=0, ReplicaCount=0, HotCount=0
2021-04-18 15:55:06,375     INFO [ common.classes.archiver ] - BucketDiscovery Index=summary, Action=discover, Status=complete, PrimaryCount=0, ReplicaCount=0, HotCount=0
2021-04-18 15:55:06,375     INFO [ common.classes.archiver ] - BucketDiscovery Action=discover, Status=complete, PrimaryCount=22, ReplicaCount=0, HotCount=4
2021-04-18 15:55:06,504     INFO [ PeriodicStatus ] - PeriodicStatus: total=22, queued=18, succeeded=0, failed=0, cancelled=0, attempting=4
2021-04-18 15:55:16,249     INFO [ PeriodicStatus ] - PeriodicStatus: total=22, queued=1, succeeded=17, failed=0, cancelled=0, attempting=4
2021-04-18 15:55:31,311     INFO [ PeriodicStatus ] - PeriodicStatus: total=22, queued=0, succeeded=19, failed=0, cancelled=0, attempting=3
2021-04-18 15:55:46,250     INFO [ PeriodicStatus ] - PeriodicStatus: total=22, queued=0, succeeded=19, failed=0, cancelled=0, attempting=3
2021-04-18 15:56:01,308     INFO [ PeriodicStatus ] - PeriodicStatus: total=22, queued=0, succeeded=19, failed=0, cancelled=0, attempting=3
2021-04-18 15:56:16,311     INFO [ PeriodicStatus ] - PeriodicStatus: total=22, queued=0, succeeded=19, failed=0, cancelled=0, attempting=3
2021-04-18 15:56:31,328     INFO [ PeriodicStatus ] - PeriodicStatus: total=22, queued=0, succeeded=19, failed=0, cancelled=0, attempting=3
2021-04-18 15:56:46,253     INFO [ PeriodicStatus ] - PeriodicStatus: total=22, queued=0, succeeded=19, failed=0, cancelled=0, attempting=3
2021-04-18 15:57:01,299     INFO [ PeriodicStatus ] - PeriodicStatus: total=22, queued=0, succeeded=20, failed=0, cancelled=0, attempting=2
2021-04-18 15:57:16,306     INFO [ PeriodicStatus ] - PeriodicStatus: total=22, queued=0, succeeded=20, failed=0, cancelled=0, attempting=2
2021-04-18 15:57:31,268     INFO [ PeriodicStatus ] - PeriodicStatus: total=22, queued=0, succeeded=21, failed=0, cancelled=0, attempting=1
2021-04-18 15:57:46,245     INFO [ PeriodicStatus ] - PeriodicStatus: total=22, queued=0, succeeded=21, failed=0, cancelled=0, attempting=1
2021-04-18 15:57:57,464     INFO [ __main__ ] - Work Complete
2021-04-18 15:57:57,464     INFO [ __main__ ] - Attempting Graceful Shutdown
2021-04-18 15:57:57,464     INFO [ __main__ ] - Sending Exit Event to all processes
2021-04-18 15:57:57,464     INFO [ __main__ ] - Waiting for Process Cleanup...
2021-04-18 15:57:58,085     INFO [ PeriodicStatus ] - FinalStatus: total=22, queued=0, succeeded=22, failed=0, cancelled=0, attempting=0
```

**errors.log**

```
2022-05-14T13:42:48-0400  WARNING [ downloadWorker-3 ] - BucketTransfer Bucket=_internal~12~3F2A1770-3FD3-49B3-85EE-1F6A770988C4, State=thawed, Path=/opt/splunk-8.2.4/var/lib/splunk/_internaldb/thaweddb/db_1649942557_1649710085_12_3F2A1770-3FD3-49B3-85EE-1F6A770988C4/rawdata/slicemin.dat, Action=download, Status=retry, Uploaded=13, Skipped=0, Errors=1, ElapsedSec=1.42
2022-05-14T13:42:49-0400    ERROR [ common.transfer.sources.filesys ] - Expected file data/test1/_internal/db/81/16/12~3F2A1770-3FD3-49B3-85EE-1F6A770988C4/guidSplunk-3F2A1770-3FD3-49B3-85EE-1F6A770988C4/.rawSize not found.
```

Additionally, if the --debug switch is used a third log will be created. It is extremely verbose.

**debug.log**

```
2021-04-18 15:55:00,617    DEBUG [ __main__:<module>() ] file="snapshot.py", line_no="188", Logging Process Started
2021-04-18 15:55:00,620    DEBUG [ __main__:<module>() ] file="snapshot.py", line_no="200", Creating Upload Worker Processes
2021-04-18 15:55:01,231    DEBUG [ uploadWorker-3:uploadWorker() ] file="workers2.py", line_no="38", hello
2021-04-18 15:55:01,232    DEBUG [ uploadWorker-3:uploadWorker() ] file="workers2.py", line_no="39", BucketTransfer dest=gcs
2021-04-18 15:55:01,234    DEBUG [ uploadWorker-1:uploadWorker() ] file="workers2.py", line_no="38", hello
2021-04-18 15:55:01,235    DEBUG [ uploadWorker-1:uploadWorker() ] file="workers2.py", line_no="39", BucketTransfer dest=gcs
2021-04-18 15:55:01,236    DEBUG [ uploadWorker-0:uploadWorker() ] file="workers2.py", line_no="38", hello
2021-04-18 15:55:01,236    DEBUG [ uploadWorker-0:uploadWorker() ] file="workers2.py", line_no="39", BucketTransfer dest=gcs
2021-04-18 15:55:01,237    DEBUG [ uploadWorker-2:uploadWorker() ] file="workers2.py", line_no="38", hello
2021-04-18 15:55:01,237    DEBUG [ uploadWorker-2:uploadWorker() ] file="workers2.py", line_no="39", BucketTransfer dest=gcs
2021-04-18 15:55:06,282    DEBUG [ common.classes.archiver:run() ] file="archiver.py", line_no="17", hello
2021-04-18 15:55:06,283    DEBUG [ common:discover_splunk() ] file="__init__.py", line_no="54", Connect to local instance
2021-04-18 15:55:06,283    DEBUG [ <bucket_snapshot.SplunkInstance object at 0x10fe05d30>:__init__() ] file="__init__.py", line_no="132", Initializing...
2021-04-18 15:55:06,283    DEBUG [ <bucket_snapshot.SplunkInstance object at 0x10fe05d30>:__init__() ] file="__init__.py", line_no="37", Initializing...
2021-04-18 15:55:06,299    DEBUG [ <bucket_snapshot.SplunkInstance object at 0x10fe05d30>:__init__() ] file="__init__.py", line_no="39", Got connection <splunklib.binding.Context object at 0x10fe56b50>
2021-04-18 15:55:06,299    DEBUG [ root:get() ] file="binding.py", line_no="684", GET request to https://127.0.0.1:8089/services/server/info (body: {})
2021-04-18 15:55:06,304    DEBUG [ root:new_f() ] file="binding.py", line_no="73", Operation took 0:00:00.004474
2021-04-18 15:55:06,305    DEBUG [ root:get() ] file="binding.py", line_no="684", GET request to https://127.0.0.1:8089/services/server/settings (body: {})
2021-04-18 15:55:06,326    DEBUG [ root:new_f() ] file="binding.py", line_no="73", Operation took 0:00:00.021256
2021-04-18 15:55:06,327    DEBUG [ root:get() ] file="binding.py", line_no="684", GET request to https://127.0.0.1:8089/servicesNS/nobody/search/configs/conf-server/clustering (body: {})
2021-04-18 15:55:06,335    DEBUG [ root:new_f() ] file="binding.py", line_no="73", Operation took 0:00:00.008416
2021-04-18 15:55:06,336    DEBUG [ root:get() ] file="binding.py", line_no="684", GET request to https://127.0.0.1:8089/services/data/indexes (body: {'count': 0})
2021-04-18 15:55:06,358    DEBUG [ root:new_f() ] file="binding.py", line_no="73", Operation took 0:00:00.021974
2021-04-18 15:55:06,362    DEBUG [ common:discover_splunk() ] file="__init__.py", line_no="116", I AM STAND ALONE
2021-04-18 15:55:06,363    DEBUG [ common.classes.archiver:run() ] file="archiver.py", line_no="30", BucketDiscovery Action=recover, Status=begin
2021-04-18 15:55:06,363    DEBUG [ common.classes.archiver:run() ] file="archiver.py", line_no="36", BucketDiscovery Action=recover, Status=skipped, Reason=FileNotFound
2021-04-18 15:55:06,363    DEBUG [ common.classes.archiver:run() ] file="archiver.py", line_no="44", BucketDiscovery Action=discover, Status=begin
2021-04-18 15:55:06,363    DEBUG [ common.classes.archiver:run() ] file="archiver.py", line_no="56", BucketDiscovery Index=_audit, homePath=/Users/mdoyle/VSCode/splunk/splunk/var/lib/splunk/audit/db
2021-04-18 15:55:06,364    DEBUG [ common.classes.archiver:run() ] file="archiver.py", line_no="62", BucketDiscovery /Users/mdoyle/VSCode/splunk/splunk/var/lib/splunk/audit/db/.bucketManifest: {'id': '_audit~6~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56', 'path': 'hot_v1_6', 'raw_size': '', 'event_count': '', 'host_count': '', 'source_count': '', 'sourcetype_count': '', 'size_on_disk': '', 'modtime': '', 'frozen_in_cluster': '0', 'origin_site': '', 'tsidx_minified': '0', 'journal_size': ''}
2021-04-18 15:55:06,364    DEBUG [ common.classes.archiver:run() ] file="archiver.py", line_no="65", BucketDiscovery Index=_audit, Bucket=hot_v1_6, Action=discover, Status=skipped, Reason=hot
2021-04-18 15:55:06,364    DEBUG [ common.classes.archiver:run() ] file="archiver.py", line_no="62", BucketDiscovery /Users/mdoyle/VSCode/splunk/splunk/var/lib/splunk/audit/db/.bucketManifest: {'id': '_audit~5~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56', 'path': 'db_1618691466_1618691046_5', 'raw_size': '54945', 'event_count': '353', 'host_count': '1', 'source_count': '1', 'sourcetype_count': '1', 'size_on_disk': '114688', 'modtime': '1618691832', 'frozen_in_cluster': '0', 'origin_site': '', 'tsidx_minified': '0', 'journal_size': '8289'}
2021-04-18 15:55:06,364    DEBUG [ s2lib:__init__() ] file="__init__.py", line_no="116", hello
...
2021-04-18 15:55:06,378    DEBUG [ uploadWorker-2:uploadWorker() ] file="workers2.py", line_no="79", BucketTransfer Searching for Bucket=_audit~5~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56
2021-04-18 15:55:06,378    DEBUG [ PeriodicStatus:status_worker() ] file="snapshot.py", line_no="76", PeriodicStatus: total=22, queued=21, succeeded=0, failed=0, cancelled=0, attempting=1
2021-04-18 15:55:06,378    DEBUG [ uploadWorker-2:uploadWorker() ] file="workers2.py", line_no="118", BucketTransfer Bucket=_audit~5~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56, State=warm, Path=/Users/mdoyle/VSCode/splunk/splunk/var/lib/splunk/audit/db/db_1618691466_1618691046_5, Action=scan, Status=attempting, Retries=0
2021-04-18 15:55:06,385    DEBUG [ uploadWorker-1:uploadWorker() ] file="workers2.py", line_no="79", BucketTransfer Searching for Bucket=_audit~4~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56
2021-04-18 15:55:06,385    DEBUG [ uploadWorker-1:uploadWorker() ] file="workers2.py", line_no="118", BucketTransfer Bucket=_audit~4~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56, State=warm, Path=/Users/mdoyle/VSCode/splunk/splunk/var/lib/splunk/audit/db/db_1618515821_1615216083_4_370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56, Action=scan, Status=attempting, Retries=0
2021-04-18 15:55:06,385    DEBUG [ PeriodicStatus:status_worker() ] file="snapshot.py", line_no="76", PeriodicStatus: total=22, queued=20, succeeded=0, failed=0, cancelled=0, attempting=2
2021-04-18 15:55:06,399    DEBUG [ uploadWorker-3:uploadWorker() ] file="workers2.py", line_no="79", BucketTransfer Searching for Bucket=_audit~3~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56
2021-04-18 15:55:06,399    DEBUG [ uploadWorker-3:uploadWorker() ] file="workers2.py", line_no="118", BucketTransfer Bucket=_audit~3~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56, State=warm, Path=/Users/mdoyle/VSCode/splunk/splunk/var/lib/splunk/audit/db/db_1615216070_1615214162_3_370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56, Action=scan, Status=attempting, Retries=0
2021-04-18 15:55:06,399    DEBUG [ PeriodicStatus:status_worker() ] file="snapshot.py", line_no="76", PeriodicStatus: total=22, queued=19, succeeded=0, failed=0, cancelled=0, attempting=3
2021-04-18 15:55:06,401    DEBUG [ uploadWorker-0:uploadWorker() ] file="workers2.py", line_no="79", BucketTransfer Searching for Bucket=_audit~2~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56
2021-04-18 15:55:06,402    DEBUG [ uploadWorker-0:uploadWorker() ] file="workers2.py", line_no="118", BucketTransfer Bucket=_audit~2~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56, State=warm, Path=/Users/mdoyle/VSCode/splunk/splunk/var/lib/splunk/audit/db/db_1615210089_1615210069_2_370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56, Action=scan, Status=attempting, Retries=0
2021-04-18 15:55:06,402    DEBUG [ PeriodicStatus:status_worker() ] file="snapshot.py", line_no="76", PeriodicStatus: total=22, queued=18, succeeded=0, failed=0, cancelled=0, attempting=4
2021-04-18 15:55:06,504     INFO [ PeriodicStatus:status_worker() ] file="snapshot.py", line_no="60", PeriodicStatus: total=22, queued=18, succeeded=0, failed=0, cancelled=0, attempting=4
2021-04-18 15:55:06,657    DEBUG [ uploadWorker-3:uploadWorker() ] file="workers2.py", line_no="204", BucketTransfer Bucket=_audit~3~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56, State=warm, Path=/Users/mdoyle/VSCode/splunk/splunk/var/lib/splunk/audit/db/db_1615216070_1615214162_3_370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56, Action=repair, Status=skipped, ElapsedSec=0.26, Retries=0, Reason="--fix_buckets not selected"
2021-04-18 15:55:06,657    DEBUG [ uploadWorker-3:uploadWorker() ] file="workers2.py", line_no="206", BucketTransfer Bucket=_audit~3~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56, State=warm, Path=/Users/mdoyle/VSCode/splunk/splunk/var/lib/splunk/audit/db/db_1615216070_1615214162_3_370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56, Action=update_manifest, Status=attempting, Retries=0
2021-04-18 15:55:06,657    DEBUG [ uploadWorker-0:uploadWorker() ] file="workers2.py", line_no="204", BucketTransfer Bucket=_audit~2~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56, State=warm, Path=/Users/mdoyle/VSCode/splunk/splunk/var/lib/splunk/audit/db/db_1615210089_1615210069_2_370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56, Action=repair, Status=skipped, ElapsedSec=0.26, Retries=0, Reason="--fix_buckets not selected"
2021-04-18 15:55:06,657    DEBUG [ uploadWorker-0:uploadWorker() ] file="workers2.py", line_no="206", BucketTransfer Bucket=_audit~2~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56, State=warm, Path=/Users/mdoyle/VSCode/splunk/splunk/var/lib/splunk/audit/db/db_1615210089_1615210069_2_370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56, Action=update_manifest, Status=attempting, Retries=0
2021-04-18 15:55:06,660    DEBUG [ uploadWorker-1:uploadWorker() ] file="workers2.py", line_no="204", BucketTransfer Bucket=_audit~4~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56, State=warm, Path=/Users/mdoyle/VSCode/splunk/splunk/var/lib/splunk/audit/db/db_1618515821_1615216083_4_370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56, Action=repair, Status=skipped, ElapsedSec=0.28, Retries=0, Reason="--fix_buckets not selected"
2021-04-18 15:55:06,660    DEBUG [ uploadWorker-1:uploadWorker() ] file="workers2.py", line_no="206", BucketTransfer Bucket=_audit~4~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56, State=warm, Path=/Users/mdoyle/VSCode/splunk/splunk/var/lib/splunk/audit/db/db_1618515821_1615216083_4_370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56, Action=update_manifest, Status=attempting, Retries=0
2021-04-18 15:55:06,662    DEBUG [ uploadWorker-0:uploadWorker() ] file="workers2.py", line_no="221", BucketTransfer Bucket=_audit~2~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56, State=warm, Path=/Users/mdoyle/VSCode/splunk/splunk/var/lib/splunk/audit/db/db_1615210089_1615210069_2_370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56, Action=upload, Status=attempting, Retries=0
2021-04-18 15:55:06,662    DEBUG [ uploadWorker-3:uploadWorker() ] file="workers2.py", line_no="221", BucketTransfer Bucket=_audit~3~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56, State=warm, Path=/Users/mdoyle/VSCode/splunk/splunk/var/lib/splunk/audit/db/db_1615216070_1615214162_3_370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56, Action=upload, Status=attempting, Retries=0
2021-04-18 15:55:06,662    DEBUG [ uploadWorker-0:uploadWorker() ] file="workers2.py", line_no="230", {'object_prefix': './guidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56', 'earliest_event': 1615210069, 'latest_event': 1615210089, 'first_index_time': 1615210070, 'last_index_time': 1615210089}
2021-04-18 15:55:06,662    DEBUG [ uploadWorker-3:uploadWorker() ] file="workers2.py", line_no="230", {'object_prefix': './guidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56', 'earliest_event': 1615214162, 'latest_event': 1615216070, 'first_index_time': 1615214163, 'last_index_time': 1615216070}
...
2021-04-18 15:55:06,671    DEBUG [ common.transfer.destinations.gcs:upload_file() ] file="gcs.py", line_no="27", hello
2021-04-18 15:55:06,670    DEBUG [ uploadWorker-1:uploadWorker() ] file="workers2.py", line_no="257", Uploading Bucket=_audit~4~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56, File=1618515821-1615411510-859724802777538989.tsidx, SourcePath=/Users/mdoyle/VSCode/splunk/splunk/var/lib/splunk/audit/db/db_1618515821_1615216083_4_370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/1618515821-1615411510-859724802777538989.tsidx, DestPath=_audit/db/6e/90/4~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/guidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/1618515821-1615411510-859724802777538989.tsidx, SizeMB=3.25
...
2021-04-18 15:55:07,177    DEBUG [ urllib3.connectionpool:_make_request() ] file="connectionpool.py", line_no="452", https://storage.googleapis.com:443 "GET /storage/v1/b/doyle_upload/o/gcp_doyle_2021_04_17%2F_audit%2Fdb%2F6c%2F9b%2F5~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56%2FguidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56%2FHosts.data?projection=noAcl&prettyPrint=false HTTP/1.1" 404 425
2021-04-18 15:55:07,177     INFO [ common.transfer.destinations.gcs:upload_file() ] file="gcs.py", line_no="44", CopyFile src=/Users/mdoyle/VSCode/splunk/splunk/var/lib/splunk/audit/db/db_1618691466_1618691046_5/Hosts.data, dest=gcp_doyle_2021_04_17/_audit/db/6c/9b/5~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/guidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/Hosts.data
2021-04-18 15:55:07,180    DEBUG [ urllib3.connectionpool:_make_request() ] file="connectionpool.py", line_no="452", https://storage.googleapis.com:443 "GET /storage/v1/b/doyle_upload/o/gcp_doyle_2021_04_17%2F_audit%2Fdb%2F6c%2F9b%2F5~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56%2FguidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56%2FSources.data?projection=noAcl&prettyPrint=false HTTP/1.1" 404 429
2021-04-18 15:55:07,180     INFO [ common.transfer.destinations.gcs:upload_file() ] file="gcs.py", line_no="44", CopyFile src=/Users/mdoyle/VSCode/splunk/splunk/var/lib/splunk/audit/db/db_1618691466_1618691046_5/Sources.data, dest=gcp_doyle_2021_04_17/_audit/db/6c/9b/5~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/guidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/Sources.data
2021-04-18 15:55:07,184    DEBUG [ urllib3.connectionpool:_make_request() ] file="connectionpool.py", line_no="452", https://storage.googleapis.com:443 "GET /storage/v1/b/doyle_upload/o/gcp_doyle_2021_04_17%2F_audit%2Fdb%2F6c%2F9b%2F5~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56%2FguidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56%2Fbloomfilter?projection=noAcl&prettyPrint=false HTTP/1.1" 404 427
2021-04-18 15:55:07,185     INFO [ common.transfer.destinations.gcs:upload_file() ] file="gcs.py", line_no="44", CopyFile src=/Users/mdoyle/VSCode/splunk/splunk/var/lib/splunk/audit/db/db_1618691466_1618691046_5/bloomfilter, dest=gcp_doyle_2021_04_17/_audit/db/6c/9b/5~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/guidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/bloomfilter
2021-04-18 15:55:07,200    DEBUG [ urllib3.connectionpool:_make_request() ] file="connectionpool.py", line_no="452", https://storage.googleapis.com:443 "GET /storage/v1/b/doyle_upload/o/gcp_doyle_2021_04_17%2F_audit%2Fdb%2F6e%2F90%2F4~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56%2FguidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56%2Fbloomfilter?projection=noAcl&prettyPrint=false HTTP/1.1" 200 1204
2021-04-18 15:55:07,201    DEBUG [ common.transfer.destinations.gcs:upload_file() ] file="gcs.py", line_no="56", SkippingFile src=/Users/mdoyle/VSCode/splunk/splunk/var/lib/splunk/audit/db/db_1618515821_1615216083_4_370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/bloomfilter, dest=gcp_doyle_2021_04_17/_audit/db/6e/90/4~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/guidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/bloomfilter
2021-04-18 15:55:07,200    DEBUG [ urllib3.connectionpool:_make_request() ] file="connectionpool.py", line_no="452", https://storage.googleapis.com:443 "GET /storage/v1/b/doyle_upload/o/gcp_doyle_2021_04_17%2F_audit%2Fdb%2F64%2F5d%2F2~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56%2FguidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56%2Frawdata%2Fslicemin.dat?projection=noAcl&prettyPrint=false HTTP/1.1" 200 1240
2021-04-18 15:55:07,201    DEBUG [ urllib3.connectionpool:_make_request() ] file="connectionpool.py", line_no="452", https://storage.googleapis.com:443 "GET /storage/v1/b/doyle_upload/o/gcp_doyle_2021_04_17%2F_audit%2Fdb%2F64%2F5d%2F2~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56%2FguidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56%2FHosts.data?projection=noAcl&prettyPrint=false HTTP/1.1" 200 1198
2021-04-18 15:55:07,201    DEBUG [ common.transfer.destinations.gcs:upload_file() ] file="gcs.py", line_no="56", SkippingFile src=/Users/mdoyle/VSCode/splunk/splunk/var/lib/splunk/audit/db/db_1615210089_1615210069_2_370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/rawdata/slicemin.dat, dest=gcp_doyle_2021_04_17/_audit/db/64/5d/2~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/guidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/rawdata/slicemin.dat
2021-04-18 15:55:07,201    DEBUG [ common.transfer.destinations.gcs:upload_file() ] file="gcs.py", line_no="56", SkippingFile src=/Users/mdoyle/VSCode/splunk/splunk/var/lib/splunk/audit/db/db_1615210089_1615210069_2_370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/Hosts.data, dest=gcp_doyle_2021_04_17/_audit/db/64/5d/2~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/guidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/Hosts.data
2021-04-18 15:55:07,201    DEBUG [ urllib3.connectionpool:_make_request() ] file="connectionpool.py", line_no="452", https://storage.googleapis.com:443 "GET /storage/v1/b/doyle_upload/o/gcp_doyle_2021_04_17%2F_audit%2Fdb%2F6c%2F9b%2F5~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56%2FguidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56%2Foptimize.result?projection=noAcl&prettyPrint=false HTTP/1.1" 404 435
2021-04-18 15:55:07,202     INFO [ common.transfer.destinations.gcs:upload_file() ] file="gcs.py", line_no="44", CopyFile src=/Users/mdoyle/VSCode/splunk/splunk/var/lib/splunk/audit/db/db_1618691466_1618691046_5/optimize.result, dest=gcp_doyle_2021_04_17/_audit/db/6c/9b/5~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/guidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/optimize.result
2021-04-18 15:55:07,202    DEBUG [ urllib3.connectionpool:_make_request() ] file="connectionpool.py", line_no="452", https://storage.googleapis.com:443 "GET /storage/v1/b/doyle_upload/o/gcp_doyle_2021_04_17%2F_audit%2Fdb%2Fae%2F71%2F3~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56%2FguidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56%2Foptimize.result?projection=noAcl&prettyPrint=false HTTP/1.1" 200 1216
...
2021-04-18 15:55:07,252    DEBUG [ common.transfer.destinations.gcs:upload_receipt() ] file="gcs.py", line_no="78", Writing /var/folders/dt/h2tm0q3s69n8hbtst5vndvfm0000gn/T/tmpmatoywaf to gcp_doyle_2021_04_17/_audit/db/6e/90/4~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/receipt.json
2021-04-18 15:55:07,252    DEBUG [ common.transfer.destinations.gcs:upload_receipt() ] file="gcs.py", line_no="78", Writing /var/folders/dt/h2tm0q3s69n8hbtst5vndvfm0000gn/T/tmpgcqnmeg9 to gcp_doyle_2021_04_17/_audit/db/ae/71/3~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/receipt.json
...
2021-04-18 16:06:18,766    DEBUG [ downloadWorker-3:downloadWorker() ] file="workers2.py", line_no="430", Downloading Bucket=_internal~1~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56, File=guidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/SourceTypes.data, SourcePath=gcp_doyle_2021_04_17/_internal/db/a2/6c/1~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/guidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/SourceTypes.data, DestPath=/Users/mdoyle/VSCode/splunk/splunk/var/lib/splunk/_internaldb/thaweddb/db_1618258770_1615209949_1_370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/SourceTypes.data, SizeMB=0.00
2021-04-18 16:06:18,767    DEBUG [ google.auth.transport.requests:__call__() ] file="requests.py", line_no="182", Making request: POST https://oauth2.googleapis.com/token
2021-04-18 16:06:18,767    DEBUG [ common.transfer.sources.gcs:localize_file() ] file="gcs.py", line_no="88", hello
2021-04-18 16:06:18,768    DEBUG [ urllib3.connectionpool:_new_conn() ] file="connectionpool.py", line_no="971", Starting new HTTPS connection (1): oauth2.googleapis.com:443
2021-04-18 16:06:18,773    DEBUG [ urllib3.util.retry:from_int() ] file="retry.py", line_no="332", Converted retries value: 3 -> Retry(total=3, connect=None, read=None, redirect=None, status=None)
2021-04-18 16:06:18,774    DEBUG [ google.auth.transport.requests:__call__() ] file="requests.py", line_no="182", Making request: POST https://oauth2.googleapis.com/token
2021-04-18 16:06:18,775    DEBUG [ urllib3.connectionpool:_new_conn() ] file="connectionpool.py", line_no="971", Starting new HTTPS connection (1): oauth2.googleapis.com:443
2021-04-18 16:06:18,905    DEBUG [ urllib3.connectionpool:_make_request() ] file="connectionpool.py", line_no="452", https://oauth2.googleapis.com:443 "POST /token HTTP/1.1" 200 None
2021-04-18 16:06:18,908    DEBUG [ urllib3.connectionpool:_new_conn() ] file="connectionpool.py", line_no="971", Starting new HTTPS connection (1): storage.googleapis.com:443
2021-04-18 16:06:18,910    DEBUG [ urllib3.connectionpool:_make_request() ] file="connectionpool.py", line_no="452", https://oauth2.googleapis.com:443 "POST /token HTTP/1.1" 200 None
2021-04-18 16:06:18,912    DEBUG [ urllib3.connectionpool:_new_conn() ] file="connectionpool.py", line_no="971", Starting new HTTPS connection (1): storage.googleapis.com:443
2021-04-18 16:06:19,100    DEBUG [ urllib3.connectionpool:_make_request() ] file="connectionpool.py", line_no="452", https://storage.googleapis.com:443 "GET /download/storage/v1/b/doyle_upload/o/gcp_doyle_2021_04_17%2F_internal%2Fdb%2F6d%2F70%2F9~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56%2FguidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56%2Fbloomfilter?alt=media HTTP/1.1" 200 1391323
2021-04-18 16:06:19,104    DEBUG [ urllib3.connectionpool:_make_request() ] file="connectionpool.py", line_no="452", https://storage.googleapis.com:443 "GET /download/storage/v1/b/doyle_upload/o/gcp_doyle_2021_04_17%2F_internal%2Fdb%2Fa2%2F6c%2F1~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56%2FguidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56%2FSourceTypes.data?alt=media HTTP/1.1" 200 723
2021-04-18 16:06:19,105    DEBUG [ downloadWorker-3:downloadWorker() ] file="workers2.py", line_no="430", Downloading Bucket=_internal~1~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56, File=guidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/bucket_info.csv, SourcePath=gcp_doyle_2021_04_17/_internal/db/a2/6c/1~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/guidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/bucket_info.csv, DestPath=/Users/mdoyle/VSCode/splunk/splunk/var/lib/splunk/_internaldb/thaweddb/db_1618258770_1615209949_1_370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/bucket_info.csv, SizeMB=0.00
2021-04-18 16:06:19,105    DEBUG [ common.transfer.sources.gcs:localize_file() ] file="gcs.py", line_no="88", hello
2021-04-18 16:06:19,111    DEBUG [ urllib3.util.retry:from_int() ] file="retry.py", line_no="332", Converted retries value: 3 -> Retry(total=3, connect=None, read=None, redirect=None, status=None)
2021-04-18 16:06:19,112    DEBUG [ google.auth.transport.requests:__call__() ] file="requests.py", line_no="182", Making request: POST https://oauth2.googleapis.com/token
2021-04-18 16:06:19,113    DEBUG [ urllib3.connectionpool:_new_conn() ] file="connectionpool.py", line_no="971", Starting new HTTPS connection (1): oauth2.googleapis.com:443
2021-04-18 16:06:19,136    DEBUG [ urllib3.connectionpool:_make_request() ] file="connectionpool.py", line_no="452", https://storage.googleapis.com:443 "GET /download/storage/v1/b/doyle_upload/o/gcp_doyle_2021_04_17%2F_internal%2Fdb%2Fa4%2Fa5%2F7~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56%2Freceipt.json?alt=media HTTP/1.1" 200 1925
2021-04-18 16:06:19,138    DEBUG [ downloadWorker-1:downloadWorker() ] file="workers2.py", line_no="430", Downloading Bucket=_internal~7~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56, File=guidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/SourceTypes.data, SourcePath=gcp_doyle_2021_04_17/_internal/db/a4/a5/7~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/guidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/SourceTypes.data, DestPath=/Users/mdoyle/VSCode/splunk/splunk/var/lib/splunk/_internaldb/thaweddb/db_1616944087_1616512107_7_370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/SourceTypes.data, SizeMB=0.00

```

As each bucket completes the upload or download process, an entry is added to **uploadResult.json** or **downloadResult.json** respectively, 
which are composed of single line JSON status events that can be ingested into Splunk for analysis. 
These logs contain the final status of the operation on each bucket -- success, failure, or skipped.

Example below is formatted for easy reading, actual entries are all in a single line.


```
{
    "timestamp": 1618775877.463422,
    "result": "success",
    "receipt": {
        "objects": [
            {
                "name": "./guidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/SourceTypes.data",
                "size": 252
            },
            {
                "name": "./guidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/bucket_info.csv",
                "size": 67
            },
            {
                "name": "./guidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/1618691031-1618690798-16168717449324002908.tsidx",
                "size": 152225
            },
            {
                "name": "./guidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/Sources.data",
                "size": 1345
            },
            {
                "name": "./guidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/splunk-autogen-params.dat",
                "size": 96
            },
            {
                "name": "./guidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/merged_lexicon.lex",
                "size": 16076418
            },
            {
                "name": "./guidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/1618603501-1618600381-15139669359907172547.tsidx",
                "size": 1098000
            },
            {
                "name": "./guidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/optimize.result",
                "size": 49
            },
            {
                "name": "./guidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/1618690788-1615209971-15146989465947506625.tsidx",
                "size": 66145566
            },
            {
                "name": "./guidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/Hosts.data",
                "size": 111
            },
            {
                "name": "./guidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/1618600381-1618593360-15139306654213974882.tsidx",
                "size": 2322768
            },
            {
                "name": "./guidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/bloomfilter",
                "size": 2485619
            },
            {
                "name": "./guidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/.rawSize",
                "size": 10
            },
            {
                "name": "./guidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/Strings.data",
                "size": 42921
            },
            {
                "name": "./guidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/rawdata/journal.gz",
                "size": 23404338
            },
            {
                "name": "./guidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/rawdata/slicemin.dat",
                "size": 4991
            },
            {
                "name": "./guidSplunk-370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56/rawdata/slicesv2.dat",
                "size": 41563
            }
        ],
        "manifest": {
            "id": "_introspection~0~370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56",
            "path": "db_1618691031_1615209971_0_370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56",
            "raw_size": "156046520",
            "event_count": "105480",
            "host_count": "1",
            "source_count": "9",
            "sourcetype_count": "3",
            "size_on_disk": "94064640",
            "modtime": "1618691047",
            "frozen_in_cluster": "0",
            "origin_site": "",
            "tsidx_minified": "0",
            "journal_size": "23404338"
        },
        "user_data": {
            "uploader_guid": "370EDF4A-4B9D-4D28-A021-8DFC2BAE4C56",
            "content_hash": "0B58E405420786C0593548E52AE339CF94B2C4468DC994AD52BF10D7A2B491F8"
        }
    }
}
```

## Who do I talk to ##

Michael F. Doyle, SplunkCloud PS  
mdoyle@splunk.com
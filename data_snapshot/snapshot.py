import datetime
import json
import multiprocessing as mp
import os
import pickle
import platform
import posixpath
import queue
import random
import signal
import sys

if sys.version_info[0] < 3:
    raise SystemExit(f"Requires Python 3.7+ installed={sys.version.split()[0]!r}")
elif sys.version_info[1] < 7:
    raise SystemExit(f"Requires Python 3.7+ installed={sys.version.split()[0]!r}")
import time
import uuid
from getpass import getpass

# from common import *
# try:
#     from cryptography.fernet import Fernet
# except ImportError as e:
#     raise SystemExit(f"{e!r}: Install this module (e.g. pip install <<module>>)")

from common import *

if __name__ == "__main__":
    # logListenerConfig(debug=True, verbose=False)

    # args
    _args = arg_parser()

    # transfer settings
    try:
        with open (_args.conf, 'r') as f:
            _args.settings = json.load(f)
    except FileNotFoundError:
        sys.stderr.write('Missing {}\n'.format(_args.conf))
        raise SystemExit(4)

    try:
        validate_settings(_args.dest, _args.settings)
    except AttributeError:
        try:
            validate_settings(_args.src, _args.settings)
        except Exception as e:
            sys.stderr.write("Invalid Settings: {}\n".format(repr(e)))
            raise SystemExit(5)
    except Exception as e:
        sys.stderr.write("Invalid Settings: {}\n".format(repr(e)))
        raise SystemExit(5)

    # Silly conditional import trick
    # This code supports both multiprocessing and threads
    # threading.Thread is imported as Process and the interface
    # is the same as multiprocessing.Process
    if _args.threads:
        from queue import Queue
        from threading import Event, Lock
        from threading import Thread as Process
    else:
        from multiprocessing import Event
        from multiprocessing import JoinableQueue as Queue
        from multiprocessing import (Lock, Process, current_process,
                                     get_start_method, set_start_method)

        # to test compatibility with windows multiprocessing:
        if _args.mp_start_method is not None:
            mp.set_start_method(_args.mp_start_method)
        else:
            mp.get_start_method()

    # save the default
    default_handler = signal.getsignal(signal.SIGINT)

    # Set signal handling of SIGINT to ignore mode.
    signal.signal(signal.SIGINT, signal.SIG_IGN)

    exit_event = Event()
    resume_event = Event()
    status_exit = Event()
    work_queue = Queue(500)
    retry_queue = Queue()
    logging_queue = Queue()
    status_queue = Queue()
    pickle_lock = Lock()

    logProducerConfig(logging_queue)
    logger = logging.getLogger(__name__)
    logger.debug("Logging Process Started")
    logger.debug("Running on {} {}".format(platform.system(), platform.version()))
    logger.debug("platform: {} ({})".format(sys.platform, os.name))
    logger.debug("Python Version: {}".format(sys.version))

    # Start the logging processes
    # start the log worker
    log_worker = Process(target=logListenerProcess, args=(logging_queue,_args.debug, _args.verbose, _args.settings['log_dir']))
    log_worker.daemon = True
    log_worker.start()

    # Start the transfer worker processes
    workers = []
    if _args.cmd == "archive":

        if _args.debug: logger.debug("Creating Upload Worker Processes")
        for i in range(_args.workers):
            # Silly conditional import trick
            # This code supports both multiprocessing and threads
            # threading.Thread is imported as Process and the interface
            # is the same as multiprocessing.Process
            process = Process(target=uploadWorker, name="uploadWorker-{}".format(i), args=(_args, exit_event, resume_event, work_queue, retry_queue, logging_queue, status_queue, pickle_lock),)
            process.start()
            workers.append(process)

    elif _args.cmd == "restore":
        logger.debug("Creating Download Worker Processes")
        for i in range(_args.workers):
            # Silly conditional import trick
            # This code supports both multiprocessing and threads
            # threading.Thread is imported as Process and the interface
            # is the same as multiprocessing.Process
            process = Process(target=downloadWorker, name="downloadWorker-{}".format(i), args=(_args, exit_event, resume_event, work_queue, retry_queue, logging_queue, status_queue, pickle_lock),)
            process.start()
            workers.append(process)

    else:
        raise NotImplementedError(_args.cmd)

    # start the periodic status worker
    status_process = Process(target=status_worker, name="PeriodicStatus", args=(_args, status_exit, resume_event, status_queue, logging_queue),)
    status_process.daemon = True
    status_process.start()

    # restore default signal handling for the parent process.
    signal.signal(signal.SIGINT, default_handler)

    # try:
    #     with open('.secret_key', 'rb') as f:
    #         key = f.read()
    # except FileNotFoundError:
    #     key = Fernet.generate_key()
    #     with open('.secret_key', 'wb') as f:
    #         f.write(key)

    # fernet = Fernet(key)

    try:
        print("Provide the authentication information for Splunk")
        username = input("Enter username (must have 'admin' Splunk role assigned): ")
        # password = fernet.encrypt(getpass().encode())
        # password = getpass().encode()
        password = getpass()

        # mark the start of this run
        start_t = time.time()

        _args.connect = {
            'host': '127.0.0.1',
            'port': _args.local_port,
            'scheme': _args.scheme,
            'timeout': _args.timeout,
            'username': username,
            'password': password
            }

        resume_event.set()

        if _args.cmd == "archive":
            archive().run(_args, work_queue, status_queue, pickle_lock)
        elif _args.cmd == "restore":
            restore().run(_args, work_queue, status_queue, pickle_lock)
        elif _args.cmd == "discover":
            pass
            # discover().run(_args)
        else:
            raise NotImplementedError(_args.cmd)

        # check to be sure the workers are alive
        # while True:
        #     if not work_queue.empty:
        #         print("q")
        #         if len([x for x in workers if x.is_alive()]) == 0:
        #             raise UnrecoverableError("All Worker Threads have died")
        #         else:
        #             continue
        #     else:
        #         break
        work_queue.join()
        retry_queue.join()

    except Exception as e:
        logger.debug(repr(e), stack_info=True)
        logger.critical(f"Abnormal Termination {repr(e)}")
        raise SystemExit(e)
    else:
        logger.info(f"Job Complete")
    finally:
        logger.info("Attempting Graceful Shutdown")
        logger.info("Sending Exit Event to all processes")
        exit_event.set()
        logger.info("Waiting for Process Cleanup...")
        try:
            for worker in [x for x in workers if x.is_alive()]:
                try: worker.join(timeout=10)
                except: pass
                try: worker.terminate()
                except: continue
        except:
            pass

        logger.info("All Worker Processes Stopped")
        status_queue.join()
        logger.info("Stopping Status Process...")
        status_exit.set()
        try: status_process.join(10)
        except: status_process.terminate()
        logger.debug("Status Process Stopped")

        del work_queue
        del retry_queue
        del status_queue
    if start_t is None:
        print("[Shutdown Complete]", file=sys.stdout)
    else:
        print("[Shutdown Complete] Total Running Time: {}".format(str(datetime.timedelta(seconds=time.time() - start_t))), file=sys.stdout)

import logging
import multiprocessing as mp
import os
import subprocess
import sys
import threading

from common import *

common.lib_path.make_lib_path()

from ps_common import *
# from ps_common import Splunk, SplunkIndexer, SplunkManager
from ps_common.exceptions import *

logListenerConfig(debug=True, log_dir="scan_log", verbose=True)
# from common import *

@debug_wrapper(True)
def main(args):
    workers = []
    
    for _root, _dirs, _files in os.walk(os.path.join(args.index_path), topdown=True):
        for bucket in [ x for x in _dirs if (x.startswith('db_') or x.startswith('rb_')) and not x.endswith('-tmp')]:
            path = os.path.normpath(os.path.join(_root, bucket))
            p = repairWorker(kwargs={"path": path, "splunk_home": args.splunk_home, "threads": args.thread_limit, "force_repair": args.force})
            workers.append(p)
            p.daemon = True
            p.start()
    
    for p in [x for x in workers if x.is_alive]:
        success, msg = p.join()
        if not success: 
            logger.error(msg)
        else:
            logger.info(msg)
            
if __name__ == '__main__':
    logger = logging.getLogger(os.path.basename(sys.argv[0]))
    import argparse
    
    cli = argparse.ArgumentParser()
    cli.add_argument('splunk_home')
    cli.add_argument('index_path')
    cli.add_argument('--force', '-f', help='Force repair of all buckets', action='store_true')
    cli.add_argument('--thread-limit', '-t', type=int, metavar='INT', help=argparse.SUPPRESS, default=30)
    args = cli.parse_args()
    
    main(args)

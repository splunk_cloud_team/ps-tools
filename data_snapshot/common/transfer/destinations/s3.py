TRACE = True
import io
import json
import logging
import os
import posixpath
import sys
import time
from tempfile import NamedTemporaryFile

import boto3
from boto3.exceptions import S3UploadFailedError
from botocore.exceptions import ClientError
from ps_common import *
from ps_common.s2lib import ReceiptJson

from ..common import *

@unrecoverable_wrapper
@debug_wrapper(TRACE)
def upload_file(src, dest, dest_config, e_q, u_q, s_q, hash_func):
    try:
        logger = logging.getLogger(__name__)
        logger.debug('hello')
        with open(os.devnull, 'w') as sys.stderr:
            
            s3, transfer_config = setup_aws_session(dest_config)
            
            _dest = posixpath.normpath(posixpath.join(dest_config['bucket_path'], dest.replace(os.sep, posixpath.sep)))

            # Check if object is already there:
            try:
                response = s3.head_object(Bucket=dest_config['bucket_name'], Key=_dest)
                
            except ClientError as e:
                if int(e.response['Error']['Code']) in [403, 404]:
                    logger.debug('CopyFile src={}, dest={}'.format(src, _dest))
                    start_t = time.time()
                    try:
                        s3.upload_file(src,
                            dest_config['bucket_name'],
                            _dest,
                            Config = transfer_config)
                    except ClientError as e:
                        logger.warn('CopyFile src={}, dest={}, exception={}'.format(src, _dest, repr(e)))
                        logger.debug(repr(e), stack_info=True, exc_info=True)
                        e_q.put(e)
                    else:
                        logger.debug('CopyFile src={}, dest={}, elapsed_sec={}'.format(src, _dest, time.time() - start_t))
                        u_q.put(1)
                        
                    return
                
                elif int(e.response['Error']['Code']) == 403:
                    logger.warn('CopyFile bucket={}, key={}, exception={}'.format(dest_config['bucket_name'], _dest, repr(e)))
                    e_q.put(e)
                    return
                
                else:
                    logger.warn('CopyFile src={}, dest={}, exception={}'.format(src, _dest, repr(e)))
                    logger.debug(repr(e), stack_info=True, exc_info=True)
                    e_q.put(e)
                    return
            
            logger.debug('SkippingFile src={}, dest={}'.format(src, _dest))
            
            s_q.put(1)
            
    except Exception as e:
        logger.error(repr(e))
        logger.debug(repr(e), stack_info=True, exc_info=True)
        e_q.put(e)

@unrecoverable_wrapper
@debug_wrapper(TRACE)
def upload_receipt(receipt, dest_config):
    try:
        logger = logging.getLogger(__name__)
        logger.debug('hello')
        with open(os.devnull, 'w') as sys.stderr:
            
            s3, transfer_config = setup_aws_session(dest_config)
            
            _dest = posixpath.normpath(posixpath.join(dest_config['bucket_path'], receipt.s2_path, 'receipt.json'))
            
            buf = io.BytesIO(receipt.json.encode())
            logger.debug('Writing receipt to {}'.format(_dest))
            s3.upload_fileobj(buf,
                dest_config['bucket_name'],
                _dest,
                Config = transfer_config)

            # with NamedTemporaryFile("w+") as f:
            #     logger.debug('Writing {} to {}'.format(f.name, _dest))
            #     f.write(receipt.json)
            #     f.seek(0)
            #     s3.upload_file(f.name,
            #         dest_config['bucket_name'],
            #         _dest,
            #         Config = transfer_config)
            #     f.seek(0)
            #     logger.debug(f.read())
                
    except Exception as e:
        logger.debug(repr(e), stack_info=True, exc_info=True)
        return False
    return True

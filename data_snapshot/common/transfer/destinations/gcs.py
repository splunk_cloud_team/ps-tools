TRACE = True
import io
import json
import logging
import os
import posixpath
import sys
import time
from tempfile import NamedTemporaryFile

from requests import JSONDecodeError

from ps_common import *
from ps_common.s2lib import ReceiptJson

from ..common import *

try:
    from google.cloud import storage
except ImportError as e:
    logger.debug("GCS_Transfer Failed Exception={}".format(e))
    logger.error("Did you 'pip install google.cloud.storage' ?")
    raise UnrecoverableError('Failed to load google cloud storage')

@unrecoverable_wrapper
@debug_wrapper(TRACE)
def upload_file(src, dest, dest_config, e_q, u_q, s_q, hash_func):
    CHUNK_SIZE = 128 * 1024 * 1024
    try:
        logger = logging.getLogger(__name__)
        logger.debug('hello')
        with open(os.devnull, 'w') as sys.stderr:

            try:
                storage_client = storage.Client.from_service_account_json(dest_config['gcs_auth_file'])
            except FileNotFoundError:
                logger.critical(f"Unable to load gcs_auth_file: {dest_config.get('gcs_auth_file', None)}. Check settings.conf")
                raise UnrecoverableError("Unable to load gcs_auth_file: \"{}\". Check settings.conf".format(dest_config.get('gcs_auth_file', "None")))
            except JSONDecodeError as e:
                logger.critical(f"Bad JSON Format in {dest_config.get('gcs_auth_file', None)!r}")
                raise Exception(f"Bad JSON Format in {dest_config.get('gcs_auth_file', None)!r} {repr(e)}")
            
            try:
                bucket = storage_client.bucket(dest_config['bucket_name'])
            except:
                raise UnrecoverableError("Not a valid Bucket Name")
            
            _dest = posixpath.normpath(posixpath.join(dest_config['bucket_path'], dest.replace(os.sep, posixpath.sep)))

            # Check if object is already there:
            if bucket.get_blob(_dest) is None:
                logger.info('CopyFile src={}, dest={}'.format(src, _dest))
                blob = bucket.blob(_dest, chunk_size=CHUNK_SIZE)
            
                start_t = time.time()
                with open(src, "rb") as f:
                    blob.upload_from_file(f, timeout=(5,900))
                    
                    
                logger.info('CopyFile src={}, elapsed_sec={}'.format(src, time.time() - start_t))
                u_q.put(1)
                return
            
            logger.debug('SkippingFile src={}, dest={}'.format(src, _dest))
            s_q.put(1)
    
    except Exception as e:
        logger.error(repr(e))
        logger.debug(repr(e), stack_info=True, exc_info=True)
        e_q.put(e)

@unrecoverable_wrapper
@debug_wrapper(TRACE)
def upload_receipt(receipt, dest_config):
    CHUNK_SIZE = 128 * 1024 * 1024
    try:
        logger = logging.getLogger(__name__)
        logger.debug('hello')
        with open(os.devnull, 'w') as sys.stderr:
            
            try:
                storage_client = storage.Client.from_service_account_json(dest_config['gcs_auth_file'])
            except FileNotFoundError:
                logger.critical(f"Unable to load gcs_auth_file: {dest_config.get('gcs_auth_file', None)}. Check settings.conf")
                raise UnrecoverableError("Unable to load gcs_auth_file: \"{}\". Check settings.conf".format(dest_config.get('gcs_auth_file', "None")))
            except JSONDecodeError as e:
                logger.critical(f"Bad JSON Format in {dest_config.get('gcs_auth_file', None)!r}")
                raise Exception(f"Bad JSON Format in {dest_config.get('gcs_auth_file', None)!r} {repr(e)}")
                
            bucket = storage_client.bucket(dest_config['bucket_name'])
            
            _dest = posixpath.normpath(posixpath.join(dest_config['bucket_path'], receipt.s2_path, 'receipt.json'))
            blob = bucket.blob(_dest, chunk_size=CHUNK_SIZE)
            
            with NamedTemporaryFile("w+") as f:
                logger.debug('Writing {} to {}'.format(f.name, _dest))
                f.write(receipt.json)
                f.seek(0)
                blob.upload_from_file(f, timeout=(5,900))
                f.seek(0)
                logger.debug(f.read())
                
    except Exception as e:
        logger.debug(repr(e), stack_info=True, exc_info=True)
        return False
    return True
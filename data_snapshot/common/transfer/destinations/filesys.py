TRACE = True
import io
import json
import logging
import os
import posixpath
import shutil
import sys
import time

from ps_common import *
from ps_common.s2lib import ReceiptJson

from ..common import *

@unrecoverable_wrapper
@debug_wrapper(TRACE)
def upload_file(src, dest, dest_config, e_q, u_q, s_q, hash_func):
    try:
        logger = logging.getLogger(__name__)
        logger.debug('hello')
        
        _dest = os.path.join(dest_config['path'], dest.replace(posixpath.sep, os.path.sep))
        os.makedirs(os.path.dirname(_dest), exist_ok=True)
        logger.debug('CopyFile src={}, dest={}'.format(src, _dest))
        shutil.copyfile(src, _dest)
        u_q.put(1)
        
    except Exception as e:
        logger.debug(repr(e), stack_info=True, exc_info=True)
        e_q.put(e)

@unrecoverable_wrapper
@debug_wrapper(TRACE)
def upload_receipt(receipt, dest_config):
    try:
        logger = logging.getLogger(__name__)
        logger.debug('hello')
    
        _dest = os.path.abspath(os.path.join(dest_config['path'], receipt.s2_path, 'receipt.json'))
        logger.debug('Writing receipt.json to {}'.format(_dest))
        
        with open(_dest, 'w') as f:
            f.write(receipt.json)
        
    except Exception as e:
        logger.debug(repr(e), stack_info=True, exc_info=True)
        return False
    
    else:
        return True


# def dest_filesys(src, dest, dest_config, e_q, u_q, s_q, hash_func):
#     try:
#         logger = logging.getLogger(__name__)
#         logger.debug('hello')
        
#         _dest = os.path.join(dest_config['path'], dest.replace(posixpath.sep, os.path.sep))
#         os.makedirs(os.path.dirname(_dest), exist_ok=True)
#         logger.debug('CopyFile src={}, dest={}'.format(src, _dest))
#         shutil.copyfile(src, _dest)
#         u_q.put(1)
        
#     except Exception as e:
#         logger.debug(repr(e), stack_info=True, exc_info=True)
#         e_q.put(e)

# def finalize_filesys(receipt, dest_config):
#     try:
#         logger = logging.getLogger(__name__)
#         logger.debug('hello')
    
#         _dest = os.path.abspath(os.path.join(dest_config['path'], receipt.s2_path, 'receipt.json'))
#         logger.debug('Writing receipt.json to {}'.format(_dest))
#         with open(_dest, 'w') as f:
#             f.write(receipt.json)
#     except Exception as e:
#         logger.debug(repr(e), stack_info=True, exc_info=True)
#         return False
    
#     return True

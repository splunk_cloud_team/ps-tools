TRACE=True
import boto3
from boto3.s3.transfer import TransferConfig
from botocore.config import Config
from botocore.exceptions import ClientError
from ps_common import *

@unrecoverable_wrapper
@debug_wrapper(TRACE)
def setup_aws_session(config):
    session_config = Config(
                max_pool_connections=100,
                connect_timeout=60,
                read_timeout=3600,
                retries = {"max_attempts": 3}
                )
    
    transfer_config = TransferConfig(
                multipart_threshold=8 * 1024 * 1024, 
                max_concurrency=10, 
                multipart_chunksize=8 * 1024 * 1024, 
                num_download_attempts=5, 
                max_io_queue=100, 
                io_chunksize=1024 * 1024, 
                use_threads=True)
    
    session = boto3.session.Session()
    
    s3_client = session.client('s3', 
                        endpoint_url=config.get('endpoint_url', None), 
                        aws_access_key_id=config.get('aws_access_key_id', None), 
                        aws_secret_access_key=config.get('aws_secret_access_key', None), 
                        region_name=config.get('aws_region', None),
                        verify=config.get('verify', True), 
                        config=session_config)
    
    return s3_client, transfer_config


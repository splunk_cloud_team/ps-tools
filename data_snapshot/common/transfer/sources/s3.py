TRACE = True
import io
import json
import logging
import os
import posixpath
import sys
import time
# from tempfile import NamedTemporaryFile
from collections import OrderedDict

import boto3
from boto3.exceptions import S3UploadFailedError
from botocore.exceptions import ClientError
from ps_common import *
from ps_common import debug_wrapper
# from ps_common.s2lib import ReceiptJson

from ..common import *

@debug_wrapper(TRACE)
def list_indexes(src_config):
    try:
        logger = logging.getLogger(__name__)
        logger.debug('hello')
        logger.debug(src_config)
        
        with open(os.devnull, 'w') as sys.stderr:
            
            s3, transfer_config = setup_aws_session(src_config)
            
            retries = 0
            while True:
                try:
                    paginator = s3.get_paginator('list_objects')
                except Exception as e:
                    retries += 1
                    if retries < 5:
                        time.sleep(retries)
                        logger.warning("AWS connection problems {}".format(e.args))
                        continue # retry
                    else:
                        logger.error("AWS connection failure {}".format(e.args))
                        raise UnrecoverableError(e)
                else:
                    break # while
                
            pages = paginator.paginate(Bucket=src_config['bucket_name'],
                                       Prefix=posixpath.normpath(src_config['bucket_path']) + posixpath.sep,
                                       Delimiter=posixpath.sep,
                                       PaginationConfig={'PageSize': 1000})
            for page in pages:
                try:
                    contents = page['CommonPrefixes']
                except KeyError:
                    logger.debug("GetReceipts Action=download, Status=complete")
                    break
                
                for item in contents:
                    yield(item['Prefix'].split(posixpath.sep)[-2])
            
            
    except Exception as e:
        logger.debug(repr(e), stack_info=True, exc_info=True)
        return False
    
    return True

@debug_wrapper(TRACE)
def list_receipts(my_num, idx_num, src_config):
    try:
        logger = logging.getLogger(__name__)
        logger.debug('hello')
        logger.debug(src_config)
        
        with open(os.devnull, 'w') as sys.stderr:
            
            s3, transfer_config = setup_aws_session(src_config)
            
            retries = 0
            while True:
                try:
                    paginator = s3.get_paginator('list_objects')
                except Exception as e:
                    retries += 1
                    if retries < 5:
                        time.sleep(retries * 10)
                        logger.warning("AWS connection problems {}".format(e.args))
                        continue # retry
                    else:
                        logger.error("AWS connection failure {}".format(e.args))
                        time.sleep(300)
                        retries = 0
                        continue
                else:
                    break # while
            
            retries = 0
            while True:
                try:
                    pages = paginator.paginate(Bucket=src_config['bucket_name'],
                                                Prefix=posixpath.normpath(src_config['bucket_path']) + posixpath.sep,
                                                # Delimiter=posixpath.sep,
                                                PaginationConfig={'PageSize': 1000})
                except Exception as e:
                    retries += 1
                    if retries < 5:
                        logger.warning(repr(e), stack_info=True)
                        time.sleep(retries * 10)
                        continue
                    else:
                        logger.error(repr(e), stack_info=True)
                        time.sleep(300)
                        retries = 0
                        continue
                else:
                    break
            
            for page in pages:
                try:
                    contents = page['Contents']
                except KeyError:
                    logger.info("GetReceipts Action=download, Status=complete")
                    break
                
                for item in [x for x in contents if x['Key'].endswith('receipt.json')]:
                    # if not item['Key'].endswith('receipt.json'):
                    #     continue
                    try:
                        bkt_num = int(posixpath.dirname(item['Key']).split(posixpath.sep)[-1].split("~")[0])
                    except ValueError:
                        continue
                    if bkt_num % idx_num == my_num:
                        yield(item['Key'])
                    else:
                        continue
                    
                    
    except Exception as e:
        logger.debug(repr(e), stack_info=True, exc_info=True)
        return False
    
    return True
    
@debug_wrapper(TRACE)
def get_receipt(src, src_config):
    try:
        logger = logging.getLogger(__name__)
        logger.debug('hello')
        
        with open(os.devnull, 'w') as sys.stderr:
            
            # f = io.BytesIO() # In memory file object
            
            s3, transfer_config = setup_aws_session(src_config)

            # download the receipt to a temp file
            # with NamedTemporaryFile('w+b') as f:
            with io.BytesIO() as f:
                s3.download_fileobj(src_config['bucket_name'], 
                                src, 
                                f,
                                Config=transfer_config)
                # f.seek(0)
                return json.loads(f.getvalue().decode("utf-8"), object_pairs_hook=OrderedDict)
                # return json.load(f)
            
    except Exception as e:
        logger.debug(repr(e), stack_info=True, exc_info=True)
        return None
        
@debug_wrapper(TRACE)          
def localize_file(src, dest, src_config, e_q, d_q):
    try:
        logger = logging.getLogger(__name__)
        logger.debug('hello')

        with open(os.devnull, 'w') as sys.stderr:
            
            os.makedirs(os.path.dirname(dest), exist_ok=True)
            
            s3, transfer_config = setup_aws_session(src_config)
        
            s3.download_file(src_config['bucket_name'], 
                                 src, 
                                 dest,
                                 Config=transfer_config)

    except ClientError as e:
        if e.response['Error']['Code'] == "404":
           logger.error(f"Expected object {posixpath.join(src_config['bucket_name'], src)} does not exist.")
        else:
           logger.debug(repr(e), stack_info=True, exc_info=True)    
        e_q.put(e)
        return repr(e)

    except Exception as e:
        logger.debug(repr(e), stack_info=True, exc_info=True)
        e_q.put(e)
        return repr(e)
    
    d_q.put(1)

TRACE = True
import io
import json
import logging
import os
import posixpath
import shutil
import sys
import time

from ps_common import *
from ps_common.s2lib import ReceiptJson

from ..common import *

@debug_wrapper(TRACE)
def list_receipts(my_num, idx_num, src_config):
    try:
        logger = logging.getLogger(__name__)
        
        with open(os.devnull, 'w') as sys.stderr:
            for root, _dir, files in os.walk(src_config['path']):
                for file in [x for x in files if x.endswith('receipt.json')]:
                    bkt_num = int(os.path.dirname(os.path.join(root, file)).split(os.path.sep)[-1].split("~")[0])
                    if bkt_num % idx_num == my_num:
                        yield os.path.normpath(os.path.join(root, file))
                    else:
                        continue
            
            logger.debug("GetReceipts Action=download, Status=complete")
                    
    except Exception as e:
        logger.debug(repr(e), stack_info=True, exc_info=True)
        return False
    
    return True

@debug_wrapper(TRACE)
def get_receipt(src, src_config):
    try:
        logger = logging.getLogger(__name__)
        logger.debug('hello')
        
        with open(os.devnull, 'w') as sys.stderr:
            # download the receipt to a temp file
            with open(src, 'r') as f:
                return json.load(f)
            
    except Exception as e:
        logger.debug(repr(e), stack_info=True, exc_info=True)
        return None
        
@debug_wrapper(TRACE)            
def localize_file(src, dest, src_config, e_q, d_q):
    try:
        logger = logging.getLogger(__name__)
        logger.debug('hello')

        with open(os.devnull, 'w') as sys.stderr:
            os.makedirs(os.path.dirname(dest), exist_ok=True)
            shutil.copyfile(src, dest)

    except FileNotFoundError as e:
        logger.error(f"Expected file {src} not found.")   
        e_q.put(e)
        return repr(e)

    except Exception as e:
        logger.debug(repr(e), stack_info=True, exc_info=True)
        e_q.put(e)
        return repr(e)
        
    d_q.put(1)

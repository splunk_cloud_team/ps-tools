TRACE = True
import io
import json
import logging
import os
import posixpath
import sys
import time
# from tempfile import NamedTemporaryFile
from collections import OrderedDict

from json import JSONDecodeError

from ps_common import *
from ps_common import debug_wrapper, fatal_wrapper, UnrecoverableError
# from ps_common.s2lib import ReceiptJson

from ..common import *

logger = logging.getLogger(__name__)

try:
    from google.cloud import storage
except ImportError as e:
    logger.debug("GCS_Transfer Failed Exception={}".format(e))
    logger.error("Did you 'pip install google.cloud.storage' ?")
    raise UnrecoverableError('Failed to load google cloud storage')

@debug_wrapper(TRACE)
def list_receipts(my_num, idx_num, src_config):
    try:
        logger = logging.getLogger(__name__)
        logger.debug('hello')
        logger.debug(src_config)

        with open(os.devnull, 'w') as sys.stderr:

            try:
                storage_client = storage.Client.from_service_account_json(src_config['gcs_auth_file'])
            except FileNotFoundError:
                raise UnrecoverableError("Unable to load gcs_auth_file: \"{}\". Check settings.conf".format(src_config.get('gcs_auth_file', "None")))

            try:
                bucket = storage_client.bucket(src_config['bucket_name'])
            except:
                raise UnrecoverableError("Not a valid Bucket Name")

            bucket_path = src_config.get('bucket_path', None)

            for blob in list(storage_client.list_blobs(bucket, prefix=bucket_path)):
                if not blob.name.endswith('receipt.json'):
                    continue
                try:
                    bkt_num = int(posixpath.dirname(blob.name).split(posixpath.sep)[-1].split("~")[0])
                except ValueError:
                    continue
                if bkt_num % idx_num == my_num:
                    yield(blob.name)
                else:
                    continue


    except Exception as e:
        logger.debug(repr(e), stack_info=True, exc_info=True)
        return False

    return True

@debug_wrapper(TRACE)
def get_receipt(src, src_config):
    try:
        logger = logging.getLogger(__name__)
        logger.debug('hello')

        with open(os.devnull, 'w') as sys.stderr:
            try:
                storage_client = storage.Client.from_service_account_json(src_config['gcs_auth_file'])
            except FileNotFoundError:
                logger.critical(f"Unable to load gcs_auth_file: {src_config.get('gcs_auth_file', None)}. Check settings.conf")
                raise UnrecoverableError("Unable to load gcs_auth_file: \"{}\". Check settings.conf".format(src_config.get('gcs_auth_file', "None")))
            except JSONDecodeError as e:
                logger.critical(f"Bad JSON Format in {src_config.get('gcs_auth_file', None)!r}")
                raise Exception(f"Bad JSON Format in {src_config.get('gcs_auth_file', None)!r} {repr(e)}")
            try:
                bucket = storage_client.bucket(src_config['bucket_name'])
            except:
                raise UnrecoverableError("Not a valid Bucket Name")

            blob = storage.Blob(src, bucket)

            with io.BytesIO() as f:
                storage_client.download_blob_to_file(blob, f)

                return json.loads(f.getvalue().decode("utf-8"), object_pairs_hook=OrderedDict)
                # return json.loads(f.getvalue().decode("utf-8"))

    except Exception as e:
        logger.debug(repr(e), stack_info=True, exc_info=True)
        return None

@debug_wrapper(TRACE)
def localize_file(src, dest, src_config, e_q, d_q):
    try:
        logger = logging.getLogger(__name__)
        logger.debug('hello')

        with open(os.devnull, 'w') as sys.stderr:
            try:
                storage_client = storage.Client.from_service_account_json(src_config['gcs_auth_file'])
            except FileNotFoundError:
                logger.critical(f"Unable to load gcs_auth_file: {src_config.get('gcs_auth_file', None)}. Check settings.conf")
                raise UnrecoverableError("Unable to load gcs_auth_file: \"{}\". Check settings.conf".format(src_config.get('gcs_auth_file', "None")))
            except JSONDecodeError as e:
                logger.critical(f"Bad JSON Format in {src_config.get('gcs_auth_file', None)!r}")
                raise Exception(f"Bad JSON Format in {src_config.get('gcs_auth_file', None)!r} {repr(e)}")

            try:
                bucket = storage_client.bucket(src_config['bucket_name'])
            except:
                raise UnrecoverableError("Not a valid Bucket Name")

            blob = storage.Blob(src, bucket)

            os.makedirs(os.path.dirname(dest), exist_ok=True)

            # blob.download_to_filename(dest)
            with open(dest, 'wb') as f:
                try:
                    storage_client.download_blob_to_file(blob, f, timeout=(5, 900))
                except:
                    storage_client.download_blob_to_file(blob, f)

    except Exception as e:
        logger.debug(repr(e), stack_info=True, exc_info=True)
        e_q.put(e)
        return repr(e)

    d_q.put(1)

TRACE = True
import base64
import datetime
import hashlib
import json
import logging
import multiprocessing as mp
import os
import pickle
import posixpath
# import signal
# import sys
import time
# import subprocess
from queue import Empty, Full, Queue
# from shutil import copytree, rmtree
# from tempfile import NamedTemporaryFile
from threading import Lock, Thread

import ps_common
import ps_common.s2lib
# from bucket_snapshot import SplunkBucket, SplunkInstance
# from logger import logProducerConfig
from ps_common.exceptions import *
from ps_common.decorators import debug_wrapper, unrecoverable_wrapper, fatal_wrapper
from ps_common.logger import logProducerConfig
from ps_common.utils import online_fsck
from ps_common.s2lib import ReceiptJson
from common import *

# from common.transfer.destinations import filesys, gcs, s3
# from common.transfer.destinations.filesys import *
# from common.transfer.destinations.gcs import *
# from common.transfer.destinations.s3 import *
# from common.transfer.sources import filesys, gcs, s3
# from common.transfer.sources.filesys import *
# from common.transfer.sources.gcs import *
# from common.transfer.sources.s3 import *

# @unrecoverable_wrapper
@fatal_wrapper
@debug_wrapper(TRACE)
def uploadWorker (_args, exit_event, resume_event, transfer_queue, retry_queue, log_queue, status_queue, pickle_lock):
    logProducerConfig(log_queue)
    logger = logging.getLogger(mp.current_process().name)
    logger.debug("hello")
    logger.debug('BucketTransfer dest={}'.format(_args.dest))

    if _args.settings.get('http_proxy', None) is not None:
        os.environ['HTTP_PROXY'] = _args.settings['http_proxy']
        os.environ['http_proxy'] = _args.settings['http_proxy']
        logger.warning(f"BucketTransfer Using HTTP PROXY={os.environ['HTTP_PROXY']}")
    if _args.settings.get('https_proxy', None) is not None:
        os.environ['HTTPS_PROXY'] = _args.settings['https_proxy']
        os.environ['https_proxy'] = _args.settings['https_proxy']
        logger.warning(f"BucketTransfer Using HTTPS PROXY={os.environ['HTTPS_PROXY']}")

    if _args.dest == 's3':
        from common.transfer.destinations import s3
        from common.transfer.destinations.s3 import (upload_file,
                                                        upload_receipt)
        dest_list = _args.settings.get('s3_config')
    elif _args.dest == 'gcs':
        from common.transfer.destinations import gcs
        from common.transfer.destinations.gcs import (upload_file,
                                                        upload_receipt)
        dest_list = _args.settings.get('gcs_config')
    elif _args.dest == 'filesys':
        from common.transfer.destinations import filesys
        from common.transfer.destinations.filesys import (upload_file,
                                                            upload_receipt)
        dest_list = _args.settings.get('fs_config')
    elif _args.dest == 'test':
        from common.transfer.destinations import test
        from common.transfer.destinations.test import (upload_file,
                                                            upload_receipt)
        dest_list = [{'path': 'nowhere'}]
    else:
        raise UnrecoverableError('Unknown Destination \'{}\''.format(_args.dest))

    while not exit_event.is_set():
        if not resume_event.wait(0.1):
            continue

        logger.debug("Running...")
        # get from the queue
        active_q = transfer_queue

        try:
            receipt, startup_time, retry, splunk_home, homePath, coldPath, thawedPath = transfer_queue.get(timeout=0.2)
        except Empty:
            logger.debug("BucketTransfer Transfer Queue is Empty")
            try:
                receipt, startup_time, retry, splunk_home, homePath, coldPath, thawedPath = retry_queue.get(timeout=0.2)
            except Empty:
                logger.debug("BucketTransfer Retry Queue is Empty")
                continue
            else:
                active_q = retry_queue

        except Exception as e:
            logger.debug("BucketTransfer Exception {}".format(repr(e)), exc_info=True, stack_info=True)
            status_queue.put({"failed": 1})
            with pickle_lock:
                with open("uploadResults.json", "a") as f:
                    f.write(json.dumps({'timestamp': time.time(), 'result': 'failure', 'reason': repr(e), 'receipt': json.loads(receipt.json)}))
                    f.write("\n")
            active_q.task_done()
            continue

        else:
            active_q = transfer_queue

        start_t = time.time()
        status_queue.put({"attempting": 1})
        logger.debug("BucketTransfer Searching for Bucket={}".format(receipt.id))

        # find the bucket
        if os.path.isdir(homePath):
            current_state = "warm"
            current_path = homePath
        elif os.path.isdir(coldPath):
            current_state = "cold"
            current_path = coldPath
        elif os.path.isdir(thawedPath):
            current_state = "thawed"
            current_path = thawedPath
            logger.warning("BucketTransfer Bucket={}, State={}, Path={}, Action={}, Status={}, Reason=thawed, ElapsedSec={:.2f}, Retries={}".format(receipt.id, "thawed", current_path, "upload", "cancelled", time.time() - start_t, retry))
            status_queue.put({"cancelled": 1, "attempting": -1})
            with pickle_lock:
                with open("uploadResults.json", "a") as f:
                    f.write(json.dumps({'timestamp': time.time(), 'result': 'skipped', 'receipt': json.loads(receipt.json)}))
                    f.write("\n")
            active_q.task_done()
            continue
        elif retry < 4:
            logger.warning(f"BucketTransfer Action=upload, Status=retry, Bucket={receipt.id}, State=not_found, Path=not_found, ElapsedSec={time.time() - start_t:.2f}, Retries={retry}")
            retry += 1
            retry_queue.put((receipt, startup_time, retry, splunk_home, homePath, coldPath, thawedPath))
            status_queue.put({"attempting": -1})
            active_q.task_done()
            continue
        else:
            logger.error("BucketTransfer Bucket={}, State={}, Path={}, Action={}, Status={}, ElapsedSec={:.2f}, Retries={}".format(receipt.id, "not_found", "not_found", "upload", "failed", time.time() - start_t, retry))
            status_queue.put({"failed": 1, "attempting": -1})
            with pickle_lock:
                with open("uploadResults.json", "a") as f:
                    f.write(json.dumps({'timestamp': time.time(), 'result': 'failure', 'reason': 'not_found', 'receipt': json.loads(receipt.json)}))
                    f.write("\n")
            active_q.task_done()
            continue

        # scan the bucket to see if repair is needed
        logger.debug("BucketTransfer Action=fsck_scan, Status=attempting, Bucket={}, State={}, Path={}, Retries={}".format(receipt.id, current_state, current_path, retry))
        p = repairWorker(kwargs={"path": current_path, "splunk_home": splunk_home, "force_repair": _args.fix_buckets})
        p.start()
        success, msg = p.join()

        if success:
            logger.debug(f"BucketTransfer Action=fsck_scan, Status=success, Bucket={receipt.id}, State={current_state}, Path={current_path}, Reason=\"{msg}\"")
            # add this bucket to the validated list
        else:
            logger.error("BucketTransfer Action=fsck_scan, Status=failed, Bucket={}, State={}, Path={}, Reason=\"{}\"".format(receipt.id, current_state, current_path, msg))
            with pickle_lock:
                with open("uploadResults.json", "a") as f:
                    f.write(json.dumps({'timestamp': time.time(), 'result': 'failure', 'reason': 'failed to repair bucket', 'receipt': json.loads(receipt.json)}))
                    f.write("\n")
            status_queue.put({"failed": 1, "attempting": -1})
            active_q.task_done()
            continue

        # update the receipt.json
        try:
            receipt.create_from_dir(current_path, update_manifest=True)
        except Exception as e:
            logger.error("BucketTransfer Bucket={}, State={}, Path={}, Action={}, Status={}, Retries={}, Reason=\"{}\"".format(receipt.id, current_state, current_path, "update_manifest", "failed", retry, repr(e)))
            status_queue.put({"failed": 1, "attempting": -1})
            with pickle_lock:
                with open("uploadResults.json", "a") as f:
                    f.write(json.dumps({'timestamp': time.time(), 'result': 'failure', 'reason': 'update manifest failed: {}'.format(repr(e)), 'receipt': json.loads(receipt.json)}))
                    f.write("\n")
            active_q.task_done()
            continue

        logger.debug("BucketTransfer Bucket={}, State={}, Path={}, Action={}, Status={}, Retries={}".format(receipt.id, current_state, current_path, "upload", "attempting", retry))

        # check the latest indextime and skip if after last restart
        # if the origin_guid is not in the peerList (e.g. decommissioned indexer)
        # then always upload the bucket. Otherwise, check the latest indextime against
        # the restart time of the indexer that created the bucket and skip
        # buckets modified after that indexer was restarted.
        _meta = receipt.metadata

        logger.debug(_meta)

        if startup_time is None or _meta.get('last_index_time', 0) < startup_time:
            if _args.earliest_event is not None and _meta.get('latest_event') < _args.earliest_event:
                logger.warning("BucketTransfer Action=upload, Status=cancelled, Bucket={}, Reason=too_old".format(receipt.id))
                logger.debug("BucketTransfer Bucket={}, State={}, Path={}, Action={}, Status={}, Reason=too_old, Cutoff_Time={}, Bucket_Time={}, ElapsedSec={:.2f}".format(receipt.id, current_state, current_path, "upload", "cancelled", datetime.datetime.fromtimestamp(_args.earliest_event).isoformat(), datetime.datetime.fromtimestamp(_meta.get('latest_event')).isoformat(), time.time() - start_t))
                status_queue.put({"cancelled": 1, "attempting": -1})
                with pickle_lock:
                    with open("uploadResults.json", "a") as f:
                                f.write(json.dumps({'timestamp': time.time(), 'result': 'skipped', 'reason': 'too_old', 'receipt': json.loads(receipt.json)}))
                                f.write("\n")
                active_q.task_done()
                continue

            error_queue = Queue()
            uploaded_queue = Queue()
            skipped_queue = Queue()
            workers = []
            _dest_config = dest_list[receipt.id_num % len(dest_list)]
            total_size = 0

            for _file, _size in [(x['name'], x['size']) for x in receipt.objects]:
                total_size += _size
                _dest = posixpath.normpath(posixpath.join(receipt.s2_path, _file))
                _rel_file = os.path.relpath(_file, _meta['object_prefix'])
                _src = os.path.abspath(os.path.join(current_path, _rel_file))

                logger.debug("Uploading Bucket={}, File={}, SourcePath={}, DestPath={}, SizeMB={:.2f}".format(receipt.id, _rel_file, _src, _dest, _size/1000000))
                try:
                    worker = Thread(target=upload_file, name="{}_fileTransferWorker".format(receipt.id), args=(_src, _dest, _dest_config, error_queue, uploaded_queue, skipped_queue, None))
                    worker.daemon = False
                    worker.start()
                    workers.append(worker)
                except Exception as e:
                    error_queue.put("error")
                    continue

            for worker in workers:
                worker.join()

            if error_queue.qsize() > 0:
                if retry < 5:
                    logger.warning("BucketTransfer Bucket={}, State={}, Path={}, Action={}, Status={}, Uploaded={}, Skipped={}, Errors={}, ElapsedSec={:.2f}".format(receipt.id, current_state, current_path, "upload", "retry", uploaded_queue.qsize(), skipped_queue.qsize(), error_queue.qsize(), time.time() - start_t))
                    retry += 1
                    retry_queue.put((receipt, startup_time, retry, splunk_home, homePath, coldPath, thawedPath))
                    status_queue.put({"attempting": -1})
                    active_q.task_done()
                    continue

                else:
                    logger.error("BucketTransfer Bucket={}, State={}, Path={}, Action={}, Status={}, ElapsedSec={:.2f}, Retries={}, Reason=\"Retries Exceeded\"".format(receipt.id, current_state, current_path, "upload", "failed", time.time() - start_t, retry))
                    status_queue.put({"failed": 1, "attempting": -1})
                    with pickle_lock:
                        with open("uploadResults.json", "a") as f:
                            f.write(json.dumps({'timestamp': time.time(), 'result': 'failure', 'reason': 'retries_exceeded', 'receipt': json.loads(receipt.json)}))
                            f.write("\n")
                    active_q.task_done()
                    continue

            # upload receipt.json

            if not upload_receipt(receipt, _dest_config):
                if retry < 3:
                    logger.warning("BucketTransfer Bucket={}, State={}, Path={}, Action={}, Status={}, Uploaded={}, Skipped={}, Errors={}, ElapsedSec={:.2f}".format(receipt.id, current_state, current_path, "upload", "retry", uploaded_queue.qsize(), skipped_queue.qsize(), error_queue.qsize() + 1, time.time() - start_t))
                    retry += 1
                    retry_queue.put((receipt, startup_time, retry, splunk_home, homePath, coldPath, thawedPath), timeout=0.2)
                    status_queue.put({"attempting": -1})
                    active_q.task_done()
                    continue
                else:
                    logger.error("BucketTransfer Bucket={}, State={}, Path={}, Action={}, Status={}, ElapsedSec={:.2f}, Retries={}, Reason=\"Retries Exceeded\"".format(receipt.id, current_state, current_path, "upload", "failed", time.time() - start_t, retry))
                    status_queue.put({"failed": 1, "attempting": -1})
                    with pickle_lock:
                        with open("uploadResults.json", "a") as f:
                            f.write(json.dumps({'timestamp': time.time(), 'result': 'failure', 'reason': 'failed to upload receipt', 'receipt': json.loads(receipt.json)}))
                            f.write("\n")
                    active_q.task_done()
                    continue

            # pickle successful bucket
            with pickle_lock:
                with open("uploadedBuckets.pickle", "ab") as f:
                    pickle.dump(receipt.id, f, 3)

                with open("uploadResults.json", "a") as f:
                    f.write(json.dumps({'timestamp': time.time(), 'result': 'success', 'receipt': json.loads(receipt.json)}))
                    f.write("\n")

                with open(f"bucket_list_for_s2dc_{receipt.uploader_guid}.csv", "a") as f:
                    bucket_id = receipt.id
                    index, bid = bucket_id.split("~",1)
                    f.write(f"\"{bucket_id}\",\"{index}\",\"{bid}\",\"{homePath.split(os.sep)[-1]!s}\"\n")

            status_queue.put({"succeeded": 1, "attempting": -1})
            active_q.task_done()
            elapsed_sec = time.time() - start_t
            logger.debug("BucketTransfer Bucket={}, State={}, Path={}, Action={}, Status={}, Uploaded={}, Skipped={}, Errors={}, SizeMB={:.2f}, ElapsedSec={:.2f}".format(receipt.id, current_state, current_path, "upload", "success", uploaded_queue.qsize(), skipped_queue.qsize(), error_queue.qsize(), total_size/1000000, elapsed_sec))
            continue

        else:
            logger.warning("BucketTransfer Action=upload, Status=cancelled, Bucket={}, Reason=too_new".format(receipt.id))
            logger.debug("BucketTransfer Bucket={}, State={}, Path={}, Action={}, Status={}, Reason=too_new, Cutoff_Time={}, Bucket_Time={}, ElapsedSec={:.2f}".format(receipt.id, current_state, current_path, "upload", "cancelled", datetime.datetime.fromtimestamp(startup_time).isoformat(), datetime.datetime.fromtimestamp(_meta['last_index_time']).isoformat(), time.time() - start_t))
            status_queue.put({"cancelled": 1, "attempting": -1})
            with pickle_lock:
                with open("uploadResults.json", "a") as f:
                    f.write(json.dumps({'timestamp': time.time(), 'result': 'skipped', 'reason': 'too_new', 'receipt': json.loads(receipt.json)}))
                    f.write("\n")
            active_q.task_done()
            continue

@fatal_wrapper
@debug_wrapper(TRACE)
def downloadWorker (_args, exit_event, resume_event, transfer_queue, retry_queue, log_queue, status_queue, pickle_lock):
    logProducerConfig(log_queue)
    logger = logging.getLogger(mp.current_process().name)
    logger.debug("hello")
    logger.debug('BucketTransfer src={}'.format(_args.src))

    if _args.src == 's3':
        from common.transfer.sources import s3
        from common.transfer.sources.s3 import get_receipt, localize_file
        _src_config = _args.settings.get('s3_config')
    elif _args.src == 'gcs':
        from common.transfer.sources import gcs
        from common.transfer.sources.gcs import get_receipt, localize_file
        _src_config = _args.settings.get('gcs_config')
    elif _args.src == 'filesys':
        from common.transfer.sources import filesys
        from common.transfer.sources.filesys import (get_receipt,
                                                        localize_file)
        _src_config = _args.settings.get('fs_config')
    elif _args.src == 'test':
        raise NotImplementedError
    else:
        raise UnrecoverableError('Unknown Destination \'{}\''.format(_args.src))

    if isinstance(_src_config, list):
        if len(_src_config) == 1:
            _src_config = _src_config[0]
        else:
            raise UnrecoverableError('Too many entries in source config file. Only one entry is allowed for the source')

    while not exit_event.is_set():
        if not resume_event.wait(0.1):
            continue

        # get from the queue
        active_q = transfer_queue

        try:
            receiptPath, retry, thawedPath = transfer_queue.get(timeout=0.2)
        except Empty:
            logger.debug("BucketTransfer Transfer Queue is Empty")
            try:
                receiptPath, retry, thawedPath = retry_queue.get(timeout=0.2)
            except Empty:
                logger.debug("BucketTransfer Retry Queue is Empty")
                continue
            else:
                active_q = retry_queue
        # try:
        #     receiptPath, retry, thawedPath = transfer_queue.get(timeout=0.2)
        # except Empty:
        #     continue
        # except Exception as e:
        #     logger.debug("BucketTransfer Exception {}".format(repr(e)))
        #     status_queue.put({"failed": 1})
        #     transfer_queue.task_done()
        #     continue

        start_t = time.time()
        status_queue.put({"attempting": 1})
        logger.debug("BucketTransfer Fetching receipt Path={}".format(receiptPath))

        # get the receipt
        receipt = get_receipt(receiptPath, _src_config)
        if receipt is None:
            if retry < 5:
                logger.warning("BucketTransfer File=receipt.json, State={}, Path={}, Action={}, Retries={}, ElapsedSec={:.2f}".format('remote', receiptPath, "retry", retry, time.time() - start_t))
                retry += 1
                retry_queue.put((receiptPath, retry, thawedPath))
                status_queue.put({"attempting": -1})
                active_q.task_done()
                continue
            else:
                logger.error("BucketTransfer File=receipt.json, State={}, Path={}, Action={}, Status={}, ElapsedSec={:.2f}, Retries={}, Reason=\"Retries Exceeded\"".format('remote', receiptPath, "get_receipt", "failed", time.time() - start_t, retry))
                status_queue.put({"failed": 1, "attempting": -1})
                with pickle_lock:
                    with open("downloadResults.json", "a") as f:
                        f.write(json.dumps({'timestamp': time.time(), 'result': 'failure', 'reason': 'failed to download receipt', 'receipt': receipt, 'path': receiptPath}))
                        f.write("\n")
                active_q.task_done()
                continue

        _src_prefix = posixpath.dirname(receiptPath)

        try:
            _dest_prefix = os.path.normpath(os.path.join(thawedPath, receipt['manifest']['path']))
        except Exception as e:
            logger.error("BucketTransfer File=receipt.json, State={}, Path={}, Action={}, Status={}, ElapsedSec={:.2f}, Retries={}, Reason=\"Corrupt Receipt\"".format('remote', receiptPath, "get_receipt", "failed", time.time() - start_t, retry))
            status_queue.put({"failed": 1, "attempting": -1})
            with pickle_lock:
                with open("downloadResults.json", "a") as f:
                    f.write(json.dumps({'timestamp': time.time(), 'result': 'failure', 'reason': repr(e), 'receipt': receipt}))
                    f.write("\n")
            active_q.task_done()
            continue

        error_queue = Queue()
        uploaded_queue = Queue()
        skipped_queue = Queue()
        workers = []
        total_size = 0

        for file, _size in [(x['name'], x['size']) for x in receipt['objects']]:
            total_size += _size
            _tmp = file.split(posixpath.sep)
            _src_file = posixpath.join(*_tmp[1:])
            _dest_file = os.path.join(*_tmp[2:])
            _src_path = posixpath.normpath(posixpath.join(_src_prefix, _src_file))
            _dest_path = os.path.normpath(os.path.join(_dest_prefix, _dest_file))

            logger.debug("Downloading Bucket={}, File={}, SourcePath={}, DestPath={}, SizeMB={:.2f}".format(receipt['manifest']['id'], _src_file, _src_path, _dest_path, _size/1000000))
            try:
                worker = Thread(target=localize_file, name="{}_fileTransferWorker".format(receipt['manifest']['id']), args=(_src_path, _dest_path, _src_config, error_queue, uploaded_queue))
                worker.daemon = False
                worker.start()
                workers.append(worker)
            except Exception as e:
                error_queue.put(repr(e))
            finally:
                for worker in workers:
                    worker.join()

        if not error_queue.empty():
            if retry < 5:
                logger.warning("BucketTransfer Bucket={}, State={}, Path={}, Action={}, Status={}, Downloaded={}, Skipped={}, Errors={}, ElapsedSec={:.2f}".format(receipt['manifest']['id'], 'thawed', _dest_path, "download", "retry", uploaded_queue.qsize(), skipped_queue.qsize(), error_queue.qsize(), time.time() - start_t))
                retry += 1
                retry_queue.put((receiptPath, retry, thawedPath))
                status_queue.put({"attempting": -1})
                active_q.task_done()
                continue

            else:
                logger.error("BucketTransfer Bucket={}, State={}, Path={}, Action={}, Status={}, ElapsedSec={:.2f}, Retries={}, Reason=\"Retries Exceeded\"".format(receipt['manifest']['id'], 'thawed', _dest_path, "download", "failed", time.time() - start_t, retry))
                status_queue.put({"failed": 1, "attempting": -1})
                with pickle_lock:
                    with open("downloadResults.json", "a") as f:
                        f.write(json.dumps({'timestamp': time.time(), 'result': 'failure', 'reason': 'retries_exceeded', 'receipt': receipt}))
                        f.write("\n")
                active_q.task_done()
                continue

        # pickle successful bucket
        with pickle_lock:
            with open("downloadedBuckets.pickle", "ab") as f:
                pickle.dump(receiptPath, f, 3)

            with open("downloadResults.json", "a") as f:
                f.write(json.dumps({'timestamp': time.time(), 'result': 'success', 'receipt': receipt}))
                f.write("\n")                

        status_queue.put({"succeeded": 1, "attempting": -1})
        active_q.task_done()
        elapsed_sec = time.time() - start_t
        logger.debug("BucketTransfer Bucket={}, State={}, Path={}, Action={}, Status={}, Downloaded={}, Skipped={}, Errors={}, SizeMB={:.2f}, ElapsedSec={:.2f}".format(receipt['manifest']['id'], 'thawed', _dest_path, "download", "success", uploaded_queue.qsize(), skipped_queue.qsize(), error_queue.qsize(), total_size/1000000, elapsed_sec))
        continue

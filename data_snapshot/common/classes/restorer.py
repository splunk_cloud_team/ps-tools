TRACE = True
import csv
import datetime
import json
import logging
import os
import pickle
import posixpath
import time

from ps_common import Self, UnrecoverableError
from ps_common import debug_wrapper, unrecoverable_wrapper
from ps_common.s2lib import ReceiptJson


class restore():
    
    def __init__(self):
        pass
    
    @debug_wrapper(TRACE)
    def run(self, _args, transfer_queue, status_queue, pickle_lock):
        self._logger = logging.getLogger(__name__)
        self._logger.debug("hello")
        
        if _args.src == 's3':
            from common.transfer.sources import s3
            from common.transfer.sources.s3 import list_indexes, list_receipts
            _src_config = _args.settings.get('s3_config')
        elif _args.src == 'gcs':
            from common.transfer.sources import gcs
            from common.transfer.sources.gcs import list_receipts
            _src_config = _args.settings.get('gcs_config')
        elif _args.src == 'filesys':
            from common.transfer.sources import filesys
            from common.transfer.sources.filesys import list_receipts
            _src_config = _args.settings.get('fs_config')
        elif _args.src == 'test':
            raise NotImplementedError
        else:
            raise UnrecoverableError('Unknown Destination \'{}\''.format(_args.src))
            
        if isinstance(_src_config, list):
            if len(_src_config) == 1:
                _src_config = _src_config[0]
            else:
                raise UnrecoverableError('Too many entries in source config file. Only one entry is allowed for the source')
        
        connect = _args.connect
        this = Self(**connect, get_primaries=False)
        
        if this is None:
            raise UnrecoverableError("Unable to complete Splunk Discovery")

        peerList = sorted(this.peers.keys())
        if _args.force_n is None:
            total_idx = len(this.peers.keys())
        else:
            total_idx = _args.force_n

        if _args.force_x is None:
            this_idx = peerList.index(this.guid)
        else:
            this_idx = _args.force_x
        
        bucketList = []
        
        if False:
            self._logger.debug("BucketDiscovery Action=recover, Status=begin")
            
            try:
                with open("restoreList.pickle", "rb") as f:
                    bucketList = pickle.load(f)
            except FileNotFoundError:
                self._logger.debug("BucketDiscovery Action=recover, Status=skipped, Reason=FileNotFound")
            except (OSError, EOFError) as e:
                self._logger.warning("BucketDiscovery Action=recover, Status=skipped, Reason=\"{}\"".format(e))
            else:
                self._logger.info("BucketDiscovery Action=recover, Status=complete, Count={}".format(len(bucketList)))
        
        if len(bucketList) == 0:
            
            _my_start = time.time()
            self._logger.debug("BucketDiscovery Action=discover, Status=begin")

            # for index in _src_index_list:
            #     if index not in [x.title for x in __self.indexList]:
            #         self._logger.debug("BucketDiscovery Action=discover, Status=skipped, Index={}, Reason=\"not_defined\"".format(item, _index_name))
            #         continue
            #     elif [x.disabled for x in __self.indexList if x.title == index][0]:
            #         self._logger.debug("BucketDiscovery Action=discover, Status=skipped, Index={}, Reason=\"disabled\"".format(item, _index_name))
            #         continue
            #     else:
            #         self._logger.info("BucketDiscovery Action=discover, Status=added, Index={}".format(index))
            #         _index_list.append(index)
                    # with pickle_lock:

            completedList = []
            total = 0
            try:
                with open("downloadedBuckets.pickle", "rb") as f:
                    while True:
                        try:
                            completedList.append(pickle.load(f))                            
                        except EOFError:
                            break
                        
            except FileNotFoundError:
                self._logger.debug("CompletedBucketDiscovery Action=recover, Status=skipped, Reason=FileNotFound")
                
            except (OSError, EOFError) as e:
                self._logger.warning("CompletedBucketDiscovery Action=recover, Status=skipped, Reason=\"{}\"".format(e))
                
            else:
                self._logger.info("CompletedBucketDiscovery Action=recover, Status=complete, Count={}".format(len(completedList)))

            for item in list_receipts(this_idx, total_idx, _src_config):
                self._logger.debug(item)
                if item in completedList:
                    total += 1
                    status_queue.put({'total': total, 'cancelled': 1})
                    self._logger.debug("BucketDiscovery Action=discover, Status=skipped, File={}, Reason=\"already downloaded\"".format(item))
                    continue

                _index_name = item.split(posixpath.sep)[-6:-5][0]
                _index_def = this.index(_index_name)
                try:
                    assert _index_def is not None
                except AssertionError as e:
                    raise AssertionError(f"{_index_name} returned {_index_def}")

                if _index_def.get('title', None) is None:
                    self._logger.error("BucketDiscovery Action=discover, Status=skipped, File={}, Reason=\"\'{}\' not defined\"".format(item, _index_name))
                    continue
                elif str(_index_def['disabled']).lower() in ["1", "true"]:
                    self._logger.warning("BucketDiscovery Action=discover, Status=skipped, File={}, Reason=\"\'{}\' is disabled\"".format(item, _index_name))
                    continue
                else:
                    self._logger.debug("BucketDiscovery Action=discover, Status=success, File={}".format(item))
                    transfer_queue.put((item, 0, _index_def['thawedPath']))
                    total += 1
                    status_queue.put({'total': total})
                # bucketList.append((item, 0, _index_def['thawedPath']))
                
            # self._logger.info("BucketDiscovery Action=discover, Status=complete, Count={}, Elapsed={}".format(len(bucketList), str(datetime.timedelta(seconds=time.time() - _my_start))))
            self._logger.info("BucketDiscovery Action=discover, Status=complete, Count={}, Elapsed={}".format(total, str(datetime.timedelta(seconds=time.time() - _my_start))))

            # persist bucketlist
            # with open("restoreList.pickle", "wb") as f:
            #     pickle.dump(bucketList, f, 3)
                       
        # total_buckets = len(bucketList)
        # status_queue.put({'total': total_buckets})
            
        # TODO remove successful buckets from the list
        # completedList = []
        
        # with pickle_lock:
        #     try:
        #         with open("downloadedBuckets.pickle", "rb") as f:
        #             while True:
        #                 try:
        #                     completedList.append(pickle.load(f))                            
        #                 except EOFError:
        #                     break
                        
        #     except FileNotFoundError:
        #         self._logger.debug("CompletedBucketDiscovery Action=recover, Status=skipped, Reason=FileNotFound")
                
        #     except (OSError, EOFError) as e:
        #         self._logger.warning("CompletedBucketDiscovery Action=recover, Status=skipped, Reason=\"{}\"".format(e))
                
        #     else:
        #         self._logger.info("CompletedBucketDiscovery Action=recover, Status=complete, Count={}".format(len(completedList)))

        #     bucketList = [ x for x in bucketList if x[0] not in completedList ]
            
        #     status_queue.put({"succeeded": total_buckets - len(bucketList)})
        
        # total = 0
        # if len(bucketList) > 0:
        #     for item in bucketList:
        #         transfer_queue.put(item)
        # else:
        #     self._logger.warning("Nothing to do")
            
        # return True
        # while len(bucketList) > 0:
        #     total += 1
        #     transfer_queue.put(bucketList.pop())
        
        # if total == 0:
        #     self._logger.warning("Nothing to do")
            
        return True, total


# _dest_prefix = os.path.normpath(os.path.join('thaweddb', receipt.get('manifies').get('path')))
            
            # for name in [os.path.join(*x['name'].split(posixpath.sep)[2:]) for x in receipt['objects']]:
            # for name in [x['name'] for x in receipt['objects']]:
            #     _src = posixpath.normpath(posixpath.join(src_config['bucket_path'], ))

import csv
import datetime
import json
import logging
import os
import pickle
import posixpath
import time

from ps_common import *
from ps_common import debug_wrapper, unrecoverable_wrapper
from ps_common.s2lib import ReceiptJson


class discover():
    
    def __init__(self):
        pass
    
    def run(self, _args):
        self._logger = logging.getLogger(__name__)        
        
        connect = _args.connect
        this = Self(**connect)
        
        if this is None:
            raise UnrecoverableError("Unable to complete Splunk Discovery")


        return True

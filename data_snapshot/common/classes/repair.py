import os
import sys
import subprocess
import threading
import multiprocessing as mp
import common

common.lib_path.make_lib_path()

# from ps_common import Splunk, SplunkIndexer, SplunkManager
from ps_common import *
from ps_common.utils import online_fsck

class repairWorker(threading.Thread):
# class repairWorker(mp.Process):
    # class variable
    sem = threading.Semaphore(20)

    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs=None):

        super(repairWorker, self).__init__(group=group, target=target,
                            name=name)
        self.args = args
        self.kwargs = kwargs
        self._result = None
        # repairWorker.sem = threading.Semaphore(1)

    # def __new__(cls, *args, **kwargs):
    #     # repairWorker.sem = threading.Semaphore(kwargs.get('threads', 10))
    #     this = object.__new__(cls)
    #     this.sem = threading.Semaphore(kwargs.get('threads', 10))
    #     return this

    @debug_wrapper(False)
    def scan(self):
        outs, errs = online_fsck(self.kwargs['splunk_home'], cmd='scan', path=self.kwargs['path'])
        _status = errs.split('\n')[-3:-2][0]
        if len(set(errs.lower().split()).intersection(set(['corruption', 'rawdata']))) == 0:
            return True
        else:
            return False

    @debug_wrapper(False)
    def repair(self):
        try:
            outs, errs = online_fsck(self.kwargs['splunk_home'], path=self.kwargs['path'], timeout=86400)
        except subprocess.TimeoutExpired:
            return False

        _status = errs.split('\n')[-3:-2][0]

        try:
            _status.index('failReason')
        except ValueError:
            return self.scan()
        else:
            False

    @debug_wrapper(True)
    def run(self):
        # print("waiting for semaphore", file=sys.stderr)
        with repairWorker.sem:
            # print("got semaphore", file=sys.stderr)
            self._result = (False, "Cannot repair: {}".format(self.kwargs['path']))
            if self.kwargs.get('force_repair', False) is False and self.scan():
                self._result = (True, "Validated: {}".format(self.kwargs['path']))
            elif self.repair():
                self._result = (True, "Repaired: {}".format(self.kwargs['path']))
            # else:
            #     self._return = (False, "Cannot repair {}\n".format(self.kwargs['path']))

    @debug_wrapper(False)
    def join(self):
        super(repairWorker, self).join()
        return self._result
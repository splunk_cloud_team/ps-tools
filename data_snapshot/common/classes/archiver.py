TRACE = True
import csv
import json
import logging
import os
import pickle
import sys
from threading import Condition

from ps_common import *
from ps_common import debug_wrapper, unrecoverable_wrapper
from ps_common.s2lib import ReceiptJson


class archive():
    _logger = logging.getLogger(__name__)

    def __init__(self):
        pass
    
    @unrecoverable_wrapper
    @debug_wrapper(TRACE)
    def run(self, _args, transfer_queue, status_queue, pickle_lock):
        # self._logger = logging.getLogger(__name__)
        # self._logger.debug("hello")
        self._logger.debug("hello")
        
        # __self, __parent, peerList = discover_splunk("127.0.0.1", _args.local_port, _args.username, _args.password)
        connect = _args.connect
        this = Self(**connect)
        
        if this is None:
            raise UnrecoverableError("Unable to complete Splunk Discovery")
        
        bucketList = []
        hot_count = 0
        primary_count = 0
        replica_count = 0
        
        if True:
            self._logger.debug("BucketDiscovery Action=recover, Status=begin")
            
            try:
                with open("archiveList.pickle", "rb") as f:
                    bucketList = pickle.load(f)
            except FileNotFoundError:
                self._logger.debug("BucketDiscovery Action=recover, Status=skipped, Reason=FileNotFound")
            except (OSError, EOFError) as e:
                self._logger.warning("BucketDiscovery Action=recover, Status=skipped, Reason=\"{}\"".format(e))
            else:
                self._logger.info("BucketDiscovery Action=recover, Status=complete, Count={}".format(len(bucketList)))
        
        if len(bucketList) == 0:
            
            # self._logger.debug("BucketDiscovery Action=discover, Status=begin")
            index_filter = _args.settings.get('included_indexes', "__ALL__")

            if index_filter == "__ALL__":
                self._logger.debug("BucketDiscovery Action=discover, Status=begin, Reason=\"discover all indexes\"")
            else:
                if not isinstance(index_filter, list):
                    index_filter = [i.strip() for i in index_filter.split(",")]
                else:
                    index_filter = [i.strip() for i in index_filter]

                self._logger.warning("BucketDiscovery Action=discover, Status=begin, Reason=\"Using Index Filter\"")
            
            for index in this.indexes:
                if _args.skip_internal and str(index['isInternal']).lower() in ['true', '1']:
                    self._logger.warning(f"BucketDiscovery Action=discover_index, Index={index['title']}, Status=skipped, Reason=\"--skip-internal option specified\"")
                    continue

                if index_filter != "__ALL__" and index['title'] not in index_filter:
                    self._logger.warning(f"BucketDiscovery Action=_index, Index={index['title']}, Status=skipped, Reason=\"excluded by filter\"")
                    continue

                idx_primary = 0
                idx_replica = 0
                idx_hot = 0
                
                self._logger.debug(f"BucketDiscovery Action=discover_index, Index={index['title']}, homePath={index['homePath']}")
                try:
                    with open(os.path.join(index['homePath'], ".bucketManifest"), "r") as manifest:
                        reader  = csv.DictReader(manifest)
                        for row in reader:
                            self._logger.debug("BucketDiscovery {}: {}".format(manifest.name, row))
                            # filter out hot buckets
                            if not (row['path'].startswith("db_") or row['path'].startswith("rb_")):
                                self._logger.debug("BucketDiscovery Action=discover_bucket, Index={}, Bucket={}, Status=skipped, Reason=hot".format(index['title'], row['path']))
                                hot_count += 1
                                idx_hot += 1
                                continue
                            
                            # check that bucket is primary
                            if not this.is_primary_bucket(index['title'], row['id']):
                                self._logger.debug("BucketDiscovery Action=discover_bucket, Index={}, Bucket={}, Status=skipped, Reason=not_primary".format(index['title'], row['path']))
                                replica_count += 1
                                idx_replica += 1
                                continue
                            
                            receipt = ReceiptJson(this.guid)
                            receipt.create_from_manifest(dict(row))
                            
                            try:
                                startup_time = int(this.peers[receipt.origin_guid]['startup_time'])
                            except (KeyError, TypeError):
                                startup_time = None
                                
                            self._logger.debug("BucketDiscovery Action=discover_bucket, Index={}, Bucket={}, Status=added, Reason=primary".format(index['title'], row['path']))
                            bucketList.append((receipt, startup_time, 0, os.path.join(this.SPLUNK_HOME), os.path.join(index['homePath'], row['path']), os.path.join(index['coldPath'], row['path']), os.path.join(index['thawedPath'], row['path'])))
                            primary_count += 1
                            idx_primary += 1
                            
                except FileNotFoundError:
                    self._logger.error("BucketDiscovery Action=discover_index, Index={}, homePath={}, Status=failed, Reason=\"{}\"".format(index['title'], index['homePath'], ".bucketManifest not found"))
                    continue
                
                except Exception as e:
                    self._logger.error("BucketDiscovery Action=discover_index, Index={}, homePath={}, Status=failed, Reason=\"{}\"".format(index['title'], index['homePath'], e))
                    raise UnrecoverableError(e)
                
                self._logger.info("BucketDiscovery Action=discover_index, Index={}, Status=complete, PrimaryCount={}, ReplicaCount={}, HotCount={}".format(index['title'], idx_primary, idx_replica, idx_hot))
            

            self._logger.info("BucketDiscovery Action=discover, Status=complete, PrimaryCount={}, ReplicaCount={}, HotCount={}".format(primary_count, replica_count, hot_count))
                        
            # persist bucketlist
            with open("archiveList.pickle", "wb") as f:
                pickle.dump(bucketList, f, 3)
        try:
            open(f"bucket_list_for_s2dc_{this.guid}.csv", 'r')
        except FileNotFoundError:    
            with open(f"bucket_list_for_s2dc_{this.guid}.csv", 'w') as f:
                f.write('"bucketId","index","bid","path"\n')
                # for receipt, startup_time, retries, splunk_home, homepath, coldpath, thawedpath in bucketList:
                #     bucket_id = receipt.id
                #     index, bid = bucket_id.split("~",1)
                #     f.write(f"\"{bucket_id}\",\"{index}\",\"{bid}\",\"{homepath.split(os.sep)[-1]!s}\"\n")

        total_buckets = len(bucketList)
        status_queue.put({'total': total_buckets})
        
        with open("indexes.conf", "w") as f:
            for item in this.indexes:
                if str(item['isInternal']).lower() in ['true', '1'] or item['title'] in ['history', 'main', 'summary']:
                    continue
                else:
                    stanza = "[{name}]\n"
                    stanza = stanza + f"datatype = {item['datatype']}\n"
                    stanza = stanza + f"frozenTimePeriodInSecs = {item['frozenTimePeriodInSecs']}\n"
                    stanza = stanza + "homePath = $SPLUNK_DB{sep}{name}{sep}db\n"
                    stanza = stanza + "coldPath = $SPLUNK_DB{sep}{name}{sep}colddb\n"
                    stanza = stanza + "thawedPath = $SPLUNK_DB{sep}{name}{sep}thaweddb\n"
                    stanza = stanza + "\n"
                    f.write(stanza.format(name=item['title'], sep=os.path.sep))
            
        # TODO remove successful buckets from the list
        completedList = []
        
        with pickle_lock:
            try:
                with open("uploadedBuckets.pickle", "rb") as f:
                    while True:
                        try:
                            completedList.append(pickle.load(f))
                            
                        except EOFError:
                            break
                        
            except FileNotFoundError:
                self._logger.debug("CompletedBucketDiscovery Action=recover, Status=skipped, Reason=FileNotFound")
                
            except (OSError, EOFError) as e:
                self._logger.warning("CompletedBucketDiscovery Action=recover, Status=skipped, Reason=\"{}\"".format(e))
                
            else:
                self._logger.info("CompletedBucketDiscovery Action=recover, Status=complete, Count={}".format(len(completedList)))

            bucketList = [ x for x in bucketList if x[0].manifest.get("id") not in completedList ]
            
            status_queue.put({"succeeded": total_buckets - len(bucketList)})
        
        total = 0

        while len(bucketList) > 0:
            total += 1
            transfer_queue.put(bucketList.pop())
        
        if total == 0:
            self._logger.warning("Nothing to do")
            
        return True, total

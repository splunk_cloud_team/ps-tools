TRACE = True
import logging
import multiprocessing as mp
import queue
from time import time

from ps_common import *

@unrecoverable_wrapper
@debug_wrapper(TRACE)
def thread_failure(signum, frame):
    raise UnrecoverableError('thread failure')

@unrecoverable_wrapper
@debug_wrapper(TRACE)
def status_worker(_args, exit_event, resume_event, status_queue, log_queue):
    logProducerConfig(log_queue)
    logger = logging.getLogger(mp.current_process().name)
    
    # wait 30 seconds before first update
    next_update = time.time() + 30
    
    status = {"total": "Unknown",
              "queued": "Unknown",
              "succeeded": 0,
              "failed": 0,
              "cancelled": 0,
              "attempting": 0
              }
    
    while not exit_event.is_set():
        if resume_event.wait(0.1):
            try: 
                msg = status_queue.get(timeout=0.1)
                
            except queue.Empty:
                # not perfect, but good enough for our purposes
                if time.time() >= next_update:
                    # set the next update to 15 seconds from the previous value
                    next_update = next_update + 15
                    logger.info("PeriodicStatus: {}".format(", ".join([ k + "=" + str(v) for k,v in status.items()])))

                continue
            
            for k, v in [ (key, msg[key]) for key in msg.keys() if key in status.keys() ]:
                if k == "total":
                    status['total'] = int(v)
                else:
                    try:
                        status[k] += int(v)
                    except KeyError:
                        status[k] = int(v)
            
            if status['total'] != "Unknown":
                status['queued'] = status['total'] - status['succeeded'] - status['failed'] - status['cancelled'] - status['attempting']
                
            logger.debug("PeriodicStatus: {}".format(", ".join([ k + "=" + str(v) for k,v in status.items()])))
            
            status_queue.task_done()
            
    logger.info("FinalStatus: {}".format(", ".join([ k + "=" + str(v) for k,v in status.items()])))

@unrecoverable_wrapper
@debug_wrapper(TRACE)
def _normalize(item):
    
    if isinstance(item, dict):
        _tmp = [dict(item)]
    elif isinstance(item, list):
        _tmp = [dict(x) for x in item]
    else:
        raise UnrecoverableError('settings.conf:{}'.format(item))
    
    return _tmp

@unrecoverable_wrapper
@debug_wrapper(TRACE)
def validate_settings(remote, conf):
    logger = logging.getLogger(__name__)
    
    for key in ['work_dir']:
        try: conf[key]
        except KeyError: raise UnrecoverableError('settings.conf:{}'.format(key))

    if remote == "s3":
        _tmp = _normalize(conf['s3_config'])
            
        for idx, item in enumerate(_tmp):
            for key in ['bucket_name', 'bucket_path', 'endpoint_url']:
                try: 
                    if not isinstance(item[key], str):
                        raise UnrecoverableError('settings.conf:s3_config[{}]:{} must be {}'.format(idx, key, type("")))
                except KeyError: raise UnrecoverableError('settings.conf:s3_config[{}]:{}'.format(idx, key))
            for key in ['aws_access_key_id', 'aws_secret_access_key']:
                if item.get(key, None) is not None:
                    if not isinstance(item[key], str):
                        raise UnrecoverableError('settings.conf:s3_config[{}]:{} must be {}'.format(idx, key, type("")))
                
    
    elif remote == "gcs":
        _tmp = _normalize(conf['gcs_config'])
        
        for idx, item in enumerate(_tmp):
            for key in ['bucket_name', 'bucket_path', 'gcs_auth_file']:
                try: isinstance(item[key], str)
                except KeyError: raise UnrecoverableError('settings.conf:gcs_config[{}]:{}'.format(idx, key))
    
    elif remote == "filesystem":
        _tmp = _normalize(conf['fs_config'])
        
        for idx, item in enumerate(_tmp):
            for key in ['path']:
                try: isinstance(item[key], str)
                except KeyError: raise UnrecoverableError('settings.conf:fs_config[{}]:{}'.format(idx, key))
    
    
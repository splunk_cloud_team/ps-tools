import argparse
import multiprocessing as mp
import os
import posixpath
import sys
import uuid

def arg_parser():
    
    # global options
    g_parser = argparse.ArgumentParser(add_help=False)
    
    # Global Arguments for all functions
    g_optional = g_parser.add_argument_group('Global Optional Arguments')
    g_optional.add_argument('--local_port', type=int, help='The local REST port (default 8089)', default=8089)
    g_optional.add_argument('--scheme', help='Splunk connection scheme', choices=['http', 'https'], default='https')
    g_optional.add_argument('--debug', help='Enable DEBUG logging (slower)', action='store_true')
    g_optional.add_argument('--verbose', help='Add timestamp and process information to the console log (always present in the logfile)', action='store_true')
    g_optional.add_argument('--timeout', type=int, help="Timeout Seconds for REST Calls", default=120)
    g_optional.add_argument('-w', '--workers', type=int, help='Number of worker threads (default 4 -- more is not always better)', default = 4 )
    # g_optional.add_argument('-t', '--test', help='Don\'t actually execute the copy operation', action='store_true')
    # g_optional.add_argument('--log_dir', help='Directory for logs.', default = 'log')
    g_optional.add_argument('--conf', help='Relative path to the settings.conf file', default = 'settings.conf')
    g_testing = g_parser.add_argument_group('For Testing ONLY')
    g_testing.add_argument('--mp_start_method', choices=['fork', 'spawn', 'forkserver'], help='DO NOT USE', default='spawn')
    g_testing.add_argument('--threads', help='DO NOT USE', action='store_true')
    g_testing.add_argument('--test-retry', help='DO NOT USE', action='store_true')
    g_testing.add_argument('--guid', help='DO NOT USE', default = uuid.uuid4())
    
    a_parser = argparse.ArgumentParser(add_help=False)
    
    # Required
    a_required = a_parser.add_argument_group('Archive Required Arguments')
    
    # Optional
    a_optional = a_parser.add_argument_group('Archive Optional Arguments')
    
    a_optional.add_argument('--skip-internal', help='Skip internal indexes to save time and space', action='store_true')
    a_optional.add_argument('-x', '--earliest_event', type=int, help='Epoch time of beginning of data window', default = None )
    a_optional.add_argument('--fix_buckets', help='Repair the buckets before copy', action='store_true' )
    
    # Experimental
    a_experimental = a_parser.add_argument_group('Archive Experimental Arguments -- TESTING ONLY')
    a_experimental.add_argument('--skip-recovery', help='Do not attempt to recover prior bucketList', action='store_true')
    a_experimental.add_argument('--dest-format', help='DO NOT USE', choices=['bid', 'classic', 's2'], default='bid')
    a_experimental.add_argument('--freeze', help='DO NOT USE', action='store_true')
    
    r_parser = argparse.ArgumentParser(add_help=False)
    r_required = a_parser.add_argument_group('Restore Required Arguments')

    # Optional
    r_optional = r_parser.add_argument_group('Restore Optional Arguments')
    r_optional.add_argument('--force_x', type=int, help="Force the indexer number to override the computed value")
    r_optional.add_argument('--force_n', type=int, help="Force the indexer count to override the computed value")
    

    m_parser = argparse.ArgumentParser(add_help=False)
    m_required = a_parser.add_argument_group('Discover Required Arguments')
    
    main_parser = argparse.ArgumentParser(prog=os.path.basename(sys.argv[0]), description="A utility to archive and restore Splunk buckets", conflict_handler='resolve')
    
    action_parser = main_parser.add_subparsers()
    action_parser.dest = 'cmd'
    action_parser.required = True

    # Actions
    action_a_parser = action_parser.add_parser('archive', parents=[g_parser, a_parser], help="Copy buckets to remote location", conflict_handler='resolve')
    action_r_parser = action_parser.add_parser('restore', parents=[g_parser, r_parser], help="Restore copied buckets to a Splunk Indexer", conflict_handler='resolve')
    action_m_parser = action_parser.add_parser('discover', parents=[g_parser, m_parser], help="Manually discover cluster config if REST not allowed between indexers", conflict_handler='resolve')

    # Positional
    d_parser = action_a_parser.add_subparsers()
    d_parser.dest = 'dest'
    d_parser.required = True
    d_parser.add_parser('s3', parents=[g_parser, a_parser], help="Amazon s3 or s3-like destinations using HMAC and boto3")
    d_parser.add_parser('gcs', parents=[g_parser, a_parser], help="Google Cloud Storage using the native API and gcs_auth.json")
    d_parser.add_parser('filesys', parents=[g_parser, a_parser], help="Any destination mounted locally -- Google Transfer Appliance, Local File Share, etc.")
    d_parser.add_parser('test', parents=[g_parser, a_parser], help="Dummy transfer module for testing.")
    
    s_parser = action_r_parser.add_subparsers()
    s_parser.dest = 'src'
    s_parser.required = True
    s_parser.add_parser('s3', parents=[g_parser, r_parser], help="Amazon s3 or s3-like destinations using HMAC and boto3")
    s_parser.add_parser('gcs', parents=[g_parser, r_parser], help="Google Cloud Storage using the native API and gcs_auth.json")
    s_parser.add_parser('filesys', parents=[g_parser, r_parser], help="Any destination mounted locally -- Google Transfer Appliance, Local File Share, etc.")
    s_parser.add_parser('test', parents=[g_parser, r_parser], help="Dummy transfer module for testing.")
    
    return main_parser.parse_args()
    
    
    
@placement search-head

[global]
allowGetAuth=[true|false]
allowRestReplay=[true|false]
defaultRestReplayStanza=<string>
pythonHandlerPath=<path>

[<rest endpoint name>:<endpoint description string>]
match=<path>
requireAuthentication=[true|false]
authKeyStanza=<stanza>
restReplay=[true|false]
restReplayStanza=<string>
capability=<capabilityName>
capability.<post|delete|get|put>=<capabilityName>
acceptFrom=<network_acl> ...
includeInAccessLog=[true|false]

[script:<uniqueName>]
scripttype=python
handler=<SCRIPT>.<CLASSNAME>
xsl=<path to XSL transform file>
script=<path to a script executable>
script.arg.<N>=<string>
script.param=<string>
output_modes=<csv list>
passSystemAuth=<bool>
driver=<path>
driver.arg.<n> = <string>
driver.env.<name>=<value>
passConf=<bool>
passPayload=[true | false | base64]
passSession=<bool>
passHttpHeaders=<bool>
passHttpCookies=<bool>
python.version = [python|python2|python3|default]

[admin:<uniqueName>]
match=<partial URL>
members=<csv list>

[admin_external:<uniqueName>]
handlertype=<script type>
handlerfile=<unique filename>
handlerpersistentmode=[true|false]
handleractions=<comma separated list>

[validation:<handler-name>]
<field> =  <validation-rule> 

[eai:<EAI handler name>]
showInDirSvc = [true|false]
desc = <human readable string>

[input:...]
dynamic = [true|false]
[peerupload:...]
path = <directory path>
untar = [true|false]
[restreplayshc]
methods =  <comma separated strings>
nodelists = <comma separated string>
nodes = <comma separated management uris>
filternodes = <comma separated management uris>
[proxy:appsbrowser]
destination = <splunkbaseAPIURL>

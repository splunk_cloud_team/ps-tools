@placement search-head
[<datamodel_name>]
acceleration = <bool>
acceleration.earliest_time = <relative-time-str>
acceleration.backfill_time = <relative-time-str>
acceleration.max_time = <unsigned int>
acceleration.cron_schedule = <cron-string>
acceleration.manual_rebuilds = <bool>
acceleration.max_concurrent = <unsigned int>
acceleration.schedule_priority = default | higher | highest
acceleration.hunk.compression_codec = <string>
acceleration.hunk.dfs_block_size = <unsigned int>
acceleration.hunk.file_format = <string>
dataset.description = <string>
dataset.type = [datamodel|table]
dataset.commands = [<object>(, <object>)*]
dataset.fields = [<string>(, <string>)*]
dataset.display.diversity = [latest|random|diverse|rare]
dataset.display.sample_ratio = <int>
dataset.display.limiting = <int>
dataset.display.currentCommand = <int>
dataset.display.mode = [table|datasummary]
dataset.display.datasummary.earliestTime = <time-str>
dataset.display.datasummary.latestTime = <time-str>

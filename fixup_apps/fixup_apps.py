import __future__

import json
import os
import shutil
import subprocess as sub
import sys
import time
from datetime import datetime as dt

import lxml
os.environ["SPLUNK_HOME"] = "."
os.environ["SPLUNK_DB"] = "."
import lib_path

lib_path.make_lib_path()

# splunkPath = "../splunk"
# os.environ["SPLUNK_HOME"] = splunkPath
# os.environ["SPLUNK_DB"] = "%s/var/lib" % splunkPath
# sys.path.insert(0, os.path.join(splunkPath, 'lib', 'python3.7', 'site-packages'))

import argparse
import configparser
import logging
import re
import sys

import requests
from pkg_resources import load_entry_point
from splunk.clilib import cli_common  # pylint: disable=import-error

from fix_xml import fix_and_write_xml

# global vars set on init
NOW = dt.fromtimestamp(time.time())
VERSION_NOW = NOW.strftime("%y.%-j.%-H%M%S")
BUILD_NOW = NOW.strftime("%s")

_config = {}
_config['contexts'] = { 
        '_local': {"dirs_to_create": ["local", "metadata"], "src_default": "default", "src_local": "local", "dest_default": "default", "dest_local": "local", "defaultDir": "local", "metaConf": "local.meta", "appConf": False, "action": "delta"},
        '_local_as_default': {"dirs_to_create": ["default", "metadata"],"src_default": "default", "src_local": "local", "dest_default": "default", "dest_local": "local", "defaultDir": "default", "metaConf": "default.meta", "appConf": True, "action": "delta"},
        '_default': {"dirs_to_create": ["default", "metadata"],"src_default": "default", "src_local": "local", "dest_default": "default", "dest_local": "local", "defaultDir": "default", "metaConf": "default.meta", "appConf": True, "action": None},
        '_merged': {"dirs_to_create": ["default", "metadata"],"src_default": "default", "src_local": "local", "dest_default": "default", "dest_local": "local", "defaultDir": "default", "metaConf": "default.meta", "appConf": True, "action": "merge"},
        '_as_is_cleaned': {"dirs_to_create": ["default", "local", "metadata"],"src_default": "default", "src_local": "local", "dest_default": "default", "dest_local": "local", "defaultDir": "default", "metaConf": "default.meta", "appConf": True, "action": None},
        }

_config['splunkbase'] = False
_helper_config = {'_splunkbase_helpers': {"dirs_to_create": ["default", "metadata"],"src_default": "default", "src_local": "local", "dest_default": "default", "dest_local": "local", "defaultDir": "default", "metaConf": "default.meta", "appConf": True, "action": "delta"}}

EXCLUDED = ["app.conf", "limits.conf", "viewstates.conf", "server.conf", "password.conf"]

OTHER_DIRS =    [
                    "appserver",
                    "README",
                    "static"
                    
]

# if include_bin:
if True:
    OTHER_DIRS.extend(["bin", "lib"])
    
# if include_lookups:
if True:
    OTHER_DIRS.append("lookups")

def createTmpDir(srcApp, config, context):
    
    # if there is a file with the same name as the destination, delete it
    if os.path.isfile(TMP_PATH):
        os.remove(TMP_PATH)
    
    if context == "_splunkbase_helpers":
        srcApp = "zz-" + srcApp + "-splunkbase_helper"

    _dest_path = os.path.join(TMP_PATH, context, srcApp)
    
    # delete the existing directory
    if os.path.isdir(_dest_path):
        shutil.rmtree(_dest_path)

    # create a new one
    os.makedirs(_dest_path)
    os.chmod(_dest_path, 0o755)
    
    return _dest_path
    
def createBaseFiles(srcApp, config, context):

    if context == "_splunkbase_helpers":
        _desc = "A helper app to get around SSAI and SplunkBase apps. This app will override searchtime fields from the default \"{}\" app.".format(srcApp)
        srcApp = "zz-" + srcApp + "-splunkbase_helper"
    else:
        _desc = ""

    _dest_path = config[context]['tmp_dir']
        
    _default_dir = os.path.join(_dest_path, config[context]['defaultDir'])
    
    for _item in os.listdir(_dest_path):
        try:
            os.remove(os.path.join(_dest_path, _item))
        except:
            raise
    
    _tmp_app_conf = {}
    _tmp_default_meta = {}
    
    _tmp_app_conf['package'] =  {   "id": srcApp,
                                    "check_for_updates": 0
                                }
    
    _tmp_app_conf['install'] =  {   "is_configured": 0,
                                    "state": "enabled",
                                    "build": BUILD_NOW
                                }
    
    _tmp_app_conf['ui'] =       {   "is_visible": 0,
                                    "label": srcApp
                                }
    
    _tmp_app_conf['launcher'] = {   "author": "",
                                    "description": _desc,
                                    "version": VERSION_NOW
                                }
    
    # _tmp_app_conf['id'] =       {   "version": VERSION_NOW }

    _tmp_default_meta[''] = {   "access": "read : [ * ], write : [ sc_admin ]",
                                "export": "none",
                                "owner": "nobody"
                            }
        
    for _dir in config[context]['dirs_to_create']:
        os.makedirs(os.path.join(_dest_path,_dir))
        os.chmod(os.path.join(_dest_path,_dir), 0o750)

    if config[context]['appConf']:
        cli_common.writeConfFile(os.path.join(_dest_path, config[context]['defaultDir'], "app.conf"), _tmp_app_conf)
        cli_common.writeConfFile(os.path.join(_dest_path, "metadata", config[context]['metaConf']), _tmp_default_meta)
        
    shutil.copytree(os.path.join("resources", "slim", "cloud_readme", "README"), os.path.join(_dest_path, "README"))
    os.chmod(os.path.join(_dest_path, "README"), 0o750)

def isSplunkApp(srcApp):
    
    
    
    isSplunkApp = os.path.isdir(os.path.join(args.SRC_PATH, srcApp)) and (os.path.isdir(os.path.join(args.SRC_PATH, srcApp, "default")) or os.path.isdir(os.path.join(args.SRC_PATH, srcApp, "local")))
    if isSplunkApp:
        return True
    
    else:
        debug_msg = "srcApp={}, srcPath={}, defaultDir={}, defaultDirIsDir={}, localDir={}, localDirIsDir={}, isSplunkApp={}".format(srcApp, 
                                                                                                                                os.path.join(args.SRC_PATH, srcApp), 
                                                                                                                                os.path.join(args.SRC_PATH, srcApp, "default"),
                                                                                                                                os.path.isdir(os.path.join(args.SRC_PATH, srcApp, "default")), 
                                                                                                                                os.path.join(args.SRC_PATH, srcApp, "local"),
                                                                                                                                os.path.isdir(os.path.join(args.SRC_PATH, srcApp, "local")),
                                                                                                                                isSplunkApp
                                                                                                                                )
        logger.debug(debug_msg)
        return False


def isSplunkbaseApp(srcApp):
    
    # url = "https://splunkbase.splunk.com/api/v1/app/?appid={}&include=release,releases.splunk_compatibility,release.splunk_compatibility&instance_type=cloud".format(srcApp)
    
    url = "https://splunkbase.splunk.com/api/v1/app/?appid={}&include=release,releases.splunk_compatibility,release.splunk_compatibility".format(srcApp)
    
    logger.debug("Checking Splunkbase for {}".format(srcApp))
    
    retries = 0
    
    while retries < 5:
        
        try:
            resp = requests.get(url)
            
        except:
            logger.error("Problem connecting to Splunkbase")
            retries += 1
            continue
        
        if resp.status_code != 200:
            logger.warning("Splunkbase: Response {}".format(repr(resp)))
            retries +=1
            continue
        
        else:
            break
    
    if resp.json()["total"] > 0:
        logger.info("--SPLUNKBASE_APP_DETECTED--")
        for item in resp.json()["results"]:
            for k, v in item.items():
                logger.info("SPLUNKBASE_RESULTS: {}: {}".format(k,v))

        return True
    
    return False

def processConfs(firstConf, secondConf, action="merge", appConf=False):
    """ Returns a merged copy of 2 conf files, or the delta between 2 conf files.

    Args:
        firstConf (dict): This is the lower priority file.
        secondConf (dict): This is the higer priority file.
        action (str, optional): [merge|delta]. Defaults to "merge".
    """    
    _result = {}
    
    if action == "merge":
        for section in secondConf.keys():
                for k, v in secondConf[section].items():
                    if not ((appConf) and k in ["version", "build"]):
                        try:
                            firstConf[section][k] = v
                            
                        except KeyError:
                            firstConf[section] = {k: v}

        return firstConf
    
    elif action == "delta":
        for section in secondConf.keys():
            for k, v in secondConf[section].items():
                if not ((appConf) and k in ["version", "build"]):
                    try:
                        firstConf[section][k]
                        
                    except KeyError:
                        try:
                            _result[section][k] = v
                        except KeyError:
                            _result[section] = {k: v}
                    
                    else:
                        if firstConf[section][k] != v:
                            try:
                                _result[section][k] = v
                                
                            except KeyError:
                                _result[section] = {k: v}
        
        return _result
    
def copy_other(srcPath, destPath, rootPath, files, destDir):
    
    for file in [ x for x in files if x.endswith((".xml", ".html", ".json"))]:

        rel_file = os.path.relpath(os.path.join(rootPath, file), srcPath)
        src_file = os.path.join(srcPath, rel_file)
        dest_file = os.path.join(destPath, destDir, *rel_file.split(os.sep)[1:])

        try:
            os.makedirs(os.path.dirname(dest_file))
            
        except FileExistsError:
            pass
        
        # fix splunk xml dashboard syntax
        if src_file.endswith(".xml"):
            logger.debug("CHECK_XML: {}".format(rel_file))
            fix_and_write_xml(src_file, dest_file)

        else:
            logger.debug("COPY_FILE: {}".format(rel_file))
            shutil.copy(src_file, dest_file)
        
        os.chmod(dest_file, 0o640)

def readMetaConf(srcPath):
    
    _meta_conf = configparser.ConfigParser(default_section="", interpolation=None, strict=False)
    _meta_conf.optionxform = str
    
    try:
        with open(srcPath, "r") as f:
            _tmp = f.read().strip()
            
    except FileNotFoundError:
        _tmp = ""
    
    _meta_conf.read_string(_tmp.replace("[]", "[__META-DEFAULT__]"))
    
    for section in _meta_conf.sections():
        _meta_conf[section] = {key: _meta_conf[section][key] for key in _meta_conf[section].keys() if key not in ["modtime", "version"]}

    return _meta_conf

def writeMetaConf(destPath, conf):
    
    _meta_conf = configparser.ConfigParser(default_section="", interpolation=None, strict=False)
    _meta_conf.optionxform = str
    
    _meta_conf.read_dict(conf)
    
    try:
        _meta_conf[''] = _meta_conf['__META-DEFAULT__']
        
    except KeyError:
        pass
    
    _meta_conf.remove_section('__META-DEFAULT__')
    
    with open(destPath, "w") as f:
        _meta_conf.write(f)
        
def main():
    global args
    global TMP_PATH
    global VETTED_PATH
    global UNVETTED_PATH
    
    
    logger.debug("SRC_PATH={}".format(args.SRC_PATH))
    
    if isSplunkApp(args.SRC_PATH):
        appList = [os.path.basename(args.SRC_PATH)]
        args.SRC_PATH = os.path.dirname(args.SRC_PATH)
            
    else:
        try:
            appList = sorted(os.listdir(args.SRC_PATH))
            
        except FileNotFoundError:
            print("Path does not exist -- {}".format(args.SRC_PATH), file=sys.stderr)
            sys.exit(1)
    
    if args.dest is None:
        TMP_PATH = os.path.join(args.SRC_PATH, "_tmp")
    else:
        TMP_PATH = os.path.join(args.dest, 'tmp')
        
    VETTED_PATH = os.path.join(args.SRC_PATH, "_vetted")
    UNVETTED_PATH = os.path.join(args.SRC_PATH, "_unvetted")
    
    for srcApp in appList:
    # for srcApp in ["bmo_bcadmin_home"]: 
        
        if srcApp in [  
                        "alert_logevent",
                        "alert_webhook",
                        "appsbrowser",
                        "introspection_generator_addon",
                        "journald_input",
                        "launcher",
                        "learned",
                        "sample_app",
                        "search",
                        "splunk_archiver",
                        "splunk_gdi",
                        "splunk_httpinput",
                        "splunk_instrumentation",
                        "splunk_internal_metrics",
                        "splunk_metrics_workspace",
                        "splunk_monitoring_console",
                        "splunk_rapid_diag",
                        "splunk_secure_gateway",
                        "SplunkForwarder",
                        "SplunkLightForwarder",
                        "user-prefs"
                        ]:
            logger.warning("The app \"{}\" was found and cannot be processed by this script -- manual intervention required.".format(srcApp))
            continue
        
        elif isSplunkApp(srcApp):
            logger.info("APP_ID={}".format(srcApp))
        else:
            logger.warning("\"{}\" is not a valid Splunk App Directory".format(os.path.join(args.SRC_PATH, srcApp)))
            continue
        
        _my_config = dict(_config)
        
        if isSplunkbaseApp(srcApp):
            logger.warning("\"{}\" is a Splunkbase App and cannot be installed by SSAI -- rename it to create your own custom version.".format(srcApp))
            logger.warning("{}: Creating locals only as search-time helper app".format(srcApp))
            _my_config['contexts'] = _helper_config
            _my_config['splunkbase'] = True
        
        
        for context in _my_config['contexts'].keys():
            logger.debug("srcApp={}, context={}".format(srcApp, context))
            _my_config['contexts'][context]["src_dir"] = os.path.join(args.SRC_PATH, srcApp)
            _my_config['contexts'][context]["tmp_dir"] = createTmpDir(srcApp, _my_config['contexts'], context)
            createBaseFiles(srcApp, _my_config['contexts'], context)
            os.makedirs(VETTED_PATH, exist_ok=True)
            os.makedirs(UNVETTED_PATH, exist_ok=True)
            
            logger.debug("--STARTING CONTEXT-- {}:{}".format(srcApp, context))
            
            _my_conf_files = {}
            _context = _my_config['contexts'][context]
            for k, v in _context.items():
                logger.debug("CONTEXT_DUMP: {} = {}".format(k, v))
            _src = _context["src_dir"]
            logger.debug("LOCAL_DUMP: _src = {}".format(_src))
            _dest = _context["tmp_dir"]
            logger.debug("LOCAL_DUMP: _dest = {}".format(_dest))
            _action = _context["action"]
            logger.debug("LOCAL_DUMP: _action = {}".format(_action))
            _all = []
            
            # collect the list of unique conf files
            for _dir in [ "src_default", "src_local" ]:
                logger.debug("UNIQUE_CONFS: {} = {}".format(_dir, os.path.join(_src, _context[_dir])))
                
                try:
                    _tmp = os.listdir(os.path.join(_src, _context[_dir]))
                    
                except FileNotFoundError:
                    logger.debug("NOT_FOUND: {}".format(os.path.join(_src, _context[_dir])))
                    continue
                
                for _ in _tmp:
                    if os.path.isdir(os.path.join(_src, _context[_dir], _)):
                        logger.debug("FOUND_DIR: {}".format(_))
                    else:
                        logger.debug("FOUND_FILE: {}".format(_))
                    
                _my_conf_files[_dir] = [x for x in _tmp if x.endswith(".conf")]
                
                _all.extend(_my_conf_files[_dir])
                
            for _ in _all:
                logger.debug("UNIQUE_CONFS: {}".format(_))
            
            # process the conf files
            logger.debug("--PROCESS_CONFS-- {}:{}".format(srcApp, context))
            for _ in [ x for x in EXCLUDED if x in _all and not x.startswith(".")]:
                logger.debug("EXCLUDED: {}".format(_))
                
            for _this_file in set([ x for x in _all if x not in EXCLUDED and not x.startswith(".")]):
                default = os.path.join(_src, _context["src_default"], _this_file)
                local = os.path.join(_src, _context["src_local"], _this_file)
                
                if os.path.isfile(default):
                    _default_conf = cli_common.readConfFile(os.path.join(_src, _context["src_default"], _this_file))
                else:
                    _default_conf = {}

                if os.path.isfile(local):
                    _local_conf = cli_common.readConfFile(os.path.join(_src, _context["src_local"], _this_file))
                else:
                    _local_conf = {}

                if context == "_splunkbase_helpers":

                    _result = processConfs(_default_conf, _local_conf, action="delta")
                    
                    if _result != {}:
                        cli_common.writeConfFile(os.path.join(_dest, _context["dest_default"], _this_file), _result)
                
                
                elif context == "_as_is_cleaned":
                    cli_common.writeConfFile(os.path.join(_dest, _context["dest_default"], _this_file), _default_conf)
                    cli_common.writeConfFile(os.path.join(_dest, _context["dest_local"], _this_file), _local_conf)

                    
                elif context == "_default":
                    cli_common.writeConfFile(os.path.join(_dest, _context["dest_default"], _this_file), _default_conf)
                
                elif context == "_local_as_default":

                    _result = processConfs(_default_conf, _local_conf, action="delta")
                    
                    if _result != {}:
                        cli_common.writeConfFile(os.path.join(_dest, _context["dest_default"], _this_file), _result)
                        
                
                elif context == "_local":

                    _result = processConfs(_default_conf, _local_conf, action="delta")
                    
                    if _result != {}:
                        cli_common.writeConfFile(os.path.join(_dest, _context["dest_local"], _this_file), _result)
                        
                        
                elif context == "_merged":
 
                    _result = processConfs(_default_conf, _local_conf, action="merge")
                    
                    if _result != {}:
                        cli_common.writeConfFile(os.path.join(_dest, _context["dest_default"], _this_file), _result)
                        
            
            # process the (default|local)/data directory
            if context == "_splunkbase_helpers":

                for _dir in ['local']:
                    for _top, _dirs, _files in os.walk(os.path.join(_src, _dir, "data")):
                        for file in _files:
                            logger.debug("DATA_DIR: {}:{} {}".format(srcApp, context, os.path.relpath(os.path.join(_top, file), _src)))
                        copy_other(_src, _dest, _top, _files, "default")
                
                
            elif context == "_as_is_cleaned":
                
                for _dir in ['default', 'local']:
                    for _top, _dirs, _files in os.walk(os.path.join(_src, _dir, "data")):
                        for file in _files:
                            logger.debug("DATA_DIR: {}:{} {}".format(srcApp, context, os.path.relpath(os.path.join(_top, file), _src)))
                            
                        copy_other(_src, _dest, _top, _files, _dir)

                
            elif context == "_default":
                
                for _dir in ['default']:
                    for _top, _dirs, _files in os.walk(os.path.join(_src, _dir, "data")):
                        for file in _files:
                            logger.debug("DATA_DIR: {}:{} {}".format(srcApp, context, os.path.relpath(os.path.join(_top, file), _src)))
                                            
                        copy_other(_src, _dest, _top, _files, _dir)
            
            elif context == "_local_as_default":
                    
                for _dir in ['local']:
                    for _top, _dirs, _files in os.walk(os.path.join(_src, _dir, "data")):
                        for file in _files:
                            logger.debug("DATA_DIR: {}:{} {}".format(srcApp, context, os.path.relpath(os.path.join(_top, file), _src)))
                        copy_other(_src, _dest, _top, _files, "default")
            
            elif context == "_local":
                    
                for _dir in ['local']:
                    for _top, _dirs, _files in os.walk(os.path.join(_src, _dir, "data")):
                        for file in _files:
                            logger.debug("DATA_DIR: {}:{} {}".format(srcApp, context, os.path.relpath(os.path.join(_top, file), _src)))
                        copy_other(_src, _dest, _top, _files, _dir)
                    
            elif context == "_merged":
                    
                for _dir in ["default", "local"]:
                    for _top, _dirs, _files in os.walk(os.path.join(_src, _dir, "data")):
                        for file in _files:
                            logger.debug("DATA_DIR: {}:{} {}".format(srcApp, context, os.path.relpath(os.path.join(_top, file), _src)))
                        copy_other(_src, _dest, _top, _files, "default")
            
            # process the metadata files
            if os.path.isfile(os.path.join(_dest, "metadata", "default.meta")):
                _tmp_meta = readMetaConf(os.path.join(_dest, "metadata", "default.meta"))
            else:
                _tmp_meta = {}
                
            if os.path.isfile(os.path.join(_src, "metadata", "default.meta")):
                _default_meta = readMetaConf(os.path.join(_src, "metadata", "default.meta"))
            else:
                _default_meta = {}
                
            if os.path.isfile(os.path.join(_src, "metadata", "local.meta")):
                _local_meta = readMetaConf(os.path.join(_src, "metadata", "local.meta"))
            else:
                _local_meta = {}
                
            if context == "_as_is_cleaned":
                _default_meta = processConfs(_tmp_meta, _default_meta, action="merge")
                _meta = processConfs(_default_meta, _local_meta, action="delta")
                writeMetaConf(os.path.join(_dest, "metadata", "default.meta"), _default_meta)
                if _meta != {}:
                    writeMetaConf(os.path.join(_dest, "metadata", "local.meta"), _meta)
            
            elif context == "_default":
                _default_meta = processConfs(_tmp_meta, _default_meta, action="merge")
                writeMetaConf(os.path.join(_dest, "metadata", "default.meta"), _default_meta)
            
            elif context == "_local":
                _default_meta = processConfs(_tmp_meta, _default_meta, action="merge")
                _meta = processConfs(_default_meta, _local_meta, action="delta")
                    
                if _meta != {}:
                    writeMetaConf(os.path.join(_dest, "metadata", "local.meta"), _meta)
                    
            elif context == "_local_as_default":
                _default_meta = processConfs(_tmp_meta, _default_meta, action="merge")
                _meta = processConfs(_default_meta, _local_meta, action="delta")
                    
                if _meta != {}:
                    writeMetaConf(os.path.join(_dest, "metadata", "default.meta"), _meta)
            
            elif context == "_merged":
                _default_meta = processConfs(_tmp_meta, _default_meta, action="merge")
                _meta = processConfs(_default_meta, _local_meta, action="merge")
                    
                if _meta != {}:
                    writeMetaConf(os.path.join(_dest, "metadata", "default.meta"), _meta)
            
            elif context == "_splunkbase_helpers":
                _default_meta = processConfs(_tmp_meta, _default_meta, action="merge")
                _meta = processConfs(_default_meta, _local_meta, action="merge")
                    
                if _meta != {}:
                    writeMetaConf(os.path.join(_dest, "metadata", "default.meta"), _meta)
            
            else:
                logger.error("Unknown context \"{}\"".format(context))
                continue

            # process the app.conf files
            if os.path.isfile(os.path.join(_dest, "default", "app.conf")):
                _tmp_app_conf = cli_common.readConfFile(os.path.join(_dest, "default", "app.conf"))
            else:
                _tmp_app_conf = {}
                
            if os.path.isfile(os.path.join(_src, "default", "app.conf")):
                _default_app_conf = cli_common.readConfFile(os.path.join(_src, "default", "app.conf"))
            else:
                _default_app_conf = {}
                
            if os.path.isfile(os.path.join(_src, "local", "app.conf")):
                _local_app_conf = cli_common.readConfFile(os.path.join(_src, "local", "app.conf"))
            else:
                _local_app_conf = {}
                
            if context == "_as_is_cleaned":
                _default_app_conf = processConfs(_tmp_app_conf, _default_app_conf, action="merge", appConf=True)
                _app_conf = processConfs(_default_app_conf, _local_app_conf, action="delta", appConf=True)
                
                cli_common.writeConfFile(os.path.join(_dest, "default", "app.conf"), _default_app_conf)
                
                if _app_conf != {}:
                    cli_common.writeConfFile(os.path.join(_dest, "local", "app.conf"), _app_conf)

            elif context == "_default":
                _default_app_conf = processConfs(_tmp_app_conf, _default_app_conf, action="merge", appConf=True)
                
                if _default_app_conf != {}:
                    cli_common.writeConfFile(os.path.join(_dest, "default", "app.conf"), _default_app_conf)
                    
            elif context == "_local":
                _default_app_conf = processConfs(_tmp_app_conf, _default_app_conf, action="merge", appConf=True)
                _app_conf = processConfs(_default_app_conf, _local_app_conf, action="delta", appConf=True)
                
                if _app_conf !={}:
                    cli_common.writeConfFile(os.path.join(_dest, "local", "app.conf"), _app_conf)
                    
            elif context == "_local_as_default":
                _app_conf = processConfs(_tmp_app_conf, _local_app_conf, action="merge", appConf=True)
                
                if _app_conf != {}:
                    cli_common.writeConfFile(os.path.join(_dest, "default", "app.conf"), _app_conf)
                    
            elif context == "_merged":
                _default_app_conf = processConfs(_tmp_app_conf, _default_app_conf, action="merge", appConf=True)
                _app_conf = processConfs(_default_app_conf, _local_app_conf, action="merge", appConf=True)
                
                if _app_conf != {}:
                    cli_common.writeConfFile(os.path.join(_dest, "default", "app.conf"), _app_conf)
                    
            elif context == "_splunkbase_helpers":
                _default_app_conf = processConfs(_tmp_app_conf, _default_app_conf, action="merge", appConf=True)
                _app_conf = processConfs(_default_app_conf, _local_app_conf, action="delta", appConf=True)
                
                if _app_conf != {}:
                    cli_common.writeConfFile(os.path.join(_dest, "default", "app.conf"), _app_conf)
                    
            else:
                logger.error("Unknown context \"{}\"".format(context))
                continue
            
            
            # copy the balance of the allowed subdirs unchanged
            for _dir in OTHER_DIRS:
                
                _this_dir = os.path.join(_src, _dir)
                _dest_dir = os.path.join(_dest, _dir)
                
                if _dir in ["lookups", "static", "README"]:
                    # TODO copy each file to the dest
                    # don't gp any deeper on these directories
                    if os.path.isdir(_this_dir):
                        for file in os.listdir(_this_dir):
                            _src_file = os.path.join(_this_dir, file)
                            _dest_file = os.path.join(_dest_dir, file)
                            
                            if os.path.isdir(_src_file):
                                logger.debug("Skipping subdirectory {}".format(_src_file))
                                continue
                            
                            if (file.endswith((".index.alive", ".tsidx", ".index")) or re.search(r'\.cs\.index', file)):
                                logger.debug("Ignoring lookup index file {}".format(_src_file))
                                continue
                            
                            if context in ["_default", "_merged", "_as_is_cleaned"]:
                                
                                try:
                                    os.mkdir(os.path.dirname(_dest_file))
                                    os.chmod(os.path.dirname(_dest_file), 0o750)
                                    
                                except FileExistsError:
                                    pass
                                
                                shutil.copyfile(_src_file, _dest_file)
                                os.chmod(_dest_file, 0o640)
                    else:
                        if os.path.isfile(_this_dir):
                            logger.warning("{} is not a directory, but it should be -- delete it and try again.".format(_this_dir))
                            
                        continue
                else:
                        if context in ["_default", "_merged", "_as_is_cleaned"]:
                            try:
                                shutil.copytree(_this_dir, _dest_dir)
                                
                            except FileNotFoundError:
                                continue
                            
            # slim generate manifest
            # if context not in ["_local"]:
            #     import subprocess as sub
            #     _manifest = os.path.join(_dest, "app.manifest")
            #     cmd = ["slim", "generate-manifest", "{}".format(_dest), "-o {}".format(_manifest)]
                
            #     if os.path.isfile(os.path.join(_manifest)):
            #         cmd.append("--update")
                    
            #     slim = sub.Popen(cmd, stdout=sub.PIPE, stderr=sub.PIPE)
                
            #     try:
            #         outs, errs = slim.communicate(timeout=15)
                    
            #     except sub.TimeoutExpired:
            #         slim.kill()
            #         outs, errs = slim.communicate()
                
            #     outs = outs.decode('utf-8').split("\n")
            #     errs = errs.decode('utf-8').split("\n")
                
            #     for line in errs:
            #         if line == (""):
            #             continue
                    
            #         try:
            #             line.index("[WARNING]")
                    
            #         except ValueError:
                        
            #             try:
            #                 line.index("[INFO]")
                            
            #             except ValueError:
            #                 continue
                        
            #             else:
            #                 logger.info("SLIM: {}".format(line))
                            
            #         else:
            #             logger.warning("SLIM: {}".format(line))
                                
            #     if os.path.isfile(_manifest):
            #         # remove the stupid comments
            #         lines = []
            #         with open(_manifest, "r") as f:
            #             for line in f:
            #                 if line.startswith("#"):
            #                     break
                            
            #                 lines.append(line)
                    
            #         manifest = json.loads("".join(lines))
                    
            #         manifest["supportedDeployments"].extend([x for x in ["_distributed", "_standalone", "_search_head_clustering"] if not x in manifest["supportedDeployments"]])
            #         manifest["targetWorkloads"] = [ "_search_heads" ]
                    
            #         with open(_manifest, "w") as f:
            #             json.dump(manifest, f, indent=4)

                
    return
        
if __name__ == "__main__":
    
    global args
    
    opts = argparse.ArgumentParser()
    opts.add_argument("--verbose", action='store_true')
    opts.add_argument("--debug", action='store_true')
    opts.add_argument("SRC_PATH", default="./")
    opts.add_argument('--dest', default=None)
    
    args = opts.parse_args()
    
    if args.debug:
        logging.basicConfig(level=logging.DEBUG, format='%(asctime)s (%(levelname)s) [ %(name)s:%(funcName)s() ] file="%(filename)s", line_no="%(lineno)s", %(message)s')
        
    elif args.verbose:
        logging.basicConfig(level=logging.INFO, format='%(asctime)s (%(levelname)s) %(message)s')
        
    else:
        logging.basicConfig(level=logging.WARNING, format='%(asctime)s (%(levelname)s) %(message)s')

    logger = logging.getLogger(__name__)
    
    print("Starting", file=sys.stderr)
    
    main()
    
    print("\
    Process Complete:\n\n\
    All apps from the source directory have been processed and the following directories have been created:\n\
        _tmp\n\
            _as_is_cleaned - preserves local and default, fixes app.conf and other issues. Filters out local settings that match the defaults.\n\
            _default - only the app defaults, fixes app.conf and other issues\n\
            _local - only the app local, fixes other issues, no app.conf\n\
            _local_as_default - creates app default containing the actual local content. Used for vetting local settings. fixes app.conf and other issues.\n\
            _merged - creates app default whith the merged content of default and local, fixes app.conf and other issues.\n\
            _splunkbase_helpers - creates local settings for SplunkBase apps. These are effectively new apps to override SplunkBase app defaults at searchtime\n\
    ")


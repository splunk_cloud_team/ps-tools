import re

# patterns
upperList = []
for item in ['EVAL', 'SEDCMD', '']:
    upperList.append(re.compile(r'^({})'.format(item), flags=re.I))


def fix_case(match):
    
    return match.group(1).upper()

if __name__ == "__main__":
    x = 'EVAl-test = something'
    for item in upperList:
        item.sub(fix_case, x)
    print(x)



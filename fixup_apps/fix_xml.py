import xml.etree.ElementTree as ET
import os


def fixtime(element, child):
    
    for key in [("earliestTime", "earliest"), ("latestTime", "latest"), ("earliest", "earliest"), ("latest", "latest")]:

        try:
            _tmp = element.find(key[0])
            
        except AttributeError:
            pass
        
        else:
            if _tmp is not None:
                _new = ET.SubElement(child, key[1])
                _new.text = str(_tmp.text)
                element.remove(_tmp)
    
    return element

def _process(element):
    
    if element.find("option[@name='afterLabel']") is not None:
        
        _this = element.find("option[@name='afterLabel']")
        _this.set('name', "unit")
        
    if element.find("option[@name='beforeLabel']") is not None:
        
        _this = element.find("option[@name='beforeLabel']")
        _this.set = ('name', "unit")
        _new = ET.SubElement(element, "option")
        _new.set('name', "unitPosition")
        _new.text = "before"
    
    if element.find('searchString') is not None:
        
        _this = element.find('searchString')
        _this.tag = "search"
        searchString = _this.text
        _this.text = None
        
        _query = ET.SubElement(_this, "query")
        _query.text = str(searchString)
        
        element = fixtime(element, _this)
    
    if element.find("searchTemplate") is not None:
        
        _this = element.find('searchTemplate')
        _this.tag = "search"
        _this.set("id", "base_search")
        searchString = _this.text
        _this.text = None
        
        _query = ET.SubElement(_this, "query")
        _query.text = str(searchString)
        
        element = fixtime(element, _this)
        
    if element.find("searchPostProcess") is not None:
        
        _this = element.find('searchPostProcess')
        _this.tag = "search"
        _this.set("base", "base_search")
        searchString = _this.text
        _this.text = None
        
        _query = ET.SubElement(_this, "query")
        _query.text = str(searchString)
        
        element = fixtime(element, _this)
        
    if element.find("populatingSearch") is not None:
        
        _this = element.find('populatingSearch')
        _this.tag = "search"
        searchString = _this.text
        _this.text = None
        
        _query = ET.SubElement(_this, "query")
        _query.text = str(searchString)

        for k, v in [ (k, _this.attrib[k]) for k in ["earliest", "latest"] if k in _this.attrib]:
            _tmp = ET.SubElement(_this, k)
            _tmp.text = str(v)

        for k, v in [ (k, _this.attrib[k]) for k in _this.attrib.keys() if k not in ["earliest", "latest"]]:
            _tmp = ET.SubElement(element, k)
            _tmp.text = str(v)
        
        _this.attrib = {}
        
    if element.find("searchName") is not None:
        
        _this = element.find('searchName')
        _this.tag = "search"
        searchString = _this.text
        _this.text = None
        _this.attrib = {"ref": searchString}
        
    if element.find("option[@name='linkView']") is not None:
        
        _this = element.find("option[@name='linkView']")

        
        if element.find("option[@name='drilldown']") is not None:

            _tmp = element.find("option[@name='drilldown']")
            _tmp.text = "all"
            element.remove(_this)
        
        
    if element.find("option[@name='height']") is not None:
        
        _this = element.find("option[@name='height']")
        _this.text = str(_this.text).rstrip("px")
    
    element = fixtime(element, element)
    
    for child in element:
        child = _process(child)
        
    return element

def fix_and_write_xml(srcFile, destFile):
    
    _src = ET.parse(srcFile)
    _src_root = _src.getroot()
    _src_root = _process(_src_root)
    _src.write(destFile)
    
if __name__ == "__main__":
    
    import argparse
    
    parser = argparse.ArgumentParser()
    parser.add_argument('SRC_PATH')
    
    args = parser.parse_args()
    
    for _top, _dirs, _files in os.walk(args.SRC_PATH):
        for _file in [ x for x in _files if x.endswith(".xml") and not x.endswith(".tmp.xml")]:
            _src_file = os.path.join(_top, _file)
            _dest_file = _src_file.replace(".xml", ".tmp.xml")
            _src = ET.parse(_src_file)
            _src_root = _src.getroot()
            _src_root = _process(_src_root)
            
                
            _src.write(_dest_file)

